import xml.etree.ElementTree as ET

from DocumentTypAlgorithm import LaboratoryReport, MedicalDischargeLetter, ImmunizationSummaryReport, DiagnosticImagingStudy, MedicationSummaryDocument, NurseDischargeSummary
from OMOP import WriteMedicalDischargeLetterToOMOP, OMOP_LaboratoryReport, OMOP_MedicalDischargeSummary, OMOP_NurseDischargeSummary, OMOP_DiagnosticImagingReport, OMOP_ImmunizationSummaryReport, OMOP_MedicalSummaryDocument

if __name__ == '__main__':

    # Parsen des XML files
    #tree = ET.parse('CDA-Files/ELGA-043-Laborbefund_EIS-FullSupport.xml')
    #tree = ET.parse('CDA-Files/eImpf-Kompletter_Immunisierungsstatus.xml')
    tree = ET.parse('CDA-Files/ELGA-023-Entlassungsbrief_aerztlich_EIS-FullSupport.xml')
    #tree = ET.parse('CDA-Files/ELGA-053-Befund_bildgebende_Diagnostik_EIS-FullSupport.xml')
    #tree = ET.parse('CDA-Files/ELGA-053-Befund_bildgebende_Diagnostik_Mammographie_EIS-FullSupport.xml')
    #tree = ET.parse('CDA-Files/ELGA-083-e-Medikation_5-Medikationsliste.xml')
    #tree = ET.parse('CDA-Files/ELGA-033-Entlassungsbrief_Pflege_EIS-FullSupport.xml')
    root = tree.getroot()

    #Check Dokumententyp
    template_IDs = root.findall('{urn:hl7-org:v3}templateId')
    template = template_IDs[1].attrib['root']
    #Entlassungsbrief ärztlich
    if template == "1.2.40.0.34.11.2":
        medicaldischargeletter = MedicalDischargeLetter.MedicalDischargeLetter(root)
        print(medicaldischargeletter.document.title)
        medicaldischargeletter.writeMedicalDischargeLetterToDB_v2()
        omop = OMOP_MedicalDischargeSummary.MedialDischargeSummaryToOMOP()
        omop.write_medicaldischarge_to_OMOP(medicaldischargeletter.document.document_id, medicaldischargeletter.document.code)
    #Entlassungsbrief Pflege
    elif template == "1.2.40.0.34.11.3":
        nursedischargesummary = NurseDischargeSummary.NurseDischargeSummary(root)
        print(nursedischargesummary.document.title)
        nursedischargesummary.writeNurseDischargeSumaryToDB()
        omop = OMOP_NurseDischargeSummary.NurseDischargeSummaryToOMOP()
        omop.write_nursedischarge_to_OMOP(nursedischargesummary.document.document_id, nursedischargesummary.document.code)

    #Laborbefund
    elif template == "1.2.40.0.34.11.4":
        laboratoryreport = LaboratoryReport.LabReport(root)
        print(laboratoryreport.document.title)
        laboratoryreport.writeLabReportToDB()
        omop = OMOP_LaboratoryReport.LaboratorReportToOMOP()
        omop.write_labreport_to_OMOP(laboratoryreport.document.document_id, laboratoryreport.document.code)

    #Befund bildgebende Diagnostik
    elif template == "1.2.40.0.34.11.5":
        diagnosticimagingstudy = DiagnosticImagingStudy.DiagnosticImagingStudy(root)
        print(diagnosticimagingstudy.document.title)
        diagnosticimagingstudy.writeDiagnosticImagingStudyToDB()
        omop = OMOP_DiagnosticImagingReport.DiagnosticImagingReportToOMOP()
        omop.write_diagnostic_imaging_to_OMOP(diagnosticimagingstudy.document.document_id, diagnosticimagingstudy.document.code)

    #Pflegesituationsbericht
    elif template == "1.2.40.0.34.11.12":
        print("to be done in the future")

    # e-Medikation Medikationsliste
    elif template == "1.2.40.0.34.11.8.3":
        medicationsummarydocument = MedicationSummaryDocument.MedicationSummaryDocument(root)
        print(medicationsummarydocument.document.title)
        medicationsummarydocument.writeMedicationSummaryDocumentToDB()
        omop = OMOP_MedicalSummaryDocument.MedicalSummaryDocumentToOMOP()
        omop.write_medicalsummary_to_OMOP(medicationsummarydocument.document.document_id, medicationsummarydocument.document.code)

    #e-Impfass allg.
    elif template == "1.2.40.0.34.7.19":
        immunizationsummaryreport = ImmunizationSummaryReport.ImmunizationSummaryReport(root)
        print(immunizationsummaryreport.document.title)
        immunizationsummaryreport.writeImmunizationSummaryReportToDB()
        omop = OMOP_ImmunizationSummaryReport.ImmunizationSummaryReportToOMOP()
        omop.write_immunizationsummary_to_OMOP(immunizationsummaryreport.document.document_id, immunizationsummaryreport.document.code)


