CREATE DATABASE `CDA_Entlassungsbrief_A` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `CDA_Entlassungsbrief_A`;
CREATE TABLE `addr` (
  `id_addr` int NOT NULL AUTO_INCREMENT,
  `streetAddressLine` varchar(45) NOT NULL,
  `postalCode` int NOT NULL,
  `city` varchar(45) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `additionalLocator` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`streetAddressLine`,`postalCode`),
  UNIQUE KEY `id_addr_UNIQUE` (`id_addr`)
) ENGINE=InnoDB AUTO_INCREMENT=4272 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `code` (
  `code` varchar(45) NOT NULL,
  `codeSystem` varchar(45) NOT NULL,
  `displayName` varchar(45) DEFAULT NULL,
  `codeSystemName` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`code`,`codeSystem`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `templateId` (
  `root` varchar(45) NOT NULL,
  `assigningAuthorityName` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`root`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `encompassingEncounter` (
  `id` varchar(45) NOT NULL,
  `code` varchar(45) DEFAULT NULL,
  `effectiveTime_low` datetime DEFAULT NULL,
  `effectiveTime_high` datetime DEFAULT NULL,
  `assignedEntity_id` varchar(45) DEFAULT NULL,
  `person_name` varchar(80) DEFAULT NULL,
  `person_addr` int DEFAULT NULL,
  `organisation_id` varchar(45) DEFAULT NULL,
  `organisation_name` varchar(80) DEFAULT NULL,
  `organisation_addr` int DEFAULT NULL,
  `location_id` varchar(45) DEFAULT NULL,
  `location_name` varchar(80) DEFAULT NULL,
  `location_addr` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_encompassingEnc_code_idx` (`code`),
  KEY `fk_person_addr_idx` (`person_addr`),
  KEY `fk_EC-organisation_addr_idx` (`organisation_addr`),
  KEY `fk_EC-location_addr_idx` (`location_addr`),
  CONSTRAINT `fk_EC-location_addr` FOREIGN KEY (`location_addr`) REFERENCES `addr` (`id_addr`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_EC-organisation_addr` FOREIGN KEY (`organisation_addr`) REFERENCES `addr` (`id_addr`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_EC-person_addr` FOREIGN KEY (`person_addr`) REFERENCES `addr` (`id_addr`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_encompassingEnc_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `participant` (
  `id_participant` int NOT NULL AUTO_INCREMENT,
  `typeCode` varchar(45) DEFAULT NULL,
  `templateId` varchar(45) DEFAULT NULL,
  `functionCode` varchar(45) DEFAULT NULL,
  `person_id` varchar(45) DEFAULT NULL,
  `person_name` varchar(45) DEFAULT NULL,
  `organisation_id` varchar(45) DEFAULT NULL,
  `organisation_name` varchar(45) DEFAULT NULL,
  `organisation_addr` int DEFAULT NULL,
  PRIMARY KEY (`id_participant`),
  KEY `fk_participant_templateID_idx` (`templateId`),
  KEY `fk_participant_code_idx` (`functionCode`),
  KEY `fk_participant-org_addr_idx` (`organisation_addr`),
  CONSTRAINT `fk_participant-org_addr` FOREIGN KEY (`organisation_addr`) REFERENCES `addr` (`id_addr`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_participant_code` FOREIGN KEY (`functionCode`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_participant_templateID` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `patientRole` (
  `id_SVNR` varchar(45) NOT NULL,
  `id_local` varchar(45) DEFAULT NULL,
  `id_BPKGH` varchar(45) DEFAULT NULL,
  `addr` int DEFAULT NULL,
  `given` varchar(45) DEFAULT NULL,
  `family` varchar(45) DEFAULT NULL,
  `administrativeGenderCode` varchar(45) DEFAULT NULL,
  `birthTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id_SVNR`),
  UNIQUE KEY `id_SVNR_UNIQUE` (`id_SVNR`),
  UNIQUE KEY `id_local_UNIQUE` (`id_local`),
  UNIQUE KEY `id_BPKGH_UNIQUE` (`id_BPKGH`),
  KEY `fk_home_addr_idx` (`addr`),
  KEY `fk_gender_code_idx` (`administrativeGenderCode`),
  CONSTRAINT `fk_gender_code` FOREIGN KEY (`administrativeGenderCode`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_home_addr` FOREIGN KEY (`addr`) REFERENCES `addr` (`id_addr`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `section_Brieftext` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_Brieftext_template_idx` (`templateId`),
  KEY `fk_Brieftext_code_idx` (`code`),
  CONSTRAINT `fk_Brieftext_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Brieftext_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=149 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `section_Aufnahmegrund` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_Aufnahmegrund_template_idx` (`templateId`),
  KEY `fk_Aufnahmegrund_code_idx` (`code`),
  CONSTRAINT `fk_Aufnahmegrund_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Aufnahmegrund_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=145 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `section_Rehabilitationsziele` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_Rehabilitationsziele_template_idx` (`templateId`),
  KEY `fk_Rehabilitationsziele_code_idx` (`code`),
  CONSTRAINT `fk_Rehabilitationsziele_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Rehabilitationsziele_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `section_OutcomeMeasurement` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_OutcomeMeasurement_template_idx` (`templateId`),
  KEY `fk_OutcomeMeasurement_code_idx` (`code`),
  CONSTRAINT `fk_OutcomeMeasurement_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_OutcomeMeasurement_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `section_ProcedureNarrative` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_ProcedureNarrative_template_idx` (`templateId`),
  KEY `fk_ProcedureNarrative_code_idx` (`code`),
  CONSTRAINT `fk_ProcedureNarrative_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ProcedureNarrative_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `section_ZusammenfassungDesAufenthalts` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_ZusammenfassungDesAufenthalts_template_idx` (`templateId`),
  KEY `fk_ZusammenfassungDesAufenthalts_code_idx` (`code`),
  CONSTRAINT `fk_ZusammenfassungDesAufenthalts_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ZusammenfassungDesAufenthalts_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `section_TreatmentPlan` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_TreatmentPlan_template_idx` (`templateId`),
  KEY `fk_TreatmentPlan_code_idx` (`code`),
  CONSTRAINT `fk_TreatmentPlan_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_TreatmentPlan_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `section_TreatmentPlan_Termin` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_TP_Termin_template_idx` (`templateId`),
  KEY `fk_TP_Termin_code_idx` (`code`),
  CONSTRAINT `fk_TP_Termin_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_TP_Termin_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `section_TreatmentPlan_Entlassungszustand` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_TP_Entlassungszustand_template_idx` (`templateId`),
  KEY `fk_TP_Entlassungszustand_code_idx` (`code`),
  CONSTRAINT `fk_TP_Entlassungszustand_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_TP_Entlassungszustand_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `section_TreatmentPlan_PlanofCareNote` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_TP_PlanofCareNote_template_idx` (`templateId`),
  KEY `fk_TP_PlanofCareNote_code_idx` (`code`),
  CONSTRAINT `fk_TP_PlanofCareNote_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_TP_PlanofCareNote_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `section_Allergie` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_Allergie_template_idx` (`templateId`),
  KEY `fk_Allergie_code_idx` (`code`),
  CONSTRAINT `fk_Allergie_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Allergie_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=145 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `section_AbschliessendeBemerkung` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_AbschliessendeBemerkung_template_idx` (`templateId`),
  KEY `fk_AbschliessendeBemerkung_code_idx` (`code`),
  CONSTRAINT `fk_AbschliessendeBemerkung_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_AbschliessendeBemerkung_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=145 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `section_Anamnese` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_Anamnese_template_idx` (`templateId`),
  KEY `fk_Anamnese_code_idx` (`code`),
  CONSTRAINT `fk_Anamnese_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Anamnese_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=145 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `section_FruehereErkrankungen` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_FruehereErkrankungen_template_idx` (`templateId`),
  KEY `fk_FruehereErkrankungen_code_idx` (`code`),
  CONSTRAINT `fk_FruehereErkrankungen_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_FruehereErkrankungen_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


 CREATE TABLE `section_FruehereErkrankungen_BisherigeMassnahmen` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_FE_BisherigeMassnahmen_template_idx` (`templateId`),
  KEY `fk_FE_BisherigeMassnahmen_code_idx` (`code`),
  CONSTRAINT `fk_FE_BisherigeMassnahmen_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_FE_BisherigeMassnahmen_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `section_MedicationAdministered` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_MedicationAdministered_template_idx` (`templateId`),
  KEY `fk_MedicationAdministered_code_idx` (`code`),
  CONSTRAINT `fk_MedicationAdministered_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_MedicationAdministered_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `section_AdvancedDirectives` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_AdvancedDirectives_template_idx` (`templateId`),
  KEY `fk_AdvancedDirectives_code_idx` (`code`),
  CONSTRAINT `fk_AdvancedDirectives_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_AdvancedDirectives_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=145 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `section_ErhobeneBefunde` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_ErhobeneBefunde_template_idx` (`templateId`),
  KEY `fk_ErhobeneBefunde_code_idx` (`code`),
  CONSTRAINT `fk_ErhobeneBefunde_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ErhobeneBefunde_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=126 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `section_ErhobeneBefunde_AustehendeBefunde` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_EB_AustehendeBefunde_template_idx` (`templateId`),
  KEY `fk_EB_AustehendeBefunde_code_idx` (`code`),
  CONSTRAINT `fk_EB_AustehendeBefunde_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_EB_AustehendeBefunde_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=126 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `section_ErhobeneBefunde_AuszugAusBefunde` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_EB_AuszugAusBefunde_template_idx` (`templateId`),
  KEY `fk_EB_AuszugAusBefunde_code_idx` (`code`),
  CONSTRAINT `fk_EB_AuszugAusBefunde_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_EB_AuszugAusBefunde_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=126 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `section_ErhobeneBefunde_Operationsbericht` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_EB_Operationsbericht_template_idx` (`templateId`),
  KEY `fk_EB_Operationsbericht_code_idx` (`code`),
  CONSTRAINT `fk_EB_Operationsbericht_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_EB_Operationsbericht_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=126 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `section_ErhobeneBefunde_BeigelegteBefunde` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_EB_BeigelegteBefunde_template_idx` (`templateId`),
  KEY `fk_EB_BeigelegteBefunde_code_idx` (`code`),
  CONSTRAINT `fk_EB_BeigelegteBefunde_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_EB_BeigelegteBefunde_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=126 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `section_ErhobeneBefunde_Vitalparameter` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_EB_Vitalparameter_template_idx` (`templateId`),
  KEY `fk_EB_Vitalparameter_code_idx` (`code`),
  CONSTRAINT `fk_EB_Vitalparameter_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_EB_Vitalparameter_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=125 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `EB_EntryOrganizer` (
  `EO_id` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `id` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `statusCode` varchar(45) DEFAULT NULL,
  `effectiveTime_low` datetime DEFAULT NULL,
  `effectiveTime_high` datetime DEFAULT NULL,
  `sectio_id` int DEFAULT NULL,
  PRIMARY KEY (`EO_id`),
  KEY `fk_EO_template_idx` (`templateId`),
  KEY `fk_EO_code_idx` (`code`),
  KEY `fk_EO_sectionVitalparameter_idx` (`sectio_id`),
  CONSTRAINT `fk_EO_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_EO_sectionVitalparameter` FOREIGN KEY (`sectio_id`) REFERENCES `section_ErhobeneBefunde_Vitalparameter` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_EO_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `EB_EO_observation` (
  `id` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `effectiveTime` datetime DEFAULT NULL,
  `interpretationCode` varchar(45) DEFAULT NULL,
  `value` varchar(45) DEFAULT NULL,
  `unit` varchar(45) DEFAULT NULL,
  `range_low` varchar(45) DEFAULT NULL,
  `range_high` varchar(45) DEFAULT NULL,
  `entryOrganizer_ID` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_code_idx` (`code`),
  KEY `fk_observation_templateID_idx` (`templateId`),
  KEY `fk_interpretationCode_code_idx` (`interpretationCode`),
  KEY `fk_observation_entryOrganizer_idx` (`entryOrganizer_ID`),
  CONSTRAINT `fk_EO_code_observation` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_EO_interpretationCode_code` FOREIGN KEY (`interpretationCode`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_EO_observation_entryOrganizer` FOREIGN KEY (`entryOrganizer_ID`) REFERENCES `EB_EntryOrganizer` (`EO_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_EO_observation_templateID` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1338 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `section_HospitalDischarge` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_HospitalDischarge_template_idx` (`templateId`),
  KEY `fk_HospitalDischarge_code_idx` (`code`),
  CONSTRAINT `fk_HospitalDischarge_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_HospitalDischarge_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=117 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `HD_entry` (
  `id` int NOT NULL AUTO_INCREMENT,
  `act_templateId` varchar(45) DEFAULT NULL,
  `act_id` varchar(45) DEFAULT NULL,
  `act_code` varchar(45) DEFAULT NULL,
  `status_code` varchar(45) DEFAULT NULL,
  `effectiveTime_low` datetime DEFAULT NULL,
  `effectiveTime_high` datetime DEFAULT NULL,
  `HD_ID` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_code_idx` (`act_code`),
  KEY `fk_HD_entry_template_idx` (`act_templateId`),
  KEY `fk_HD_entry_HD_section_idx` (`HD_ID`),
  CONSTRAINT `fk_act_code_observation` FOREIGN KEY (`act_code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_HD_entry_HD_section` FOREIGN KEY (`HD_ID`) REFERENCES `section_HospitalDischarge` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_HD_entry_template` FOREIGN KEY (`act_templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=1188 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `HD_entry_observation` (
  `id` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `effectiveTime_low` datetime DEFAULT NULL,
  `effectiveTime_high` datetime DEFAULT NULL,
  `value_code` varchar(45) DEFAULT NULL,
  `entry_ID` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_code_idx` (`code`),
  KEY `fk_observation_templateID_idx` (`templateId`),
  KEY `fk_value_code_code_idx` (`value_code`),
  KEY `fk_observation_entry_idx` (`entry_ID`),
  CONSTRAINT `fk_code_observation` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_observation_entry` FOREIGN KEY (`entry_ID`) REFERENCES `HD_entry` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_observation_templateID` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_value_code_code` FOREIGN KEY (`value_code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1187 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `section_EmpfohleneMedikation` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_EmpfohleneMedikation_template_idx` (`templateId`),
  KEY `fk_EmpfohleneMedikation_code_idx` (`code`),
  CONSTRAINT `fk_EmpfohleneMedikation_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_EmpfohleneMedikation_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `EM_substanceAdministration` (
  `sA_id` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `id` varchar(45) DEFAULT NULL,
  `statusCode` varchar(45) DEFAULT NULL,
  `effectiveTime_low` datetime DEFAULT NULL,
  `effectiveTime_high` datetime DEFAULT NULL,
  `repeatNumber` int DEFAULT NULL,
  `routeCode` varchar(45) DEFAULT NULL,
  `doseQuantity` varchar(45) DEFAULT NULL,
  `eR_supply_independentInd` varchar(45) DEFAULT NULL,
  `eR_supply_quantity` varchar(45) DEFAULT NULL,
  `eR_act_templateID` varchar(45) DEFAULT NULL,
  `eR_act_code` varchar(45) DEFAULT NULL,
  `section_id` int DEFAULT NULL,
  PRIMARY KEY (`sA_id`),
  KEY `fk_EM_sA_template_idx` (`templateId`),
  KEY `fk_EM_sA_routeCode_idx` (`routeCode`),
  KEY `fk_EM_sA_act_code_idx` (`eR_act_code`),
  KEY `fk_EM_sA_act_template_idx` (`eR_act_templateID`),
  KEY `fk_EM_sA_sectionEM_idx` (`section_id`),
  CONSTRAINT `fk_EM_sA_act_code` FOREIGN KEY (`eR_act_code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_EM_sA_act_template` FOREIGN KEY (`eR_act_templateID`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_EM_sA_routeCode` FOREIGN KEY (`routeCode`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_EM_sA_sectionEM` FOREIGN KEY (`section_id`) REFERENCES `section_EmpfohleneMedikation` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_EM_sA_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=138 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `EM_consumable` (
  `id_consumable` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `manufacturedMaterial_templateID` varchar(45) DEFAULT NULL,
  `manufacturedMaterial_code` varchar(45) DEFAULT NULL,
  `manufacturedMaterial_name` varchar(45) DEFAULT NULL,
  `formCode` varchar(45) DEFAULT NULL,
  `capacityQuantity` varchar(45) DEFAULT NULL,
  `capacityUnit` varchar(45) DEFAULT NULL,
  `pharmCode` varchar(45) DEFAULT NULL,
  `pharmName` varchar(45) DEFAULT NULL,
  `EM_sA_id` int DEFAULT NULL,
  PRIMARY KEY (`id_consumable`),
  KEY `fk_EMc_template_idx` (`templateId`),
  KEY `fk_EMc_template2_idx` (`manufacturedMaterial_templateID`),
  KEY `fk_EMc_code_idx` (`manufacturedMaterial_code`),
  KEY `fk_EMc_EMsA_idx` (`EM_sA_id`),
  KEY `fk_pharmcode_code_idx` (`pharmCode`),
  KEY `fk_formcode_code_idx` (`formCode`),
  CONSTRAINT `fk_EMc_code` FOREIGN KEY (`manufacturedMaterial_code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_EMc_EMsA` FOREIGN KEY (`EM_sA_id`) REFERENCES `EM_substanceAdministration` (`sA_id`) ON DELETE CASCADE,
  CONSTRAINT `fk_EMc_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_EMc_template2` FOREIGN KEY (`manufacturedMaterial_templateID`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_formcode_code` FOREIGN KEY (`formCode`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_pharmcode_code` FOREIGN KEY (`pharmCode`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=214 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `section_MedicationOnAdmission` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_MedicationOnAdmission_template_idx` (`templateId`),
  KEY `fk_MedicationOnAdmission_code_idx` (`code`),
  CONSTRAINT `fk_MedicationOnAdmission_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_MedicationOnAdmission_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `MOA_su_product` (
  `id` int NOT NULL AUTO_INCREMENT,
  `manufacturedProduct_templateId` varchar(45) DEFAULT NULL,
  `manufacturedMaterial_templateId` varchar(45) DEFAULT NULL,
  `manufacturedMaterial_code` varchar(45) DEFAULT NULL,
  `manufacturedMaterial_name` varchar(45) DEFAULT NULL,
  `manufacturedMaterial_pharm_formCode` varchar(45) DEFAULT NULL,
  `manufacturedMaterial_pharm_capacityQuantity` varchar(45) DEFAULT NULL,
  `manufacturedMaterial_pharm_ingredient_code` varchar(45) DEFAULT NULL,
  `manufacturedMaterial_pharm_ingredient_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_su_prod_template_idx` (`manufacturedProduct_templateId`),
  KEY `fk_su_prod_template2_idx` (`manufacturedMaterial_templateId`),
  KEY `fk_su_prod_code_idx` (`manufacturedMaterial_code`),
  KEY `fk_su_prod_code_idx1` (`manufacturedMaterial_pharm_formCode`),
  KEY `fk_su_prod_code3_idx` (`manufacturedMaterial_pharm_ingredient_code`),
  CONSTRAINT `fk_su_prod_code` FOREIGN KEY (`manufacturedMaterial_code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_su_prod_code2` FOREIGN KEY (`manufacturedMaterial_pharm_formCode`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_su_prod_code3` FOREIGN KEY (`manufacturedMaterial_pharm_ingredient_code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_su_prod_template` FOREIGN KEY (`manufacturedProduct_templateId`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_su_prod_template2` FOREIGN KEY (`manufacturedMaterial_templateId`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `MOA_supply` (
  `idMOA_supply` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `quantity` varchar(45) DEFAULT NULL,
  `product_id` int DEFAULT NULL,
  `section_id` int DEFAULT NULL,
  PRIMARY KEY (`idMOA_supply`),
  KEY `fk_supply_section_idx` (`section_id`),
  KEY `fk_supply_product_idx` (`product_id`),
  CONSTRAINT `fk_supply_product` FOREIGN KEY (`product_id`) REFERENCES `MOA_su_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_supply_section` FOREIGN KEY (`section_id`) REFERENCES `section_MedicationOnAdmission` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `MOA_substanceAdministration` (
  `MOA_sA_id` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `id` varchar(45) DEFAULT NULL,
  `statusCode` varchar(45) DEFAULT NULL,
  `effectiveTime_low` datetime DEFAULT NULL,
  `effectiveTime_high` datetime DEFAULT NULL,
  `effectiveTime_period_value` varchar(45) DEFAULT NULL,
  `effectiveTime_period_unit` varchar(45) DEFAULT NULL,
  `effectiveTime_width_value` varchar(45) DEFAULT NULL,
  `effectiveTime_width_unit` varchar(45) DEFAULT NULL,
  `effectiveTime_event_code` varchar(45) DEFAULT NULL,
  `effectiveTime_offset_value` varchar(45) DEFAULT NULL,
  `effectiveTime_offset_unit` varchar(45) DEFAULT NULL,
  `dosing` varchar(45) DEFAULT NULL,
  `repeatNumber` int DEFAULT NULL,
  `routeCode` varchar(45) DEFAULT NULL,
  `doseQuantity` varchar(45) DEFAULT NULL,
  `eR_supply_independentInd` varchar(45) DEFAULT NULL,
  `eR_supply_quantity` varchar(45) DEFAULT NULL,
  `eR_act_templateID` varchar(45) DEFAULT NULL,
  `eR_act_code` varchar(45) DEFAULT NULL,
  `section_id` int DEFAULT NULL,
  `sA_dosing` int DEFAULT NULL,
  `supply_id` int DEFAULT NULL,
  PRIMARY KEY (`MOA_sA_id`),
  KEY `fk_MOA_sA_template_idx` (`templateId`),
  KEY `fk_MOA_sA_route_idx` (`routeCode`),
  KEY `fk_MOA_sA_act_code_idx` (`eR_act_code`),
  KEY `fk_MOA_sA_section_idx` (`section_id`),
  KEY `fk_MOA_sA_sA_idx` (`sA_dosing`),
  KEY `fk_MOA_sA_supply_idx` (`supply_id`),
  CONSTRAINT `fk_MOA_sA_act_code` FOREIGN KEY (`eR_act_code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_MOA_sA_route` FOREIGN KEY (`routeCode`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_MOA_sA_sA` FOREIGN KEY (`sA_dosing`) REFERENCES `MOA_substanceAdministration` (`MOA_sA_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_MOA_sA_section` FOREIGN KEY (`section_id`) REFERENCES `section_MedicationOnAdmission` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_MOA_sA_supply` FOREIGN KEY (`supply_id`) REFERENCES `MOA_supply` (`idMOA_supply`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_MOA_sA_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=125 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `MOA_sA_consumable` (
  `id_consumable` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `manufacturedMaterial_templateID` varchar(45) DEFAULT NULL,
  `manufacturedMaterial_code` varchar(45) DEFAULT NULL,
  `manufacturedMaterial_name` varchar(45) DEFAULT NULL,
  `formCode` varchar(45) DEFAULT NULL,
  `capacityQuantity` varchar(45) DEFAULT NULL,
  `capacityUnit` varchar(45) DEFAULT NULL,
  `pharmCode` varchar(45) DEFAULT NULL,
  `pharmName` varchar(45) DEFAULT NULL,
  `MOA_sA_id` int DEFAULT NULL,
  PRIMARY KEY (`id_consumable`),
  KEY `fk_c_code_idx` (`manufacturedMaterial_code`),
  KEY `fk_sA_idx` (`MOA_sA_id`),
  KEY `fk_pharm2_code_idx` (`pharmCode`),
  KEY `fk_formcode2_code_idx` (`formCode`),
  CONSTRAINT `fk_c_code` FOREIGN KEY (`manufacturedMaterial_code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_formcode2_code` FOREIGN KEY (`formCode`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_pharm2_code` FOREIGN KEY (`pharmCode`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_sA` FOREIGN KEY (`MOA_sA_id`) REFERENCES `MOA_substanceAdministration` (`MOA_sA_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `ClinicalDocument_CDAEntlassungsbrief_A` (
  `id` varchar(45) NOT NULL,
  `realmCode` varchar(45) DEFAULT NULL,
  `effectiveTime` datetime DEFAULT NULL,
  `typeId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `confidentialityCode` varchar(45) DEFAULT NULL,
  `languageCode` varchar(45) DEFAULT NULL,
  `recordTarget` varchar(45) DEFAULT NULL,
  `encompassingEncounter` varchar(45) DEFAULT NULL,
  `sectionAbschliessendeBemerkung_Id` int DEFAULT NULL,
  `sectionAdvancedDirectives_Id` int DEFAULT NULL,
  `sectionAllergie_Id` int DEFAULT NULL,
  `sectionAnamnese_Id` int DEFAULT NULL,
  `sectionAufnahmegrund_Id` int DEFAULT NULL,
  `sectionBrieftext_Id` int DEFAULT NULL,
  `sectionEmpfohleneMedikation_Id` int DEFAULT NULL,
  `section_ErhobeneBefunde_Id` int DEFAULT NULL,
  `section_ErhobeneBefunde_AustehendeBefunde_Id` int DEFAULT NULL,
  `section_ErhobeneBefunde_AuszugAusBefunde_Id` int DEFAULT NULL,
  `section_ErhobeneBefunde_BeigelegteBefunde_Id` int DEFAULT NULL,
  `section_ErhobeneBefunde_Operationsbericht_Id` int DEFAULT NULL,
  `section_ErhobeneBefunde_Vitalparameter_Id` int DEFAULT NULL,
  `sectionFruehereErkrankungen_Id` int DEFAULT NULL,
  `sectionFruehereErkrankungen_BisherigeMassnahmen_Id` int DEFAULT NULL,
  `sectionHospitalDischarge` int DEFAULT NULL,
  `sectionMedicationAdministered` int DEFAULT NULL,
  `sectionMedicationOnAdmission` int DEFAULT NULL,
  `sectionOutcomesMeasurement` int DEFAULT NULL,
  `sectionProcedureNarrative` int DEFAULT NULL,
  `sectionRehabilitationsziele` int DEFAULT NULL,
  `sectionTreatmentPlan` int DEFAULT NULL,
  `sectionTreatmentPlan_Entlassungszustand` int DEFAULT NULL,
  `sectionTreatmentPlan_PlanOfCareNote` int DEFAULT NULL,
  `sectionTreatmentPlan_Termin` int DEFAULT NULL,
  `sectionZusammenfassungDesAufenthalts` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_CDA_patient_idx` (`recordTarget`),
  KEY `fk_CDA_brief_idx` (`sectionBrieftext_Id`),
  KEY `fk_CDA_encompassingEnc_idx` (`encompassingEncounter`),
  KEY `fk_CDA_code_idx` (`code`),
  KEY `fk_section_1_idx` (`sectionAbschliessendeBemerkung_Id`),
  KEY `fk_section_2_idx` (`sectionAdvancedDirectives_Id`),
  KEY `fk_section_3_idx` (`sectionAllergie_Id`),
  KEY `fk_section_4_idx` (`sectionAnamnese_Id`),
  KEY `fk_section_5_idx` (`sectionAufnahmegrund_Id`),
  KEY `fk_section_6_idx` (`sectionEmpfohleneMedikation_Id`),
  KEY `fk_section_7_idx` (`section_ErhobeneBefunde_Id`),
  KEY `fk_section_8_idx` (`section_ErhobeneBefunde_AustehendeBefunde_Id`),
  KEY `fk_section_9_idx` (`section_ErhobeneBefunde_AuszugAusBefunde_Id`),
  KEY `fk_section_10_idx` (`section_ErhobeneBefunde_BeigelegteBefunde_Id`),
  KEY `fk_section_11_idx` (`section_ErhobeneBefunde_Operationsbericht_Id`),
  KEY `fk_section_12_idx` (`section_ErhobeneBefunde_Vitalparameter_Id`),
  KEY `fk_section_13_idx` (`sectionFruehereErkrankungen_Id`),
  KEY `fk_section_14_idx` (`sectionFruehereErkrankungen_BisherigeMassnahmen_Id`),
  KEY `fk_section_15_idx` (`sectionHospitalDischarge`),
  KEY `fk_section_16_idx` (`sectionMedicationAdministered`),
  KEY `fk_section_17_idx` (`sectionMedicationOnAdmission`),
  KEY `fk_section_18_idx` (`sectionOutcomesMeasurement`),
  KEY `fk_section_19_idx` (`sectionProcedureNarrative`),
  KEY `fk_section_20_idx` (`sectionRehabilitationsziele`),
  KEY `fk_section_21_idx` (`sectionTreatmentPlan`),
  KEY `fk_section_22_idx` (`sectionTreatmentPlan_Entlassungszustand`),
  KEY `fk_section_23_idx` (`sectionTreatmentPlan_PlanOfCareNote`),
  KEY `fk_section_24_idx` (`sectionTreatmentPlan_Termin`),
  KEY `fk_section_25_idx` (`sectionZusammenfassungDesAufenthalts`),
  CONSTRAINT `fk_CDA_brief` FOREIGN KEY (`sectionBrieftext_Id`) REFERENCES `section_Brieftext` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CDA_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CDA_encompassingEnc` FOREIGN KEY (`encompassingEncounter`) REFERENCES `encompassingEncounter` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CDA_patient` FOREIGN KEY (`recordTarget`) REFERENCES `patientRole` (`id_SVNR`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_section_1` FOREIGN KEY (`sectionAbschliessendeBemerkung_Id`) REFERENCES `section_AbschliessendeBemerkung` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_section_10` FOREIGN KEY (`section_ErhobeneBefunde_BeigelegteBefunde_Id`) REFERENCES `section_ErhobeneBefunde_BeigelegteBefunde` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_section_11` FOREIGN KEY (`section_ErhobeneBefunde_Operationsbericht_Id`) REFERENCES `section_ErhobeneBefunde_Operationsbericht` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_section_12` FOREIGN KEY (`section_ErhobeneBefunde_Vitalparameter_Id`) REFERENCES `section_ErhobeneBefunde_Vitalparameter` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_section_13` FOREIGN KEY (`sectionFruehereErkrankungen_Id`) REFERENCES `section_FruehereErkrankungen` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_section_14` FOREIGN KEY (`sectionFruehereErkrankungen_BisherigeMassnahmen_Id`) REFERENCES `section_FruehereErkrankungen_BisherigeMassnahmen` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_section_15` FOREIGN KEY (`sectionHospitalDischarge`) REFERENCES `section_HospitalDischarge` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_section_16` FOREIGN KEY (`sectionMedicationAdministered`) REFERENCES `section_MedicationAdministered` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_section_17` FOREIGN KEY (`sectionMedicationOnAdmission`) REFERENCES `section_MedicationOnAdmission` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_section_18` FOREIGN KEY (`sectionOutcomesMeasurement`) REFERENCES `section_OutcomeMeasurement` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_section_19` FOREIGN KEY (`sectionProcedureNarrative`) REFERENCES `section_ProcedureNarrative` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_section_2` FOREIGN KEY (`sectionAdvancedDirectives_Id`) REFERENCES `section_AdvancedDirectives` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_section_20` FOREIGN KEY (`sectionRehabilitationsziele`) REFERENCES `section_Rehabilitationsziele` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_section_21` FOREIGN KEY (`sectionTreatmentPlan`) REFERENCES `section_TreatmentPlan` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_section_22` FOREIGN KEY (`sectionTreatmentPlan_Entlassungszustand`) REFERENCES `section_TreatmentPlan_Entlassungszustand` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_section_23` FOREIGN KEY (`sectionTreatmentPlan_PlanOfCareNote`) REFERENCES `section_TreatmentPlan_PlanofCareNote` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_section_24` FOREIGN KEY (`sectionTreatmentPlan_Termin`) REFERENCES `section_TreatmentPlan_Termin` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_section_25` FOREIGN KEY (`sectionZusammenfassungDesAufenthalts`) REFERENCES `section_ZusammenfassungDesAufenthalts` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_section_3` FOREIGN KEY (`sectionAllergie_Id`) REFERENCES `section_Allergie` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_section_4` FOREIGN KEY (`sectionAnamnese_Id`) REFERENCES `section_Anamnese` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_section_5` FOREIGN KEY (`sectionAufnahmegrund_Id`) REFERENCES `section_Aufnahmegrund` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_section_6` FOREIGN KEY (`sectionEmpfohleneMedikation_Id`) REFERENCES `section_EmpfohleneMedikation` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_section_7` FOREIGN KEY (`section_ErhobeneBefunde_Id`) REFERENCES `section_ErhobeneBefunde` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_section_8` FOREIGN KEY (`section_ErhobeneBefunde_AustehendeBefunde_Id`) REFERENCES `section_ErhobeneBefunde_AustehendeBefunde` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_section_9` FOREIGN KEY (`section_ErhobeneBefunde_AuszugAusBefunde_Id`) REFERENCES `section_ErhobeneBefunde_AuszugAusBefunde` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `ClinicalDocument_has_participant` (
  `idClinicalDocument_has_participant` int NOT NULL AUTO_INCREMENT,
  `ClinicalDocument_ID` varchar(45) DEFAULT NULL,
  `Participant_ID` int DEFAULT NULL,
  PRIMARY KEY (`idClinicalDocument_has_participant`),
  KEY `fk_CHP_CDA_idx` (`ClinicalDocument_ID`),
  KEY `fk_CHP_Part_idx` (`Participant_ID`),
  CONSTRAINT `fk_CHP_CDA` FOREIGN KEY (`ClinicalDocument_ID`) REFERENCES `ClinicalDocument_CDAEntlassungsbrief_A` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CHP_Part` FOREIGN KEY (`Participant_ID`) REFERENCES `participant` (`id_participant`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `ClinicalDocument_has_template` (
  `idClinicalDocument_has_template` int NOT NULL AUTO_INCREMENT,
  `documentID` varchar(45) DEFAULT NULL,
  `templateID` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idClinicalDocument_has_template`),
  KEY `fk_document_idx` (`documentID`),
  KEY `fk_template_idx` (`templateID`),
  CONSTRAINT `fk_document` FOREIGN KEY (`documentID`) REFERENCES `ClinicalDocument_CDAEntlassungsbrief_A` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_template` FOREIGN KEY (`templateID`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `serviceEvent` (
  `serviceEvent_ID` int NOT NULL AUTO_INCREMENT,
  `code` varchar(45) DEFAULT NULL,
  `effectiveTime_low` datetime DEFAULT NULL,
  `effectiveTime_high` datetime DEFAULT NULL,
  `templateId` varchar(45) DEFAULT NULL,
  `person_name` varchar(45) DEFAULT NULL,
  `person_addr` int DEFAULT NULL,
  `organisation_id` varchar(45) DEFAULT NULL,
  `organisation_name` varchar(45) DEFAULT NULL,
  `organisation_addr` int DEFAULT NULL,
  `CDA_ID` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`serviceEvent_ID`),
  KEY `fk_code_service_event_idx` (`code`),
  KEY `fK_service_event_templateId_idx` (`templateId`),
  KEY `fk_serviceEvent-Organisation_addr_idx` (`organisation_addr`),
  KEY `fk_serviceEvent-Person_addr_idx` (`person_addr`),
  KEY `fk_serviceEvent_CDA_idx` (`CDA_ID`),
  CONSTRAINT `fk_code_service_event` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fK_service_event_templateId` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_serviceEvent-Organisation_addr` FOREIGN KEY (`organisation_addr`) REFERENCES `addr` (`id_addr`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_serviceEvent-Person_addr` FOREIGN KEY (`person_addr`) REFERENCES `addr` (`id_addr`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_serviceEvent_CDA` FOREIGN KEY (`CDA_ID`) REFERENCES `ClinicalDocument_CDAEntlassungsbrief_A` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;











