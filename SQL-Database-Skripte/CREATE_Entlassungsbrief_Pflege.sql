CREATE DATABASE `CDA_Entlassungsbrief_P` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

-- Sammlung aller Codes, die in einem CDA-File vorkommen
CREATE TABLE `code` (
  `code` varchar(45) NOT NULL,
  `codeSystem` varchar(45) NOT NULL,
  `displayName` varchar(45) DEFAULT NULL,
  `codeSystemName` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`code`,`codeSystem`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Sammlung aller Adressen, die in einem CDA-File vorkommen
CREATE TABLE `addr` (
  `id_addr` int NOT NULL AUTO_INCREMENT,
  `streetAddressLine` varchar(45) NOT NULL,
  `postalCode` int NOT NULL,
  `city` varchar(45) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `additionalLocator` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`streetAddressLine`,`postalCode`),
  UNIQUE KEY `id_addr_UNIQUE` (`id_addr`)
) ENGINE=InnoDB AUTO_INCREMENT=4272 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Sammlung aller templateIds, die in einem CDA-File vorkommen
CREATE TABLE `templateId` (
  `root` varchar(45) NOT NULL,
  `assigningAuthorityName` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`root`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Daten des extrahierten Patienten/Patientin (Header)
CREATE TABLE `patientRole` (
  `id_SVNR` varchar(45) NOT NULL,
  `id_local` varchar(45) DEFAULT NULL,
  `id_BPKGH` varchar(45) DEFAULT NULL,
  `addr` int DEFAULT NULL,
  `given` varchar(45) DEFAULT NULL,
  `family` varchar(45) DEFAULT NULL,
  `administrativeGenderCode` varchar(45) DEFAULT NULL,
  `birthTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id_SVNR`),
  UNIQUE KEY `id_SVNR_UNIQUE` (`id_SVNR`),
  UNIQUE KEY `id_local_UNIQUE` (`id_local`),
  UNIQUE KEY `id_BPKGH_UNIQUE` (`id_BPKGH`),
  KEY `fk_home_addr_idx` (`addr`),
  KEY `fk_gender_code_idx` (`administrativeGenderCode`),
  CONSTRAINT `fk_gender_code` FOREIGN KEY (`administrativeGenderCode`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_home_addr` FOREIGN KEY (`addr`) REFERENCES `addr` (`id_addr`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- CDA-Element encompassing Encounter (Header)
CREATE TABLE `encompassingEncounter` (
  `id` varchar(45) NOT NULL,
  `code` varchar(45) DEFAULT NULL,
  `effectiveTime_low` datetime DEFAULT NULL,
  `effectiveTime_high` datetime DEFAULT NULL,
  `assignedEntity_id` varchar(45) DEFAULT NULL,
  `person_name` varchar(80) DEFAULT NULL,
  `person_addr` int DEFAULT NULL,
  `organisation_id` varchar(45) DEFAULT NULL,
  `organisation_name` varchar(80) DEFAULT NULL,
  `organisation_addr` int DEFAULT NULL,
  `location_id` varchar(45) DEFAULT NULL,
  `location_name` varchar(80) DEFAULT NULL,
  `location_addr` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_encompassingEnc_code_idx` (`code`),
  KEY `fk_person_addr_idx` (`person_addr`),
  KEY `fk_EC-organisation_addr_idx` (`organisation_addr`),
  KEY `fk_EC-location_addr_idx` (`location_addr`),
  CONSTRAINT `fk_EC-location_addr` FOREIGN KEY (`location_addr`) REFERENCES `addr` (`id_addr`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_EC-organisation_addr` FOREIGN KEY (`organisation_addr`) REFERENCES `addr` (`id_addr`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_EC-person_addr` FOREIGN KEY (`person_addr`) REFERENCES `addr` (`id_addr`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_encompassingEnc_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- CDA-Element Participant (Header)
CREATE TABLE `participant` (
  `id_participant` int NOT NULL AUTO_INCREMENT,
  `typeCode` varchar(45) DEFAULT NULL,
  `templateId` varchar(45) DEFAULT NULL,
  `functionCode` varchar(45) DEFAULT NULL,
  `person_id` varchar(45) DEFAULT NULL,
  `person_name` varchar(45) DEFAULT NULL,
  `organisation_id` varchar(45) DEFAULT NULL,
  `organisation_name` varchar(45) DEFAULT NULL,
  `organisation_addr` int DEFAULT NULL,
  PRIMARY KEY (`id_participant`),
  KEY `fk_participant_templateID_idx` (`templateId`),
  KEY `fk_participant_code_idx` (`functionCode`),
  KEY `fk_participant-org_addr_idx` (`organisation_addr`),
  CONSTRAINT `fk_participant-org_addr` FOREIGN KEY (`organisation_addr`) REFERENCES `addr` (`id_addr`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_participant_code` FOREIGN KEY (`functionCode`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_participant_templateID` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- CDA-Element Author (Header)
CREATE TABLE `author` (
  `author_id` int NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `functionCode` varchar(45) DEFAULT NULL,
  `ass_author_id` varchar(45) DEFAULT NULL,
  `ass_author_code` varchar(45) DEFAULT NULL,
  `ass_author_name` varchar(45) DEFAULT NULL,
  `auth_device_code` varchar(45) DEFAULT NULL,
  `auth_device_manufacturerModelName` varchar(45) DEFAULT NULL,
  `auth_device_softwareName` varchar(45) DEFAULT NULL,
  `rep_organisation_id` varchar(45) DEFAULT NULL,
  `rep_organisation_name` varchar(45) DEFAULT NULL,
  `rep_organisation_addr` int DEFAULT NULL,
  PRIMARY KEY (`author_id`),
  KEY `fk_author_aa_code_idx` (`ass_author_code`),
  KEY `fk_author_functionCode_idx` (`functionCode`),
  KEY `fk_author_ad_code_idx` (`auth_device_code`),
  KEY `fk_author_addr_idx` (`rep_organisation_addr`),
  CONSTRAINT `fk_author_aa_code` FOREIGN KEY (`ass_author_code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_author_ad_code` FOREIGN KEY (`auth_device_code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_author_addr` FOREIGN KEY (`rep_organisation_addr`) REFERENCES `addr` (`id_addr`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_author_functionCode` FOREIGN KEY (`functionCode`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Brieftext Sektion nicht codiert
CREATE TABLE `section_Brieftext` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_Brieftext_template_idx` (`templateId`),
  KEY `fk_Brieftext_code_idx` (`code`),
  CONSTRAINT `fk_Brieftext_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Brieftext_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=149 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- #Pflegediagnosen codiert (+ act + observation)
CREATE TABLE `section_Pflegediagnosen` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` int DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_PF_template_idx` (`templateId`),
  KEY `fk_PF_code_idx` (`code`),
  KEY `fk_PF_entry_idx` (`entry`),
  CONSTRAINT `fk_PF_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_PF_entry` FOREIGN KEY (`entry`) REFERENCES `Pflegediagnosen_act` (`idact`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_PF_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `Pflegediagnosen_act` (
  `idact` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `id` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `statusCode` varchar(45) DEFAULT NULL,
  `effectiveTime_low` datetime DEFAULT NULL,
  `effectiveTime_high` datetime DEFAULT NULL,
  `entryRelationship` int DEFAULT NULL,
  PRIMARY KEY (`idact`),
  KEY `fk_pfact_entryRel_idx` (`entryRelationship`),
  KEY `fk_pfact_code_idx` (`code`),
  KEY `fk_pfact_template_idx` (`templateId`),
  CONSTRAINT `fk_pfact_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_pfact_entryRel` FOREIGN KEY (`entryRelationship`) REFERENCES `observation` (`id_observation`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_pfact_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `observation` (
  `id_observation` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `id` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `text` varchar(500) DEFAULT NULL,
  `statusCode` varchar(45) DEFAULT NULL,
  `effectiveTime_low` datetime DEFAULT NULL,
  `effectiveTime_high` datetime DEFAULT NULL,
  `value` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_observation`),
  KEY `fk_observation_template_idx` (`templateId`),
  KEY `fk_observation_code_idx` (`code`),
  KEY `fk_observation_value_idx` (`value`),
  CONSTRAINT `fk_observation_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`),
  CONSTRAINT `fk_observation_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`),
  CONSTRAINT `fk_observation_value` FOREIGN KEY (`value`) REFERENCES `code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- #Mobilität + Hilfsmittel Untersektion
CREATE TABLE `section_Mobilitaet` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_Mobilitaet_template_idx` (`templateId`),
  KEY `fk_Mobilitaet_code_idx` (`code`),
  CONSTRAINT `fk_Mobilitaet_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Mobilitaet_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `section_MobilitaetHilfsmittel` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_MobilitaetHilfsmittel_template_idx` (`templateId`),
  KEY `fk_MobilitaetHilfsmittel_code_idx` (`code`),
  CONSTRAINT `fk_MobilitaetHilfsmittel_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_MobilitaetHilfsmittel_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Körperpflege nicht codiert
CREATE TABLE `section_Koerperpflege` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_Koerperpflege_template_idx` (`templateId`),
  KEY `fk_Koerperpflege_code_idx` (`code`),
  CONSTRAINT `fk_Koerperpflege_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Koerperpflege_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Ernährung nicht codiert
CREATE TABLE `section_Ernaehrung` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_Ernaehrung_template_idx` (`templateId`),
  KEY `fk_Ernaehrung_code_idx` (`code`),
  CONSTRAINT `fk_Ernaehrung_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Ernaehrung_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Ausscheidung nicht codiert
CREATE TABLE `section_Ausscheidung` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_Ausscheidung_template_idx` (`templateId`),
  KEY `fk_Ausscheidung_code_idx` (`code`),
  CONSTRAINT `fk_Ausscheidung_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Ausscheidung_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Hautzustand nicht codiert
CREATE TABLE `section_Hautzustand` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_Hautzustand_template_idx` (`templateId`),
  KEY `fk_Hautzustand_code_idx` (`code`),
  CONSTRAINT `fk_Hautzustand_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Hautzustand_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Atmung nicht codiert
CREATE TABLE `section_Atmung` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_Atmung_template_idx` (`templateId`),
  KEY `fk_Atmung_code_idx` (`code`),
  CONSTRAINT `fk_Atmung_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Atmung_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Schlaf nicht codiert
CREATE TABLE `section_Schlaf` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_Schlaf_template_idx` (`templateId`),
  KEY `fk_Schlaf_code_idx` (`code`),
  CONSTRAINT `fk_Schlaf_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Schlaf_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Schmerz nicht codiert
CREATE TABLE `section_Schmerz` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_Schmerz_template_idx` (`templateId`),
  KEY `fk_Schmerz_code_idx` (`code`),
  CONSTRAINT `fk_Schmerz_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Schmerz_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- #Orientierung + Risiken Untersektion
CREATE TABLE `section_Orientierung` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_Orientierung_template_idx` (`templateId`),
  KEY `fk_Orientierung_code_idx` (`code`),
  CONSTRAINT `fk_Orientierung_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Orientierung_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `section_OrientierungRisiken` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_OrientierungRisiken_template_idx` (`templateId`),
  KEY `fk_OrientierungRisiken_code_idx` (`code`),
  CONSTRAINT `fk_OrientierungRisiken_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_OrientierungRisiken_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


-- Soziale Umstände nicht codiert
CREATE TABLE `section_SozialeUmstaende` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_SozialeUmstaende_template_idx` (`templateId`),
  KEY `fk_SozialeUmstaende_code_idx` (`code`),
  CONSTRAINT `fk_SozialeUmstaende_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_SozialeUmstaende_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- #Kommunikation + Hilfsmittel Untersektion
CREATE TABLE `section_Kommunikation` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_Kommunikation_template_idx` (`templateId`),
  KEY `fk_Kommunikation_code_idx` (`code`),
  CONSTRAINT `fk_Kommunikation_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Kommunikation_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `section_KommunikationHilfsmittel` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_KommunikationHilfsmittel_template_idx` (`templateId`),
  KEY `fk_KommunikationHilfsmittel_code_idx` (`code`),
  CONSTRAINT `fk_KommunikationHilfsmittel_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_KommunikationHilfsmittel_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Rollenwahrnehmung nicht codiert
CREATE TABLE `section_Rollenwahrnehmung` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_Rollenwahrnehmung_template_idx` (`templateId`),
  KEY `fk_Rollenwahrnehmung_code_idx` (`code`),
  CONSTRAINT `fk_Rollenwahrnehmung_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Rollenwahrnehmung_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- #Vitalparameter codiert
CREATE TABLE `section_Vitalparameter` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_vital_template_idx` (`templateId`),
  KEY `fk_vital_code_idx` (`code`),
  CONSTRAINT `fk_vital_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_vital_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `vitalparameter_entryOrganizer` (
  `EO_id` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `id` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `statusCode` varchar(45) DEFAULT NULL,
  `effectiveTime_low` datetime DEFAULT NULL,
  `effectiveTime_high` datetime DEFAULT NULL,
  `sectio_id` int DEFAULT NULL,
  PRIMARY KEY (`EO_id`),
  KEY `fk_EO_templateId_idx` (`templateId`),
  KEY `fk_EO_code_idx` (`code`),
  KEY `fk_EO_section_idx` (`sectio_id`),
  CONSTRAINT `fk_EO_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_EO_section` FOREIGN KEY (`sectio_id`) REFERENCES `section_Vitalparameter` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_EO_templateId` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `vitalparameter_observation` (
  `id` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `effectiveTime` datetime DEFAULT NULL,
  `interpretationCode` varchar(45) DEFAULT NULL,
  `value` varchar(45) DEFAULT NULL,
  `unit` varchar(45) DEFAULT NULL,
  `range_low` varchar(45) DEFAULT NULL,
  `range_high` varchar(45) DEFAULT NULL,
  `entryOrganizer_ID` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_vit_obs_code_idx` (`code`),
  KEY `fk_vit_obs_templateId_idx` (`templateId`),
  KEY `fk_vit_obs_entryOrganizer_idx` (`entryOrganizer_ID`),
  CONSTRAINT `fk_vit_obs_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_vit_obs_entryOrganizer` FOREIGN KEY (`entryOrganizer_ID`) REFERENCES `vitalparameter_entryOrganizer` (`EO_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_vit_obs_templateId` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- #Pflegerelevante Information + Risiken Untersektion
CREATE TABLE `section_PflegerelevanteInformationen` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_PflegerelevanteInformationen_template_idx` (`templateId`),
  KEY `fk_PflegerelevanteInformationen_code_idx` (`code`),
  CONSTRAINT `fk_PflegerelevanteInformationen_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_PflegerelevanteInformationen_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `section_PflegerelevanteInformationenRisiken` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_PflegerelevanteInformationenRisiken_template_idx` (`templateId`),
  KEY `fk_PflegerelevanteInformationenRisiken_code_idx` (`code`),
  CONSTRAINT `fk_PflegerelevanteInformationenRisiken_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_PflegerelevanteInformationenRisiken_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Medikamentenverabreichung nicht codiert
CREATE TABLE `section_Medikamentenverabreichung` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_Medikamentenverabreichung_template_idx` (`templateId`),
  KEY `fk_Medikamentenverabreichung_code_idx` (`code`),
  CONSTRAINT `fk_Medikamentenverabreichung_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Medikamentenverabreichung_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Anmerkungen nicht codiert
CREATE TABLE `section_Anmerkungen` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_Anmerkungen_template_idx` (`templateId`),
  KEY `fk_Anmerkungen_code_idx` (`code`),
  CONSTRAINT `fk_Anmerkungen_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Anmerkungen_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- #Entlassungmanagement codiert
CREATE TABLE `Entlassungsmanagement_act` (
  `id_act` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `id` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `text` varchar(1000) DEFAULT NULL,
  `statusCode` varchar(45) DEFAULT NULL,
  `effectiveTime_low` datetime DEFAULT NULL,
  `effectiveTime_high` datetime DEFAULT NULL,
  PRIMARY KEY (`id_act`),
  KEY `fk_EM_template_idx` (`templateId`),
  KEY `fk_EM_code_idx` (`code`),
  CONSTRAINT `fk_EM_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_EM_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `section_Entlassungsmanagement` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` int DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_sec_EM_act_idx` (`entry`),
  KEY `fk_sec_EM_template_idx` (`templateId`),
  KEY `fk_sec_EM_code_idx` (`code`),
  CONSTRAINT `fk_sec_EM_act` FOREIGN KEY (`entry`) REFERENCES `Entlassungsmanagement_act` (`id_act`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_sec_EM_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_sec_EM_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


-- Patientenverfügung nicht codiert
CREATE TABLE `section_Patientenverfuegung` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_Patientenverfuegung_template_idx` (`templateId`),
  KEY `fk_Patientenverfuegung_code_idx` (`code`),
  CONSTRAINT `fk_Patientenverfuegung_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Patientenverfuegung_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Abschliessende Bemerkungen nicht codiert
CREATE TABLE `section_AbschliessendeBemerkungen` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_AbschliessendeBemerkungen_template_idx` (`templateId`),
  KEY `fk_AbschliessendeBemerkungen_code_idx` (`code`),
  CONSTRAINT `fk_AbschliessendeBemerkungen_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_AbschliessendeBemerkungen_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Beilagen nicht codiert
CREATE TABLE `section_Beilagen` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_Beilagen_template_idx` (`templateId`),
  KEY `fk_Beilagen_code_idx` (`code`),
  CONSTRAINT `fk_Beilagen_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Beilagen_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- CDA Dokument Entlassungsbrief Pflege
CREATE TABLE `ClinicalDocument_CDAEntlassungsbrief_P` (
  `id` varchar(45) NOT NULL,
  `realmCode` varchar(45) DEFAULT NULL,
  `effectiveTime` datetime DEFAULT NULL,
  `typeId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `confidentialityCode` varchar(45) DEFAULT NULL,
  `languageCode` varchar(45) DEFAULT NULL,
  `recordTarget` varchar(45) DEFAULT NULL,
  `encompassingEncounter` varchar(45) DEFAULT NULL,
  `section_Brieftext` int DEFAULT NULL,
  `section_Pflegediagnosen` int DEFAULT NULL,
  `section_Mobilitaet` int DEFAULT NULL,
  `section_MobilitaetHilfsmittel` int DEFAULT NULL,
  `section_Koerperpflege` int DEFAULT NULL,
  `section_Ernaehrung` int DEFAULT NULL,
  `section_Ausscheidung` int DEFAULT NULL,
  `section_Hautzustand` int DEFAULT NULL,
  `section_Atmung` int DEFAULT NULL,
  `section_Schlaf` int DEFAULT NULL,
  `section_Schmerz` int DEFAULT NULL,
  `section_Orientierung` int DEFAULT NULL,
  `section_OrientierungRisiken` int DEFAULT NULL,
  `section_SozialeUmstaende` int DEFAULT NULL,
  `section_Kommunikation` int DEFAULT NULL,
  `section_KommunikationHilfsmittel` int DEFAULT NULL,
  `section_Rollenwahrnehmung` int DEFAULT NULL,
  `section_Vitalparameter` int DEFAULT NULL,
  `section_PflegerelevanteInformationen` int DEFAULT NULL,
  `section_PflegerelevanteInformationenRisiken` int DEFAULT NULL,
  `section_Medikamentenverabreichung` int DEFAULT NULL,
  `section_Anmerkungen` int DEFAULT NULL,
  `section_Entlassungsmanagement` int DEFAULT NULL,
  `section_Patientenverfuegung` int DEFAULT NULL,
  `section_AbschliessendeBemerkungen` int DEFAULT NULL,
  `section_Beilagen` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_CDA_P_code_idx` (`code`),
  KEY `fk_CDA_P_patient_idx` (`recordTarget`),
  KEY `fk_CDA_P_encompassing_idx` (`encompassingEncounter`),
  KEY `fk_CDA_section_Brieftext_idx` (`section_Brieftext`),
  KEY `fk_CDA_section_Pflegediagnosen_idx` (`section_Pflegediagnosen`),
  KEY `fk_CDA_section_Mobilitaet_idx` (`section_Mobilitaet`),
  KEY `fk_CDA_section_Mobilitaet2_idx` (`section_MobilitaetHilfsmittel`),
  KEY `fk_CDA_section_Koerperpflege_idx` (`section_Koerperpflege`),
  KEY `fk_CDA_section_Ernaehrung_idx` (`section_Ernaehrung`),
  KEY `fk_CDA_section_Ausscheidung_idx` (`section_Ausscheidung`),
  KEY `fk_CDA_section_Hautzustand_idx` (`section_Hautzustand`),
  KEY `fk_CDA_section_Atmung_idx` (`section_Atmung`),
  KEY `fk_CDA_section_Schlaf_idx` (`section_Schlaf`),
  KEY `fk_CDA_section_Schmerz_idx` (`section_Schmerz`),
  KEY `fk_CDA_section_Orientierung_idx` (`section_Orientierung`),
  KEY `fk_CDA_section_Orientierung2_idx` (`section_OrientierungRisiken`),
  KEY `fk_CDA_section_SozialeUmstaende_idx` (`section_SozialeUmstaende`),
  KEY `fk_CDA_section_Kommunikation_idx` (`section_Kommunikation`),
  KEY `fk_CDA_section_Kommunikation2_idx` (`section_KommunikationHilfsmittel`),
  KEY `fk_CDA_section_Rollenwahrnehmung_idx` (`section_Rollenwahrnehmung`),
  KEY `fk_CDA_section_Vitalparameter_idx` (`section_Vitalparameter`),
  KEY `fk_CDA_section_PflegerelevanteInformationen_idx` (`section_PflegerelevanteInformationen`),
  KEY `fk_CDA_section_PflegerelevanteInformationen2_idx` (`section_PflegerelevanteInformationenRisiken`),
  KEY `fk_CDA_section_Medikamentenverabreichung_idx` (`section_Medikamentenverabreichung`),
  KEY `fk_CDA_section_Anmerkungen_idx` (`section_Anmerkungen`),
  KEY `fk_CDA_section_Entlassungsmanagement_idx` (`section_Entlassungsmanagement`),
  KEY `fk_CDA_section_Patientenverfuegung_idx` (`section_Patientenverfuegung`),
  KEY `fk_CDA_section_AbschliessendeBemerkungen_idx` (`section_AbschliessendeBemerkungen`),
  KEY `fk_CDA_section_Beilagen_idx` (`section_Beilagen`),
  CONSTRAINT `fk_CDA_P_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CDA_P_encompassing` FOREIGN KEY (`encompassingEncounter`) REFERENCES `encompassingEncounter` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CDA_P_patient` FOREIGN KEY (`recordTarget`) REFERENCES `patientRole` (`id_SVNR`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CDA_section_AbschliessendeBemerkungen` FOREIGN KEY (`section_AbschliessendeBemerkungen`) REFERENCES `section_AbschliessendeBemerkungen` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CDA_section_Anmerkungen` FOREIGN KEY (`section_Anmerkungen`) REFERENCES `section_Anmerkungen` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CDA_section_Atmung` FOREIGN KEY (`section_Atmung`) REFERENCES `section_Atmung` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CDA_section_Ausscheidung` FOREIGN KEY (`section_Ausscheidung`) REFERENCES `section_Ausscheidung` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CDA_section_Beilagen` FOREIGN KEY (`section_Beilagen`) REFERENCES `section_Beilagen` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CDA_section_Brieftext` FOREIGN KEY (`section_Brieftext`) REFERENCES `section_Brieftext` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CDA_section_Entlassungsmanagement` FOREIGN KEY (`section_Entlassungsmanagement`) REFERENCES `section_Entlassungsmanagement` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CDA_section_Ernaehrung` FOREIGN KEY (`section_Ernaehrung`) REFERENCES `section_Ernaehrung` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CDA_section_Hautzustand` FOREIGN KEY (`section_Hautzustand`) REFERENCES `section_Hautzustand` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CDA_section_Koerperpflege` FOREIGN KEY (`section_Koerperpflege`) REFERENCES `section_Koerperpflege` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CDA_section_Kommunikation` FOREIGN KEY (`section_Kommunikation`) REFERENCES `section_Kommunikation` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CDA_section_Kommunikation2` FOREIGN KEY (`section_KommunikationHilfsmittel`) REFERENCES `section_KommunikationHilfsmittel` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CDA_section_Medikamentenverabreichung` FOREIGN KEY (`section_Medikamentenverabreichung`) REFERENCES `section_Medikamentenverabreichung` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CDA_section_Mobilitaet` FOREIGN KEY (`section_Mobilitaet`) REFERENCES `section_Mobilitaet` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CDA_section_Mobilitaet2` FOREIGN KEY (`section_MobilitaetHilfsmittel`) REFERENCES `section_MobilitaetHilfsmittel` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CDA_section_Orientierung` FOREIGN KEY (`section_Orientierung`) REFERENCES `section_Orientierung` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CDA_section_Orientierung2` FOREIGN KEY (`section_OrientierungRisiken`) REFERENCES `section_OrientierungRisiken` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CDA_section_Patientenverfuegung` FOREIGN KEY (`section_Patientenverfuegung`) REFERENCES `section_Patientenverfuegung` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CDA_section_Pflegediagnosen` FOREIGN KEY (`section_Pflegediagnosen`) REFERENCES `section_Pflegediagnosen` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CDA_section_PflegerelevanteInformationen` FOREIGN KEY (`section_PflegerelevanteInformationen`) REFERENCES `section_PflegerelevanteInformationen` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CDA_section_PflegerelevanteInformationen2` FOREIGN KEY (`section_PflegerelevanteInformationenRisiken`) REFERENCES `section_PflegerelevanteInformationenRisiken` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CDA_section_Rollenwahrnehmung` FOREIGN KEY (`section_Rollenwahrnehmung`) REFERENCES `section_Rollenwahrnehmung` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CDA_section_Schlaf` FOREIGN KEY (`section_Schlaf`) REFERENCES `section_Schlaf` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CDA_section_Schmerz` FOREIGN KEY (`section_Schmerz`) REFERENCES `section_Schmerz` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CDA_section_SozialeUmstaende` FOREIGN KEY (`section_SozialeUmstaende`) REFERENCES `section_SozialeUmstaende` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CDA_section_Vitalparameter` FOREIGN KEY (`section_Vitalparameter`) REFERENCES `section_Vitalparameter` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


-- Dokument hat author
CREATE TABLE `ClinicalDocument_has_author` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ClinicalDocument_ID` varchar(45) DEFAULT NULL,
  `author` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_clinicaldocument_author_idx` (`ClinicalDocument_ID`),
  KEY `fk_author_clinicaldocument_idx` (`author`),
  CONSTRAINT `fk_author_clinicaldocument` FOREIGN KEY (`author`) REFERENCES `author` (`author_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_clinicaldocument_author` FOREIGN KEY (`ClinicalDocument_ID`) REFERENCES `ClinicalDocument_CDAEntlassungsbrief_P` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


-- Dokument hat participant

CREATE TABLE `ClinicalDocument_has_participant` (
  `idClinicalDocument_has_participant` int NOT NULL AUTO_INCREMENT,
  `ClinicalDocument_ID` varchar(45) DEFAULT NULL,
  `Participant_ID` int DEFAULT NULL,
  PRIMARY KEY (`idClinicalDocument_has_participant`),
  KEY `fk_CHP_CDA_idx` (`ClinicalDocument_ID`),
  KEY `fk_CHP_Part_idx` (`Participant_ID`),
  CONSTRAINT `fk_CHP_CDA` FOREIGN KEY (`ClinicalDocument_ID`) REFERENCES `ClinicalDocument_CDAEntlassungsbrief_P` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CHP_Part` FOREIGN KEY (`Participant_ID`) REFERENCES `participant` (`id_participant`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


-- Dokument hat templateID
CREATE TABLE `ClinicalDocument_has_template` (
  `idClinicalDocument_has_template` int NOT NULL AUTO_INCREMENT,
  `documentID` varchar(45) DEFAULT NULL,
  `templateID` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idClinicalDocument_has_template`),
  KEY `fk_document_idx` (`documentID`),
  KEY `fk_template_idx` (`templateID`),
  CONSTRAINT `fk_document` FOREIGN KEY (`documentID`) REFERENCES `ClinicalDocument_CDAEntlassungsbrief_P` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_template` FOREIGN KEY (`templateID`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- serviceEvent
CREATE TABLE `serviceEvent` (
  `serviceEvent_ID` int NOT NULL AUTO_INCREMENT,
  `code` varchar(45) DEFAULT NULL,
  `effectiveTime_low` datetime DEFAULT NULL,
  `effectiveTime_high` datetime DEFAULT NULL,
  `templateId` varchar(45) DEFAULT NULL,
  `person_name` varchar(45) DEFAULT NULL,
  `person_addr` int DEFAULT NULL,
  `organisation_id` varchar(45) DEFAULT NULL,
  `organisation_name` varchar(45) DEFAULT NULL,
  `organisation_addr` int DEFAULT NULL,
  `CDA_ID` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`serviceEvent_ID`),
  KEY `fk_code_service_event_idx` (`code`),
  KEY `fK_service_event_templateId_idx` (`templateId`),
  KEY `fk_serviceEvent-Organisation_addr_idx` (`organisation_addr`),
  KEY `fk_serviceEvent-Person_addr_idx` (`person_addr`),
  KEY `fk_serviceEvent_CDA_idx` (`CDA_ID`),
  CONSTRAINT `fk_code_service_event` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fK_service_event_templateId` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_serviceEvent-Organisation_addr` FOREIGN KEY (`organisation_addr`) REFERENCES `addr` (`id_addr`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_serviceEvent-Person_addr` FOREIGN KEY (`person_addr`) REFERENCES `addr` (`id_addr`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_serviceEvent_CDA` FOREIGN KEY (`CDA_ID`) REFERENCES `ClinicalDocument_CDAEntlassungsbrief_P` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;



