CREATE DATABASE `CDA_document_e-Impfpass` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `CDA_document_e-Impfpass`;
CREATE TABLE `addr` (
  `id_addr` int NOT NULL AUTO_INCREMENT,
  `streetAddressLine` varchar(45) NOT NULL,
  `postalCode` int NOT NULL,
  `city` varchar(45) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `additionalLocator` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`streetAddressLine`,`postalCode`),
  UNIQUE KEY `id_addr_UNIQUE` (`id_addr`)
) ENGINE=InnoDB AUTO_INCREMENT=391 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `code` (
  `code` varchar(45) NOT NULL,
  `codeSystem` varchar(45) NOT NULL,
  `displayName` varchar(45) DEFAULT NULL,
  `codeSystemName` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`code`,`codeSystem`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `templateId` (
  `root` varchar(45) NOT NULL,
  `assigningAuthorityName` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`root`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `patientRole` (
  `id_SVNR` varchar(45) NOT NULL,
  `id_local` varchar(45) DEFAULT NULL,
  `id_BPKGH` varchar(45) DEFAULT NULL,
  `addr` int DEFAULT NULL,
  `given` varchar(45) DEFAULT NULL,
  `family` varchar(45) DEFAULT NULL,
  `administrativeGenderCode` varchar(45) DEFAULT NULL,
  `birthTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id_SVNR`),
  UNIQUE KEY `id_SVNR_UNIQUE` (`id_SVNR`),
  UNIQUE KEY `id_local_UNIQUE` (`id_local`),
  UNIQUE KEY `id_BPKGH_UNIQUE` (`id_BPKGH`),
  KEY `fk_home_addr_idx` (`addr`),
  KEY `fk_gender_code_idx` (`administrativeGenderCode`),
  CONSTRAINT `fk_gender_code` FOREIGN KEY (`administrativeGenderCode`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_home_addr` FOREIGN KEY (`addr`) REFERENCES `addr` (`id_addr`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `author` (
  `id` int NOT NULL AUTO_INCREMENT,
  `effectiveTime` datetime DEFAULT NULL,
  `assignedAuthor_id` varchar(45) DEFAULT NULL,
  `assignedAuthor_name` varchar(45) DEFAULT NULL,
  `representedOrganisation_id` varchar(45) DEFAULT NULL,
  `representedOrganisation_addr` int DEFAULT NULL,
  `representedOrganisation_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_author_addr_idx` (`representedOrganisation_addr`),
  CONSTRAINT `fk_author_addr` FOREIGN KEY (`representedOrganisation_addr`) REFERENCES `addr` (`id_addr`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `section_antikoerper` (
  `id` int NOT NULL AUTO_INCREMENT,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `text` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_antiK_code_idx` (`code`),
  CONSTRAINT `fk_antiK_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `AK_act` (
  `id` int NOT NULL AUTO_INCREMENT,
  `code` varchar(45) DEFAULT NULL,
  `statusCode` varchar(45) DEFAULT NULL,
  `section_AK_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_AKact_code_idx` (`code`),
  KEY `fk_AKact_sectionAK_idx` (`section_AK_id`),
  CONSTRAINT `fk_AKact_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_AKact_sectionAK` FOREIGN KEY (`section_AK_id`) REFERENCES `section_antikoerper` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `AK_entryRelationship` (
  `id` int NOT NULL AUTO_INCREMENT,
  `code` varchar(45) DEFAULT NULL,
  `statusCode` varchar(45) DEFAULT NULL,
  `AK_act_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_AKentry_code_idx` (`code`),
  KEY `fk_AKentry_AKact_idx` (`AK_act_id`),
  CONSTRAINT `fk_AKentry_AKact` FOREIGN KEY (`AK_act_id`) REFERENCES `AK_act` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_AKentry_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `AK_observation` (
  `id` int NOT NULL AUTO_INCREMENT,
  `code` varchar(45) DEFAULT NULL,
  `statusCode` varchar(45) DEFAULT NULL,
  `effectiveTime` varchar(45) DEFAULT NULL,
  `value` varchar(45) DEFAULT NULL,
  `value_unit` varchar(45) DEFAULT NULL,
  `interpretationCode` varchar(45) DEFAULT NULL,
  `range_low` varchar(45) DEFAULT NULL,
  `range_high` varchar(45) DEFAULT NULL,
  `range_interpretationCode` varchar(45) DEFAULT NULL,
  `AK_entryrelationship_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_akobs_code_idx` (`code`),
  KEY `fk_akobs_interpretationCode_idx` (`interpretationCode`),
  KEY `fk_akobs_range-interpretationCode_idx` (`range_interpretationCode`),
  KEY `fk_akobs_AKentryrel_idx` (`AK_entryrelationship_id`),
  CONSTRAINT `fk_akobs_AKentryrel` FOREIGN KEY (`AK_entryrelationship_id`) REFERENCES `AK_entryRelationship` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_akobs_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_akobs_interpretationCode` FOREIGN KEY (`interpretationCode`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_akobs_range-interpretationCode` FOREIGN KEY (`range_interpretationCode`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `section_immunization` (
  `id` int NOT NULL AUTO_INCREMENT,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `text` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_secimmun_code_idx` (`code`),
  CONSTRAINT `fk_secimmun_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `substanceAdministration` (
  `id` int NOT NULL AUTO_INCREMENT,
  `suAd_id` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `statusCode` varchar(45) DEFAULT NULL,
  `effectiveTime` datetime DEFAULT NULL,
  `doseQuantity` varchar(45) DEFAULT NULL,
  `consumable_code` varchar(45) DEFAULT NULL,
  `consumable_name` varchar(45) DEFAULT NULL,
  `lotNumberText` varchar(45) DEFAULT NULL,
  `consumable_ATC` varchar(45) DEFAULT NULL,
  `consumable_ATC_name` varchar(45) DEFAULT NULL,
  `author_id` int DEFAULT NULL,
  `immunizationsection_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_suAD_code_idx` (`code`),
  KEY `fk_suAD_consumableCode_idx` (`consumable_code`),
  KEY `fk_suAD_ATC_idx` (`consumable_ATC`),
  KEY `fk_author_idx` (`author_id`),
  KEY `fk_suAD_section_idx` (`immunizationsection_id`),
  CONSTRAINT `fk_suAD_ATC` FOREIGN KEY (`consumable_ATC`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_suAD_author` FOREIGN KEY (`author_id`) REFERENCES `author` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_suAD_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_suAD_consumableCode` FOREIGN KEY (`consumable_code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_suAD_section` FOREIGN KEY (`immunizationsection_id`) REFERENCES `section_immunization` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `section_impfrelevanteErkrankungen` (
  `id` int NOT NULL AUTO_INCREMENT,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `text` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_secIE_code_idx` (`code`),
  CONSTRAINT `fk_secIE_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `IE_act` (
  `id` int NOT NULL AUTO_INCREMENT,
  `code` varchar(45) DEFAULT NULL,
  `statusCode` varchar(45) DEFAULT NULL,
  `effectiveTime_low` datetime DEFAULT NULL,
  `effectiveTime_high` datetime DEFAULT NULL,
  `author` int DEFAULT NULL,
  `section_impfrelevanteErkrankung_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_IEact_code_idx` (`code`),
  KEY `fk_IEact_author_idx` (`author`),
  KEY `fk_IEact_sectionIE_idx` (`section_impfrelevanteErkrankung_id`),
  CONSTRAINT `fk_IEact_author` FOREIGN KEY (`author`) REFERENCES `author` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_IEact_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_IEact_sectionIE` FOREIGN KEY (`section_impfrelevanteErkrankung_id`) REFERENCES `section_impfrelevanteErkrankungen` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `IE_observation` (
  `id` int NOT NULL AUTO_INCREMENT,
  `code` varchar(45) DEFAULT NULL,
  `statusCode` varchar(45) DEFAULT NULL,
  `effectiveTime_low` datetime DEFAULT NULL,
  `effectiveTime_high` datetime DEFAULT NULL,
  `value_code` varchar(45) DEFAULT NULL,
  `IE_act_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_IEobservation_code_idx` (`code`),
  KEY `fk_IEobservation_valueCode_idx` (`value_code`),
  KEY `fk_IEobservation_IE_act_idx` (`IE_act_id`),
  CONSTRAINT `fk_IEobservation_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_IEobservation_IE_act` FOREIGN KEY (`IE_act_id`) REFERENCES `IE_act` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_IEobservation_valueCode` FOREIGN KEY (`value_code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ClinicalDocument_CDAeImpfpass` (
  `id` varchar(45) NOT NULL,
  `realmCode` varchar(45) DEFAULT NULL,
  `effectiveTime` datetime DEFAULT NULL,
  `typeId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `confidentialityCode` varchar(45) DEFAULT NULL,
  `languageCode` varchar(45) DEFAULT NULL,
  `recordTarget` varchar(45) DEFAULT NULL,
  `section_immunization_ID` int DEFAULT NULL,
  `section_personengruppe_ID` int DEFAULT NULL,
  `section_impfrelevanteErkrankungen_ID` int DEFAULT NULL,
  `section_antikoerper_ID` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_CDAimpf_code_idx` (`code`),
  KEY `fk_CDAimpf_sectAK_idx` (`section_antikoerper_ID`),
  KEY `fk_CDAimpf_sectimmun_idx` (`section_immunization_ID`),
  KEY `fk_CDAimpf_sectIE_idx` (`section_impfrelevanteErkrankungen_ID`),
  KEY `fk_CDAimpf_patientRole_idx` (`recordTarget`),
  CONSTRAINT `fk_CDAimpf_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CDAimpf_patientRole` FOREIGN KEY (`recordTarget`) REFERENCES `patientRole` (`id_SVNR`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CDAimpf_sectAK` FOREIGN KEY (`section_antikoerper_ID`) REFERENCES `section_antikoerper` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CDAimpf_sectIE` FOREIGN KEY (`section_impfrelevanteErkrankungen_ID`) REFERENCES `section_impfrelevanteErkrankungen` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CDAimpf_sectimmun` FOREIGN KEY (`section_immunization_ID`) REFERENCES `section_immunization` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ClinicalDocument_has_template` (
  `idClinicalDocument_has_template` int NOT NULL AUTO_INCREMENT,
  `documentID` varchar(45) DEFAULT NULL,
  `templateID` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idClinicalDocument_has_template`),
  KEY `fk_template_idx` (`templateID`),
  KEY `fk_document_idx` (`documentID`),
  CONSTRAINT `fk_document` FOREIGN KEY (`documentID`) REFERENCES `ClinicalDocument_CDAeImpfpass` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_template` FOREIGN KEY (`templateID`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `serviceEvent` (
  `serviceEvent_ID` int NOT NULL AUTO_INCREMENT,
  `code` varchar(45) DEFAULT NULL,
  `effectiveTime_low` datetime DEFAULT NULL,
  `effectiveTime_high` datetime DEFAULT NULL,
  `templateId` varchar(45) DEFAULT NULL,
  `person_name` varchar(45) DEFAULT NULL,
  `person_addr` int DEFAULT NULL,
  `organisation_id` varchar(45) DEFAULT NULL,
  `organisation_name` varchar(45) DEFAULT NULL,
  `organisation_addr` int DEFAULT NULL,
  `eImpfpassID` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`serviceEvent_ID`),
  KEY `fk_code_service_event_idx` (`code`),
  KEY `fK_service_event_templateId_idx` (`templateId`),
  KEY `fk_serviceEvent-Person_addr_idx` (`person_addr`),
  KEY `fk_serviceEvent-Organisation_addr_idx` (`organisation_addr`),
  KEY `fk_serviceEvent_eImpfpass_idx` (`eImpfpassID`),
  CONSTRAINT `fk_code_service_event` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fK_service_event_templateId` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`),
  CONSTRAINT `fk_serviceEvent-Organisation_addr` FOREIGN KEY (`organisation_addr`) REFERENCES `addr` (`id_addr`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_serviceEvent-Person_addr` FOREIGN KEY (`person_addr`) REFERENCES `addr` (`id_addr`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_serviceEvent_eImpfpass` FOREIGN KEY (`eImpfpassID`) REFERENCES `ClinicalDocument_CDAeImpfpass` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=124 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;






