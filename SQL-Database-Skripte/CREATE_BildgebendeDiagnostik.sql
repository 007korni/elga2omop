CREATE DATABASE `CDA_BildgebendeDiagnostik` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE CDA_BildgebendeDiagnostik;

-- Sammlung aller Codes, die in einem CDA-File vorkommen
CREATE TABLE `code` (
  `code` varchar(45) NOT NULL,
  `codeSystem` varchar(45) NOT NULL,
  `displayName` varchar(45) DEFAULT NULL,
  `codeSystemName` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`code`,`codeSystem`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Sammlung aller Adressen, die in einem CDA-File vorkommen
CREATE TABLE `addr` (
  `id_addr` int NOT NULL AUTO_INCREMENT,
  `streetAddressLine` varchar(45) NOT NULL,
  `postalCode` int NOT NULL,
  `city` varchar(45) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `additionalLocator` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`streetAddressLine`,`postalCode`),
  UNIQUE KEY `id_addr_UNIQUE` (`id_addr`)
) ENGINE=InnoDB AUTO_INCREMENT=4272 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Sammlung aller templateIds, die in einem CDA-File vorkommen
CREATE TABLE `templateId` (
  `root` varchar(45) NOT NULL,
  `assigningAuthorityName` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`root`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Daten des extrahierten Patienten/Patientin (Header)
CREATE TABLE `patientRole` (
  `id_SVNR` varchar(45) NOT NULL,
  `id_local` varchar(45) DEFAULT NULL,
  `id_BPKGH` varchar(45) DEFAULT NULL,
  `addr` int DEFAULT NULL,
  `given` varchar(45) DEFAULT NULL,
  `family` varchar(45) DEFAULT NULL,
  `administrativeGenderCode` varchar(45) DEFAULT NULL,
  `birthTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id_SVNR`),
  UNIQUE KEY `id_SVNR_UNIQUE` (`id_SVNR`),
  UNIQUE KEY `id_local_UNIQUE` (`id_local`),
  UNIQUE KEY `id_BPKGH_UNIQUE` (`id_BPKGH`),
  KEY `fk_home_addr_idx` (`addr`),
  KEY `fk_gender_code_idx` (`administrativeGenderCode`),
  CONSTRAINT `fk_gender_code` FOREIGN KEY (`administrativeGenderCode`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_home_addr` FOREIGN KEY (`addr`) REFERENCES `addr` (`id_addr`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- CDA-Element encompassing Encounter (Header)
CREATE TABLE `encompassingEncounter` (
  `id` varchar(45) NOT NULL,
  `code` varchar(45) DEFAULT NULL,
  `effectiveTime_low` datetime DEFAULT NULL,
  `effectiveTime_high` datetime DEFAULT NULL,
  `assignedEntity_id` varchar(45) DEFAULT NULL,
  `person_name` varchar(80) DEFAULT NULL,
  `person_addr` int DEFAULT NULL,
  `organisation_id` varchar(45) DEFAULT NULL,
  `organisation_name` varchar(80) DEFAULT NULL,
  `organisation_addr` int DEFAULT NULL,
  `location_id` varchar(45) DEFAULT NULL,
  `location_name` varchar(80) DEFAULT NULL,
  `location_addr` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_encompassingEnc_code_idx` (`code`),
  KEY `fk_person_addr_idx` (`person_addr`),
  KEY `fk_EC-organisation_addr_idx` (`organisation_addr`),
  KEY `fk_EC-location_addr_idx` (`location_addr`),
  CONSTRAINT `fk_EC-location_addr` FOREIGN KEY (`location_addr`) REFERENCES `addr` (`id_addr`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_EC-organisation_addr` FOREIGN KEY (`organisation_addr`) REFERENCES `addr` (`id_addr`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_EC-person_addr` FOREIGN KEY (`person_addr`) REFERENCES `addr` (`id_addr`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_encompassingEnc_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- CDA-Element Participant (Header)
CREATE TABLE `participant` (
  `id_participant` int NOT NULL AUTO_INCREMENT,
  `typeCode` varchar(45) DEFAULT NULL,
  `templateId` varchar(45) DEFAULT NULL,
  `functionCode` varchar(45) DEFAULT NULL,
  `person_id` varchar(45) DEFAULT NULL,
  `person_name` varchar(45) DEFAULT NULL,
  `organisation_id` varchar(45) DEFAULT NULL,
  `organisation_name` varchar(45) DEFAULT NULL,
  `organisation_addr` int DEFAULT NULL,
  PRIMARY KEY (`id_participant`),
  KEY `fk_participant_templateID_idx` (`templateId`),
  KEY `fk_participant_code_idx` (`functionCode`),
  KEY `fk_participant-org_addr_idx` (`organisation_addr`),
  CONSTRAINT `fk_participant-org_addr` FOREIGN KEY (`organisation_addr`) REFERENCES `addr` (`id_addr`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_participant_code` FOREIGN KEY (`functionCode`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_participant_templateID` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- CDA-Element Author (Header)
CREATE TABLE `author` (
  `author_id` int NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `functionCode` varchar(45) DEFAULT NULL,
  `ass_author_id` varchar(45) DEFAULT NULL,
  `ass_author_code` varchar(45) DEFAULT NULL,
  `ass_author_name` varchar(45) DEFAULT NULL,
  `auth_device_code` varchar(45) DEFAULT NULL,
  `auth_device_manufacturerModelName` varchar(45) DEFAULT NULL,
  `auth_device_softwareName` varchar(45) DEFAULT NULL,
  `rep_organisation_id` varchar(45) DEFAULT NULL,
  `rep_organisation_name` varchar(45) DEFAULT NULL,
  `rep_organisation_addr` int DEFAULT NULL,
  PRIMARY KEY (`author_id`),
  KEY `fk_author_aa_code_idx` (`ass_author_code`),
  KEY `fk_author_functionCode_idx` (`functionCode`),
  KEY `fk_author_ad_code_idx` (`auth_device_code`),
  KEY `fk_author_addr_idx` (`rep_organisation_addr`),
  CONSTRAINT `fk_author_aa_code` FOREIGN KEY (`ass_author_code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_author_ad_code` FOREIGN KEY (`auth_device_code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_author_addr` FOREIGN KEY (`rep_organisation_addr`) REFERENCES `addr` (`id_addr`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_author_functionCode` FOREIGN KEY (`functionCode`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Brieftext Sektion nicht codiert
CREATE TABLE `section_Brieftext` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_Brieftext_template_idx` (`templateId`),
  KEY `fk_Brieftext_code_idx` (`code`),
  CONSTRAINT `fk_Brieftext_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Brieftext_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=149 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `section_Befund` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_Befund_template_idx` (`templateId`),
  KEY `fk_Befund_code_idx` (`code`),
  CONSTRAINT `fk_Befund_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Befund_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=149 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `observation_befund` (
  `id` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `effectiveTime` datetime DEFAULT NULL,
  `interpretationCode` varchar(45) DEFAULT NULL,
  `value` varchar(45) DEFAULT NULL,
  `unit` varchar(45) DEFAULT NULL,
  `range_low` varchar(45) DEFAULT NULL,
  `range_high` varchar(45) DEFAULT NULL,
  `section_ID` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_code_idx` (`code`),
  KEY `fk_observation_templateID_idx` (`templateId`),
  KEY `fk_interpretationCode_code_idx` (`interpretationCode`),
  KEY `fk_observation_section_ID_idx` (`section_ID`),
  CONSTRAINT `fk_code_observation` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_interpretationCode_code` FOREIGN KEY (`interpretationCode`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_observation_section_ID` FOREIGN KEY (`section_ID`) REFERENCES `section_Befund` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_observation_templateID` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1121 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `section_AktuelleUntersuchung` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_AktuelleUntersuchung_template_idx` (`templateId`),
  KEY `fk_AktuelleUntersuchung_code_idx` (`code`),
  CONSTRAINT `fk_AktuelleUntersuchung_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_AktuelleUntersuchung_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=149 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `observation_aktuelleUntersuchung` (
  `id` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `effectiveTime` datetime DEFAULT NULL,
  `interpretationCode` varchar(45) DEFAULT NULL,
  `value` varchar(45) DEFAULT NULL,
  `unit` varchar(45) DEFAULT NULL,
  `range_low` varchar(45) DEFAULT NULL,
  `range_high` varchar(45) DEFAULT NULL,
  `section_ID` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_code_idx` (`code`),
  KEY `fk_observation_templateID_idx` (`templateId`),
  KEY `fk_interpretationCode_code_idx` (`interpretationCode`),
  KEY `fk_observation_section_ID_idx` (`section_ID`),
  CONSTRAINT `fk_code_observationAU` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_interpretationCodeAU_code` FOREIGN KEY (`interpretationCode`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_observationAU_section_ID` FOREIGN KEY (`section_ID`) REFERENCES `section_AktuelleUntersuchung` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_observationAU_templateID` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1121 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- DICOM Object Catalog Section
CREATE TABLE `section_DICOMObjectCatalog` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) NOT NULL,
  `code` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_DICOM_templateId_idx` (`templateId`),
  KEY `fk_DICOMsec_code_idx` (`code`),
  CONSTRAINT `fk_DICOMsec_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_DICOMsec_templateId` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- DICOM Studie
CREATE TABLE `act_study` (
  `id_act` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `id` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `effectiveTime` datetime DEFAULT NULL,
  `section_id` int DEFAULT NULL,
  PRIMARY KEY (`id_act`),
  KEY `fk_actstudy_template_idx` (`templateId`),
  KEY `fk_actstudy_code_idx` (`code`),
  KEY `fk_actstudy_section_idx` (`section_id`),
  CONSTRAINT `fk_actstudy_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_actstudy_section` FOREIGN KEY (`section_id`) REFERENCES `section_DICOMObjectCatalog` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_actstudy_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- DICOM Serie
CREATE TABLE `act_series` (
  `id_act` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `id` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `qualifierName` varchar(45) DEFAULT NULL,
  `qualifierValue` varchar(45) DEFAULT NULL,
  `study_id` int DEFAULT NULL,
  PRIMARY KEY (`id_act`),
  KEY `fk_actseries_template_idx` (`templateId`),
  KEY `fk_actseries_code_idx` (`code`),
  KEY `fk_actseries_study_idx` (`study_id`),
  CONSTRAINT `fk_actseries_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_actseries_study` FOREIGN KEY (`study_id`) REFERENCES `act_study` (`id_act`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_actseries_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- DICOM SOP Instance
CREATE TABLE `observation_SOP` (
  `id_observation` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `id` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `effectiveTime` datetime DEFAULT NULL,
  `series_id` int DEFAULT NULL,
  PRIMARY KEY (`id_observation`),
  KEY `fk_observationSOP_template_idx` (`templateId`),
  KEY `fk_observationSOP_code_idx` (`code`),
  KEY `fk_observationSOP_series_idx` (`series_id`),
  CONSTRAINT `fk_observationSOP_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_observationSOP_series` FOREIGN KEY (`series_id`) REFERENCES `act_series` (`id_act`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_observationSOP_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- CDA-Dokument Bildgebende Diagnostik
CREATE TABLE `ClinicalDocument_CDA-BildgebendeDiagnostik` (
  `id` varchar(45) NOT NULL,
  `realmCode` varchar(45) DEFAULT NULL,
  `effectiveTime` datetime DEFAULT NULL,
  `typeId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `confidentialityCode` varchar(45) DEFAULT NULL,
  `languageCode` varchar(45) DEFAULT NULL,
  `recordTarget` varchar(45) DEFAULT NULL,
  `encompassingEncounter` varchar(45) DEFAULT NULL,
  `section_Brieftext` int DEFAULT NULL,
  `section_DICOMObjectCatalog` int DEFAULT NULL,
  `section_AktuelleUntersuchungen` int DEFAULT NULL,
  `section_Befund` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_CDA_code_idx` (`code`),
  KEY `fk_CDA_patient_idx` (`recordTarget`),
  KEY `fk_CDA_brieftext_idx` (`section_Brieftext`),
  KEY `fk_CDA_befund_idx` (`section_Befund`),
  KEY `fk_CDA_aktuelleUntersuchung_idx` (`section_AktuelleUntersuchungen`),
  KEY `fk_CDA_DICOM_idx` (`section_DICOMObjectCatalog`),
  KEY `fk_CDA_encompassing_idx` (`encompassingEncounter`),
  CONSTRAINT `fk_CDA_aktuelleUntersuchung` FOREIGN KEY (`section_AktuelleUntersuchungen`) REFERENCES `section_AktuelleUntersuchung` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CDA_befund` FOREIGN KEY (`section_Befund`) REFERENCES `section_Befund` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CDA_brieftext` FOREIGN KEY (`section_Brieftext`) REFERENCES `section_Brieftext` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CDA_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CDA_DICOM` FOREIGN KEY (`section_DICOMObjectCatalog`) REFERENCES `section_DICOMObjectCatalog` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CDA_encompassing` FOREIGN KEY (`encompassingEncounter`) REFERENCES `encompassingEncounter` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CDA_patient` FOREIGN KEY (`recordTarget`) REFERENCES `patientRole` (`id_SVNR`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;



-- Dokument hat author
CREATE TABLE `ClinicalDocument_has_author` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ClinicalDocument_ID` varchar(45) DEFAULT NULL,
  `author` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_clinicaldocument_author_idx` (`ClinicalDocument_ID`),
  KEY `fk_author_clinicaldocument_idx` (`author`),
  CONSTRAINT `fk_author_clinicaldocument` FOREIGN KEY (`author`) REFERENCES `author` (`author_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_clinicaldocument_author` FOREIGN KEY (`ClinicalDocument_ID`) REFERENCES `ClinicalDocument_CDA-BildgebendeDiagnostik` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dokument hat participant

CREATE TABLE `ClinicalDocument_has_participant` (
  `idClinicalDocument_has_participant` int NOT NULL AUTO_INCREMENT,
  `ClinicalDocument_ID` varchar(45) DEFAULT NULL,
  `Participant_ID` int DEFAULT NULL,
  PRIMARY KEY (`idClinicalDocument_has_participant`),
  KEY `fk_CHP_CDA_idx` (`ClinicalDocument_ID`),
  KEY `fk_CHP_Part_idx` (`Participant_ID`),
  CONSTRAINT `fk_CHP_CDA` FOREIGN KEY (`ClinicalDocument_ID`) REFERENCES `ClinicalDocument_CDA-BildgebendeDiagnostik` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CHP_Part` FOREIGN KEY (`Participant_ID`) REFERENCES `participant` (`id_participant`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dokument hat templateID
CREATE TABLE `ClinicalDocument_has_template` (
  `idClinicalDocument_has_template` int NOT NULL AUTO_INCREMENT,
  `documentID` varchar(45) DEFAULT NULL,
  `templateID` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idClinicalDocument_has_template`),
  KEY `fk_document_idx` (`documentID`),
  KEY `fk_template_idx` (`templateID`),
  CONSTRAINT `fk_document` FOREIGN KEY (`documentID`) REFERENCES `ClinicalDocument_CDA-BildgebendeDiagnostik` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_template` FOREIGN KEY (`templateID`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- serviceEvent
CREATE TABLE `serviceEvent` (
  `serviceEvent_ID` int NOT NULL AUTO_INCREMENT,
  `code` varchar(45) DEFAULT NULL,
  `effectiveTime_low` datetime DEFAULT NULL,
  `effectiveTime_high` datetime DEFAULT NULL,
  `templateId` varchar(45) DEFAULT NULL,
  `person_name` varchar(45) DEFAULT NULL,
  `person_addr` int DEFAULT NULL,
  `organisation_id` varchar(45) DEFAULT NULL,
  `organisation_name` varchar(45) DEFAULT NULL,
  `organisation_addr` int DEFAULT NULL,
  `CDA_ID` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`serviceEvent_ID`),
  KEY `fk_code_service_event_idx` (`code`),
  KEY `fK_service_event_templateId_idx` (`templateId`),
  KEY `fk_serviceEvent-Organisation_addr_idx` (`organisation_addr`),
  KEY `fk_serviceEvent-Person_addr_idx` (`person_addr`),
  KEY `fk_serviceEvent_CDA_idx` (`CDA_ID`),
  CONSTRAINT `fk_code_service_event` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fK_service_event_templateId` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_serviceEvent-Organisation_addr` FOREIGN KEY (`organisation_addr`) REFERENCES `addr` (`id_addr`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_serviceEvent-Person_addr` FOREIGN KEY (`person_addr`) REFERENCES `addr` (`id_addr`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_serviceEvent_CDA` FOREIGN KEY (`CDA_ID`) REFERENCES `ClinicalDocument_CDA-BildgebendeDiagnostik` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
