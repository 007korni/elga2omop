CREATE DATABASE `CDA_Medikationsliste` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

-- Sammlung aller Codes, die in einem CDA-File vorkommen
CREATE TABLE `code` (
  `code` varchar(45) NOT NULL,
  `codeSystem` varchar(45) NOT NULL,
  `displayName` varchar(45) DEFAULT NULL,
  `codeSystemName` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`code`,`codeSystem`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Sammlung aller Adressen, die in einem CDA-File vorkommen
CREATE TABLE `addr` (
  `id_addr` int NOT NULL AUTO_INCREMENT,
  `streetAddressLine` varchar(45) NOT NULL,
  `postalCode` int NOT NULL,
  `city` varchar(45) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `additionalLocator` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`streetAddressLine`,`postalCode`),
  UNIQUE KEY `id_addr_UNIQUE` (`id_addr`)
) ENGINE=InnoDB AUTO_INCREMENT=4272 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Sammlung aller templateIds, die in einem CDA-File vorkommen
CREATE TABLE `templateId` (
  `root` varchar(45) NOT NULL,
  `assigningAuthorityName` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`root`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Daten des extrahierten Patienten/Patientin
CREATE TABLE `patientRole` (
  `id_SVNR` varchar(45) NOT NULL,
  `id_local` varchar(45) DEFAULT NULL,
  `id_BPKGH` varchar(45) DEFAULT NULL,
  `addr` int DEFAULT NULL,
  `given` varchar(45) DEFAULT NULL,
  `family` varchar(45) DEFAULT NULL,
  `administrativeGenderCode` varchar(45) DEFAULT NULL,
  `birthTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id_SVNR`),
  UNIQUE KEY `id_SVNR_UNIQUE` (`id_SVNR`),
  UNIQUE KEY `id_local_UNIQUE` (`id_local`),
  UNIQUE KEY `id_BPKGH_UNIQUE` (`id_BPKGH`),
  KEY `fk_home_addr_idx` (`addr`),
  KEY `fk_gender_code_idx` (`administrativeGenderCode`),
  CONSTRAINT `fk_gender_code` FOREIGN KEY (`administrativeGenderCode`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_home_addr` FOREIGN KEY (`addr`) REFERENCES `addr` (`id_addr`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Section "History of Medication Use = Medikationsliste"
CREATE TABLE `section_HistoryOfMedicationUse` (
  `section_id` int NOT NULL AUTO_INCREMENT,
  `templateID` varchar(45) DEFAULT NULL,
  `id` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `text` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`section_id`),
  KEY `fk_HOMU_code_idx` (`code`),
  CONSTRAINT `fk_HOMU_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- CDA-Element Produkt aus dem Supply-Element
CREATE TABLE `product` (
  `id_product` int NOT NULL AUTO_INCREMENT,
  `manufacturedProduct_templateId` varchar(45) DEFAULT NULL,
  `manufacturedMaterial_templateId` varchar(45) DEFAULT NULL,
  `manufacturedMaterial_code` varchar(45) DEFAULT NULL,
  `manufacturedMaterial_name` varchar(45) DEFAULT NULL,
  `manufacturedMaterial_pharm_formCode` varchar(45) DEFAULT NULL,
  `manufacturedMaterial_pharm_capacityQuantity` varchar(45) DEFAULT NULL,
  `manufacturedMaterial_pharm_ingredient_code` varchar(45) DEFAULT NULL,
  `manufacturedMaterial_pharm_ingredient_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_product`),
  KEY `fk_su_prod_template_idx` (`manufacturedProduct_templateId`),
  KEY `fk_su_prod_template2_idx` (`manufacturedMaterial_templateId`),
  KEY `fk_su_prod_code_idx` (`manufacturedMaterial_code`),
  KEY `fk_su_prod_code_idx1` (`manufacturedMaterial_pharm_formCode`),
  KEY `fk_su_prod_code3_idx` (`manufacturedMaterial_pharm_ingredient_code`),
  CONSTRAINT `fk_su_prod_code` FOREIGN KEY (`manufacturedMaterial_code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_su_prod_code2` FOREIGN KEY (`manufacturedMaterial_pharm_formCode`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_su_prod_code3` FOREIGN KEY (`manufacturedMaterial_pharm_ingredient_code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_su_prod_template` FOREIGN KEY (`manufacturedProduct_templateId`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_su_prod_template2` FOREIGN KEY (`manufacturedMaterial_templateId`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- CDA-Element Supply 
CREATE TABLE `supply` (
  `id_supply` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `quantity` varchar(45) DEFAULT NULL,
  `product_id` int DEFAULT NULL,
  `section_id` int DEFAULT NULL,
  PRIMARY KEY (`id_supply`),
  KEY `fk_supply_section_idx` (`section_id`),
  KEY `fk_supply_product_idx` (`product_id`),
  CONSTRAINT `fk_supply_product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id_product`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_supply_section` FOREIGN KEY (`section_id`) REFERENCES `section_HistoryOfMedicationUse` (`section_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- CDA-element substanceAdministration
CREATE TABLE `substanceAdministration` (
  `sA_id` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `id` varchar(45) DEFAULT NULL,
  `id2` varchar(45) DEFAULT NULL,
  `statusCode` varchar(45) DEFAULT NULL,
  `effectiveTime_low` datetime DEFAULT NULL,
  `effectiveTime_high` datetime DEFAULT NULL,
  `effectiveTime_period_value` varchar(45) DEFAULT NULL,
  `effectiveTime_period_unit` varchar(45) DEFAULT NULL,
  `effectiveTime_width_value` varchar(45) DEFAULT NULL,
  `effectiveTime_width_unit` varchar(45) DEFAULT NULL,
  `effectiveTime_event_code` varchar(45) DEFAULT NULL,
  `effectiveTime_offset_value` varchar(45) DEFAULT NULL,
  `effectiveTime_offset_unit` varchar(45) DEFAULT NULL,
  `dosing` varchar(45) DEFAULT NULL,
  `repeatNumber` int DEFAULT NULL,
  `routeCode` varchar(45) DEFAULT NULL,
  `doseQuantity` varchar(45) DEFAULT NULL,
  `eR_supply_independentInd` varchar(45) DEFAULT NULL,
  `eR_supply_quantity` varchar(45) DEFAULT NULL,
  `eR_act_templateID` varchar(45) DEFAULT NULL,
  `eR_act_code` varchar(45) DEFAULT NULL,
  `section_id` int DEFAULT NULL,
  `sA_dosing` int DEFAULT NULL,
  `supply_id` int DEFAULT NULL,
  PRIMARY KEY (`sA_id`),
  KEY `fk_sA_template_idx` (`templateId`),
  KEY `fk_sA_route_idx` (`routeCode`),
  KEY `fk_sA_act_code_idx` (`eR_act_code`),
  KEY `fk_sA_section_idx` (`section_id`),
  KEY `fk_sA_sA_idx` (`sA_dosing`),
  KEY `fk_sA_supply_idx` (`supply_id`),
  CONSTRAINT `fk_sA_act_code` FOREIGN KEY (`eR_act_code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_sA_route` FOREIGN KEY (`routeCode`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_sA_sA` FOREIGN KEY (`sA_dosing`) REFERENCES `substanceAdministration` (`sA_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_sA_section` FOREIGN KEY (`section_id`) REFERENCES `section_HistoryOfMedicationUse` (`section_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_sA_supply` FOREIGN KEY (`supply_id`) REFERENCES `supply` (`id_supply`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_sA_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=125 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `consumable` (
  `id_consumable` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `manufacturedMaterial_templateID` varchar(45) DEFAULT NULL,
  `manufacturedMaterial_code` varchar(45) DEFAULT NULL,
  `manufacturedMaterial_name` varchar(45) DEFAULT NULL,
  `formCode` varchar(45) DEFAULT NULL,
  `capacityQuantity` varchar(45) DEFAULT NULL,
  `capacityUnit` varchar(45) DEFAULT NULL,
  `pharmCode` varchar(45) DEFAULT NULL,
  `pharmName` varchar(45) DEFAULT NULL,
  `sA_id` int DEFAULT NULL,
  PRIMARY KEY (`id_consumable`),
  KEY `fk_c_code_idx` (`manufacturedMaterial_code`),
  KEY `fk_sA_idx` (`sA_id`),
  KEY `fk_pharm2_code_idx` (`pharmCode`),
  KEY `fk_formcode2_code_idx` (`formCode`),
  CONSTRAINT `fk_c_code` FOREIGN KEY (`manufacturedMaterial_code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_formcode2_code` FOREIGN KEY (`formCode`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_pharm2_code` FOREIGN KEY (`pharmCode`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_sA` FOREIGN KEY (`sA_id`) REFERENCES `substanceAdministration` (`sA_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


-- CDA-Dokument Medikationsliste
CREATE TABLE `ClinicalDocument_CDA-Medikationsliste` (
  `id` VARCHAR(45) NOT NULL,
  `realmCode` VARCHAR(45) NULL,
  `effectiveTime` DATETIME NULL,
  `typeId` VARCHAR(45) NULL,
  `code` VARCHAR(45) NULL,
  `title` VARCHAR(100) NULL,
  `confidentialityCode` VARCHAR(45) NULL,
  `languageCode` VARCHAR(45) NULL,
  `recordTarget` VARCHAR(45) NULL,
  `section_HistoryOfMedicationUse` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_CDA_patient_idx` (`recordTarget` ASC) VISIBLE,
  INDEX `fk_CDA_code_idx` (`code` ASC) VISIBLE,
  INDEX `fk_CDA_sectionHOMU_idx` (`section_HistoryOfMedicationUse` ASC) VISIBLE,
  CONSTRAINT `fk_CDA_patient`
    FOREIGN KEY (`recordTarget`)
    REFERENCES `CDA_Medikationsliste`.`patientRole` (`id_SVNR`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_CDA_code`
    FOREIGN KEY (`code`)
    REFERENCES `CDA_Medikationsliste`.`code` (`code`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_CDA_sectionHOMU`
    FOREIGN KEY (`section_HistoryOfMedicationUse`)
    REFERENCES `CDA_Medikationsliste`.`section_HistoryOfMedicationUse` (`section_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);

-- Verlinkung Dokument und templateIDs
CREATE TABLE `ClinicalDocument_has_template` (
  `idClinicalDocument_has_template` int NOT NULL AUTO_INCREMENT,
  `documentID` varchar(45) DEFAULT NULL,
  `templateID` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idClinicalDocument_has_template`),
  KEY `fk_document_idx` (`documentID`),
  KEY `fk_template_idx` (`templateID`),
  CONSTRAINT `fk_document` FOREIGN KEY (`documentID`) REFERENCES `ClinicalDocument_CDA-Medikationsliste` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_template` FOREIGN KEY (`templateID`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


-- CDA-Element author
CREATE TABLE `author` (
  `author_id` INT NOT NULL AUTO_INCREMENT,
  `time` DATETIME NULL,
  `functionCode` VARCHAR(45) NULL,
  `ass_author_id` VARCHAR(45) NULL,
  `ass_author_code` VARCHAR(45) NULL,
  `ass_author_name` VARCHAR(45) NULL,
  `auth_device_code` VARCHAR(45) NULL,
  `auth_device_manufacturerModelName` VARCHAR(45) NULL,
  `auth_device_softwareName` VARCHAR(45) NULL,
  `rep_organisation_id` VARCHAR(45) NULL,
  `rep_organisation_name` VARCHAR(45) NULL,
  `rep_organisation_addr` INT NULL,
  PRIMARY KEY (`author_id`),
  INDEX `fk_author_aa_code_idx` (`ass_author_code` ASC) VISIBLE,
  INDEX `fk_author_functionCode_idx` (`functionCode` ASC) VISIBLE,
  INDEX `fk_author_ad_code_idx` (`auth_device_code` ASC) VISIBLE,
  INDEX `fk_author_addr_idx` (`rep_organisation_addr` ASC) VISIBLE,
  CONSTRAINT `fk_author_aa_code`
    FOREIGN KEY (`ass_author_code`)
    REFERENCES `CDA_Medikationsliste`.`code` (`code`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_author_functionCode`
    FOREIGN KEY (`functionCode`)
    REFERENCES `CDA_Medikationsliste`.`code` (`code`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_author_ad_code`
    FOREIGN KEY (`auth_device_code`)
    REFERENCES `CDA_Medikationsliste`.`code` (`code`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_author_addr`
    FOREIGN KEY (`rep_organisation_addr`)
    REFERENCES `CDA_Medikationsliste`.`addr` (`id_addr`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);
    
    
    -- Verlinkung Dokument mit Autoren
    CREATE TABLE `ClinicalDocument_has_author` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ClinicalDocument` varchar(45) DEFAULT NULL,
  `author` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_clinicaldocument_author_idx` (`ClinicalDocument`),
  KEY `fk_author_clinicaldocument_idx` (`author`),
  CONSTRAINT `fk_author_clinicaldocument` FOREIGN KEY (`author`) REFERENCES `author` (`author_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_clinicaldocument_author` FOREIGN KEY (`ClinicalDocument`) REFERENCES `ClinicalDocument_CDA-Medikationsliste` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


