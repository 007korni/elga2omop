CREATE DATABASE `CDA_Laborbefunde` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE CDA_Laborbefunde;
CREATE TABLE `addr` (
  `id_addr` int NOT NULL AUTO_INCREMENT,
  `streetAddressLine` varchar(45) NOT NULL,
  `postalCode` int NOT NULL,
  `city` varchar(45) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `additionalLocator` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`streetAddressLine`,`postalCode`),
  UNIQUE KEY `id_addr_UNIQUE` (`id_addr`)
) ENGINE=InnoDB AUTO_INCREMENT=3133 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `code` (
  `code` varchar(45) NOT NULL,
  `codeSystem` varchar(45) NOT NULL,
  `displayName` varchar(45) DEFAULT NULL,
  `codeSystemName` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`code`,`codeSystem`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `templateId` (
  `root` varchar(45) NOT NULL,
  `assigningAuthorityName` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`root`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `section_Brieftext` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_Brieftext_template_idx` (`templateId`),
  KEY `fk_Brieftext_code_idx` (`code`),
  CONSTRAINT `fk_Brieftext_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Brieftext_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `section_Ueberweisungsgrund` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_Ueberweisung_templateId_idx` (`templateId`),
  KEY `fk_Ueberweisung_code_idx` (`code`),
  CONSTRAINT `fk_Ueberweisung_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Ueberweisung_templateId` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `participant` (
  `id_participant` int NOT NULL AUTO_INCREMENT,
  `typeCode` varchar(45) DEFAULT NULL,
  `templateId` varchar(45) DEFAULT NULL,
  `functionCode` varchar(45) DEFAULT NULL,
  `person_id` varchar(45) DEFAULT NULL,
  `person_name` varchar(45) DEFAULT NULL,
  `organisation_id` varchar(45) DEFAULT NULL,
  `organisation_name` varchar(45) DEFAULT NULL,
  `organisation_addr` int DEFAULT NULL,
  PRIMARY KEY (`id_participant`),
  KEY `fk_participant_templateID_idx` (`templateId`),
  KEY `fk_participant_code_idx` (`functionCode`),
  KEY `fk_participant-org_addr_idx` (`organisation_addr`),
  CONSTRAINT `fk_participant-org_addr` FOREIGN KEY (`organisation_addr`) REFERENCES `addr` (`id_addr`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_participant_code` FOREIGN KEY (`functionCode`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_participant_templateID` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `patientRole` (
  `id_SVNR` varchar(45) NOT NULL,
  `id_local` varchar(45) DEFAULT NULL,
  `id_BPKGH` varchar(45) DEFAULT NULL,
  `addr` int DEFAULT NULL,
  `given` varchar(45) DEFAULT NULL,
  `family` varchar(45) DEFAULT NULL,
  `administrativeGenderCode` varchar(45) DEFAULT NULL,
  `birthTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id_SVNR`),
  UNIQUE KEY `id_SVNR_UNIQUE` (`id_SVNR`),
  UNIQUE KEY `id_local_UNIQUE` (`id_local`),
  UNIQUE KEY `id_BPKGH_UNIQUE` (`id_BPKGH`),
  KEY `fk_home_addr_idx` (`addr`),
  KEY `fk_gender_code_idx` (`administrativeGenderCode`),
  CONSTRAINT `fk_gender_code` FOREIGN KEY (`administrativeGenderCode`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_home_addr` FOREIGN KEY (`addr`) REFERENCES `addr` (`id_addr`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `performer` (
  `id_performer` int NOT NULL AUTO_INCREMENT,
  `assignedEntity_id` varchar(45) DEFAULT NULL,
  `assignedEntity_addr` int DEFAULT NULL,
  `assignedPerson_name` varchar(45) DEFAULT NULL,
  `representedOrganisation_id` varchar(45) DEFAULT NULL,
  `representedOrganisation_name` varchar(45) DEFAULT NULL,
  `representedOrganisation_addr` int DEFAULT NULL,
  PRIMARY KEY (`id_performer`),
  KEY `fk_assEnt_addr_idx` (`assignedEntity_addr`),
  KEY `fk_repOrg_addr_idx` (`representedOrganisation_addr`),
  CONSTRAINT `fk_assEnt_addr` FOREIGN KEY (`assignedEntity_addr`) REFERENCES `addr` (`id_addr`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_repOrg_addr` FOREIGN KEY (`representedOrganisation_addr`) REFERENCES `addr` (`id_addr`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `encompassingEncounter` (
  `id` varchar(45) NOT NULL,
  `code` varchar(45) DEFAULT NULL,
  `effectiveTime_low` datetime DEFAULT NULL,
  `effectiveTime_high` datetime DEFAULT NULL,
  `assignedEntity_id` varchar(45) DEFAULT NULL,
  `person_name` varchar(80) DEFAULT NULL,
  `person_addr` int DEFAULT NULL,
  `organisation_id` varchar(45) DEFAULT NULL,
  `organisation_name` varchar(80) DEFAULT NULL,
  `organisation_addr` int DEFAULT NULL,
  `location_id` varchar(45) DEFAULT NULL,
  `location_name` varchar(80) DEFAULT NULL,
  `location_addr` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_encompassingEnc_code_idx` (`code`),
  KEY `fk_person_addr_idx` (`person_addr`),
  KEY `fk_EC-organisation_addr_idx` (`organisation_addr`),
  KEY `fk_EC-location_addr_idx` (`location_addr`),
  CONSTRAINT `fk_EC-location_addr` FOREIGN KEY (`location_addr`) REFERENCES `addr` (`id_addr`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_EC-organisation_addr` FOREIGN KEY (`organisation_addr`) REFERENCES `addr` (`id_addr`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_EC-person_addr` FOREIGN KEY (`person_addr`) REFERENCES `addr` (`id_addr`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_encompassingEnc_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `section_Specimen` (
  `id` int NOT NULL AUTO_INCREMENT,
  `code` varchar(45) DEFAULT NULL,
  `statusCode` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_sec-Spec_code_idx` (`code`),
  CONSTRAINT `fk_sec-Spec_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ClinicalDocument_CDALaborbefund` (
  `id` varchar(45) NOT NULL,
  `realmCode` varchar(45) DEFAULT NULL,
  `effectiveTime` datetime DEFAULT NULL,
  `typeId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `confidentialityCode` varchar(45) DEFAULT NULL,
  `languageCode` varchar(45) DEFAULT NULL,
  `recordTarget` varchar(45) DEFAULT NULL,
  `encompassingEncounter` varchar(45) DEFAULT NULL,
  `sectionBrieftext_Id` int DEFAULT NULL,
  `sectionUeberweisungsgrund_Id` int DEFAULT NULL,
  `sectionSpecimen_Id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_CDA_patient_idx` (`recordTarget`),
  KEY `fk_code_idx` (`code`),
  KEY `fk_cdaLab_encompassingEnc_idx` (`encompassingEncounter`),
  KEY `fk_cdaLab_Brieftext_idx` (`sectionBrieftext_Id`),
  KEY `fk_cdaLab_Ueberweisung_idx` (`sectionUeberweisungsgrund_Id`),
  KEY `fk_cdaLab_Specimen_idx` (`sectionSpecimen_Id`),
  CONSTRAINT `fk_CDA_patient` FOREIGN KEY (`recordTarget`) REFERENCES `patientRole` (`id_SVNR`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_cdaLab_Brieftext` FOREIGN KEY (`sectionBrieftext_Id`) REFERENCES `section_Brieftext` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_cdaLab_encompassingEnc` FOREIGN KEY (`encompassingEncounter`) REFERENCES `encompassingEncounter` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_cdaLab_Specimen` FOREIGN KEY (`sectionSpecimen_Id`) REFERENCES `section_Specimen` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_cdaLab_Ueberweisung` FOREIGN KEY (`sectionUeberweisungsgrund_Id`) REFERENCES `section_Ueberweisungsgrund` (`id_section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ClinicalDocument_has_participant` (
  `idClinicalDocument_has_participant` int NOT NULL AUTO_INCREMENT,
  `ClinicalDocument_ID` varchar(45) DEFAULT NULL,
  `Participant_ID` int DEFAULT NULL,
  PRIMARY KEY (`idClinicalDocument_has_participant`),
  KEY `fk_CHP_CDALAb_idx` (`ClinicalDocument_ID`),
  KEY `fk_CHP_Part_idx` (`Participant_ID`),
  CONSTRAINT `fk_CHP_CDALAb` FOREIGN KEY (`ClinicalDocument_ID`) REFERENCES `ClinicalDocument_CDALaborbefund` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CHP_Part` FOREIGN KEY (`Participant_ID`) REFERENCES `participant` (`id_participant`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ClinicalDocument_has_template` (
  `idClinicalDocument_has_template` int NOT NULL AUTO_INCREMENT,
  `documentID` varchar(45) DEFAULT NULL,
  `templateID` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idClinicalDocument_has_template`),
  KEY `fk_template_idx` (`templateID`),
  KEY `fk_document_idx` (`documentID`),
  CONSTRAINT `fk_document` FOREIGN KEY (`documentID`) REFERENCES `ClinicalDocument_CDALaborbefund` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_template` FOREIGN KEY (`templateID`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=152 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `serviceEvent` (
  `serviceEvent_ID` int NOT NULL AUTO_INCREMENT,
  `code` varchar(45) DEFAULT NULL,
  `effectiveTime_low` datetime DEFAULT NULL,
  `effectiveTime_high` datetime DEFAULT NULL,
  `templateId` varchar(45) DEFAULT NULL,
  `person_name` varchar(45) DEFAULT NULL,
  `person_addr` int DEFAULT NULL,
  `organisation_id` varchar(45) DEFAULT NULL,
  `organisation_name` varchar(45) DEFAULT NULL,
  `organisation_addr` int DEFAULT NULL,
  `LaborbefundID` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`serviceEvent_ID`),
  KEY `fk_code_service_event_idx` (`code`),
  KEY `fK_service_event_templateId_idx` (`templateId`),
  KEY `fk_serviceEvent-Person_addr_idx` (`person_addr`),
  KEY `fk_serviceEvent-Organisation_addr_idx` (`organisation_addr`),
  KEY `fk_serviceEvent_Laborbefund_idx` (`LaborbefundID`),
  CONSTRAINT `fk_code_service_event` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fK_service_event_templateId` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`),
  CONSTRAINT `fk_serviceEvent-Organisation_addr` FOREIGN KEY (`organisation_addr`) REFERENCES `addr` (`id_addr`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_serviceEvent-Person_addr` FOREIGN KEY (`person_addr`) REFERENCES `addr` (`id_addr`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_serviceEvent_Laborbefund` FOREIGN KEY (`LaborbefundID`) REFERENCES `ClinicalDocument_CDALaborbefund` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=138 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `section_speciality` (
  `id` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `Laborbefund_Id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_speciality_code_idx` (`code`),
  KEY `fk_speciality_templateId_idx` (`templateId`),
  KEY `fk_speciality_CDALab_idx` (`Laborbefund_Id`),
  CONSTRAINT `fk_speciality_CDALab` FOREIGN KEY (`Laborbefund_Id`) REFERENCES `ClinicalDocument_CDALaborbefund` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_speciality_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_speciality_templateId` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `entryRelationship` (
  `id_entryRelationship` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(50) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `speciality_section_id` int DEFAULT NULL,
  PRIMARY KEY (`id_entryRelationship`),
  KEY `fk_entryRel_template_idx` (`templateId`),
  KEY `fk_entryRel_code_idx` (`code`),
  KEY `fk_entryRel_speciality_idx` (`speciality_section_id`),
  CONSTRAINT `fk_entryRel_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_entryRel_speciality` FOREIGN KEY (`speciality_section_id`) REFERENCES `section_speciality` (`id`),
  CONSTRAINT `fk_entryRel_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=203 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `observation` (
  `id` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `effectiveTime` datetime DEFAULT NULL,
  `interpretationCode` varchar(45) DEFAULT NULL,
  `value` varchar(45) DEFAULT NULL,
  `unit` varchar(45) DEFAULT NULL,
  `range_low` varchar(45) DEFAULT NULL,
  `range_high` varchar(45) DEFAULT NULL,
  `entryRelationship_ID` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_code_idx` (`code`),
  KEY `fk_observation_templateID_idx` (`templateId`),
  KEY `fk_interpretationCode_code_idx` (`interpretationCode`),
  KEY `fk_observation_entryRel_idx` (`entryRelationship_ID`),
  CONSTRAINT `fk_code_observation` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_interpretationCode_code` FOREIGN KEY (`interpretationCode`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_observation_entryRel` FOREIGN KEY (`entryRelationship_ID`) REFERENCES `entryRelationship` (`id_entryRelationship`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_observation_templateID` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1121 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `lab_procedure` (
  `id` int NOT NULL AUTO_INCREMENT,
  `code` varchar(45) DEFAULT NULL,
  `effectiveTime` datetime DEFAULT NULL,
  `targetSiteCode` varchar(45) DEFAULT NULL,
  `performer` int DEFAULT NULL,
  `section_specimen` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_proc_code_idx` (`code`),
  KEY `fk_targetSite_code_idx` (`targetSiteCode`),
  KEY `fk_proc_performer_idx` (`performer`),
  KEY `fk_proc_sec-Spec_idx` (`section_specimen`),
  CONSTRAINT `fk_proc_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_proc_performer` FOREIGN KEY (`performer`) REFERENCES `performer` (`id_performer`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_proc_sec-Spec` FOREIGN KEY (`section_specimen`) REFERENCES `section_Specimen` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_targetSite_code` FOREIGN KEY (`targetSiteCode`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `participant_specimen` (
  `id` varchar(50) NOT NULL,
  `code` varchar(45) DEFAULT NULL,
  `procedure_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_part-spec_code_idx` (`code`),
  KEY `fk_proc-spec_proc_idx` (`procedure_id`),
  CONSTRAINT `fk_part-spec_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_proc-spec_proc` FOREIGN KEY (`procedure_id`) REFERENCES `lab_procedure` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;