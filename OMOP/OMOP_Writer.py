import psycopg2


class OMOPWriter:

    def __init__(self):
        self.OMOPdatabase = self.connectToOMOP()

    # Connection to OMOP database
    def connectToOMOP(self):
        con = psycopg2.connect(
            host="localhost",  # enter your database ip here
            database="postgres",  # enter your database name here
            user="postgres",  # enter your username here
            password="password")  # enter your database password here
        return con

    def getNextIdForTable(self, table_id, table):
        con = self.connectToOMOP()
        cur = con.cursor()
        sql = f"SELECT MAX({table_id} ) FROM {table}"
        cur.execute(sql)
        current_id = cur.fetchone()[0]
        try:
            next_id = current_id + 1
        except:
            next_id = 1
        con.commit()
        cur.close()
        con.close()
        return next_id

    def getGenderConceptID(self, gender_source_value):
        gender_concept_id = 0

        if gender_source_value == "F":
            gender_concept_id = 8532
        elif gender_source_value == "M":
            gender_concept_id = 8507
        elif gender_source_value == "UN":
            gender_concept_id = 8551

        return gender_concept_id

    def getLocation_id(self, address1, zip):
        con = self.connectToOMOP()
        cur = con.cursor()
        sql = f"SELECT location_id FROM location WHERE address_1 = '{address1}' AND zip = '{zip}'"
        cur.execute(sql)
        try:
            location_id = cur.fetchone()[0]
        except:
            location_id = None
        con.commit()
        cur.close()
        con.close()
        return location_id

    def getMeasurement_concept_id(self, code_value):
        con = self.connectToOMOP()
        cur = con.cursor()
        sql = "SELECT concept_id FROM concept WHERE concept_code = %s AND standard_concept = 'S' AND domain_id = 'Measurement';"
        cur.execute(sql, (code_value,))
        res = cur.fetchall()
        con.commit()
        cur.close()
        con.close()
        try:
            concept_id = res[0][0]
        except:
            concept_id = 0
        return concept_id

    def getUnit_concept_id(self, unit_value):
        con = self.connectToOMOP()
        cur = con.cursor()
        sql = "SELECT concept_id FROM concept WHERE concept_class_id = 'Unit' AND concept_code = %s AND standard_concept = 'S';"
        cur.execute(sql, (unit_value,))
        res = cur.fetchall()
        con.commit()
        cur.close()
        con.close()
        try:
            concept_id = res[0][0]
        except:
            concept_id = 0
        return concept_id

    def getConceptId_from_source_to_concept(self, code):
        con = self.connectToOMOP()
        cur = con.cursor()
        sql = f"SELECT target_concept_id FROM source_to_concept_map WHERE source_code = '{code}' ;"
        cur.execute(sql,)
        res = cur.fetchone()
        con.commit()
        cur.close()
        con.close()
        try:
            concept_id = res[0]
        except:
            concept_id = 0
        return concept_id

    def writePersonToDB(self, person, location_id, provider_id, care_site_id):
        con = self.connectToOMOP()
        cur = con.cursor()
        sql = "INSERT INTO person (" \
              "person_id, " \
              "gender_concept_id, " \
              "year_of_birth, " \
              "month_of_birth, " \
              "day_of_birth, " \
              "birth_datetime, " \
              "race_concept_id, " \
              "ethnicity_concept_id, " \
              "location_id, " \
              "provider_id, " \
              "care_site_id, " \
              "person_source_value, " \
              "gender_source_value, " \
              "gender_source_concept_id, " \
              "race_source_value, " \
              "race_source_concept_id, " \
              "ethnicity_source_value, " \
              "ethnicity_source_concept_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        # Prepare data for insert
        person_id = self.getNextIdForTable("person_id", "person")
        gender_concept_id = self.getGenderConceptID(person[6])
        year_of_birth = person[7].year
        month_of_birth = person[7].month
        day_of_birth = person[7].day
        birth_datetime = person[7]  # patientRole.birthTime
        location_id = location_id
        provider_id = provider_id
        care_site_id = care_site_id
        person_source_value = person[0]  # patientRole.SVNR
        gender_source_value = person[6]
        gender_source_concept_id = 0  # possible through source_to_concept_map
        race_concept_id = 0  # not used in ELGA context
        ethnicity_concept_id = 0  # not used in ELGA context
        race_source_concept_id = 0  # not used in ELGA context
        ethnicity_source_concept_id = 0  # not used in ELGA context
        race_source_value = None  # not used in ELGA context
        ethnicity_source_value = None  # not used in ELGA context

        val = (person_id,
               gender_concept_id,
               year_of_birth,
               month_of_birth,
               day_of_birth,
               birth_datetime,
               race_concept_id,
               ethnicity_concept_id,
               location_id,
               provider_id,
               care_site_id,
               person_source_value,
               gender_source_value,
               gender_source_concept_id,
               race_source_value,
               race_source_concept_id,
               ethnicity_source_value,
               ethnicity_source_concept_id)
        cur.execute(sql, val)
        con.commit()
        cur.close()
        con.close()
        return person_id

    def writeLocationToDB(self, location):
        con = self.connectToOMOP()
        cur = con.cursor()
        sql = "INSERT INTO location (" \
              "location_id, " \
              "address_1, " \
              "address_2, " \
              "city, " \
              "state, " \
              "zip, " \
              "county, " \
              "location_source_value, " \
              "country_concept_id, " \
              "country_source_value, " \
              "latitude, " \
              "longitude) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        # Prepare data for insert
        location_id = self.getNextIdForTable("location_id", "location")
        address_1 = location[1]  # CDA-streetAddressLine
        address_2 = location[6]  # CDA-additionalLocator
        city = location[3]  # CDA-city
        state = None  # Only varchar(2) -> does not make sense for austria
        zip = location[2]  # CDA-postalCode
        county = location[4]  # not used in CDA
        location_source_value = location[1] + " " + str(location[2]) + " " + location[3] + " " + location[5]
        if location[5] == "AUT":
            country_concept_id = 4329596  # currently hardcoded - since most addresses are located in Austria - otherwise mapping of 269 ELGA-Laendercodes
        else:
            country_concept_id = 0
        country_source_value = location[5]
        latitude = None
        longitude = None

        val = (location_id,
               address_1,
               address_2,
               city,
               state,
               zip,
               county,
               location_source_value,
               country_concept_id,
               country_source_value,
               latitude,
               longitude)
        cur.execute(sql, val)
        con.commit()
        cur.close()
        con.close()
        return location_id

    def writeCareSite_ToDB(self, care_site, location_id, place_of_service_concept_id):
        con = self.connectToOMOP()
        cur = con.cursor()
        sql = "INSERT INTO care_site (" \
              "care_site_id, " \
              "care_site_name, " \
              "place_of_service_concept_id, " \
              "location_id, " \
              "care_site_source_value, " \
              "place_of_service_source_value) VALUES (%s, %s, %s, %s, %s, %s)"
        # Prepare data for insert
        care_site_id = self.getNextIdForTable("care_site_id", "care_site")
        care_site_name = care_site[1]  # CDA-participant -> organisation_name
        place_of_service_concept_id = place_of_service_concept_id  # not coded/used in CDA only with encompassing encounter
        location_id = location_id
        care_site_source_value = care_site[0]  # organisation_id
        place_of_service_source_value = None  # not coded/used in CDA
        val = (care_site_id,
               care_site_name,
               place_of_service_concept_id,
               location_id,
               care_site_source_value,
               place_of_service_source_value)
        cur.execute(sql, val)
        con.commit()
        cur.close()
        con.close()
        return care_site_id

    def writeProviderToDB(self, provider, care_site_id):
        con = self.connectToOMOP()
        cur = con.cursor()
        sql = "INSERT INTO provider (" \
              "provider_id, " \
              "provider_name, " \
              "npi, " \
              "dea, " \
              "specialty_concept_id, " \
              "care_site_id, " \
              "year_of_birth, " \
              "gender_concept_id, " \
              "provider_source_value, " \
              "specialty_source_value, " \
              "specialty_source_concept_id, " \
              "gender_source_value, " \
              "gender_source_concept_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        # Prepare data for insert
        provider_id = self.getNextIdForTable("provider_id", "provider")
        provider_name = provider[5]  # CDA-person_name
        npi = None  # Not used in CDA
        dea = None # Not used in CDA
        if provider[3] == "PCP":
            specialty_concept_id = 38004446
        else:
            specialty_concept_id = 0  # TODO hier muss noch ein Mapping für ELGA_AuthorSpeciality erstellt werden!
        care_site_id = care_site_id
        year_of_birth = None  # not used in CDA
        gender_concept_id = 0  # not used in CDA
        provider_source_value = provider[4] + provider[5]  # person_id + person_name
        specialty_source_value = provider[3]
        specialty_source_concept_id = 0  # sobald Mapping erstellt ist
        gender_source_value = None  # not used in CDA
        gender_source_concept_id = 0  # not used in CDA
        val = (provider_id,
               provider_name,
               npi,
               dea,
               specialty_concept_id,
               care_site_id,
               year_of_birth,
               gender_concept_id,
               provider_source_value,
               specialty_source_value,
               specialty_source_concept_id,
               gender_source_value,
               gender_source_concept_id)
        cur.execute(sql, val)
        con.commit()
        cur.close()
        con.close()
        return provider_id

    def writeProviderEncompassingEncounterToDB(self, provider, care_site_id):
        con = self.connectToOMOP()
        cur = con.cursor()
        sql = "INSERT INTO provider (" \
              "provider_id, " \
              "provider_name, " \
              "npi, " \
              "dea, " \
              "specialty_concept_id, " \
              "care_site_id, " \
              "year_of_birth, " \
              "gender_concept_id, " \
              "provider_source_value, " \
              "specialty_source_value, " \
              "specialty_source_concept_id, " \
              "gender_source_value, " \
              "gender_source_concept_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        # Prepare data for insert
        provider_id = self.getNextIdForTable("provider_id", "provider")
        provider_name = provider[1]  # CDA-person_name
        npi = None  # Not used in CDA
        dea = None # Not used in CDA
        specialty_concept_id = 0
        care_site_id = care_site_id
        year_of_birth = None  # not used in CDA
        gender_concept_id = 0  # not used in CDA
        provider_source_value = provider[0] + ":" + provider[1]  # person_id + person_name
        specialty_source_value = None
        specialty_source_concept_id = 0
        gender_source_value = None  # not used in CDA
        gender_source_concept_id = 0  # not used in CDA
        val = (provider_id,
               provider_name,
               npi,
               dea,
               specialty_concept_id,
               care_site_id,
               year_of_birth,
               gender_concept_id,
               provider_source_value,
               specialty_source_value,
               specialty_source_concept_id,
               gender_source_value,
               gender_source_concept_id)
        cur.execute(sql, val)
        con.commit()
        cur.close()
        con.close()
        return provider_id

    def writeObservationPeriodToDB(self, observation_period, person_id, document_code):
        con = self.connectToOMOP()
        cur = con.cursor()
        sql = "INSERT INTO observation_period (" \
              "observation_period_id, " \
              "person_id, " \
              "observation_period_start_date, " \
              "observation_period_end_date, " \
              "period_type_concept_id) VALUES (%s, %s, %s, %s, %s)"
        # Prepare data for insert
        observation_period_id = self.getNextIdForTable("observation_period_id", "observation_period")
        person_id = person_id
        observation_period_start_date = observation_period[2].date()  # CDA-effectiveTime_low
        observation_period_end_date = observation_period[3].date()  # CDA-effectiveTime_low
        period_type_concept_id = self.getConceptId_from_source_to_concept(document_code)
        val = (observation_period_id,
               person_id,
               observation_period_start_date,
               observation_period_end_date,
               period_type_concept_id)
        cur.execute(sql, val)
        con.commit()
        cur.close()
        con.close()
        return observation_period_id

    def writeVisitOccurenceToDB(self, visit_occurence, person_id, provider_id, care_site_id, preceding_visit_occurrence_id, document_code):
        con = self.connectToOMOP()
        cur = con.cursor()
        sql = "INSERT INTO visit_occurrence (" \
              "visit_occurrence_id, " \
              "person_id, " \
              "visit_concept_id, " \
              "visit_start_date, " \
              "visit_start_datetime, " \
              "visit_end_date, " \
              "visit_end_datetime, " \
              "visit_type_concept_id, " \
              "provider_id, " \
              "care_site_id, " \
              "visit_source_value, " \
              "visit_source_concept_id, " \
              "admitted_from_concept_id, " \
              "admitted_from_source_value, " \
              "discharged_to_concept_id, " \
              "discharged_to_source_value, " \
              "preceding_visit_occurrence_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        # Prepare data for insert
        visit_occurrence_id = self.getNextIdForTable("visit_occurrence_id", "visit_occurrence")
        person_id = person_id
        if visit_occurence[1] == "IMP":
            visit_concept_id = 9201
        else:
            visit_concept_id = 0 # Mapping muss erstellt werden fuer ELGA_ActEncounterCode
        visit_start_date = visit_occurence[2].date()
        visit_start_datetime = visit_occurence[2]
        visit_end_date = visit_occurence[3].date()
        visit_end_datetime = visit_occurence[3]
        visit_type_concept_id = self.getConceptId_from_source_to_concept(document_code)
        provider_id = provider_id
        care_site_id = care_site_id
        visit_source_value = visit_occurence[1]
        visit_source_concept_id = 0  # -> bis Mapping erstellt ist
        admitted_from_concept_id = 0
        admitted_from_source_value = None # Pruefen ob in CDA verwendet wird
        discharged_to_concept_id = 0
        discharged_to_source_value = None  # Pruefen ob in CDA verwendet wird
        preceding_visit_occurrence_id = preceding_visit_occurrence_id
        val = (visit_occurrence_id,
               person_id,
               visit_concept_id,
               visit_start_date,
               visit_start_datetime,
               visit_end_date,
               visit_end_datetime,
               visit_type_concept_id,
               provider_id,
               care_site_id,
               visit_source_value,
               visit_source_concept_id,
               admitted_from_concept_id,
               admitted_from_source_value,
               discharged_to_concept_id,
               discharged_to_source_value,
               preceding_visit_occurrence_id)
        cur.execute(sql, val)
        con.commit()
        cur.close()
        con.close()
        return visit_occurrence_id

    def writeVisitDetailToDB(self, visit_detail, person_id, provider_id, care_site_id, visit_id, document_code):
        con = self.connectToOMOP()
        cur = con.cursor()
        sql = "INSERT INTO visit_detail (" \
              "visit_detail_id, " \
              "person_id, " \
              "visit_detail_concept_id, " \
              "visit_detail_start_date, " \
              "visit_detail_start_datetime, " \
              "visit_detail_end_date, " \
              "visit_detail_end_datetime, " \
              "visit_detail_type_concept_id, " \
              "provider_id, " \
              "care_site_id, " \
              "visit_detail_source_value, " \
              "visit_detail_source_concept_id, " \
              "admitted_from_concept_id, " \
              "admitted_from_source_value, " \
              "discharged_to_source_value, " \
              "discharged_to_concept_id, " \
              "preceding_visit_detail_id, " \
              "parent_visit_detail_id, " \
              "visit_occurrence_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        # Prepare data for insert
        visit_detail_id = self.getNextIdForTable("visit_detail_id", "visit_detail")
        person_id = person_id
        if visit_detail[1] == "IMP":
            visit_detail_concept_id = 9201
        else:
            visit_detail_concept_id = 0 # Mapping muss erstellt werden fuer ELGA_ActEncounterCode
        visit_detail_start_date = visit_detail[2].date()
        visit_detail_start_datetime = visit_detail[2]
        visit_detail_end_date = visit_detail[3].date()
        visit_detail_end_datetime = visit_detail[3]
        visit_detail_type_concept_id = self.getConceptId_from_source_to_concept(document_code)
        provider_id = provider_id
        care_site_id = care_site_id
        visit_detail_source_value = visit_detail[1]
        visit_detail_source_concept_id = 0 # -> Mapping
        admitted_from_concept_id = 0
        admitted_from_source_value = None
        discharged_to_source_value = 0
        discharged_to_concept_id = None
        preceding_visit_detail_id = None
        parent_visit_detail_id = None
        visit_occurrence_id = visit_id
        val = (visit_detail_id,
               person_id,
               visit_detail_concept_id,
               visit_detail_start_date,
               visit_detail_start_datetime,
               visit_detail_end_date,
               visit_detail_end_datetime,
               visit_detail_type_concept_id,
               provider_id,
               care_site_id,
               visit_detail_source_value,
               visit_detail_source_concept_id,
               admitted_from_concept_id,
               admitted_from_source_value,
               discharged_to_source_value,
               discharged_to_concept_id,
               preceding_visit_detail_id,
               parent_visit_detail_id,
               visit_occurrence_id)
        cur.execute(sql, val)
        con.commit()
        cur.close()
        con.close()
        return visit_detail_id

    def writeConditionOccurenceToDB(self, person_id, provider_id, visit_id):
        con = self.connectToOMOP()
        cur = con.cursor()
        sql = "INSERT INTO condition_occurrence (" \
              "condition_occurrence_id, " \
              "person_id, " \
              "condition_concept_id, " \
              "condition_start_date, " \
              "condition_start_datetime, " \
              "condition_end_date, " \
              "condition_end_datetime, " \
              "condition_type_concept_id, " \
              "condition_status_concept_id, " \
              "stop_reason, " \
              "provider_id, " \
              "visit_occurrence_id, " \
              "visit_detail_id, " \
              "condition_source_value, " \
              "condition_source_concept_id, " \
              "condition_status_source_value) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        # Prepare data for insert
        condition_occurrence_id = self.getNextIdForTable("condition_occurrence_id", "condition_occurrence")
        person_id = person_id
        condition_concept_id = 0  # TBD
        condition_start_date = None  # TBD
        condition_start_datetime = None  # TBD
        condition_end_date = None  # TBD
        condition_end_datetime = None  # TBD
        condition_type_concept_id = 0  # TBD
        condition_status_concept_id = 0  # TBD
        stop_reason = None  # TBD
        provider_id = provider_id
        visit_occurrence_id = visit_id
        visit_detail_id = 0
        condition_source_value = None  # TBD
        condition_source_concept_id = 0  # TBD
        condition_status_source_value = None  # TBD
        val = (condition_occurrence_id,
               person_id,
               condition_concept_id,
               condition_start_date,
               condition_start_datetime,
               condition_end_date,
               condition_end_datetime,
               condition_type_concept_id,
               condition_status_concept_id,
               stop_reason,
               provider_id,
               visit_occurrence_id,
               visit_detail_id,
               condition_source_value,
               condition_source_concept_id,
               condition_status_source_value)
        cur.execute(sql, val)
        con.commit()
        cur.close()
        con.close()
        return condition_occurrence_id

    def writeDrugExposureToDB(self, person_id, provider_id, visit_id):
        con = self.connectToOMOP()
        cur = con.cursor()
        sql = "INSERT INTO drug_exposure (" \
              "drug_exposure_id, " \
              "person_id, " \
              "drug_concept_id, " \
              "drug_exposure_start_date, " \
              "drug_exposure_start_datetime, " \
              "drug_exposure_end_date, " \
              "drug_exposure_end_datetime, " \
              "verbatim_end_date, " \
              "drug_type_concept_id, " \
              "stop_reason, " \
              "refills, " \
              "quantity, " \
              "days_supply, " \
              "sig, " \
              "route_concept_id, " \
              "lot_number, " \
              "provider_id, " \
              "visit_occurrence_id, " \
              "visit_detail_id, " \
              "drug_source_value, " \
              "drug_source_concept_id, " \
              "route_source_value, " \
              "dose_unit_source_value) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        # Prepare data for insert
        drug_exposure_id = self.getNextIdForTable("drug_exposure_id", "drug_exposure")
        person_id = person_id
        drug_concept_id = 0  # TBD
        drug_exposure_start_date = None  # TBD
        drug_exposure_start_datetime = None  # TBD
        drug_exposure_end_date = None  # TBD
        drug_exposure_end_datetime = None  # TBD
        verbatim_end_date = None  # TBD
        drug_type_concept_id = 0  # TBD
        stop_reason = None  # TBD
        refills = None  # TBD
        quantity = None  # TBD
        days_supply = None  # TBD
        sig = None  # TBD
        route_concept_id = 0  # TBD
        lot_number = None  # TBD
        provider_id = provider_id
        visit_occurrence_id = visit_id
        visit_detail_id = None  # TBD
        drug_source_value = None  # TBD
        drug_source_concept_id = 0  # TBD
        route_source_value = None  # TBD
        dose_unit_source_value = None  # TBD
        val = (drug_exposure_id,
               person_id,
               drug_concept_id,
               drug_exposure_start_date,
               drug_exposure_start_datetime,
               drug_exposure_end_date,
               drug_exposure_end_datetime,
               verbatim_end_date,
               drug_type_concept_id,
               stop_reason,
               refills,
               quantity,
               days_supply,
               sig,
               route_concept_id,
               lot_number,
               provider_id,
               visit_occurrence_id,
               visit_detail_id,
               drug_source_value,
               drug_source_concept_id,
               route_source_value,
               dose_unit_source_value)
        cur.execute(sql, val)
        con.commit()
        cur.close()
        con.close()
        return drug_exposure_id

    def writeProcedureOccurenceToDB(self, procedure, person_id, provider_id, visit_id, code, document_code):
        con = self.connectToOMOP()
        cur = con.cursor()
        sql = "INSERT INTO procedure_occurrence (" \
              "procedure_occurrence_id, " \
              "person_id, " \
              "procedure_concept_id, " \
              "procedure_date, " \
              "procedure_datetime, " \
              "procedure_end_date, " \
              "procedure_end_datetime, " \
              "procedure_type_concept_id, " \
              "modifier_concept_id, " \
              "quantity, " \
              "provider_id, " \
              "visit_occurrence_id, " \
              "visit_detail_id, " \
              "procedure_source_value, " \
              "procedure_source_concept_id, " \
              "modifier_source_value) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        # Prepare data for insert
        procedure_occurrence_id = self.getNextIdForTable("procedure_occurrence_id", "procedure_occurrence")
        person_id = person_id
        procedure_concept_id = self.getConceptId_from_source_to_concept(code[0])  # TBD
        procedure_date = procedure[1].date()
        procedure_datetime = procedure[1]
        procedure_end_date = procedure[2].date()
        procedure_end_datetime = procedure[2]
        procedure_type_concept_id = self.getConceptId_from_source_to_concept(document_code)
        modifier_concept_id = 0
        quantity = None
        provider_id = provider_id
        visit_occurrence_id = visit_id
        visit_detail_id = 0
        procedure_source_value = code[1]
        procedure_source_concept_id = 0
        modifier_source_value = None
        val = (procedure_occurrence_id,
               person_id,
               procedure_concept_id,
               procedure_date,
               procedure_datetime,
               procedure_end_date,
               procedure_end_datetime,
               procedure_type_concept_id,
               modifier_concept_id,
               quantity,
               provider_id,
               visit_occurrence_id,
               visit_detail_id,
               procedure_source_value,
               procedure_source_concept_id,
               modifier_source_value)
        cur.execute(sql, val)
        con.commit()
        cur.close()
        con.close()
        return procedure_occurrence_id

    def writeDeviceExposureToDB(self, person_id, provider_id, visit_id):
        con = self.connectToOMOP()
        cur = con.cursor()
        sql = "INSERT INTO device_exposure (" \
              "device_exposure_id, " \
              "person_id, " \
              "device_concept_id, " \
              "device_exposure_start_date, " \
              "device_exposure_start_datetime, " \
              "device_exposure_end_date, " \
              "device_exposure_end_datetime, " \
              "device_type_concept_id, " \
              "unique_device_id, " \
              "production_id, " \
              "quantity, " \
              "provider_id, " \
              "visit_occurrence_id, " \
              "visit_detail_id, " \
              "device_source_value, " \
              "device_source_concept_id, " \
              "unit_concept_id, " \
              "unit_source_value, " \
              "unit_source_concept_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        # Prepare data for insert
        device_exposure_id = self.getNextIdForTable("device_exposure_id", "device_exposure")
        person_id = person_id
        device_concept_id = 0  # TBD
        device_exposure_start_date = None  # TBD
        device_exposure_start_datetime = None  # TBD
        device_exposure_end_date = None  # TBD
        device_exposure_end_datetime = None  # TBD
        device_type_concept_id = 0  # TBD
        unique_device_id = None  # TBD
        production_id = None  # TBD
        quantity = None  # TBD
        provider_id = None  # TBD
        visit_occurrence_id = None  # TBD
        visit_detail_id = None  # TBD
        device_source_value = None  # TBD
        device_source_concept_id = 0  # TBD
        unit_concept_id = 0  # TBD
        unit_source_value = None  # TBD
        unit_source_concept_id = 0  # TBD
        val = (device_exposure_id,
               person_id,
               device_concept_id,
               device_exposure_start_date,
               device_exposure_start_datetime,
               device_exposure_end_date,
               device_exposure_end_datetime,
               device_type_concept_id,
               unique_device_id,
               production_id,
               quantity,
               provider_id,
               visit_occurrence_id,
               visit_detail_id,
               device_source_value,
               device_source_concept_id,
               unit_concept_id,
               unit_source_value,
               unit_source_concept_id)
        cur.execute(sql, val)
        con.commit()
        cur.close()
        con.close()
        return device_exposure_id

    def writeMeasurementToDB(self, measurement, person_id, provider_id, visit_occurence_id, visit_detail_id, document_code):
        con = self.connectToOMOP()
        cur = con.cursor()
        sql = "INSERT INTO measurement (" \
              "measurement_id, " \
              "person_id, " \
              "measurement_concept_id, " \
              "measurement_date, " \
              "measurement_datetime, " \
              "measurement_time, " \
              "measurement_type_concept_id, " \
              "operator_concept_id, " \
              "value_as_number, " \
              "value_as_concept_id, " \
              "unit_concept_id, " \
              "range_low, " \
              "range_high, " \
              "provider_id, " \
              "visit_occurrence_id, " \
              "visit_detail_id, " \
              "measurement_source_value, " \
              "measurement_source_concept_id, " \
              "unit_source_value, " \
              "unit_source_concept_id, " \
              "value_source_value, " \
              "measurement_event_id, " \
              "meas_event_field_concept_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        # Prepare data for insert
        measurement_id = self.getNextIdForTable("measurement_id", "measurement")
        person_id = person_id
        measurement_concept_id = self.getMeasurement_concept_id(measurement[2])
        measurement_date = measurement[3].date()
        measurement_datetime = measurement[3]
        measurement_time = measurement[3].time()
        measurement_type_concept_id = self.getConceptId_from_source_to_concept(document_code)
        operator_concept_id = 0  # not used
        value_as_number = measurement[5]
        value_as_concept_id = 0
        unit = measurement[6]
        if unit == '[IU]/L':
            unit = '[iU]/L'
        unit_concept_id = self.getUnit_concept_id(unit)
        if measurement[7] != "":
            range_low = measurement[7]
        else:
            range_low = None
        if measurement[8] != "":
            range_high = measurement[8]
        else:
            range_high = None
        provider_id = provider_id
        visit_occurrence_id = visit_occurence_id
        visit_detail_id = visit_detail_id
        measurement_source_value = measurement[2]
        measurement_source_concept_id = 44819139  # LOINC
        unit_source_value = measurement[6]
        if unit_concept_id == 0:
            unit_source_concept_id = 0  # bis Mapping erstellt ist
        else:
            unit_source_concept_id = 44819107
        value_source_value = measurement[5]
        measurement_event_id = None
        meas_event_field_concept_id = None
        val = (measurement_id,
               person_id,
               measurement_concept_id,
               measurement_date,
               measurement_datetime,
               measurement_time,
               measurement_type_concept_id,
               operator_concept_id,
               value_as_number,
               value_as_concept_id,
               unit_concept_id,
               range_low,
               range_high,
               provider_id,
               visit_occurrence_id,
               visit_detail_id,
               measurement_source_value,
               measurement_source_concept_id,
               unit_source_value,
               unit_source_concept_id,
               value_source_value,
               measurement_event_id,
               meas_event_field_concept_id)
        cur.execute(sql, val)
        con.commit()
        cur.close()
        con.close()
        return measurement_id

    def writeObservationToDB(self, observation, person_id, provider_id, visit_id, observation_id):
        con = self.connectToOMOP()
        cur = con.cursor()
        sql = "INSERT INTO observation (" \
              "observation_id, " \
              "person_id, " \
              "observation_concept_id, " \
              "observation_date, " \
              "observation_datetime, " \
              "observation_type_concept_id, " \
              "value_as_number, " \
              "value_as_string, " \
              "value_as_concept_id, " \
              "qualifier_concept_id, " \
              "unit_concept_id, " \
              "provider_id, " \
              "visit_occurrence_id, " \
              "visit_detail_id, " \
              "observation_source_value, " \
              "observation_source_concept_id, " \
              "unit_source_value, " \
              "qualifier_source_value, " \
              "value_source_value, " \
              "observation_event_id, " \
              "obs_event_field_concept_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        # Prepare data for insert
        observation_id = self.getNextIdForTable("observation_id", "observation")
        person_id = person_id
        observation_concept_id = 0  #  TBD
        observation_date = None # TBD
        observation_datetime = None # TBD
        observation_type_concept_id = 0 # TBD
        value_as_number = None # TBD
        value_as_string = None # TBD
        value_as_concept_id = 0 # TBD
        qualifier_concept_id = 0 # TBD
        unit_concept_id = 0 # TBD
        provider_id = provider_id
        visit_occurrence_id = visit_id
        visit_detail_id = None
        observation_source_value = None # TBD
        observation_source_concept_id = 0 # TBD
        unit_source_value = None # TBD
        qualifier_source_value = None # TBD
        value_source_value = None # TBD
        observation_event_id = observation_id
        obs_event_field_concept_id = None
        val = (observation_id,
               person_id,
               observation_concept_id,
               observation_date,
               observation_datetime,
               observation_type_concept_id,
               value_as_number,
               value_as_string,
               value_as_concept_id,
               qualifier_concept_id,
               unit_concept_id,
               provider_id,
               visit_occurrence_id,
               visit_detail_id,
               observation_source_value,
               observation_source_concept_id,
               unit_source_value,
               qualifier_source_value,
               value_source_value,
               observation_event_id,
               obs_event_field_concept_id)
        cur.execute(sql, val)
        con.commit()
        cur.close()
        con.close()
        return observation_id

    def writeSpecimenToDB(self, specimen, person_id, document_code):
        con = self.connectToOMOP()
        cur = con.cursor()
        sql = "INSERT INTO specimen (" \
              "specimen_id, " \
              "person_id, " \
              "specimen_concept_id, " \
              "specimen_type_concept_id, " \
              "specimen_date, " \
              "specimen_datetime, " \
              "quantity, " \
              "unit_concept_id, " \
              "anatomic_site_concept_id, " \
              "disease_status_concept_id, " \
              "specimen_source_id, " \
              "specimen_source_value, " \
              "unit_source_value, " \
              "anatomic_site_source_value, " \
              "disease_status_source_value) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        # Prepare data for insert
        specimen_id = self.getNextIdForTable("specimen_id", "specimen")
        person_id = person_id
        specimen_concept_id = self.getConceptId_from_source_to_concept(specimen[1])
        specimen_type_concept_id = self.getConceptId_from_source_to_concept(document_code)
        specimen_date = specimen[9].date()
        specimen_datetime = specimen[9]
        quantity = None
        unit_concept_id = 0
        anatomic_site_concept_id = self.getConceptId_from_source_to_concept(specimen[10])
        disease_status_concept_id = 0
        specimen_source_id = specimen[0]
        specimen_source_value = specimen[1]
        unit_source_value = None
        anatomic_site_source_value = specimen[10]
        disease_status_source_value = None
        val = (specimen_id,
               person_id,
               specimen_concept_id,
               specimen_type_concept_id,
               specimen_date,
               specimen_datetime,
               quantity,
               unit_concept_id,
               anatomic_site_concept_id,
               disease_status_concept_id,
               specimen_source_id,
               specimen_source_value,
               unit_source_value,
               anatomic_site_source_value,
               disease_status_source_value)
        cur.execute(sql, val)
        con.commit()
        cur.close()
        con.close()
        return specimen_id
