import mysql.connector
import psycopg2
from DocumentTypAlgorithm import MedicalDischargeLetter


class MedicalDischargeLetterToOMOP:

    def __init__(self):
        self.documentDatabase = self.connectToELGA()
        self.OMOPdatabase = self.connectToOMOP()

    # Connection to Document database - ELGA
    def connectToELGA(self):
        mydb = mysql.connector.connect(
            host="192.168.1.127",
            user="root",
            password="PASSWORD",
            database="CDA_Entlassungsbrief_A"
        )
        return mydb

    # Connection to OMOP database
    def connectToOMOP(self):
        con = psycopg2.connect(
            host="192.168.1.127",
            database="ELGA_import",
            user="postgres",
            password="password")
        return con

    # ELGA Funktion: CDA Ärztlicher Entlassungbrief getCode:
    def getCode_Information(self, code):
        mydb = self.connectToELGA()
        mycursor = mydb.cursor()
        sql = "SELECT * FROM code WHERE code = %s;"
        val = (code,)
        mycursor.execute(sql, val)
        code_info = mycursor.fetchone()

        mydb.commit()
        mycursor.close()
        mydb.close()
        return code_info

    # ELGA Funktion: get Adresse nach ID
    def getAdressID(self, addr_id):
        mydb = self.connectToELGA()
        mycursor = mydb.cursor()
        sql = "SELECT * FROM addr WHERE id_addr = %s"
        val = (addr_id,)
        mycursor.execute(sql, val)
        address = mycursor.fetchone()
        mydb.commit()
        mycursor.close()
        mydb.close()
        return address

    def getAllAdressesInELGA(self):
        mydb = self.connectToELGA()
        mycursor = mydb.cursor()
        sql = "SELECT * FROM addr"
        mycursor.execute(sql)
        address_result = mycursor.fetchall()
        mydb.commit()
        print(mycursor.rowcount, "records.")
        mycursor.close()
        mydb.close()
        return address_result

    def getAllPatientsInELGA(self):
        mydb = self.connectToELGA()
        mycursor = mydb.cursor()
        sql = "SELECT * FROM patientRole"
        mycursor.execute(sql)
        patient_result = mycursor.fetchall()
        mydb.commit()
        print(mycursor.rowcount, "records.")
        mycursor.close()
        mydb.close()
        return patient_result

    def getEntlassungsbriefeForPatientID_ELGA(self, patientid):
        mydb = self.connectToELGA()
        mycursor = mydb.cursor()
        sql = "SELECT * FROM ClinicalDocument_CDAEntlassungsbrief_A WHERE recordTarget = %s"
        patient = (patientid,)
        mycursor.execute(sql, patient)
        entlassungsbriefe_result = mycursor.fetchall()
        mydb.commit()
        print(mycursor.rowcount, "records.")
        mycursor.close()
        mydb.close()
        return entlassungsbriefe_result

    def getPrimaryCarePhysicanFromEntlassungbrief_ELGA(self, entlassungsbrief_id):
        mydb = self.connectToELGA()
        mycursor = mydb.cursor()
        sql = "SELECT * FROM participant WHERE id_participant IN (SELECT Participant_ID FROM ClinicalDocument_has_participant WHERE ClinicalDocument_ID = %s) AND functionCode = 'PCP'"
        lab_id = (entlassungsbrief_id,)
        mycursor.execute(sql, lab_id)
        pcp = mycursor.fetchall()
        mydb.commit()
        print(mycursor.rowcount, "records.")
        mycursor.close()
        mydb.close()
        return pcp

    def getConceptID_Measurement_OMOP(self, code):
        con = self.connectToOMOP()
        cur = con.cursor()
        sql = "SELECT concept_id FROM concept WHERE concept_code = %s AND standard_concept = 'S' AND domain_id = 'Measurement';"
        cur.execute(sql, (code,))
        res = cur.fetchall()
        con.commit()
        cur.close()
        con.close()
        try:
            concept_id = res[0][0]
        except:
            concept_id = None
        return concept_id

    def getConceptID_Unit_OMOP(self, unit_value):
        con = self.connectToOMOP()
        cur = con.cursor()
        sql = "SELECT concept_id FROM concept WHERE concept_class_id = 'Unit' AND concept_code = %s AND standard_concept = 'S';"
        cur.execute(sql, (unit_value,))
        res = cur.fetchall()
        con.commit()
        cur.close()
        con.close()
        try:
            concept_id = res[0][0]
        except:
            concept_id = None
        return concept_id

    def writeAllAdressToLocation_OMOP(self, address_result):
        con = self.connectToOMOP()

        for x in address_result:
            location_id = x[0]
            location_source_value = location_id
            city = x[3]
            state = None  # Wird nicht verwendet
            country = x[5]
            zip = x[2]
            address_1 = x[1]
            address_2 = x[6]
            # country_concept_id = 4329596 erst ab CDM 5.4
            cur = con.cursor()
            sql = "INSERT INTO location (location_id, location_source_value, address_1, address_2, city, zip, state, country) VALUES (%s, %s, %s, %s , %s, %s, %s, %s)"
            val = (location_id, location_source_value, address_1, address_2, city, zip, state, country)
            cur.execute(sql, val)
            con.commit()
            cur.close()
        con.close()

    def writeCareSite_OMOP(self, care_site):
        con = self.connectToOMOP()
        cur = con.cursor()
        sql = "INSERT INTO care_site (care_site_id, care_site_name, place_of_service_concept_id, location_id, care_site_source_value, place_of_service_source_value) VALUES (%s, %s, %s, %s, %s, %s)"
        val = (care_site[8], care_site[7], 0, care_site[8], care_site[7], None)
        cur.execute(sql, val)
        con.commit()
        cur.close()

    def writePCPToProvider_OMOP(self, pcp):
        con = self.connectToOMOP()
        cur = con.cursor()
        sql = "INSERT INTO provider (provider_id, provider_name, npi, dea, specialty_concept_id, care_site_id, year_of_birth, gender_concept_id, provider_source_value, specialty_source_value, specialty_source_concept_id, gender_source_value, gender_source_concept_id) VALUES (%s, %s, %s, %s , %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        val = (pcp[0], pcp[5], None, None, 0, pcp[8], None, 0, pcp[0], None, 0, None, 0)
        cur.execute(sql, val)
        con.commit()
        cur.close()
        con.close()

    def writePatientRoleToPerson_OMOP(self, patient_result, pcp):
        x = patient_result
        person_id = x[0]  # SVNR oder autogen
        person_source_value = person_id
        year_of_birth = x[7].year
        month_of_birth = x[7].month
        day_of_birth = x[7].day
        birth_datetime = x[7]
        death_datetime = None
        race_concept_id = 0  # not in ELGA
        ethnicity_concept_id = 0  # not in ELGA
        ethnicity_source_concept_id = 0
        location_id = x[3]
        gender_source_value = x[6]
        # Da es hier nur zwei Values gibt - harcodiert
        if gender_source_value == "F":
            gender_concept_id = 8532
        if gender_source_value == "M":
            gender_concept_id = 8507
        gender_source_concept_id = 0
        race_source_concept_id = 0
        race_source_value = None
        provider_id = pcp[0]
        care_site = pcp[8]
        con = self.connectToOMOP()
        cur = con.cursor()
        sql = "INSERT INTO Person (person_id, ethnicity_source_concept_id, person_source_value, year_of_birth, month_of_birth, day_of_birth, birth_datetime, death_datetime, race_concept_id, ethnicity_concept_id, location_id, gender_source_value, gender_concept_id, gender_source_concept_id, race_source_concept_id, race_source_value, care_site_id, provider_id) VALUES (%s, %s, %s, %s, %s , %s, %s, %s, %s, %s, %s, %s , %s, %s, %s, %s, %s, %s)"
        val = (person_id, ethnicity_source_concept_id, person_source_value, year_of_birth, month_of_birth, day_of_birth,
               birth_datetime, death_datetime, race_concept_id, ethnicity_concept_id, location_id, gender_source_value,
               gender_concept_id, gender_source_concept_id, race_source_concept_id, race_source_value, care_site,
               provider_id)
        cur.execute(sql, val)
        con.commit()
        cur.close()
        con.close()

    def getEncompassingEncounter_ELGA(self, entlassungsbrief_id):
        mydb = self.connectToELGA()
        mycursor = mydb.cursor()
        sql = "SELECT * FROM encompassingEncounter WHERE id IN(SELECT encompassingEncounter FROM ClinicalDocument_CDAEntlassungsbrief_A WHERE id = %s);"
        val = (entlassungsbrief_id,)
        mycursor.execute(sql, val)
        encompassing_encounter = mycursor.fetchone()
        mydb.commit()
        mycursor.close()
        mydb.close()
        return encompassing_encounter

    def writeCareSiteEncompassing_Encounter_OMOP(self, encompassing_encounter):
        con = self.connectToOMOP()
        cur = con.cursor()
        sql = "INSERT INTO care_site (care_site_id, care_site_name, place_of_service_concept_id, location_id, care_site_source_value, place_of_service_source_value) VALUES (%s, %s, %s, %s, %s, %s)"
        # 9201 - Inpatient
        val = (encompassing_encounter[12], encompassing_encounter[11], 9201, encompassing_encounter[12],
               encompassing_encounter[11], encompassing_encounter[1])
        cur.execute(sql, val)
        con.commit()
        cur.close()

    def writeProviderEncompassing_Encounter_OMOP(self, encompassing_encounter):
        con = self.connectToOMOP()
        cur = con.cursor()
        sql = "INSERT INTO provider (provider_id, provider_name, npi, dea, specialty_concept_id, care_site_id, year_of_birth, gender_concept_id, provider_source_value, specialty_source_value, specialty_source_concept_id, gender_source_value, gender_source_concept_id) VALUES (%s, %s, %s, %s , %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        val = (
        encompassing_encounter[4], encompassing_encounter[5], None, None, 0, encompassing_encounter[12],
        None, 0, encompassing_encounter[4], None, 0, None, 0)
        cur.execute(sql, val)
        con.commit()
        cur.close()
        con.close()

    def writeVisit_OMOP(self, visit, patient_id):
        visit_occurrence_id = 500  # lab_encompassing_encounter[0] # autogen
        person_id = patient_id
        visit_concept_id = 9201  # Inpatient
        visit_start_date = visit[2].date()
        visit_start_datetime = visit[2]
        visit_end_date = visit[3].date()
        visit_end_datetime = visit[3]
        visit_type_concept_id = 32817  # Code for EHR
        provider_id = visit[4]
        care_site_id = visit[12]
        visit_source_value = visit[1]
        visit_source_concept_id = 0  # HL7 Act Code
        admitted_from_concept_id = 0
        admitted_from_source_value = None
        discharge_to_concept_id = 0
        discharge_to_source_value = None
        preceding_visit_occurrence_id = None

        con = self.connectToOMOP()

        cur = con.cursor()
        sql = "INSERT INTO visit_occurrence (visit_occurrence_id, person_id, visit_concept_id, visit_start_date, visit_start_datetime, visit_end_date, visit_end_datetime, visit_type_concept_id, provider_id, care_site_id, visit_source_value, visit_source_concept_id, admitted_from_concept_id, admitted_from_source_value, discharge_to_concept_id, discharge_to_source_value, preceding_visit_occurrence_id) VALUES (%s, %s, %s, %s , %s, %s, %s, %s, %s, %s, %s, %s, %s,%s, %s, %s, %s)"
        val = (visit_occurrence_id, person_id, visit_concept_id, visit_start_date, visit_start_datetime, visit_end_date,
               visit_end_datetime, visit_type_concept_id, provider_id, care_site_id, visit_source_value,
               visit_source_concept_id, admitted_from_concept_id, admitted_from_source_value, discharge_to_concept_id,
               discharge_to_source_value, preceding_visit_occurrence_id)
        cur.execute(sql, val)
        con.commit()
        cur.close()
        con.close()
        return visit_occurrence_id

    def writeObservation_period_OMOP(self, encompassing_encounter, patient_id):
        con = self.connectToOMOP()
        cur = con.cursor()
        sql = "INSERT INTO observation_period (observation_period_id, person_id, observation_period_start_date, observation_period_end_date, period_type_concept_id) VALUES (%s, %s, %s, %s, %s)"
        observation_period_id = 700  # autogen
        person_id = patient_id
        observation_period_start_date = encompassing_encounter[2].date()
        observation_period_end_date = encompassing_encounter[3].date()
        period_type_concept_id = 32817  # Code for EHR
        # 9201 - Inpatient
        val = (observation_period_id, person_id, observation_period_start_date, observation_period_end_date,
               period_type_concept_id)
        cur.execute(sql, val)
        con.commit()
        cur.close()

    def getVitalparamterObservation_ELGA(self, document_id):
        mydb = self.connectToELGA()
        mycursor = mydb.cursor()
        sql = "SELECT * FROM EB_EO_observation WHERE entryOrganizer_ID IN (SELECT EO_id FROM EB_EntryOrganizer WHERE sectio_id IN (SELECT section_ErhobeneBefunde_Vitalparameter_Id FROM ClinicalDocument_CDAEntlassungsbrief_A WHERE id = %s));"
        val = (document_id,)
        mycursor.execute(sql, val)
        observations = mycursor.fetchall()
        mydb.commit()
        mycursor.close()
        mydb.close()
        return observations

    def writeVitalparameterObservationToMeasurement_OMOP(self, observations, patient, encompassing_encounter):
        for m in observations:
            measurement_concept_id = self.getConceptID_Measurement_OMOP(m[2])  # 3
            unit = m[6]
            if unit == '[IU]/L':
                unit = '[iU]/L'
            unit_concept_id = self.getConceptID_Unit_OMOP(unit)  # 11
            if unit_concept_id == None:
                unit_concept_id = 0
            if measurement_concept_id != None:
                measurement_ID = m[0]  # 1
                person_ID = patient[0]  # 2
                measurement_source_value = m[2]  # 17
                measurement_source_concept_id = measurement_concept_id  # 18
                measurement_date = m[3].date()  # 4
                measurement_datetime = m[3]  # 5
                measurement_time = m[3].time()  # 6
                measurement_type_concept_id = 32817  # Code for EHR #7
                operator_concept_id = 0  # 8
                value_as_number = m[5]  # 9
                value_as_concept_id = 0  # 10
                unit_source_value = m[6]  # 19
                range_low = m[7]  # 12
                if range_low == '':
                    range_low = None
                range_high = m[8]  # 13
                if range_high == '':
                    range_high = None
                provider_id = encompassing_encounter[4]  # 14
                visit_occurrence_id = 500  # 15 #TODO: reference
                visit_detail_id = None  # 16
                value_source_value = m[5]  # 20
                con = self.connectToOMOP()
                cur = con.cursor()
                sql = "INSERT INTO measurement (measurement_id, person_id, measurement_concept_id, measurement_date, measurement_datetime, measurement_time, measurement_type_concept_id, operator_concept_id, value_as_number, value_as_concept_id, unit_concept_id, range_low, range_high, provider_id, visit_occurrence_id, visit_detail_id, measurement_source_value, measurement_source_concept_id, unit_source_value, value_source_value) VALUES (%s, %s, %s, %s, %s , %s, %s, %s, %s, %s, %s, %s , %s, %s, %s, %s, %s, %s, %s, %s)"
                val = (measurement_ID, person_ID, measurement_concept_id, measurement_date, measurement_datetime,
                       measurement_time, measurement_type_concept_id, operator_concept_id, value_as_number,
                       value_as_concept_id, unit_concept_id, range_low, range_high, provider_id, visit_occurrence_id,
                       visit_detail_id, measurement_source_value, measurement_source_concept_id, unit_source_value,
                       value_source_value)
                cur.execute(sql, val)
                con.commit()
                cur.close()
            else:
                measurement_concept_id = 0
                measurement_ID = m[0]  # 1
                person_ID = patient[0]  # 2
                measurement_source_value = m[2]  # 17
                measurement_source_concept_id = measurement_concept_id  # 18
                measurement_date = m[3].date()  # 4
                measurement_datetime = m[3]  # 5
                measurement_time = m[3].time()  # 6
                measurement_type_concept_id = 32817  # Code for EHR #7
                operator_concept_id = 0  # 8
                value_as_number = m[5]  # 9
                value_as_concept_id = 0  # 10
                unit_source_value = m[6]  # 19
                range_low = m[7]  # 12
                if range_low == '':
                    range_low = None
                range_high = m[8]  # 13
                if range_high == '':
                    range_high = None
                provider_id = encompassing_encounter[4]  # 14
                visit_occurrence_id = visit_occurrence_id  # 15
                visit_detail_id = None  # 16
                value_source_value = m[5]  # 20
                con = self.connectToOMOP()
                cur = con.cursor()
                sql = "INSERT INTO measurement (measurement_id, person_id, measurement_concept_id, measurement_date, measurement_datetime, measurement_time, measurement_type_concept_id, operator_concept_id, value_as_number, value_as_concept_id, unit_concept_id, range_low, range_high, provider_id, visit_occurrence_id, visit_detail_id, measurement_source_value, measurement_source_concept_id, unit_source_value, value_source_value) VALUES (%s, %s, %s, %s, %s , %s, %s, %s, %s, %s, %s, %s , %s, %s, %s, %s, %s, %s, %s, %s)"
                val = (measurement_ID, person_ID, measurement_concept_id, measurement_date, measurement_datetime,
                       measurement_time, measurement_type_concept_id, operator_concept_id, value_as_number,
                       value_as_concept_id, unit_concept_id, range_low, range_high, provider_id, visit_occurrence_id,
                       visit_detail_id, measurement_source_value, measurement_source_concept_id, unit_source_value,
                       value_source_value)
                cur.execute(sql, val)
                con.commit()
                cur.close()
        con.close()

    def getHospitalDischarge_observations_ELGA(self, document_id):
        mydb = self.connectToELGA()
        mycursor = mydb.cursor()
        sql = "SELECT * FROM HD_entry_observation WHERE entry_ID IN (SELECT id from HD_entry WHERE HD_ID IN (SELECT sectionHospitalDischarge FROM ClinicalDocument_CDAEntlassungsbrief_A WHERE id = %s));"
        val = (document_id,)
        mycursor.execute(sql, val)
        observations = mycursor.fetchall()
        mydb.commit()
        mycursor.close()
        mydb.close()
        return observations

    def getTargetConceptID_source_to_concept(self, code):
        con = self.connectToOMOP()
        cur = con.cursor()
        sql = "SELECT target_concept_id FROM source_to_concept_map WHERE source_code = %s;"
        cur.execute(sql, (code,))
        res = cur.fetchone()
        con.commit()
        cur.close()
        con.close()
        try:
            concept_id = res
        except:
            concept_id = None
        return concept_id

    def getSourceConceptID_source_to_concept(self, code):
        con = self.connectToOMOP()
        cur = con.cursor()
        sql = "SELECT source_concept_id FROM source_to_concept_map WHERE source_code = %s;"
        cur.execute(sql, (code,))
        res = cur.fetchone()
        con.commit()
        cur.close()
        con.close()
        try:
            concept_id = res
        except:
            concept_id = None
        return concept_id

    def writeHD_observations_condition_OMOP(self, observations, patient_id, visit_id):
        for o in observations:
            condition_occurence_id = o[0]
            person_id = patient_id
            c_concept_id = self.getTargetConceptID_source_to_concept(o[5])
            try:
                c_start_date = o[3].date()
                c_start_time = o[3]
            except:
                c_start_date = None
                c_start_time = None
            try:
                c_end_date = o[4].date()
                c_end_time = o[4]
            except:
                c_end_date = None
                c_end_time = None
            c_type_concept_id = 32020
            c_status_concept_id = 0
            stop_reason = None
            provider_id = None
            visit_occurence_id = visit_id
            c_source_concept_id = self.getSourceConceptID_source_to_concept(o[5])
            c_source_value = o[5]
            c_status_source_value = 0
            con = self.connectToOMOP()
            cur = con.cursor()
            sql = "INSERT INTO condition_occurrence (condition_occurrence_id, person_id, condition_concept_id, condition_start_date, condition_start_datetime, condition_end_date, condition_end_datetime, condition_type_concept_id, condition_status_concept_id, stop_reason, provider_id, visit_occurrence_id, visit_detail_id, condition_source_value, condition_source_concept_id, condition_status_source_value) VALUES (%s, %s, %s, %s, %s , %s, %s, %s, %s, %s, %s, %s , %s, %s, %s, %s)"
            val = (condition_occurence_id, person_id, c_concept_id, c_start_date, c_start_time, c_end_date, c_end_time, c_type_concept_id, c_status_concept_id, stop_reason, provider_id, visit_occurence_id, None, c_source_value, c_source_concept_id, c_status_source_value)
            cur.execute(sql, val)
            con.commit()
            cur.close()
        con.close()

    def getMedicationOnAdmission_ELGA(self, document_ID):
        mydb = self.connectToELGA()
        mycursor = mydb.cursor()
        sql = "SELECT * FROM MOA_sA_consumable WHERE MOA_sA_id IN (SELECT MOA_sA_id FROM MOA_substanceAdministration WHERE section_id IN (SELECT sectionMedicationOnAdmission from ClinicalDocument_CDAEntlassungsbrief_A WHERE id = %s)) AND pharmCode IS NOT NULL;"
        val = (document_ID,)
        mycursor.execute(sql, val)
        consumables = mycursor.fetchall()
        mydb.commit()
        mycursor.close()
        mydb.close()
        if len(consumables) < 1:
            return None
        return consumables

    def getMedicationOnAdmission_Time_ELGA(self, document_ID):
        mydb = self.connectToELGA()
        mycursor = mydb.cursor()
        sql = "SELECT effectiveTime_low, effectiveTime_high FROM MOA_substanceAdministration WHERE MOA_sA_id IN (SELECT MOA_sA_id FROM MOA_sA_consumable WHERE MOA_sA_id IN (SELECT MOA_sA_id FROM MOA_substanceAdministration WHERE section_id IN (SELECT sectionMedicationOnAdmission from ClinicalDocument_CDAEntlassungsbrief_A WHERE id = %s)) AND manufacturedMaterial_code IS NOT NULL)"
        val = (document_ID,)
        mycursor.execute(sql, val)
        times = mycursor.fetchall()
        mydb.commit()
        mycursor.close()
        mydb.close()
        return times

    def getDrugConcept_ID(self, drug_code):
        con = self.connectToOMOP()
        cur = con.cursor()
        sql = "SELECT concept_id_2 standard_concept_id FROM concept_relationship INNER JOIN concept source_concept ON concept_id = concept_id_1 WHERE concept_code = %s AND vocabulary_id = 'ATC' AND relationship_id = 'Maps to';"
        cur.execute(sql, (drug_code,))
        res = cur.fetchone()
        con.commit()
        cur.close()
        con.close()
        return res


    def writeMedicationOnAdmission_Drug_exposure_OMOP(self, medications, person_id, visit_id, time_low, time_high):
        if time_low is None:
            if time_high is None:
                print("No time element for Medication On Admission - cannot write to OMOP")
            else:
                for m in medications:
                    drug_exposure_id = m[0] # id_consumable
                    person_id = person_id # SVNR
                    drug_concept_id = self.getDrugConcept_ID(m[8])
                    drug_exposure_start_date = time_low.date()
                    drug_exposure_start_datetime = time_low
                    drug_exposure_end_date = time_high.date()
                    drug_exposure_end_datetime = time_high
                    verbatim_end_date = None
                    drug_type_concept_id = 32824 #EHR discharge summary
                    stop_reason = None
                    refills = 0
                    quantity = m[6]
                    days_supply = None
                    sig = None
                    route_concept_id = 0
                    lot_number = None
                    provider_id = None
                    visit_occurrence_id = visit_id
                    visit_detail_id = None
                    drug_source_value = m[8]
                    drug_source_concept_id = self.getDrugConcept_ID(m[8])
                    route_source_value = m[5]
                    dose_unit_source_value = m[7] #capacityUnit
                    con = self.connectToOMOP()
                    cur = con.cursor()
                    sql = "INSERT INTO drug_exposure (drug_exposure_id, person_id, drug_concept_id, drug_exposure_start_date, drug_exposure_start_datetime, drug_exposure_end_date, drug_exposure_end_datetime, verbatim_end_date, drug_type_concept_id, stop_reason, refills, quantity, days_supply, sig, route_concept_id, lot_number, provider_id, visit_occurrence_id, visit_detail_id, drug_source_value, drug_source_concept_id, route_source_value, dose_unit_source_value) VALUES (%s, %s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s , %s, %s, %s, %s)"
                    val = (drug_exposure_id, person_id, drug_concept_id, drug_exposure_start_date, drug_exposure_start_datetime, drug_exposure_end_date, drug_exposure_end_datetime, verbatim_end_date, drug_type_concept_id, stop_reason, refills, quantity, days_supply, sig, route_concept_id, lot_number, provider_id, visit_occurrence_id, visit_detail_id, drug_source_value, drug_source_concept_id, route_source_value, dose_unit_source_value)
                    cur.execute(sql, val)
                    con.commit()
                    cur.close()
                con.close()

    def getEmpfohleneMedikation_ELGA(self, document_ID):
        mydb = self.connectToELGA()
        mycursor = mydb.cursor()
        sql = "SELECT * FROM EM_consumable WHERE EM_sA_id IN (SELECT sA_id FROM EM_substanceAdministration WHERE section_id IN (SELECT sectionEmpfohleneMedikation_Id from ClinicalDocument_CDAEntlassungsbrief_A WHERE id = %s)) AND pharmCode IS NOT NULL;"
        val = (document_ID,)
        mycursor.execute(sql, val)
        consumables = mycursor.fetchall()
        mydb.commit()
        mycursor.close()
        mydb.close()
        if len(consumables) < 1:
            return None
        return consumables