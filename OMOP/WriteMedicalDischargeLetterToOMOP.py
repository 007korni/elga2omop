from OMOP import OMOP_Algorithm_MedicalDischargeLetter


class OMOPWriter:

    def writeMedicalDischargeLetterToOMOP(self):

        algorithm = OMOP_Algorithm_MedicalDischargeLetter.MedicalDischargeLetterToOMOP()
        adressen = algorithm.getAllAdressesInELGA()
        #algorithm.writeAllAdressToLocation_OMOP(adressen)
        patient_list = algorithm.getAllPatientsInELGA()
        entlassungsbriefe = dict()
        for patient in patient_list:
            entlassungsbriefe_list = algorithm.getEntlassungsbriefeForPatientID_ELGA(patient[0])
            entlassungsbriefe[patient[0]] = entlassungsbriefe_list

        for patient in patient_list:
            for entlassungsbrief in entlassungsbriefe[patient[0]]:
                pcp = algorithm.getPrimaryCarePhysicanFromEntlassungbrief_ELGA(entlassungsbrief[0])
                current_pcp = pcp[-1]
                #algorithm.writeCareSite_OMOP(current_pcp)
                #algorithm.writePCPToProvider_OMOP(current_pcp)
                #algorithm.writePatientRoleToPerson_OMOP(patient, current_pcp)
                encompassing_Encounter = algorithm.getEncompassingEncounter_ELGA(entlassungsbrief[0])
                #algorithm.writeCareSiteEncompassing_Encounter_OMOP(encompassing_Encounter)
                #algorithm.writeObservation_period_OMOP(encompassing_Encounter, patient[0])
                #algorithm.writeProviderEncompassing_Encounter_OMOP(encompassing_Encounter)
                #visit_id = algorithm.writeVisit_OMOP(encompassing_Encounter, patient[0])
                observations = algorithm.getVitalparamterObservation_ELGA(entlassungsbrief[0])
                #algorithm.writeVitalparameterObservationToMeasurement_OMOP(observations, patient, encompassing_Encounter)
                conditions = algorithm.getHospitalDischarge_observations_ELGA(entlassungsbrief[0])
                #algorithm.writeHD_observations_condition_OMOP(conditions, patient[0], visit_id)
                empfohleneMedikationen = algorithm.getEmpfohleneMedikation_ELGA(entlassungsbrief[0])
                if empfohleneMedikationen != None:
                    print("Write to OMOP")
                medikationBeiEinweisung = algorithm.getMedicationOnAdmission_ELGA(entlassungsbrief[0])
                medikationBeiEinweisung_times = algorithm.getMedicationOnAdmission_Time_ELGA(entlassungsbrief[0])
                algorithm.writeMedicationOnAdmission_Drug_exposure_OMOP(medikationBeiEinweisung, patient[0], visit_id, medikationBeiEinweisung_times[0][0], medikationBeiEinweisung_times[0][1])
                if medikationBeiEinweisung != None:
                    print("Write to OMOP")

                print(empfohleneMedikationen)
                print(medikationBeiEinweisung)
