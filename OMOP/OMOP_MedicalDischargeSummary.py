import mysql.connector
import psycopg2
import datetime
from OMOP import OMOP_Writer

class MedialDischargeSummaryToOMOP:

    def __init__(self):
        self.documentDatabase = self.connectToELGA()
        self.OMOPdatabase = self.connectToOMOP()
        self.omop = OMOP_Writer.OMOPWriter()

    # Connection to Document database - ELGA
    def connectToELGA(self):
        mydb = mysql.connector.connect(
            host="localhost",
            user="root",
            password="be16b044",
            database="CDA_Entlassungsbrief_Arzt"
        )
        return mydb

    # Connection to OMOP database
    def connectToOMOP(self):
        return OMOP_Writer.OMOPWriter().connectToOMOP()

    def getPatientRoleFromELGA(self, document_id):
        mydb = self.connectToELGA()
        mycursor = mydb.cursor()
        sql = "SELECT * FROM patientRole WHERE id_SVNR IN (SELECT recordTarget FROM ClinicalDocument_CDAEntlassungsbrief_A WHERE id = %s);"
        val = (document_id,)
        mycursor.execute(sql, val)
        patient = mycursor.fetchone()
        mydb.commit()
        mycursor.close()
        mydb.close()
        return patient

    def getCodeFromELGA(self, code):
        mydb = self.connectToELGA()
        mycursor = mydb.cursor()
        sql = "SELECT * FROM code WHERE code = %s;"
        val = (code,)
        mycursor.execute(sql, val)
        code = mycursor.fetchone()
        mydb.commit()
        mycursor.close()
        mydb.close()
        return code

    def getAddressFromAddressID(self, addr_id):
        mydb = self.connectToELGA()
        mycursor = mydb.cursor()
        sql = "SELECT * FROM addr WHERE id_addr = %s"
        val = (addr_id,)
        mycursor.execute(sql, val)
        address = mycursor.fetchone()
        mydb.commit()
        mycursor.close()
        mydb.close()
        return address

    # Required for care_site -> returns address from location element
    def getAddressFromEncompassingEncounter(self, document_id):
        mydb = self.connectToELGA()
        mycursor = mydb.cursor()
        sql = "SELECT * FROM addr WHERE id_addr IN (SELECT location_addr FROM encompassingEncounter WHERE id IN (SELECT encompassingEncounter FROM ClinicalDocument_CDAEntlassungsbrief_A WHERE id = %s));"
        val = (document_id, )
        mycursor.execute(sql, val)
        address = mycursor.fetchone()
        mydb.commit()
        mycursor.close()
        mydb.close()
        return address

    # Required for care_site -> returns location_id and location_name
    def getLocationInformationFromEncompassingEncounter(self, document_id):
        mydb = self.connectToELGA()
        mycursor = mydb.cursor()
        sql = "SELECT location_id, location_name FROM encompassingEncounter WHERE id IN (SELECT encompassingEncounter FROM ClinicalDocument_CDAEntlassungsbrief_A WHERE id = %s);"
        val = (document_id,)
        mycursor.execute(sql, val)
        location_information = mycursor.fetchone()
        mydb.commit()
        mycursor.close()
        mydb.close()
        return location_information

    def getDocumentDate(self, document_id):
        mydb = self.connectToELGA()
        mycursor = mydb.cursor()
        sql = "SELECT effectiveTime FROM ClinicalDocument_CDAEntlassungsbrief_A WHERE id = %s;"
        val = (document_id,)
        mycursor.execute(sql, val)
        document_datetime = mycursor.fetchone()
        mydb.commit()
        mycursor.close()
        mydb.close()
        return document_datetime

    def getEncompassingEncounter(self, document_id):
        mydb = self.connectToELGA()
        mycursor = mydb.cursor()
        sql = "SELECT * FROM encompassingEncounter WHERE id IN (SELECT encompassingEncounter FROM ClinicalDocument_CDAEntlassungsbrief_A WHERE id = %s);"
        val = (document_id, )
        mycursor.execute(sql, val)
        encompassing_encounter = mycursor.fetchone()
        mydb.commit()
        mycursor.close()
        mydb.close()
        return encompassing_encounter

    def getPersonInformationFromEncompassingEncounter(self, document_id):
        mydb = self.connectToELGA()
        mycursor = mydb.cursor()
        sql = "SELECT assignedEntity_id, person_name FROM encompassingEncounter WHERE id IN (SELECT encompassingEncounter FROM ClinicalDocument_CDAEntlassungsbrief_A WHERE id = %s);"
        val = (document_id,)
        mycursor.execute(sql, val)
        person_information = mycursor.fetchone()
        mydb.commit()
        mycursor.close()
        mydb.close()
        return person_information

    def getPCPFromELGA(self, document_id):
        mydb = self.connectToELGA()
        mycursor = mydb.cursor()
        sql = "SELECT * FROM participant WHERE id_participant IN (SELECT Participant_ID FROM ClinicalDocument_has_participant WHERE ClinicalDocument_ID = %s) AND functionCode = 'PCP'"
        val = (document_id,)
        mycursor.execute(sql, val)
        pcp = mycursor.fetchone()
        mydb.commit()
        mycursor.close()
        mydb.close()
        return pcp

    def getAuthorFromELGA(self, document_id):
        mydb = self.connectToELGA()
        mycursor = mydb.cursor()
        sql = "SELECT * FROM author WHERE author_id IN (SELECT author FROM ClinicalDocument_has_author WHERE ClinicalDocument = %s)"
        val = (document_id,)
        mycursor.execute(sql, val)
        author = mycursor.fetchone()
        mydb.commit()
        mycursor.close()
        mydb.close()
        return author

    def getLegalAuthenticatorFromELGA(self, document_id):
        mydb = self.connectToELGA()
        mycursor = mydb.cursor()
        sql = "SELECT * FROM legalAuthenticator WHERE id IN (SELECT legalAuthenticator FROM ClinicalDocument_CDAEntlassungsbrief_A WHERE id = %s)"
        val = (document_id,)
        mycursor.execute(sql, val)
        legalauthenticator = mycursor.fetchone()
        mydb.commit()
        mycursor.close()
        mydb.close()
        return legalauthenticator
    def get_service_events(self, document_id):
        mydb = self.connectToELGA()
        mycursor = mydb.cursor()
        sql = "SELECT * FROM serviceEvent WHERE CDA_ID = %s;"
        val = (document_id,)
        mycursor.execute(sql, val)
        service_events = mycursor.fetchall()
        mydb.commit()
        mycursor.close()
        mydb.close()
        return service_events

    def get_hospital_discharge_diagnoses(self, document_id):
        mydb = self.connectToELGA()
        mycursor = mydb.cursor()
        sql = "SELECT * FROM HD_entry_observation WHERE entry_ID IN (SELECT id FROM HD_entry WHERE HD_ID IN (SELECT sectionHospitalDischarge FROM ClinicalDocument_CDAEntlassungsbrief_A WHERE id = %s));"
        val = (document_id,)
        mycursor.execute(sql, val)
        hospital_discharge_diagnoses = mycursor.fetchall()
        mydb.commit()
        mycursor.close()
        mydb.close()
        return hospital_discharge_diagnoses

    def get_hospital_discharge_medications(self, document_id):
        mydb = self.connectToELGA()
        mycursor = mydb.cursor()
        sql = "SELECT * FROM EM_consumable JOIN EM_substanceAdministration em ON em.sA_id = EM_sA_id WHERE em.section_id IN (SELECT sectionEmpfohleneMedikation_Id FROM ClinicalDocument_CDAEntlassungsbrief_A WHERE id= %s);"
        val = (document_id,)
        mycursor.execute(sql, val)
        hospital_discharge_medications = mycursor.fetchall()
        mydb.commit()
        mycursor.close()
        mydb.close()
        return hospital_discharge_medications

    def get_vital_signs(self, document_id):
        mydb = self.connectToELGA()
        mycursor = mydb.cursor()
        sql = "SELECT * FROM EB_EO_observation WHERE entryOrganizer_ID IN (SELECT EO_id FROM EB_EntryOrganizer WHERE sectio_id IN (SELECT section_ErhobeneBefunde_Vitalparameter_Id FROM ClinicalDocument_CDAEntlassungsbrief_A WHERE id = %s));"
        val = (document_id,)
        mycursor.execute(sql, val)
        vital_signs = mycursor.fetchall()
        mydb.commit()
        mycursor.close()
        mydb.close()
        return vital_signs

    def get_medications_on_admission(self, document_id):
        mydb = self.connectToELGA()
        mycursor = mydb.cursor()
        #TBD supply
        sql = "SELECT * FROM MOA_substanceAdministration LEFT JOIN MOA_sA_consumable ON MOA_substanceAdministration.MOA_sA_id = MOA_sA_consumable.MOA_sA_id WHERE section_id IN (SELECT sectionMedicationOnAdmission FROM ClinicalDocument_CDAEntlassungsbrief_A WHERE id= %s);"
        val = (document_id,)
        mycursor.execute(sql, val)
        medications_on_admission = mycursor.fetchall()
        mydb.commit()
        mycursor.close()
        mydb.close()
        return medications_on_admission

    # Writes location to OMOP -> returns location_id
    def writeLocationForCareSite(self, document_id):
        location = self.getAddressFromEncompassingEncounter(document_id)
        location_id = self.omop.writeLocationToDB(location)
        return location_id

    def writeLocationForCareSite_addr_id(self, addr_id):
        location = self.getAddressFromAddressID(addr_id)
        location_id = self.omop.writeLocationToDB(location)
        return location_id

    # Writes CareSite from encompassing Encounter to DB -> returns care_site_id
    def writeCareSiteToDB(self, document_id):
        location_id = self.writeLocationForCareSite(document_id)
        care_site_information = self.getLocationInformationFromEncompassingEncounter(document_id)
        care_site_id = self.omop.writeCareSite_ToDB(care_site_information, location_id, 9201)
        return care_site_id

    def writeCareSiteToDB_author(self, author):
        location_id = self.writeLocationForCareSite_addr_id(author[11])
        # author[9] - ID & author[10] - name
        care_site_information = [author[9], author[10]]
        care_site_id = self.omop.writeCareSite_ToDB(care_site_information, location_id, 0)
        return care_site_id

    def writeCareSiteToDB_legalAuthenticator(self, legalaut):
        location_id = self.writeLocationForCareSite_addr_id(legalaut[8])
        care_site_information = [legalaut[6], legalaut[7]]
        care_site_id = self.omop.writeCareSite_ToDB(care_site_information, location_id, 0)
        return care_site_id

    # Write Provider From Encompassing Encounter to DB
    def writeProviderToDB(self, document_id):
        provider = self.getPersonInformationFromEncompassingEncounter(document_id)
        care_site_id = self.writeCareSiteToDB(document_id)
        provider_id = self.omop.writeProviderEncompassingEncounterToDB(provider, care_site_id)
        return provider_id, care_site_id

    def writeProviderToDB_author(self, author):
        care_site_id = self.writeCareSiteToDB_author(author)
        author = [None, None, None, author[4], author[3], author[5]]
        provider_id = self.omop.writeProviderToDB(author, care_site_id)
        return provider_id, care_site_id

    def writeProviderToDB_legalauthenticator(self, legalauthenticator):
        care_site_id = self.writeCareSiteToDB_legalAuthenticator(legalauthenticator)
        legalauthenticator = [None, None, None, None, legalauthenticator[3], legalauthenticator[5]]
        provider_id = self.omop.writeProviderToDB(legalauthenticator, care_site_id)
        return provider_id, care_site_id
    def writeProvider_Participant_PCP(self, document_id):
        pcp = self.getPCPFromELGA(document_id)
        pcp_address = self.getAddressFromAddressID(pcp[8])
        location_id = self.omop.writeLocationToDB(pcp_address)
        care_site = []
        care_site.append(pcp[6])
        care_site.append(pcp[7])
        care_site_id = self.omop.writeCareSite_ToDB(care_site, location_id, 38004693)
        provider_id = self.omop.writeProviderToDB(pcp, care_site_id)
        return provider_id, care_site_id

    def writePerson_PatientRole(self, document_id):
        provider_id, care_site_id = self.writeProvider_Participant_PCP(document_id)
        person = self.getPatientRoleFromELGA(document_id)
        person_address = self.getAddressFromAddressID(person[3])
        location_id = self.omop.writeLocationToDB(person_address)
        patient_id = self.omop.writePersonToDB(person, location_id, provider_id, care_site_id)
        return patient_id

    def writeLegalAuthenticator(self, document_id):
        legalauthenticator = self.getLegalAuthenticatorFromELGA(document_id)
        provider_id, care_site_id = self.writeProviderToDB_legalauthenticator(legalauthenticator)
        return provider_id

    # Write observation_period to DB from encompassing encounter
    def write_observation_period_from_encompassing_encounter(self, document_id, person_id, document_code):
        encompassing_encounter = self.getEncompassingEncounter(document_id)
        encompassing_encounter_id = self.omop.writeObservationPeriodToDB(encompassing_encounter, person_id, document_code)
        return encompassing_encounter_id

    # Write visit_occurence to DB from encompassing encounter
    def write_visit_occurence_from_encompassing_encounter(self, document_id, person_id, provider_id, care_site_id, document_code):
        encompassing_encounter = self.getEncompassingEncounter(document_id)
        visit_id = self.omop.writeVisitOccurenceToDB(encompassing_encounter, person_id, provider_id, care_site_id, None, document_code)
        return visit_id

    def write_procedure_from_service_event(self, document_id, person, provider_id, visit_id, document_code):
        service_events = self.get_service_events(document_id)
        for s in service_events:
            procedure = [s[1], s[2], s[3]]
            code = [s[1], self.getCodeFromELGA(s[1])[2]]
            if s[5] is not None:
                if s[8] is not None:
                    addr = self.getAddressFromAddressID(s[9])
                    addr_id = self.omop.writeLocationToDB(addr)
                    care_site = [s[7], s[8]]
                    care_site_id = self.omop.writeCareSite_ToDB(care_site, addr_id, 0)
                    provider = [None, None, None, None, "UNK", s[5]]
                    provider_id = self.omop.writeProviderToDB(provider, care_site_id)
                    self.omop.writeProcedureOccurenceToDB(procedure, person, provider_id, visit_id, code, document_code)
            else:
                self.omop.writeProcedureOccurenceToDB(procedure, person, provider_id, visit_id, code, document_code)

    def write_condition_occurence_from_hospital_discharge(self, document_id, person, visit_id, legal_authenticator_id):
        hospital_discharge_diagnoses = self.get_hospital_discharge_diagnoses(document_id)
        for hdd in hospital_discharge_diagnoses:
            self.omop.writeConditionOccurenceToDB(person, legal_authenticator_id, visit_id, hdd)

    def write_hospital_discharge_medications(self, document_id, person, visit_id, legal_authenticator_id):
        hospital_discharge_medications = self.get_hospital_discharge_medications(document_id)
        for hdm in hospital_discharge_medications:
            start_date = hdm[15]
            end_date = hdm[16]
            if start_date is None:
                start_date = self.getDocumentDate(document_id)[0]
            if end_date is None:
                if hdm[7] == "{Stueck}":
                    end_date = start_date + datetime.timedelta(days=int(hdm[6]))
                else:
                    end_date = start_date

            drug = {
                "code": hdm[3],
                "pharmcode": hdm[8],
                "start": start_date,
                "end": end_date,
                "drug_type": 32824, #EHR discharge summary
                "refill": hdm[17],
                "quantity": hdm[6],
                "route": hdm[18],
                "quantity_unit": hdm[7]
            }
            self.omop.writeDrugExposureToDB(person, legal_authenticator_id, visit_id, drug)

    def write_vital_signs(self, document_id, person, visit_id, document_code, legal_authenticator_id):
        vital_signs = self.get_vital_signs(document_id)
        for vital_s in vital_signs:
            self.omop.writeMeasurementToDB(vital_s, person, legal_authenticator_id, visit_id, None, document_code)

    def write_medication_on_admission(self, document_id, person, visit_id, legal_authenticator_id):
        medications_on_admission = self.get_medications_on_admission(document_id)
        #TODO medication on admission
        pass

    def write_medicaldischarge_to_OMOP(self, document_id, document_code):
        provider_id, care_site_id = self.writeProviderToDB(document_id)
        print("GDA & organisation (encompassingEncounter) -> OMOP provider/care_site")
        pcp_id = self.writeProvider_Participant_PCP(document_id)
        print("Hausarzt -> OMOP provider")
        person = self.writePerson_PatientRole(document_id)
        print("Patient -> OMOP person")
        legal_authenticator_id = self.writeLegalAuthenticator(document_id)
        observation_period_id = self.write_observation_period_from_encompassing_encounter(document_id, person, document_code)
        print("encompassingEncounter Zeit -> OMOP observation_period")
        visit_id = self.write_visit_occurence_from_encompassing_encounter(document_id, person, provider_id, care_site_id,document_code)
        print("encompassingEncounter GDA & organisation -> OMOP visit_occurence")
        self.write_procedure_from_service_event(document_id, person, provider_id, visit_id, document_code)
        print("serviceEvents -> OMOP procedure_occurence")
        # Hospital Discharge
        self.write_condition_occurence_from_hospital_discharge(document_id, person, visit_id, legal_authenticator_id)
        print("hospital discharge -> OMOP condition_occurence")
        # Hospital discharge medications
        self.write_hospital_discharge_medications(document_id, person, visit_id, legal_authenticator_id)
        print("hospital discharge medications-> OMOP drug_exposure")
        # vital signs
        self.write_vital_signs(document_id, person, visit_id, document_code, legal_authenticator_id)
        print("vital signs -> OMOP measurement")
        # Medications on admission
        self.write_medication_on_admission(document_id, person, visit_id, legal_authenticator_id)
        print("medication on admission-> OMOP drug_exposure")














