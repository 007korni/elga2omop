import mysql.connector
from OMOP import OMOP_Writer

class LaboratorReportToOMOP:

    def __init__(self):
        self.documentDatabase = self.connectToELGA()
        self.OMOPdatabase = self.connectToOMOP()
        self.omop = OMOP_Writer.OMOPWriter()

    # Connection to Document database - ELGA
    def connectToELGA(self):
        mydb = mysql.connector.connect(
            host="localhost",
            user="root",
            password="password",
            database="CDA_Laborbefunde"
        )
        return mydb

    # Connection to OMOP database
    def connectToOMOP(self):
        return OMOP_Writer.OMOPWriter().connectToOMOP()

    def getPatientRoleFromELGA(self, document_id):
        mydb = self.connectToELGA()
        mycursor = mydb.cursor()
        sql = "SELECT * FROM patientRole WHERE id_SVNR IN (SELECT recordTarget FROM ClinicalDocument_CDALaborbefund WHERE id = %s);"
        val = (document_id,)
        mycursor.execute(sql, val)
        patient = mycursor.fetchone()
        mydb.commit()
        mycursor.close()
        mydb.close()
        return patient

    def getCodeFromELGA(self, code):
        mydb = self.connectToELGA()
        mycursor = mydb.cursor()
        sql = "SELECT * FROM code WHERE code = %s;"
        val = (code,)
        mycursor.execute(sql, val)
        code = mycursor.fetchone()
        mydb.commit()
        mycursor.close()
        mydb.close()
        return code

    def getAddressFromAddressID(self, addr_id):
        mydb = self.connectToELGA()
        mycursor = mydb.cursor()
        sql = "SELECT * FROM addr WHERE id_addr = %s"
        val = (addr_id,)
        mycursor.execute(sql, val)
        address = mycursor.fetchone()
        mydb.commit()
        mycursor.close()
        mydb.close()
        return address

    # Required for care_site -> returns address from location element
    def getAddressFromEncompassingEncounter(self, document_id):
        mydb = self.connectToELGA()
        mycursor = mydb.cursor()
        sql = "SELECT * FROM addr WHERE id_addr IN (SELECT location_addr FROM encompassingEncounter WHERE id IN (SELECT encompassingEncounter FROM ClinicalDocument_CDALaborbefund WHERE id = %s));"
        val = (document_id, )
        mycursor.execute(sql, val)
        address = mycursor.fetchone()
        mydb.commit()
        mycursor.close()
        mydb.close()
        return address

    # Required for care_site -> returns location_id and location_name
    def getLocationInformationFromEncompassingEncounter(self, document_id):
        mydb = self.connectToELGA()
        mycursor = mydb.cursor()
        sql = "SELECT location_id, location_name FROM encompassingEncounter WHERE id IN (SELECT encompassingEncounter FROM ClinicalDocument_CDALaborbefund WHERE id = %s);"
        val = (document_id,)
        mycursor.execute(sql, val)
        location_information = mycursor.fetchone()
        mydb.commit()
        mycursor.close()
        mydb.close()
        return location_information

    def getEncompassingEncounter(self, document_id):
        mydb = self.connectToELGA()
        mycursor = mydb.cursor()
        sql = "SELECT * FROM encompassingEncounter WHERE id IN (SELECT encompassingEncounter FROM ClinicalDocument_CDALaborbefund WHERE id = %s);"
        val = (document_id, )
        mycursor.execute(sql, val)
        encompassing_encounter = mycursor.fetchone()
        mydb.commit()
        mycursor.close()
        mydb.close()
        return encompassing_encounter

    def getPersonInformationFromEncompassingEncounter(self, document_id):
        mydb = self.connectToELGA()
        mycursor = mydb.cursor()
        sql = "SELECT assignedEntity_id, person_name FROM encompassingEncounter WHERE id IN (SELECT encompassingEncounter FROM ClinicalDocument_CDALaborbefund WHERE id = %s);"
        val = (document_id,)
        mycursor.execute(sql, val)
        person_information = mycursor.fetchone()
        mydb.commit()
        mycursor.close()
        mydb.close()
        return person_information

    def getPCPFromELGA(self, document_id):
        mydb = self.connectToELGA()
        mycursor = mydb.cursor()
        sql = "SELECT * FROM participant WHERE id_participant IN (SELECT Participant_ID FROM ClinicalDocument_has_participant WHERE ClinicalDocument_ID = %s) AND functionCode = 'PCP'"
        val = (document_id,)
        mycursor.execute(sql, val)
        pcp = mycursor.fetchone()
        mydb.commit()
        mycursor.close()
        mydb.close()
        return pcp

    # Get all specimen with anatomic site
    def getSpecimenFromELGA(self, document_id):
        mydb = self.connectToELGA()
        mycursor = mydb.cursor()
        sql = "SELECT * FROM participant_specimen JOIN code c on participant_specimen.code = c.code JOIN lab_procedure lp on lp.id = participant_specimen.procedure_id WHERE procedure_id IN (SELECT id FROM lab_procedure WHERE section_specimen IN (SELECT id FROM section_Specimen WHERE id IN (SELECT sectionSpecimen_Id FROM ClinicalDocument_CDALaborbefund WHERE id = %s)));"
        val = (document_id,)
        mycursor.execute(sql, val)
        specimens = mycursor.fetchall()
        mydb.commit()
        mycursor.close()
        mydb.close()
        return specimens

    def get_service_events(self, document_id):
        mydb = self.connectToELGA()
        mycursor = mydb.cursor()
        sql = "SELECT * FROM serviceEvent WHERE LaborbefundID = %s;"
        val = (document_id,)
        mycursor.execute(sql, val)
        service_events = mycursor.fetchall()
        mydb.commit()
        mycursor.close()
        mydb.close()
        return service_events

    def getAllMeasurements(self, document_id):
        mydb = self.connectToELGA()
        mycursor = mydb.cursor()
        sql = "SELECT * FROM observation WHERE entryRelationship_ID IN (SELECT id_entryRelationship FROM entryRelationship WHERE speciality_section_id IN (SELECT id FROM section_speciality WHERE Laborbefund_Id = %s));"
        val = (document_id,)
        mycursor.execute(sql, val)
        measurements = mycursor.fetchall()
        mydb.commit()
        mycursor.close()
        mydb.close()
        return measurements

    # Writes location to OMOP -> returns location_id
    def writeLocationForCareSite(self, document_id):
        location = self.getAddressFromEncompassingEncounter(document_id)
        location_id = self.omop.writeLocationToDB(location)
        return location_id

    # Writes CareSite from encompassing Encounter to DB -> returns care_site_id
    def writeCareSiteToDB(self, document_id):
        location_id = self.writeLocationForCareSite(document_id)
        care_site_information = self.getLocationInformationFromEncompassingEncounter(document_id)
        care_site_id = self.omop.writeCareSite_ToDB(care_site_information, location_id, 9201)
        return care_site_id

    # Write Provider From Encompassing Encounter to DB
    def writeProviderToDB(self, document_id):
        provider = self.getPersonInformationFromEncompassingEncounter(document_id)
        care_site_id = self.writeCareSiteToDB(document_id)
        provider_id = self.omop.writeProviderEncompassingEncounterToDB(provider, care_site_id)
        return provider_id, care_site_id

    def writeProvider_Participant_PCP(self, document_id):
        pcp = self.getPCPFromELGA(document_id)
        pcp_address = self.getAddressFromAddressID(pcp[8])
        location_id = self.omop.writeLocationToDB(pcp_address)
        care_site = []
        care_site.append(pcp[6])
        care_site.append(pcp[7])
        care_site_id = self.omop.writeCareSite_ToDB(care_site, location_id, 38004693)
        provider_id = self.omop.writeProviderToDB(pcp, care_site_id)
        return provider_id, care_site_id

    def writePerson_PatientRole(self, document_id):
        provider_id, care_site_id = self.writeProvider_Participant_PCP(document_id)
        person = self.getPatientRoleFromELGA(document_id)
        person_address = self.getAddressFromAddressID(person[3])
        location_id = self.omop.writeLocationToDB(person_address)
        patient_id = self.omop.writePersonToDB(person, location_id, provider_id, care_site_id)
        return patient_id

    # Write observation_period to DB from encompassing encounter
    def write_observation_period_from_encompassing_encounter(self, document_id, person_id, document_code):
        encompassing_encounter = self.getEncompassingEncounter(document_id)
        encompassing_encounter_id = self.omop.writeObservationPeriodToDB(encompassing_encounter, person_id, document_code)
        return encompassing_encounter_id

    # Write visit_occurence to DB from encompassing encounter
    def write_visit_occurence_from_encompassing_encounter(self, document_id, person_id, provider_id, care_site_id, document_code):
        encompassing_encounter = self.getEncompassingEncounter(document_id)
        visit_id = self.omop.writeVisitOccurenceToDB(encompassing_encounter, person_id, provider_id, care_site_id, None, document_code)
        return visit_id

    def write_specimen_to_DB(self, document_id, person, provider, visit, document_code):
        all_specimen = self.getSpecimenFromELGA(document_id)
        specimen_ids = []
        code = ["10", "Probeninformation"]
        for specimen in all_specimen:
            specimen_ids.append(self.omop.writeSpecimenToDB(specimen, person, document_code))
            procedure = [None, specimen[9], specimen[9]]
            self.write_specimen_procedure_to_DB(procedure, person, provider, visit, code, document_code)
        return specimen_ids

    def write_specimen_procedure_to_DB(self, procedure, person, provider, visit_id, code, document_code):
        self.omop.writeProcedureOccurenceToDB(procedure, person, provider, visit_id, code, document_code)

    def write_procedure_from_service_event(self, document_id, person, provider_id, visit_id, document_code):
        service_events = self.get_service_events(document_id)
        for s in service_events:
            procedure = [s[1], s[2], s[3]]
            code = [s[1], self.getCodeFromELGA(s[1])[2]]
            if s[5] is not None:
                if s[8] is not None:
                    addr = self.getAddressFromAddressID(s[9])
                    addr_id = self.omop.writeLocationToDB(addr)
                    care_site = [s[7], s[8]]
                    care_site_id = self.omop.writeCareSite_ToDB(care_site, addr_id, 0)
                    provider = [None, None, None, None, "UNK", s[5]]
                    provider_id = self.omop.writeProviderToDB(provider, care_site_id)
                    self.omop.writeProcedureOccurenceToDB(procedure, person, provider_id, visit_id, code, document_code)
            else:
                self.omop.writeProcedureOccurenceToDB(procedure, person, provider_id, visit_id, code, document_code)

    def write_measurement_from_oberservation(self, document_id, person_id, provider_id, visit_occurence_id, visit_detail_id, document_code):
        measurements = self.getAllMeasurements(document_id)
        measurement_ids = []
        for m in measurements:
            m_id = self.omop.writeMeasurementToDB(m, person_id, provider_id, visit_occurence_id, visit_detail_id, document_code)
            measurement_ids.append(m_id)
        return measurement_ids

    def write_labreport_to_OMOP(self, document_id, document_code):
        provider_id, care_site_id = self.writeProviderToDB(document_id)
        print("GDA & organisation (encompassingEncounter) -> OMOP provider/care_site")
        pcp_id = self.writeProvider_Participant_PCP(document_id)
        print("Hausarzt -> OMOP provider")
        person = self.writePerson_PatientRole(document_id)
        print("Patient -> OMOP person")
        observation_period_id = self.write_observation_period_from_encompassing_encounter(document_id, person, document_code)
        print("ecnompassingEncounter Zeit -> OMOP observation_period")
        visit_id = self.write_visit_occurence_from_encompassing_encounter(document_id, person, provider_id, care_site_id,document_code)
        print("ecnompassingEncounter GDA & organisation -> OMOP visit_occurence")
        specimen_ids = self.write_specimen_to_DB(document_id, person, provider_id, visit_id,document_code)
        print("specimen -> OMOP specimen")
        procedure_ids = self.write_procedure_from_service_event(document_id, person,provider_id, visit_id, document_code)
        print("serviceEvents -> OMOP procedure_occurence")
        measurement_ids = self.write_measurement_from_oberservation(document_id, person, provider_id, visit_id, None, document_code)
        print("observations -> OMOP measurement")