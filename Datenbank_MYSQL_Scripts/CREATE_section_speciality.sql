CREATE TABLE `section_speciality` (
  `id` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `Laborbefund_Id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_speciality_code_idx` (`code`),
  KEY `fk_speciality_templateId_idx` (`templateId`),
  KEY `fk_speciality_CDALab_idx` (`Laborbefund_Id`),
  CONSTRAINT `fk_speciality_CDALab` FOREIGN KEY (`Laborbefund_Id`) REFERENCES `ClinicalDocument_CDALaborbefund` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_speciality_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_speciality_templateId` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
