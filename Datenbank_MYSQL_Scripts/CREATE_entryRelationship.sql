CREATE TABLE `entryRelationship` (
  `id_entryRelationship` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(50) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `speciality_section_id` int DEFAULT NULL,
  PRIMARY KEY (`id_entryRelationship`),
  KEY `fk_entryRel_template_idx` (`templateId`),
  KEY `fk_entryRel_code_idx` (`code`),
  KEY `fk_entryRel_speciality_idx` (`speciality_section_id`),
  CONSTRAINT `fk_entryRel_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_entryRel_speciality` FOREIGN KEY (`speciality_section_id`) REFERENCES `section_speciality` (`id`),
  CONSTRAINT `fk_entryRel_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
