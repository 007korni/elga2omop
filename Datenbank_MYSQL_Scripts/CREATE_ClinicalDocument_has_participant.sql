CREATE TABLE `ClinicalDocument_has_participant` (
  `idClinicalDocument_has_participant` int NOT NULL AUTO_INCREMENT,
  `ClinicalDocument_ID` varchar(45) DEFAULT NULL,
  `Participant_ID` int DEFAULT NULL,
  PRIMARY KEY (`idClinicalDocument_has_participant`),
  KEY `fk_CHP_CDALAb_idx` (`ClinicalDocument_ID`),
  KEY `fk_CHP_Part_idx` (`Participant_ID`),
  CONSTRAINT `fk_CHP_CDALAb` FOREIGN KEY (`ClinicalDocument_ID`) REFERENCES `ClinicalDocument_CDALaborbefund` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CHP_Part` FOREIGN KEY (`Participant_ID`) REFERENCES `participant` (`id_participant`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
