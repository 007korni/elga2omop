CREATE TABLE `templateId` (
  `root` varchar(45) NOT NULL,
  `assigningAuthorityName` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`root`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
