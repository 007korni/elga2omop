CREATE TABLE `section_Brieftext` (
  `id_section` int NOT NULL AUTO_INCREMENT,
  `templateId` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `text` varchar(10000) DEFAULT NULL,
  `entry` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `fk_Brieftext_template_idx` (`templateId`),
  KEY `fk_Brieftext_code_idx` (`code`),
  CONSTRAINT `fk_Brieftext_code` FOREIGN KEY (`code`) REFERENCES `code` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Brieftext_template` FOREIGN KEY (`templateId`) REFERENCES `templateId` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
