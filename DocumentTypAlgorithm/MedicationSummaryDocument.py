from Database_helper import MedicationSummaryDocument_to_DB
from DocumentTypAlgorithm import CDA_R2_Header


class MedicationSummaryDocument(CDA_R2_Header.CDA_R2_Header):

    def __init__(self, root):
        super().__init__(root)
        self.root = root
        self.section = self.getHistoryOfMedicationUse()
        self.debug = None

    def getHistoryOfMedicationUse(self):
        section_element = self.root.find('{urn:hl7-org:v3}component/{urn:hl7-org:v3}structuredBody/{urn:hl7-org:v3}component/{urn:hl7-org:v3}section')
        section = Section_HistoryOfMedicationUse()
        section.extractFromXML(section_element)
        return section

    def writeMedicationSummaryDocumentToDB(self):
        if (MedicationSummaryDocument_to_DB.documentCheck(self.document.document_id)):
            print("Dokument mit ID: " + self.document.document_id + " bereits in DB vorhanden")
        else:
            MedicationSummaryDocument_to_DB.writeCodesToDB(self.all_codes)
            MedicationSummaryDocument_to_DB.writeTemplateIDsToDB(self.all_templateIds)
            MedicationSummaryDocument_to_DB.writeAdressesToDB(self.all_addresses)
            MedicationSummaryDocument_to_DB.writePatientToDB(self.patient)
            section_id = MedicationSummaryDocument_to_DB.writeHistoryOfMedicationUseToDB(self.section)
            MedicationSummaryDocument_to_DB.writeMedicationSummaryDocumentToDB(self.document, self.patient.SVNR.attrib["extension"], section_id)
            MedicationSummaryDocument_to_DB.writeDocumentTemplatesToDB(self.document)
            MedicationSummaryDocument_to_DB.writeAuthorToDB(self.authors, self.document.document_id)

class Section_HistoryOfMedicationUse:

    def __init__(self):
        self.templateIds = []
        self.id = None
        self.code = None
        self.title = None
        self.text = None
        # Entry -> substanceAdministration
        self.substanceAdministrations = []
        # Entry -> supply
        self.supplies = []
        # Alle Entry - Elemente
        self.entries = []

    def extractFromXML(self, section_element):
        for s in section_element:
            if s.tag == '{urn:hl7-org:v3}templateId':
                self.templateIds.append(s.attrib)
            elif s.tag == '{urn:hl7-org:v3}id':
                self.id = s.attrib
            elif s.tag == '{urn:hl7-org:v3}code':
                self.code = s.attrib["code"]
            elif s.tag == '{urn:hl7-org:v3}title':
                self.title = s.text
            elif s.tag == '{urn:hl7-org:v3}text':
                self.text = s.text
            elif s.tag == '{urn:hl7-org:v3}entry':
                self.entries.append(s)
                self.extractEntry(s)

    def extractEntry(self, entry_element):
        for child in entry_element:
            if child.tag == '{urn:hl7-org:v3}substanceAdministration':
                substanceadministration = Section_HistoryOfMedicationUse.substanceAdministration()
                substanceadministration.extractSubstanceAdministrationFromSection(child)
                self.substanceAdministrations.append(substanceadministration)
            elif child.tag == '{urn:hl7-org:v3}supply':
                supply = Section_HistoryOfMedicationUse.Supply()
                supply.extractSupplyFromXML(child)
                self.supplies.append(supply)

    class substanceAdministration():

        def __init__(self):
            self.templateIDs = []
            self.id = ""  # Wird zu dictonary
            self.statusCode = None
            self.effectiveTime_low = None  # Datum der Einnahme
            self.effectiveTime_high = None
            self.effectiveTime_period_value = None
            self.effectiveTime_period_unit = None
            self.effectiveTime_width_value = None
            self.effectiveTime_width_unit = None
            self.effectiveTime_event_code = None
            self.effectiveTime_offset_value = None
            self.effectiveTime_offset_unit = None
            self.dosing = ""
            self.repeatNumber = None
            self.routeCode = None
            self.doseQuantity = ""
            self.consumable = ""
            self.entryRealtionship_supply_independentInd = ""
            self.entryRealtionship_supply_quantity = ""
            self.entryRealtionship_act_templateID = ""
            self.entryRealtionship_act_code = None
            self.entryRealtionship_dosing = dict()

        def extractSubstanceAdministrationFromSection(self, substanceAdministrationElement):
            for e in substanceAdministrationElement:
                if e.tag == "{urn:hl7-org:v3}templateId":
                    self.templateIDs.append(e.attrib["root"])
                elif e.tag == "{urn:hl7-org:v3}id":
                    self.id = e.attrib
                elif e.tag == "{urn:hl7-org:v3}statusCode":
                    self.statusCode = e.attrib["code"]
                elif e.tag == "{urn:hl7-org:v3}effectiveTime":
                    if e.find("{urn:hl7-org:v3}period") is not None:
                        period_element = e.find("{urn:hl7-org:v3}period")
                        self.effectiveTime_period_value = period_element.attrib["value"]
                        self.effectiveTime_period_unit = period_element.attrib["unit"]
                    elif e.find("{urn:hl7-org:v3}width") is not None:
                        width_element = e.find("{urn:hl7-org:v3}width")
                        self.effectiveTime_width_value = width_element.attrib["value"]
                        self.effectiveTime_width_unit = width_element.attrib["unit"]
                    elif e.find("{urn:hl7-org:v3}event") is not None:
                        event_element = e.find("{urn:hl7-org:v3}event")
                        offset_element = e.find("{urn:hl7-org:v3}offset")
                        self.effectiveTime_event_code = event_element.attrib["code"]
                        self.effectiveTime_offset_value = offset_element.attrib["value"]
                        self.effectiveTime_offset_unit = offset_element.attrib["unit"]
                    else:
                        self.extractEffectiveTime(e)
                elif e.tag == "{urn:hl7-org:v3}repeatNumber":
                    self.repeatNumber = e.attrib["value"]
                elif e.tag == "{urn:hl7-org:v3}routeCode":
                    self.routeCode = e.attrib["code"]
                elif e.tag == "{urn:hl7-org:v3}doseQuantity":
                    self.doseQuantity = e.attrib["value"]
                elif e.tag == "{urn:hl7-org:v3}consumable":
                    self.consumable = self.Consumable()
                    self.consumable.extractConsumable(e)
                elif e.tag == "{urn:hl7-org:v3}entryRelationship":
                    for er in e:
                        if er.tag == "{urn:hl7-org:v3}supply":
                            self.extractEntryRelationshipSupply(er)
                        elif er.tag == "{urn:hl7-org:v3}act":
                            self.extractEntryRelationshipAct(er)
                        else:
                            self.extractEntryRelationshipDosing(e)

            self.checkDosingForm(self.templateIDs)

        def checkDosingForm(self, templateIDs):
            for templateID in templateIDs:
                if templateID == "1.3.6.1.4.1.19376.1.5.3.1.4.7.1":
                    self.dosing = "Normal dosing"
                    break
                elif templateID == "1.3.6.1.4.1.19376.1.5.3.1.4.9":
                    self.dosing = "Split dosing"
                    break
                else:
                    self.dosing = None

        def extractEntryRelationshipSupply(self, supply_element):
            for s in supply_element:
                if s.tag == "{urn:hl7-org:v3}independentInd":
                    self.entryRealtionship_supply_independentInd = s.attrib["value"]
                elif s.tag == "{urn:hl7-org:v3}quantity":
                    self.entryRealtionship_supply_quantity = s.attrib["value"]

        def extractEntryRelationshipAct(self, act_element):
            for a in act_element:
                if a.tag == "{urn:hl7-org:v3}templateId":
                    self.entryRealtionship_act_templateID = a.attrib["root"]
                elif a.tag == "{urn:hl7-org:v3}code":
                    self.entryRealtionship_act_code = a.attrib["code"]

        def extractEntryRelationshipDosing(self, entryRel_element):
            key = None
            value = None
            for eR_e in entryRel_element:
                if eR_e.tag == "{urn:hl7-org:v3}sequenceNumber":
                    key = eR_e.attrib["value"]
                elif eR_e.tag == "{urn:hl7-org:v3}substanceAdministration":
                    substanceadministration = Section_HistoryOfMedicationUse().substanceAdministration()
                    substanceadministration.extractSubstanceAdministrationFromSection(eR_e)
                    value = substanceadministration
                    self.entryRealtionship_dosing[key] = value

        def extractEffectiveTime(self, time_element):
            try:
                self.effectiveTime_low = time_element.find("{urn:hl7-org:v3}low").attrib["value"]
                if len(self.effectiveTime_low) > 14:
                    self.effectiveTime_low = self.effectiveTime_low[0:14]
                elif len(self.effectiveTime_low) == 8:
                    self.effectiveTime_low = self.effectiveTime_low[0:8]
                elif len(self.effectiveTime_low) == 6:
                    self.effectiveTime_low = self.effectiveTime_low[0:6] + "01"
                    try:
                        self.effectiveTime_high = time_element.find("{urn:hl7-org:v3}high").attrib["value"]
                    except:
                        self.effectiveTime_high = self.effectiveTime_low[0:6] + "30"
                else:
                    self.effectiveTime_low = self.effectiveTime_low[0:4]
            except:
                self.effectiveTime_low = None
            try:
                self.effectiveTime_high = time_element.find("{urn:hl7-org:v3}high").attrib["value"]
                if len(self.effectiveTime_high) > 14:
                    self.effectiveTime_high = self.effectiveTime_high[0:14]
                elif len(self.effectiveTime_high) == 8:
                    self.effectiveTime_high = self.effectiveTime_high[0:8]
                elif len(self.effectiveTime_high) == 6:
                    self.effectiveTime_high = self.effectiveTime_high[0:6] + "01"
                else:
                    self.effectiveTime_high = self.effectiveTime_high[0:4]
            except:
                self.effectiveTime_high = None

        class Consumable():

            def __init__(self):
                self.templateIDs = []
                self.manufacturedMaterial_templateIDs = []
                self.manufacturedMaterial_code = None
                self.manufacturedMaterial_name = None
                self.formCode = None
                self.capacityQuantity = None
                self.capacityUnit = None
                self.pharmCode = None
                self.pharmCode2 = None
                self.pharmName = None
                self.pharmName2 = None

            def extractConsumable(self, consumable_element):
                for e in (consumable_element.find("{urn:hl7-org:v3}manufacturedProduct")):
                    if e.tag == "{urn:hl7-org:v3}templateId":
                        self.templateIDs.append(e.attrib["root"])
                    elif e.tag == "{urn:hl7-org:v3}manufacturedMaterial":
                        for m in e:
                            if m.tag == "{urn:hl7-org:v3}templateId":
                                self.manufacturedMaterial_templateIDs.append(m.attrib["root"])
                            elif m.tag == "{urn:hl7-org:v3}code":
                                try:
                                    self.manufacturedMaterial_code = m.attrib["code"]
                                except:
                                    self.manufacturedMaterial_code = m.attrib["nullFlavor"]
                            elif m.tag == "{urn:hl7-org:v3}name":
                                self.manufacturedMaterial_name = m.text
                            elif m.tag == "{urn:ihe:pharm:medication}formCode":
                                self.formCode = m.attrib["code"]
                            elif m.tag == "{urn:ihe:pharm:medication}asContent":
                                quantitiy_element = m.find(".//{urn:ihe:pharm:medication}capacityQuantity")
                                self.capacityQuantity = quantitiy_element.attrib["value"]
                                self.capacityUnit = quantitiy_element.attrib["unit"]
                            elif m.tag == "{urn:ihe:pharm:medication}ingredient":
                                code = m.find(".//{urn:ihe:pharm:medication}code")
                                if self.pharmCode is None:
                                    self.pharmCode = code.attrib["code"]
                                else:
                                    self.pharmCode2 = code.attrib["code"]
                                name = m.find(".//{urn:ihe:pharm:medication}name")
                                if self.pharmName is None:
                                    self.pharmName = name.text
                                else:
                                    self.pharmName2 = name.text


    class Supply():

        def __init__(self):
            self.templateIDs = []
            self.id = ""
            self.quantity = ""
            self.product = ""
            self.entryRelationship_elements = []
            self.entryRelationships = []

        def extractSupplyFromXML(self, supply_element):
            for s in supply_element:
                if s.tag == "{urn:hl7-org:v3}templateId":
                    self.templateIDs.append(s.attrib["root"])
                elif s.tag == "{urn:hl7-org:v3}id":
                    self.id = s.attrib["extension"]
                elif s.tag == "{urn:hl7-org:v3}quantity":
                    self.quantity = s.attrib["value"]
                elif s.tag == "{urn:hl7-org:v3}product":
                    self.product = self.Product()
                    self.product.extractProductFromSupply(s)
                elif s.tag == "{urn:hl7-org:v3}entryRelationship":
                    self.entryRelationship_elements.append(s)
            self.extractEntryRelationships(self.entryRelationship_elements)

        def extractEntryRelationships(self, entryRelationship_elements):
            for eR_e in entryRelationship_elements:
                suA = eR_e.find("{urn:hl7-org:v3}substanceAdministration")
                if suA is not None:
                    substanceadministration = Section_HistoryOfMedicationUse().substanceAdministration()
                    substanceadministration.extractSubstanceAdministrationFromSection(suA)
                    self.entryRelationships.append(substanceadministration)

        class Product:

            def __init__(self):
                self.manufacturedProduct_templateIDs = []
                self.manufacturedMaterial_templateIDs = []
                self.manufacturedMaterial_code = ""
                self.manufacturedMaterial_name = ""
                self.manufacturedMaterial_pharm_formCode = ""
                self.manufacturedMaterial_pharm_capacityQuantity = ""
                self.manufacturedMaterial_pharm_ingredient_code = ""
                self.manufacturedMaterial_pharm_ingredient_name = ""

            def extractProductFromSupply(self, product_element):
                manufacturedProduct_element = product_element.find("{urn:hl7-org:v3}manufacturedProduct")
                manufacturedMaterial_element = ""
                for p in manufacturedProduct_element:
                    if p.tag == "{urn:hl7-org:v3}templateId":
                        self.manufacturedProduct_templateIDs.append(p.attrib["root"])
                    elif p.tag == "{urn:hl7-org:v3}manufacturedMaterial":
                        manufacturedMaterial_element = p

                for m in manufacturedMaterial_element:
                    if m.tag == "{urn:hl7-org:v3}templateId":
                        self.manufacturedMaterial_templateIDs.append(m.attrib["root"])
                    elif m.tag == "{urn:hl7-org:v3}code":
                        self.manufacturedMaterial_code = m.attrib["code"]
                    elif m.tag == "{urn:hl7-org:v3}name":
                        self.manufacturedMaterial_name = m.text
                    elif m.tag == "{urn:ihe:pharm:medication}formCode":
                        self.manufacturedMaterial_pharm_formCode = m.attrib["code"]
                    elif m.tag == "{urn:ihe:pharm:medication}asContent":
                        self.manufacturedMaterial_pharm_capacityQuantity = \
                        m.find(".//{urn:ihe:pharm:medication}capacityQuantity").attrib["value"]
                    elif m.tag == "{urn:ihe:pharm:medication}ingredient":
                        self.manufacturedMaterial_pharm_ingredient_code = \
                        m.find(".//{urn:ihe:pharm:medication}code").attrib["code"]
                        self.manufacturedMaterial_pharm_ingredient_name = m.find(
                            ".//{urn:ihe:pharm:medication}name").text