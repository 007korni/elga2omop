from Database_helper import NurseDischargeSummary_to_DB
from DocumentTypAlgorithm import CDA_R2_Header
from Element_Extractors import Section_Brieftext_Extractor
from Element_Extractors.NurseDischargeSummary_Elements import section_MD_allg_Extractor, section_Vitalparameter, section_Entlassungsmanagement, section_Pflegediagnosen


class NurseDischargeSummary(CDA_R2_Header.CDA_R2_Header):

    def __init__(self, root):
        super().__init__(root)
        self.root = root
        self.brieftextSection = self.getSection_Brieftext()  # done
        self.pflegediagnosenSection = self.getSection_Pflegediagnosen()  # done
        self.mobilitaetSection_Hilfsmittel = None  # done -> wird über getSectionMobilität befüllt
        self.mobilitaetSection = self.getSection_Mobilität()  # done
        self.koerperpflegeSection = self.getSection_Körperpflege()  # done
        self.ernaehrungSection = self.getSection_Ernährung()  # done
        self.ausscheidungSection = self.getSection_Ausscheidung()  # done
        self.hautzustandSection = self.getSection_Hautzustand()  # done
        self.atmungSection = self.getSection_Atmung()  # done
        self.schlafSection = self.getSection_Schlaf()  # done
        self.schmerzSection = self.getSection_Schmerz()  # done
        self.orientierungSection_Risiken = None  # done -> wird über getSection_Orientierung befüllt
        self.orientierungSection = self.getSection_Orientierung()
        self.sozialeumstaendeSection = self.getSection_SozialeUmstände()  # done
        self.kommunikationSection_Hilfsmittel = None  # done -> wird über getSection_Kommunikation befüllt
        self.kommunikationSection = self.getSection_Kommunikation()  # done
        self.rollenwahrnehmungSection = self.getSection_Rollenwahrnehmung()  # done
        self.vitalparameterSection = self.getSection_Vitalparameter()  # done
        self.pflegerelevanteinformationenSection_Risiken = None  # done -> wird über getSection_PflegerelevanteInformationen befüllt
        self.pflegerelevanteinformationenSection = self.getSection_PflegerelevanteInformationen()  # done
        self.medikamentenverabreichungSection = self.getSectionMedikamentenverabreichung()  # done
        self.anmerkungenSection = self.getSectionAnmerkungen()  # done
        self.entlassungsmanagementSection = self.getSectionEntlassungsmanagement()  # done
        self.patientenverfuegungenSection = self.getSectionPatientenverfügungen()  # done
        self.abschliessendebemerkungenSection = self.getSectionAbschliessendeBemerkungen()  # done
        self.beilagenSection = self.getSection_Beilagen()  # done
        self.debug = None

    # Brieftext | -> nur Text bzw. entry mit Media
    def getSection_Brieftext(self):
        brieftext_section = Section_Brieftext_Extractor.section_Brieftext()
        brieftext_section.extractBrieftextFromXML(self.root)
        return brieftext_section

    # Pflegediagnosen | codiert
    def getSection_Pflegediagnosen(self):
        pflegediagnosen_section = section_Pflegediagnosen.Pflegediagnosen()
        pflegediagnosen_section.extractPflegediagnosenFromXML(self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='PFDIAG']/.."))
        return pflegediagnosen_section

    # Mobilität | nicht codiert -> Untersektion
    def getSection_Mobilität(self):
        section_element = self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='PFMOB']/..")
        self.mobilitaetSection_Hilfsmittel = self.getSection_Mobilität_Hilfsmittel(section_element.find("{urn:hl7-org:v3}component/{urn:hl7-org:v3}section"))
        mobilitaet_section = self.extractAllgemeineSection(section_element)
        return mobilitaet_section

    # Hilfsmittel | -> nur Text
    def getSection_Mobilität_Hilfsmittel(self, section):
        hilfsmittel_section = self.extractAllgemeineSection(section)
        return hilfsmittel_section

    # Körperpflege und Kleiden nicht codiert -> nur Text
    def getSection_Körperpflege(self):
        koerperpflege_section = self.extractAllgemeineSection(self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='PFKLEI']/.."))
        return koerperpflege_section

    # Ernährung | nicht codiert -> nur Text
    def getSection_Ernährung(self):
        ernaehrung_section = self.extractAllgemeineSection(self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='PFERN']/.."))
        return ernaehrung_section

    # Ausscheidung | nicht codiert -> nur Text
    def getSection_Ausscheidung(self):
        ausscheidung_section = self.extractAllgemeineSection(self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='PFAUS']/.."))
        return ausscheidung_section

    # Hautzustand | nicht codiert -> nur Text
    def getSection_Hautzustand(self):
        hautzustand_section = self.extractAllgemeineSection(self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='PFHAUT']/.."))
        return hautzustand_section

    # Atmung  | nicht codiert -> nur Text
    def getSection_Atmung(self):
        atmung_section = self.extractAllgemeineSection(self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='PFATM']/.."))
        return atmung_section

    # Schlaf  | nicht codiert -> nur Text
    def getSection_Schlaf(self):
        schlaf_section = self.extractAllgemeineSection(self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='PFSCHL']/.."))
        return schlaf_section

    # Schmerz  | nicht codiert -> nur Text
    def getSection_Schmerz(self):
        schmerz_section = self.extractAllgemeineSection(self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='38212-7']/.."))
        return schmerz_section

    # Orientierung und Bewusstseinslage | nicht codiert -> Untersektion
    def getSection_Orientierung(self):
        section_element = self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='PFORIE']/..")
        self.orientierungSection_Risiken = self.getSection_Orientierung_Risiken(section_element.find("{urn:hl7-org:v3}component/{urn:hl7-org:v3}section"))
        orientierung_section = self.extractAllgemeineSection(section_element)
        return orientierung_section

        # Risiken | -> nur Text
    def getSection_Orientierung_Risiken(self, section):
        risiken_section = self.extractAllgemeineSection(section)
        return risiken_section

    # Soziale Umstände und Verhalten | nicht codiert -> nur Text
    def getSection_SozialeUmstände(self):
        sozialeumstaende_section = self.extractAllgemeineSection(self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='PFSOZV']/.."))
        return sozialeumstaende_section

    # Kommunikation | nicht codiert -> Untersektion
    def getSection_Kommunikation(self):
        section_element = self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='PFKOMM']/..")
        self.kommunikationSection_Hilfsmittel = self.getSection_Orientierung_Risiken(section_element.find("{urn:hl7-org:v3}component/{urn:hl7-org:v3}section"))
        kommunikation_section = self.extractAllgemeineSection(section_element)
        return kommunikation_section

    # Hilfsmittel | -> nur Text
    def getSection_Kommunikation_Hilfsmittel(self, section):
        hilfsmittel_section = self.extractAllgemeineSection(section)
        return hilfsmittel_section

    # Rollenwahrnehmung und Sinnfindung | nicht codiert -> nur Text
    def getSection_Rollenwahrnehmung(self):
        rollenwahrnehmung_section = self.extractAllgemeineSection(self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='PFROLL']/.."))
        return rollenwahrnehmung_section

    # Vitalparameter | codiert
    def getSection_Vitalparameter(self):
        vitalparameter_section = section_Vitalparameter.Section_Vitalparamater()
        vitalparameter_section.extractVitalparameterFromXML(self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='8716-3']/.."))
        return vitalparameter_section

    # Pflegerelevante Informationen zur medizinischen Behandlung | nicht codiert -> Untersektion
    def getSection_PflegerelevanteInformationen(self):
        section_element = self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='PFMEDBEH']/..")
        self.pflegerelevanteinformationenSection_Risiken = self.getSection_PflegerelevanteInformationen_Risiken(section_element.find("{urn:hl7-org:v3}component/{urn:hl7-org:v3}section"))
        pflegerelevanteinformationen_section = self.extractAllgemeineSection(section_element)
        return pflegerelevanteinformationen_section

    # Risiken | nicht codiert -> nur Text
    def getSection_PflegerelevanteInformationen_Risiken(self, section):
        risiken_section = self.extractAllgemeineSection(section)
        return risiken_section

    # Medikamentenverabreichung | nicht codiert -> nur Text
    def getSectionMedikamentenverabreichung(self):
        medikamentenverabreichung_section = self.extractAllgemeineSection(self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='PFMED']/.."))
        return medikamentenverabreichung_section

    # Anmerkungen | nicht codiert -> nur Text
    def getSectionAnmerkungen(self):
        anmerkungen_section = self.extractAllgemeineSection(self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='ANM']/.."))
        return anmerkungen_section

    # Entlassungsmanagement | codiert
    def getSectionEntlassungsmanagement(self):
        entlassungsmanagement_section = section_Entlassungsmanagement.Entlassungsmanagement()
        entlassungsmanagement_section.extractEntlassungsmanagementFromXML(self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='8650-4']/.."))
        return entlassungsmanagement_section

    # Patientenverfügungen und andere juridische Dokumente | nicht codiert -> nur Text
    def getSectionPatientenverfügungen(self):
        patientenverfuegungen_section = self.extractAllgemeineSection(self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='42348-3']/.."))
        return patientenverfuegungen_section

    # Abschließende Bemerkungen | nicht codiert -> nur Text
    def getSectionAbschliessendeBemerkungen(self):
        abschliessendebemerkungen_section = self.extractAllgemeineSection(self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='ABBEM']/.."))
        return abschliessendebemerkungen_section

    # Beilagen | nicht codiert bzw. entry mit Media
    def getSection_Beilagen(self):
        beilagen_section = self.extractAllgemeineSection(self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='BEIL']/.."))
        return beilagen_section

    def extractAllgemeineSection(self, section_element):
        allgemeineSektion = section_MD_allg_Extractor.allg_section()
        allgemeineSektion.extractSectionFromXML(section_element)
        return allgemeineSektion

    def writeNurseDischargeSumaryToDB(self):
        if (NurseDischargeSummary_to_DB.documentCheck(self.document.document_id)):
            print("Dokument mit ID: " + self.document.document_id + " bereits in DB vorhanden")
        else:
            NurseDischargeSummary_to_DB.writeCodesToDB(self.all_codes)
            NurseDischargeSummary_to_DB.writeTemplateIDsToDB(self.all_templateIds)
            NurseDischargeSummary_to_DB.writeAdressesToDB(self.all_addresses)
            NurseDischargeSummary_to_DB.writePatientToDB(self.patient)
            NurseDischargeSummary_to_DB.writeEncompassingEncounterToDB(self.encompassingEncounter)
            section_Brieftext = NurseDischargeSummary_to_DB.writeSectionBrieftextToDB(self.brieftextSection)
            section_Pflegediagnosen = NurseDischargeSummary_to_DB.writePflegediagnosenSectionToDB(self.pflegediagnosenSection)
            section_Mobilitaet = NurseDischargeSummary_to_DB.writeMobilitaet_SectionToDB(self.mobilitaetSection)
            section_MobilitaetHilfsmittel = NurseDischargeSummary_to_DB.writeMobilitaet_HilfsmittelToDB(self.mobilitaetSection_Hilfsmittel)
            section_Koerperpflege = NurseDischargeSummary_to_DB.writeKoerperpflege_SectionToDB(self.koerperpflegeSection)
            section_Ernaehrung = NurseDischargeSummary_to_DB.writeErnaehrung_SectionToDB(self.ernaehrungSection)
            section_Ausscheidung = NurseDischargeSummary_to_DB.writeAusscheidung_SectionToDB(self.ausscheidungSection)
            section_Hautzustand = NurseDischargeSummary_to_DB.writeHautzustand_SectionToDB(self.hautzustandSection)
            section_Atmung = NurseDischargeSummary_to_DB.writeAtmung_SectionToDB(self.atmungSection)
            section_Schlaf = NurseDischargeSummary_to_DB.writeSchlaf_SectionToDB(self.schlafSection)
            section_Schmerz = NurseDischargeSummary_to_DB.writeSchmerz_SectionToDB(self.schmerzSection)
            section_Orientierung = NurseDischargeSummary_to_DB.writeOrientierung_SectionToDB(self.orientierungSection)
            section_OrientierungRisiken = NurseDischargeSummary_to_DB.writeOrientierung_Risiken_SectionToDB(self.orientierungSection_Risiken)
            section_SozialeUmstaende = NurseDischargeSummary_to_DB.writeSozialeUmstaendeSectionToDB(self.sozialeumstaendeSection)
            section_Kommunikation = NurseDischargeSummary_to_DB.writeKommunikationSectionToDB(self.kommunikationSection)
            section_KommunikationHilfsmittel = NurseDischargeSummary_to_DB.writeKommunikation_HilfsmittelSectionToDB(self.kommunikationSection_Hilfsmittel)
            section_Rollenwahrnehmung = NurseDischargeSummary_to_DB.writeRollenwahrnehmungSectionToDB(self.rollenwahrnehmungSection)
            section_Vitalparameter = NurseDischargeSummary_to_DB.writeVitalparameterSectionToDB(self.vitalparameterSection)
            section_PflegerelevanteInformationen = NurseDischargeSummary_to_DB.writePflegerelevanteInformationenSectionToDB(self.pflegerelevanteinformationenSection)
            section_PflegerelevanteInformationenRisiken = NurseDischargeSummary_to_DB.writePflegerelevanteInformationen_RisikenSectionToDB(self.pflegerelevanteinformationenSection_Risiken)
            section_Medikamentenverabreichung = NurseDischargeSummary_to_DB.writeMedikamentenverabreichungSectionToDB(self.medikamentenverabreichungSection)
            section_Anmerkungen = NurseDischargeSummary_to_DB.writeAnmerkungenSectionToDB(self.anmerkungenSection)
            section_Entlassungsmanagement = NurseDischargeSummary_to_DB.writeEntlassungsmanagementSectionToDB(self.entlassungsmanagementSection)
            section_Patientenverfuegung = NurseDischargeSummary_to_DB.writePatientenverfügungSectionToDB(self.patientenverfuegungenSection)
            section_AbschliessendeBemerkungen = NurseDischargeSummary_to_DB.writeAbschliessendeBemerkungenSectionToDB(self.abschliessendebemerkungenSection)
            section_Beilagen = NurseDischargeSummary_to_DB.writeBeilagenSectionToDB(self.beilagenSection)
            NurseDischargeSummary_to_DB.writeNurseDischargeSummaryToDB(self.document, self.patient.SVNR.attrib["extension"], self.encompassingEncounter.encompassingEncounter_ID["extension"], section_Brieftext, section_Pflegediagnosen, section_Mobilitaet, section_MobilitaetHilfsmittel, section_Koerperpflege, section_Ernaehrung, section_Ausscheidung, section_Hautzustand, section_Atmung, section_Schlaf, section_Schmerz, section_Orientierung, section_OrientierungRisiken, section_SozialeUmstaende, section_Kommunikation, section_KommunikationHilfsmittel, section_Rollenwahrnehmung, section_Vitalparameter, section_PflegerelevanteInformationen, section_PflegerelevanteInformationenRisiken, section_Medikamentenverabreichung, section_Anmerkungen, section_Entlassungsmanagement, section_Patientenverfuegung, section_AbschliessendeBemerkungen, section_Beilagen)
            NurseDischargeSummary_to_DB.writeDocumentTemplatesToDB(self.document)
            NurseDischargeSummary_to_DB.writeServiceEventsToDB(self.serviceEvents, self.document.document_id)
            NurseDischargeSummary_to_DB.writeParticipantToDB(self.participants, self.document.document_id)
            NurseDischargeSummary_to_DB.writeAuthorToDB(self.authors, self.document.document_id)