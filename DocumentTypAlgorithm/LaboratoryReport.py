
#Algorithm for importing the laboratory report
from Element_Extractors import Section_Brieftext_Extractor
from Element_Extractors.LabReport_Elements import SpecialitySection_Extractor, SpecimenSection_Extractor, \
    section_Ueberweisungsgrund_Extractor
from DocumentTypAlgorithm import CDA_R2_Header

from Database_helper import LaboratoryReport_to_DB

class LabReport(CDA_R2_Header.CDA_R2_Header):

    def __init__(self, root):
        super().__init__(root)
        self.root = root
        self.specialitySections = self.getSpecialitySections()
        self.specimenSection = self.getSpecimenSection()
        self.brieftextSection = self.getBrieftextSection()
        self.ueberweisungsgrundSection = self.getUeberweisungsgrundSection()


    #Bodyinformation
    def getSpecimenSection(self):
        specimen_section_element = self.root.find('.//{urn:hl7-org:v3}component[@typeCode="COMP"]/{urn:hl7-org:v3}section[@classCode="DOCSECT"]/{urn:hl7-org:v3}templateId[@root="1.2.40.0.34.11.4.2.1"]/..')
        specimen_section = SpecimenSection_Extractor.SpecimenSection()
        specimen_section.extractSpecimenSection(specimen_section_element)
        return specimen_section

    def getSpecialitySections(self):
        speciality_section_elements = self.root.findall('.//{urn:hl7-org:v3}component[@typeCode="COMP"]/{urn:hl7-org:v3}section[@classCode="DOCSECT"]/{urn:hl7-org:v3}templateId[@root="1.3.6.1.4.1.19376.1.3.3.2.1"]/..')
        speciality_Sections = []
        for spec_sec in speciality_section_elements:
            speciality_section = SpecialitySection_Extractor.SpecialitySection()
            speciality_section.extractSpecialitySectionFromXML(spec_sec)
            speciality_Sections.append(speciality_section)
        return speciality_Sections

    def getBrieftextSection(self):
        brieftext_section = Section_Brieftext_Extractor.section_Brieftext()
        brieftext_section.extractBrieftextFromXML(self.root)
        return brieftext_section

    def getUeberweisungsgrundSection(self):
        ueberweisungsgrund_section = section_Ueberweisungsgrund_Extractor.section_Ueberweisungsgrund()
        ueberweisungsgrund_section.extractUeberweisungsgrundFromXML(self.root)
        return ueberweisungsgrund_section

    #Write Laboratory information to DB
    def writeLabReportToDB(self):
        if (LaboratoryReport_to_DB.documentCheck(self.document.document_id)):
            print("Dokument mit ID: " + self.document.document_id + " bereits in DB vorhanden")
        else:
            LaboratoryReport_to_DB.writeCodesToDB(self.all_codes)
            LaboratoryReport_to_DB.writeTemplateIDsToDB(self.all_templateIds)
            LaboratoryReport_to_DB.writeAdressesToDB(self.all_addresses)
            LaboratoryReport_to_DB.writePatientToDB(self.patient)
            LaboratoryReport_to_DB.writeEncompassingEncounterToDB(self.encompassingEncounter)
            sectionBrieftext_ID = LaboratoryReport_to_DB.writeSectionBrieftextToDB(self.brieftextSection)
            sectionUeberweisungsgrund_ID = LaboratoryReport_to_DB.writeSectionUeberweisungsgrundToDB(self.ueberweisungsgrundSection)
            sectionSpecimenSection_ID = LaboratoryReport_to_DB.writeSectionSpecimenToDB(self.specimenSection)
            LaboratoryReport_to_DB.writeDocumentInformationToDB(self.document, self.patient.SVNR.attrib["extension"], self.encompassingEncounter.encompassingEncounter_ID["extension"], sectionBrieftext_ID, sectionUeberweisungsgrund_ID, sectionSpecimenSection_ID)
            LaboratoryReport_to_DB.writeDocumentTemplatesToDB(self.document)
            LaboratoryReport_to_DB.writeServiceEventsToDB(self.serviceEvents, self.document.document_id)
            LaboratoryReport_to_DB.writeSpecialitySectionToDB(self.specialitySections, self.document.document_id)
            LaboratoryReport_to_DB.writeParticipantToDB(self.participants, self.document.document_id)