from Database_helper import MedicalDischargeLetter_to_DB
from DocumentTypAlgorithm import CDA_R2_Header
from Element_Extractors import Section_Brieftext_Extractor
from Element_Extractors.MedicalDischargeLetter_Elements import section_Aufnahmegrund_Extractor, section_HospitalDischarge_Extractor, section_MD_allg_Extractor, section_MedicationOnAdmission_Extractor, section_EmpfohleneMedikation_Extractor, section_ErhobeneBefunde_Vitalparameter


class MedicalDischargeLetter(CDA_R2_Header.CDA_R2_Header):

    def __init__(self, root):
        super().__init__(root)
        self.root = root
        self.brieftextSection = self.getBrieftextSection() #done
        self.aufnahmegrundSection = self.getReasonForReferral() #done
        self.hospitaldischargeSection = self.getHospitalDischarge()#done
        self.rehabilitationszieleSection = self.getRehabilitationsziele() #done
        self.outcomesmeasurementsSection = self.getOutcomeMeasurement() #done
        self.procedurenarrativeSection = self.getProcedureNarrative() #done
        self.zusammenfassundaufenthaltSection = self.getZusammenfassundAufenthalt() #done
        self.abschliessendebemerkungenSection = self.getAbschliessendeBemerkungen() #done
        self.allergiesandrisksSection = self.getAllergiesAndRisks() #done
        self.anamneseSection = self.getAnamnese()#done
        self.medicationadministeredSection = self.getMedicationAdministered()#done
        self.advancedirectivesSection = self.getAdvanceDirectives()#done
        self.treatmentplanSection = self.getTreatmentPlan() #done
        self.treatmentplanSectionTermin = self.getTreatmentPlanTermin() #done
        self.treatmentplanSectionEntlassungszustand = self.getTreatmentPlanEntlassungszustand() #done
        self.treatmentplanSectionPlanofCareNote = self.getTreatmentPlanPlanOfCareNote() #done
        self.fruehereErkrankungenSection = self.getFruehereErkraknungen()#done
        self.fruehereErkrankungenSectionBisherigeMassnahmen = self.getFruehereErkrankungenBisherigeMassnahmen()#done
        self.medicationsOnAdmissionSection = self.getMedicationsOnAdmission() #done
        self.empfohleneMedikationSection = self.getEmpfohleneMedikation() #done
        self.erhobeneBefundeSection = self.getErhobeneBefunde()#done
        self.erhobeneBefundeSection_AustehendeBefunde = self.getErhobeneBefunde_AustehendeBefunde()#done
        self.erhobeneBefundeSection_AuszugAusBefunde = self.getErhobeneBefunde_AuszugAusBefunde()#done
        self.erhobeneBefundeSection_Operationsbericht = self.getErhobeneBefunde_Operationsbericht()#done
        self.erhobeneBefundeSection_BeigelegteBefunde = self.getErhobeneBefunde_BeigelegteBefunde()#done
        self.erhobeneBefundeSection_Vitalparameter = self.getErhobeneBefunde_Vitalparameter()#done

        #self.beilagenSection = self.getBeilagen()

    #Sektion für den Brieftext
    def getBrieftextSection(self):
        brieftext_section = Section_Brieftext_Extractor.section_Brieftext()
        brieftext_section.extractBrieftextFromXML(self.root)
        return brieftext_section

    # Aufnahmegrund - mandatory
    def getReasonForReferral(self):
        aufnahmegrund_section = section_Aufnahmegrund_Extractor.section_Aufnahmegrund()
        aufnahmegrund_section.extractAufnahmegrundSectionFromXML(self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='42349-1']/.."))
        return aufnahmegrund_section

    # Diagnosen bei Entlassung - mandatory
    def getHospitalDischarge(self):
        hospitaldischarge_section = section_HospitalDischarge_Extractor.section_HospitalDischarge()
        hospitaldischarge_section.extractDiagnoseBeiEntlassungSectionFromXML(self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='11535-2']/.."))
        return hospitaldischarge_section

    # Rehabilitationsziele - optional | nicht codiert
    def getRehabilitationsziele(self):
        rehabilitationsziele_section = self.extractAllgemeineSection(self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='REHAZIELE']/.."))
        return rehabilitationsziele_section

    # Outcomes Measurements - optional | nicht codiert
    def getOutcomeMeasurement(self):
        outcomesmeasurements_section = self.extractAllgemeineSection(self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='OUTCOMEMEAS']/.."))
        return outcomesmeasurements_section

    # Durchgeführte Massnahmen - optional | nicht codiert
    def getProcedureNarrative(self):
        procedurenarrative_section = self.extractAllgemeineSection(self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='29554-3']/.."))
        return procedurenarrative_section

    # Empfohlene Medikation
    def getEmpfohleneMedikation(self):
        empfohlenemedikation_section = section_EmpfohleneMedikation_Extractor.section_EmpfohleneMedikation()
        empfohlenemedikation_section_element = self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='10183-2']/..")
        empfohlenemedikation_section.extractEmpfohleneMedikationFromXML(empfohlenemedikation_section_element)
        return empfohlenemedikation_section

    # Empfohlene Massnahmen
    def getTreatmentPlan(self):
        treatmentplan_section = self.extractAllgemeineSection(self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='18776-5']/.."))
        return treatmentplan_section

    # Empfohlene Massnahmen - Termine, Kontrollen, Wiederbestellung
    def getTreatmentPlanTermin(self):
        treatmentplan_termin_section = self.extractAllgemeineSection(self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='TERMIN']/.."))
        return treatmentplan_termin_section

    # Empfohlene Massnahmen - Entlassungszustand
    def getTreatmentPlanEntlassungszustand(self):
        treatmentplan_entlassungszustand_section = self.extractAllgemeineSection(self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='47420-5']/.."))
        return treatmentplan_entlassungszustand_section

    # Empfohlene Massnahmen - Empfohlene Anordnung an die Pflege
    def getTreatmentPlanPlanOfCareNote(self):
        treatmentPlanPlanOfCareNote_section = self.extractAllgemeineSection(self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='56447-6']/.."))
        return treatmentPlanPlanOfCareNote_section

    # Zusammenfassung des Aufenthalts - optional | nicht codiert
    def getZusammenfassundAufenthalt(self):
        zusammenfassungaufenthalt_section = self.extractAllgemeineSection(self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='8648-8']/.."))
        return zusammenfassungaufenthalt_section

    # Abschliessende Bemerkungen - optional | nicht codiert
    def getAbschliessendeBemerkungen(self):
        abschliessendebemerkungen_section = self.extractAllgemeineSection(self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='ABBEM']/.."))
        return abschliessendebemerkungen_section

    # Allergien, Unverträglichkeiten und Risiken - required | nicht codiert
    def getAllergiesAndRisks(self):
        allergiesandrisks_section = self.extractAllgemeineSection(self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='48765-2']/.."))
        return allergiesandrisks_section

    # Erhobene Befunde - optional
    def getErhobeneBefunde(self):
        erhobenebefunde_section = self.extractAllgemeineSection(self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='11493-4']/.."))
        return erhobenebefunde_section

    # Erhobene Befunde - Ausstehende Befunde
    def getErhobeneBefunde_AustehendeBefunde(self):
        erhobenebefunde_AB_section = self.extractAllgemeineSection(self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}component/{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='BEFAUS']/.."))
        return erhobenebefunde_AB_section

    # Erhobene Befunde - Auszüge aus erhobenen Befunden | Problem ist das hier nicht strukturiert bzw. codiert wird - deshalb wird der Text nicht genauer analysiert!
    def getErhobeneBefunde_AuszugAusBefunde(self):
        erhobenebefunde_AAB_section = self.extractAllgemeineSection(self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}component/{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='BEFERH']/.."))
        return erhobenebefunde_AAB_section

    # Erhobene Befunde - Operationsbericht
    def getErhobeneBefunde_Operationsbericht(self):
        erhobenebefunde_OP_section = self.extractAllgemeineSection(self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}component/{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='OPBER']/.."))
        return erhobenebefunde_OP_section

    # Erhobene Befunde - Beigelegte Befunde
    def getErhobeneBefunde_BeigelegteBefunde(self):
        erhobenebefunde_BB_section = self.extractAllgemeineSection(self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}component/{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='BEFBEI']/.."))
        return erhobenebefunde_BB_section

    # Erhobene Befunde - Vitalparameter
    def getErhobeneBefunde_Vitalparameter(self):
        erhobenebefunde_vitalparameter_element = self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}component/{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='8716-3']/..")
        erhobenebefunde_vitalparameter = section_ErhobeneBefunde_Vitalparameter.Section_Vitalparamater()
        erhobenebefunde_vitalparameter.extractVitalparameterFromXML(erhobenebefunde_vitalparameter_element)
        return erhobenebefunde_vitalparameter

    # Anamnese - optional | nicht codiert
    def getAnamnese(self):
        anamnese_section = self.extractAllgemeineSection(self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='10164-2']/.."))
        return anamnese_section

    # Frühere Erkrankungen - optional
    def getFruehereErkraknungen(self):
        frueheErkankungen_section = self.extractAllgemeineSection(self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='11348-0']/.."))
        return frueheErkankungen_section

    # Frühere Erkrankungen - Bisherige Massnahmen
    def getFruehereErkrankungenBisherigeMassnahmen(self):
        bisherigeMassnahmen = self.extractAllgemeineSection(self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='67803-7']/.."))
        return bisherigeMassnahmen

    # Medikation bei Einweisung - optional
    def getMedicationsOnAdmission(self):
        medicationonadmission_section = section_MedicationOnAdmission_Extractor.section_MedicationOnAdministration()
        medicationonadmission_section.extractMedicationOnAdministrationFromXML(self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='42346-7']/.."))
        return medicationonadmission_section

    # Verabreichte Medikation während des Aufenthalts - optional | nicht codiert
    def getMedicationAdministered(self):
        medicationadministered_section = self.extractAllgemeineSection(self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='18610-6']/.."))
        return medicationadministered_section

    # Patientenverfügungen und andere juridische Dokumente - optional | nicht codiert
    def getAdvanceDirectives(self):
        advancedirectives_section = self.extractAllgemeineSection(self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='42348-3']/.."))
        return advancedirectives_section

    # Beilagen - optional | nicht codiert
    def getBeilagen(self):
        print("TBD")

    def extractAllgemeineSection(self, section_element):
        allgemeineSektion = section_MD_allg_Extractor.allg_section()
        allgemeineSektion.extractSectionFromXML(section_element)
        return allgemeineSektion

    def writeMedicalDischargeLetterToDB(self):
        if (MedicalDischargeLetter_to_DB.documentCheck(self.document.document_id)):
            print("Dokument mit ID: " + self.document.document_id + " bereits in DB vorhanden")
        else:
            MedicalDischargeLetter_to_DB.writeCodesToDB(self.all_codes)
            MedicalDischargeLetter_to_DB.writeTemplateIDsToDB(self.all_templateIds)
            MedicalDischargeLetter_to_DB.writeAdressesToDB(self.all_addresses)
            MedicalDischargeLetter_to_DB.writePatientToDB(self.patient)
            MedicalDischargeLetter_to_DB.writeEncompassingEncounterToDB(self.encompassingEncounter)
            sectionBrieftext_ID = MedicalDischargeLetter_to_DB.writeSectionBrieftextToDB(self.brieftextSection)
            section_AbschliessendeBemerkung_ID = MedicalDischargeLetter_to_DB.writeGeneralSectionToDB(self.abschliessendebemerkungenSection, "section_AbschliessendeBemerkung")
            section_AdvancedDirectives_ID = MedicalDischargeLetter_to_DB.writeGeneralSectionToDB(self.advancedirectivesSection, "section_AdvancedDirectives")
            section_Allergie_ID = MedicalDischargeLetter_to_DB.writeGeneralSectionToDB(self.allergiesandrisksSection, "section_Allergie")
            section_Anamnese_ID = MedicalDischargeLetter_to_DB.writeGeneralSectionToDB(self.anamneseSection, "section_Anamnese")
            section_Aufnahmegrund_ID = MedicalDischargeLetter_to_DB.writeGeneralSectionToDB(self.aufnahmegrundSection, "section_Aufnahmegrund")
            section_EmpfohleneMedikation_ID = MedicalDischargeLetter_to_DB.writeEmpfohleneMedikationToDB(self.empfohleneMedikationSection)
            section_ErhobeneBefunde_ID = MedicalDischargeLetter_to_DB.writeGeneralSectionToDB(self.erhobeneBefundeSection, "section_ErhobeneBefunde")
            section_ErhobeneBefunde_AustehendeBefunde_ID = MedicalDischargeLetter_to_DB.writeGeneralSectionToDB(self.erhobeneBefundeSection_AustehendeBefunde, "section_ErhobeneBefunde_AustehendeBefunde")
            section_ErhobeneBefunde_AuszugAusBefunde_ID = MedicalDischargeLetter_to_DB.writeGeneralSectionToDB(self.erhobeneBefundeSection_AuszugAusBefunde, "section_ErhobeneBefunde_AuszugAusBefunde")
            section_ErhobeneBefunde_BeigelegteBefunde_ID = MedicalDischargeLetter_to_DB.writeGeneralSectionToDB(self.erhobeneBefundeSection_BeigelegteBefunde, "section_ErhobeneBefunde_BeigelegteBefunde")
            section_ErhobeneBefunde_Operationsbericht_ID = MedicalDischargeLetter_to_DB.writeGeneralSectionToDB(self.erhobeneBefundeSection_Operationsbericht, "section_ErhobeneBefunde_Operationsbericht")
            section_ErhobeneBefunde_Vitalparameter_ID = MedicalDischargeLetter_to_DB.writeErhobeneBefunde_VitalparameterToDB(self.erhobeneBefundeSection_Vitalparameter)
            section_FruehereErkrankungen_ID = MedicalDischargeLetter_to_DB.writeGeneralSectionToDB(self.fruehereErkrankungenSection, "section_FruehereErkrankungen")
            section_FruehereErkrankungen_BisherigeMassnahmen_ID = MedicalDischargeLetter_to_DB.writeGeneralSectionToDB(self.fruehereErkrankungenSectionBisherigeMassnahmen, "section_FruehereErkrankungen_BisherigeMassnahmen")
            section_HospitalDischarge_ID = MedicalDischargeLetter_to_DB.writeHospitalDischargeSectionToDB(self.hospitaldischargeSection)
            section_MedicationAdministered_ID = MedicalDischargeLetter_to_DB.writeGeneralSectionToDB(self.medicationadministeredSection, "section_MedicationAdministered")
            section_MedicationOnAdmission_ID = MedicalDischargeLetter_to_DB.writeSectionMedicationOnAdmissionToDB(self.medicationsOnAdmissionSection)
            section_OutcomesMeasurement_ID = MedicalDischargeLetter_to_DB.writeGeneralSectionToDB(self.outcomesmeasurementsSection, "section_OutcomeMeasurement")
            section_ProcedureNarrative_ID = MedicalDischargeLetter_to_DB.writeGeneralSectionToDB(self.procedurenarrativeSection, "section_ProcedureNarrative")
            section_Rehabilitationsziele_ID = MedicalDischargeLetter_to_DB.writeGeneralSectionToDB(self.rehabilitationszieleSection, "section_Rehabilitationsziele")
            section_TreatmentPlan_ID = MedicalDischargeLetter_to_DB.writeGeneralSectionToDB(self.treatmentplanSection, "section_TreatmentPlan")
            section_TreatmentPlan_Entlassungszustand_ID = MedicalDischargeLetter_to_DB.writeGeneralSectionToDB(self.treatmentplanSectionEntlassungszustand, "section_TreatmentPlan_Entlassungszustand")
            section_TreatmentPlan_PlanOfCareNote_ID = MedicalDischargeLetter_to_DB.writeGeneralSectionToDB(self.treatmentplanSectionPlanofCareNote, "section_TreatmentPlan_PlanofCareNote")
            section_TreatmentPlan_Termin_ID = MedicalDischargeLetter_to_DB.writeGeneralSectionToDB(self.treatmentplanSectionTermin, "section_TreatmentPlan_Termin")
            section_ZusammenfassungDesAufenthalts_ID = MedicalDischargeLetter_to_DB.writeGeneralSectionToDB(self.zusammenfassundaufenthaltSection, "section_ZusammenfassungDesAufenthalts")
            MedicalDischargeLetter_to_DB.writeMedicalDischargeLetterToDB(self.document, self.patient.SVNR.attrib["extension"], self.encompassingEncounter.encompassingEncounter_ID["extension"],
                                                                         section_AbschliessendeBemerkung_ID, section_AdvancedDirectives_ID, section_Allergie_ID,
                                                                         section_Anamnese_ID, section_Aufnahmegrund_ID, sectionBrieftext_ID, section_EmpfohleneMedikation_ID, section_ErhobeneBefunde_ID,
                                                                         section_ErhobeneBefunde_AustehendeBefunde_ID, section_ErhobeneBefunde_AuszugAusBefunde_ID, section_ErhobeneBefunde_BeigelegteBefunde_ID,
                                                                         section_ErhobeneBefunde_Operationsbericht_ID, section_ErhobeneBefunde_Vitalparameter_ID, section_FruehereErkrankungen_ID,
                                                                         section_FruehereErkrankungen_BisherigeMassnahmen_ID, section_HospitalDischarge_ID, section_MedicationAdministered_ID,
                                                                         section_MedicationOnAdmission_ID, section_OutcomesMeasurement_ID, section_ProcedureNarrative_ID, section_Rehabilitationsziele_ID,
                                                                         section_TreatmentPlan_ID, section_TreatmentPlan_Entlassungszustand_ID, section_TreatmentPlan_PlanOfCareNote_ID, section_TreatmentPlan_Termin_ID,
                                                                         section_ZusammenfassungDesAufenthalts_ID)
            MedicalDischargeLetter_to_DB.writeDocumentTemplatesToDB(self.document)
            MedicalDischargeLetter_to_DB.writeServiceEventsToDB(self.serviceEvents, self.document.document_id)
            MedicalDischargeLetter_to_DB.writeParticipantToDB(self.participants, self.document.document_id)

    def writeMedicalDischargeLetterToDB_v2(self):
        if (MedicalDischargeLetter_to_DB.documentCheck(self.document.document_id)):
            print("Dokument mit ID: " + self.document.document_id + " bereits in DB vorhanden")
        else:
            MedicalDischargeLetter_to_DB.writeCodesToDB(self.all_codes)
            MedicalDischargeLetter_to_DB.writeTemplateIDsToDB(self.all_templateIds)
            MedicalDischargeLetter_to_DB.writeAdressesToDB(self.all_addresses)
            MedicalDischargeLetter_to_DB.writePatientToDB(self.patient)
            legalauthenticator_id = MedicalDischargeLetter_to_DB.writelegalAuthenticatorToDB(self.legalauthenticator)
            MedicalDischargeLetter_to_DB.writeEncompassingEncounterToDB(self.encompassingEncounter)
            sectionBrieftext_ID = MedicalDischargeLetter_to_DB.writeSectionBrieftextToDB(self.brieftextSection)
            section_EmpfohleneMedikation_ID = MedicalDischargeLetter_to_DB.writeEmpfohleneMedikationToDB(self.empfohleneMedikationSection)
            section_ErhobeneBefunde_ID = MedicalDischargeLetter_to_DB.writeGeneralSectionToDB(self.erhobeneBefundeSection, "section_ErhobeneBefunde")
            section_ErhobeneBefunde_AustehendeBefunde_ID = MedicalDischargeLetter_to_DB.writeGeneralSectionToDB(self.erhobeneBefundeSection_AustehendeBefunde, "section_ErhobeneBefunde_AustehendeBefunde")
            section_ErhobeneBefunde_AuszugAusBefunde_ID = MedicalDischargeLetter_to_DB.writeGeneralSectionToDB(self.erhobeneBefundeSection_AuszugAusBefunde, "section_ErhobeneBefunde_AuszugAusBefunde")
            section_ErhobeneBefunde_BeigelegteBefunde_ID = MedicalDischargeLetter_to_DB.writeGeneralSectionToDB(self.erhobeneBefundeSection_BeigelegteBefunde, "section_ErhobeneBefunde_BeigelegteBefunde")
            section_ErhobeneBefunde_Operationsbericht_ID = MedicalDischargeLetter_to_DB.writeGeneralSectionToDB(self.erhobeneBefundeSection_Operationsbericht, "section_ErhobeneBefunde_Operationsbericht")
            section_ErhobeneBefunde_Vitalparameter_ID = MedicalDischargeLetter_to_DB.writeErhobeneBefunde_VitalparameterToDB(self.erhobeneBefundeSection_Vitalparameter)
            section_HospitalDischarge_ID = MedicalDischargeLetter_to_DB.writeHospitalDischargeSectionToDB(self.hospitaldischargeSection)
            section_MedicationOnAdmission_ID = MedicalDischargeLetter_to_DB.writeSectionMedicationOnAdmissionToDB(self.medicationsOnAdmissionSection)
            MedicalDischargeLetter_to_DB.writeMedicalDischargeLetterToDB_v2(self.document, self.patient.SVNR.attrib["extension"],legalauthenticator_id, self.encompassingEncounter.encompassingEncounter_ID["extension"],
                                                                         sectionBrieftext_ID, section_EmpfohleneMedikation_ID, section_ErhobeneBefunde_ID,
                                                                         section_ErhobeneBefunde_AustehendeBefunde_ID, section_ErhobeneBefunde_AuszugAusBefunde_ID, section_ErhobeneBefunde_BeigelegteBefunde_ID,
                                                                         section_ErhobeneBefunde_Operationsbericht_ID, section_ErhobeneBefunde_Vitalparameter_ID, section_HospitalDischarge_ID, section_MedicationOnAdmission_ID)
            MedicalDischargeLetter_to_DB.writeDocumentTemplatesToDB(self.document)
            MedicalDischargeLetter_to_DB.writeServiceEventsToDB(self.serviceEvents, self.document.document_id)
            MedicalDischargeLetter_to_DB.writeParticipantToDB(self.participants, self.document.document_id)
            MedicalDischargeLetter_to_DB.writeAuthorToDB(self.authors, self.document.document_id)