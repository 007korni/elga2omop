from Element_Extractors import Patient_Extractor, serviceEvent_Extractor, DocumentInformation_Extractor, \
    encompassingEncounter_Extractor, Participant_Extractor, Author_Extractor, Custodian_Extractor, \
    legalAuthenticator_Extractor


class CDA_R2_Header:

    def __init__(self, root):
        self.root = root
        self.all_codes = self.getAllCodes()
        self.all_templateIds = self.getAllTemplateIds()
        self.all_addresses = self.getAllAdresses()
        self.serviceEvents = self.getServiceEvents()
        self.patient = self.getPatientInformation()
        self.document = self.getDocumentInformation()
        self.encompassingEncounter = self.getEncompassingEncounter()
        self.participants = self.getParticipant()
        self.authors = self.getAllAuthors()
        self.legalauthenticator = self.getLegalAuthenticator()
        self.custodians = self.getAllCustodians()

    def getPatientInformation(self):
        patient = Patient_Extractor.Patient()
        patient.extractPatientFromXML(self.root.find('.//{urn:hl7-org:v3}recordTarget'))
        return patient

    def getServiceEvents(self):
        serviceEvent_ext = serviceEvent_Extractor.serviceEvent_Extractor()
        serviceEvents = serviceEvent_ext.extractServiceEventsFromXML(self.root)
        return serviceEvents

    def getDocumentInformation(self):
        documentinformation = DocumentInformation_Extractor.Document()
        documentinformation.extractDocumentInformationFromXML(self.root)
        return documentinformation

    def getEncompassingEncounter(self):
        encompassingEncounter = encompassingEncounter_Extractor.EncompassingEncounter()
        try:
            encompassingEncounter.extractEncompassingEncounter(self.root.find('.//{urn:hl7-org:v3}encompassingEncounter'))
            return encompassingEncounter
        except:
            return encompassingEncounter

    def getParticipant(self):
        participant_elements = self.root.findall('{urn:hl7-org:v3}participant')
        participants = []
        for x in range(0, len(participant_elements), 1):
            participant = Participant_Extractor.Participant()
            participant.extractParticipantFromXML(participant_elements[x])
            participants.append(participant)
        return participants

    def getAllCodes(self):
        codes = self.root.findall(".//{urn:hl7-org:v3}code")
        interpretation_codes = self.root.findall(".//{urn:hl7-org:v3}interpretationCode")
        gender_codes = self.root.findall(".//{urn:hl7-org:v3}administrativeGenderCode")
        translation_codes = self.root.findall(".//{urn:hl7-org:v3}translation")
        function_codes = self.root.findall(".//{urn:hl7-org:v3}functionCode")
        targetSite_code = self.root.findall(".//{urn:hl7-org:v3}targetSiteCode")
        medication_code = self.root.findall(".//{urn:ihe:pharm:medication}code")
        route_code = self.root.findall(".//{urn:hl7-org:v3}routeCode")
        form_code = self.root.findall(".//{urn:ihe:pharm:medication}formCode")
        value_code = self.root.findall(".//{urn:hl7-org:v3}observation/{urn:hl7-org:v3}value[@code]")
        codes = codes + interpretation_codes + gender_codes + translation_codes + function_codes + targetSite_code + medication_code + route_code + form_code + value_code
        all_codes = dict()
        for c in codes:
            try:
                code = c.attrib["code"]
                code_info = dict()
                code_info["code"] = c.attrib["code"]
                try:
                    code_info["displayName"] = c.attrib["displayName"]
                except:
                    code_info["displayName"] = None
                code_info["codeSystem"] = c.attrib["codeSystem"]
                try:
                    code_info["codeSystemName"] = c.attrib["codeSystemName"]
                except:
                    code_info["codeSystemName"] = None
                all_codes[code] = code_info
            except:
                code = c.attrib["nullFlavor"]
                all_codes[code] = "nullFlavor"

        return all_codes

    def getAllTemplateIds(self):
        templateids = self.root.findall(".//{urn:hl7-org:v3}templateId")
        return templateids

    def getAllAdresses(self):
        all_addr_elements = self.root.findall(".//{urn:hl7-org:v3}addr")
        addresses = []
        for a in all_addr_elements:
            adress = dict()
            for item in a:
                adress[item.tag.replace("{urn:hl7-org:v3}", "")] = item.text
            if bool(adress):
                addresses.append(adress)
        return addresses

    def getAllAuthors(self):
        all_author_elements = self.root.findall("{urn:hl7-org:v3}author")
        authors = []
        for a in all_author_elements:
             author = Author_Extractor.Author()
             author.extractAuthorsFromXML(a)
             authors.append(author)
        return authors

    def getAllCustodians(self):
        all_custodian_elements = self.root.findall("{urn:hl7-org:v3}custodian")
        custodians = []
        for c in all_custodian_elements:
            custodian = Custodian_Extractor.Custodian()
            custodian.extractCustodianFromXML(c)
            custodians.append(custodian)
        return custodians

    def getLegalAuthenticator(self):
        legalauthenticator = legalAuthenticator_Extractor.legalAuthenticator()
        legalauthenticator.extractLegalAuthenticatorFromXML(self.root.find('.//{urn:hl7-org:v3}legalAuthenticator'))
        return legalauthenticator