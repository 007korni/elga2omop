from Database_helper import DiagnosticImagingStudy_to_DB
from DocumentTypAlgorithm import CDA_R2_Header
from Element_Extractors import Section_Brieftext_Extractor
from Element_Extractors.DiagnosticImmagingStudy_Elements import Section_AktuelleUntersuchungen_Extractor, \
    Section_Befund, Section_DICOM_Object_Catalog_Extractor


class DiagnosticImagingStudy(CDA_R2_Header.CDA_R2_Header):

    def __init__(self, root):
        super().__init__(root)
        self.root = root
        self.brieftextSection = self.getSection_Brieftext()
        self.DICOMObjectCatalogSection = self.getSection_DICOM_Object_Catalog()
        self.aktuelleUntersuchungenSection = self.getSection_AktuelleUntersuchungen()
        self.befundSection = self.getSection_Befund()
        self.debug = None

    # Brieftext | -> nur Text bzw. entry mit Media
    def getSection_Brieftext(self):
        brieftext_section = Section_Brieftext_Extractor.section_Brieftext()
        brieftext_section.extractBrieftextFromXML(self.root)
        return brieftext_section


    def getSection_DICOM_Object_Catalog(self):
        DICOMObjectCatalog_section = Section_DICOM_Object_Catalog_Extractor.DICOMobjectCatalogSection()
        DICOMObjectCatalog_section.extractDICOMObjectCatalogFromXML(self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='121181']/.."))
        return DICOMObjectCatalog_section

    def getSection_AktuelleUntersuchungen(self):
        aktuelleUntersuchungen_section = Section_AktuelleUntersuchungen_Extractor.AktuelleUntersuchungenSection()
        aktuelleUntersuchungen_section.extractAktuelleUntersuchungenSectionFromXML(self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='55111-9']/.."))
        return aktuelleUntersuchungen_section

    def getSection_Befund(self):
        befund_section = Section_Befund.BefundSection()
        befund_section.extractBefundSectionFromXML(self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='18782-3']/.."))
        return befund_section

    # restlichen Sektions enthalten keine entries und sind somit nicht maschinenlesbar

    def writeDiagnosticImagingStudyToDB(self):
        if (DiagnosticImagingStudy_to_DB.documentCheck(self.document.document_id)):
            print("Dokument mit ID: " + self.document.document_id + " bereits in DB vorhanden")
        else:
            DiagnosticImagingStudy_to_DB.writeCodesToDB(self.all_codes)
            DiagnosticImagingStudy_to_DB.writeTemplateIDsToDB(self.all_templateIds)
            DiagnosticImagingStudy_to_DB.writeAdressesToDB(self.all_addresses)
            DiagnosticImagingStudy_to_DB.writePatientToDB(self.patient)
            DiagnosticImagingStudy_to_DB.writeEncompassingEncounterToDB(self.encompassingEncounter)
            section_Brieftext = DiagnosticImagingStudy_to_DB.writeSectionBrieftextToDB(self.brieftextSection)
            section_Befund = DiagnosticImagingStudy_to_DB.writeSectionBefundToDB(self.befundSection)
            section_AktuelleUntersuchung = DiagnosticImagingStudy_to_DB.writeSectionAktuelleUntersuchungToDB(self.aktuelleUntersuchungenSection)
            section_DICOM = DiagnosticImagingStudy_to_DB.writeSectionDICOMObjectCatalogToDB(self.DICOMObjectCatalogSection)
            DiagnosticImagingStudy_to_DB.writeDiagnosticImagingStudyToDB(
                                                                       self.document,
                                                                       self.patient.SVNR.attrib["extension"],
                                                                       self.encompassingEncounter.encompassingEncounter_ID["extension"],
                                                                       section_Brieftext,
                                                                       section_DICOM,
                                                                       section_Befund,
                                                                       section_AktuelleUntersuchung
                                                                       )
            DiagnosticImagingStudy_to_DB.writeDocumentTemplatesToDB(self.document)
            DiagnosticImagingStudy_to_DB.writeServiceEventsToDB(self.serviceEvents, self.document.document_id)
            DiagnosticImagingStudy_to_DB.writeParticipantToDB(self.participants, self.document.document_id)
            DiagnosticImagingStudy_to_DB.writeAuthorToDB(self.authors, self.document.document_id)