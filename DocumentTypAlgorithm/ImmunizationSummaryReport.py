from Database_helper import ImmunizationSummaryReport_to_DB
from DocumentTypAlgorithm import CDA_R2_Header
from Element_Extractors.ImmunizationsReport_Elements import ImmunizationSection_Extractor, PersonengruppenSection_Extractor, ImpfrelevanteErkrankungenSection_Extractor, AntikoerperSection_Extractor

class ImmunizationSummaryReport(CDA_R2_Header.CDA_R2_Header):

    def __init__(self, root):
        super().__init__(root)
        self.root = root
        self.immunizationsSection = self.getImmunizationsSection()
        self.personengruppeSection = self.getPersonengruppeSection()
        self.impfrelevanteErkrankungenSection = self.getImpfrelevanteErkrankungenSection()
        self.antikörperSection = self.getAntikörperSection()

    def getImmunizationsSection(self):
        immunizations_section_element = self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='11369-6']/..")
        immunizations_section = ImmunizationSection_Extractor.ImmunizationSection()
        immunizations_section.extractImmunizationSectionFromXML(immunizations_section_element)
        return immunizations_section

    def getPersonengruppeSection(self):
        personengruppe_section_element = self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='11450-4']/..")
        personengruppe_section = PersonengruppenSection_Extractor.PersonengruppenSection()
        personengruppe_section.extractPersonengruppenSectionFromXML(personengruppe_section_element)
        return personengruppe_section

    def getImpfrelevanteErkrankungenSection(self):
        impfrelevanteerkrankungen_section_element = self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='11348-0']/..")
        impfrelevanteerkrankungen_section = ImpfrelevanteErkrankungenSection_Extractor.ImpfrelevanteErkrankungenSection()
        impfrelevanteerkrankungen_section.extractImpfrelevanteErkrankungenSectionFromXML(impfrelevanteerkrankungen_section_element)
        return impfrelevanteerkrankungen_section

    def getAntikörperSection(self):
        antikoerper_section_element = self.root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='26436-6']/..")
        antikoerper_section = AntikoerperSection_Extractor.AntikoerperSection()
        antikoerper_section.extractAntikoerperSectionFromXML(antikoerper_section_element)
        return antikoerper_section

    def writeImmunizationSummaryReportToDB(self):
        if (ImmunizationSummaryReport_to_DB.documentCheck(self.document.document_id)):
            print("Dokument mit ID: " + self.document.document_id + " bereits in DB vorhanden")
        else:
            ImmunizationSummaryReport_to_DB.writeCodesToDB(self.all_codes)
            ImmunizationSummaryReport_to_DB.writeTemplateIDsToDB(self.all_templateIds)
            ImmunizationSummaryReport_to_DB.writeAdressesToDB(self.all_addresses)
            ImmunizationSummaryReport_to_DB.writePatientToDB(self.patient)
            section_immunization_id = ImmunizationSummaryReport_to_DB.writeSectionImmunizationToDB(self.immunizationsSection)
            section_impfrelevanteErkrankungen_id = ImmunizationSummaryReport_to_DB.writeSectionImpfrelevanteErkrankungenToDB(self.impfrelevanteErkrankungenSection)
            section_antikoerper_id = ImmunizationSummaryReport_to_DB.writeSectionAntikoerperToDB(self.antikörperSection)
            ImmunizationSummaryReport_to_DB.writeDocumentInformationToDB(self.document, self.patient.SVNR.attrib["extension"], section_immunization_id, None, section_impfrelevanteErkrankungen_id, section_antikoerper_id)
            ImmunizationSummaryReport_to_DB.writeDocumentTemplatesToDB(self.document)
            ImmunizationSummaryReport_to_DB.writeServiceEventsToDB(self.serviceEvents, self.document.document_id)