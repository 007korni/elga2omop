[![No Maintenance Intended](http://unmaintained.tech/badge.svg)](http://unmaintained.tech/)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
# Description
ETL implementation artefacts for Master thesis of Raffael Lukas Korntheuer that aims to implement an ETL process for importing ELGA data into the OMOP CDM.

## Prerequisites
- Python 3.6 or above
    - additional packages (see requirements.txt)
        - mysql-connector-python 8.0.25 or above
        - psycopg2 2.9.2 or above
- PostgreSQL 9.6 or above
    - OMOP CDM v5.4.0 (see https://github.com/OHDSI/CommonDataModel/releases/tag/v5.4.0)
- MySQL 5.7 or above


## Supported ELGA document classes

| Document class | Description |
|----------------|-------------|
| `ELGA Entlassungsbrief Ärztlich` | CDA-Files > ELGA-023-Entlassungsbrief_aerztlich_EIS-FullSupport.xml |
| `ELGA Entlassungsbrief Pflege` | CDA-Files > ELGA-033-Entlassungsbrief_Pflege_EIS-FullSupport.xml |
| `ELGA Laborbefund` | CDA-Files > ELGA-043-Laborbefund_EIS-FullSupport.xml |
| `ELGA Befund bildgebende Diagnostik` | CDA-Files > ELGA-053-Befund_bildgebende_Diagnostik_EIS-FullSupport.xml |
| `ELGA Befund bildgebende Diagnostik` | CDA-Files > ELGA-053-Befund_bildgebende_Diagnostik_Mammographie_EIS-FullSupport.xml |
| `ELGA e-Medikation` | CDA-Files > ELGA-083-e-Medikation_5-Medikationsliste.xml |
| `e-Impfpass` | CDA-Files > eImpf-Kompletter_Immunisierungsstatus.xml |


## Installation
### Clone repository
First step ist to clone the repository to your local machine.

### Setup databases
---
#### MySQL

1. Install MySQL
Follow the official documentation for installing MySQL:
https://dev.mysql.com/downloads/mysql/

2. Create a database for the ELGA CDA documents
Depending on the document class, one of the following SQL scripts can be used to create the database:

| Document class | SQL-script |
|----------------|------------|
| `ELGA Entlassungsbrief Ärztlich` | SQL-Database-Skripte > [CREATE_Entlassungsbrief_Aerztlich](./SQL-Database-Skripte/CREATE_Entlassungsbrief_Aerztlich.sql)|
| `ELGA Entlassungsbrief Pflege` | SQL-Database-Skripte > [CREATE_Entlassungsbrief_Pflege](./SQL-Database-Skripte/CREATE_Entlassungsbrief_Pflege.sql)|
| `ELGA Laborbefund` | SQL-Database-Skripte > [CREATE-Laborbefund_database](./SQL-Database-Skripte/CREATE-Laborbefund_database.sql)|
| `ELGA Befund bildgebende Diagnostik` | SQL-Database-Skripte > [CREATE_BildgebendeDiagnostik](./SQL-Database-Skripte/CREATE_BildgebendeDiagnostik.sql)|
| `ELGA Befund bildgebende Diagnostik` | SQL-Database-Skripte > [CREATE_BildgebendeDiagnostik](./SQL-Database-Skripte/CREATE_BildgebendeDiagnostik.sql)|
| `ELGA e-Medikation` | SQL-Database-Skripte > [CREATE_Medikationsliste](./SQL-Database-Skripte/CREATE_Medikationsliste.sql)|
| `e-Impfpass` | SQL-Database-Skripte > [CREATE_e-Impfpass_database](./SQL-Database-Skripte/CREATE_e-Impfpass_database.sql )|

*Note: We are currently working on a single script to create all databases at once.*

3. Configure MySQL connection in Python script

Please enter the MySQL connection details in the Python scripts located in the `Database_helper` folder:

Edit the following lines in the files markes with `YOUR_HOST`, `YOUR_USERNAME` and `YOUR_PASSWORD`:
```python
def connectToDB():
    mydb = mysql.connector.connect(
        host="YOUR_HOST",
        user="YOUR_USERNAME",
        password="YOUR_PASSWORD",
    )
```

Please enter the MySQL connection details in the Python scripts located in the `OMOP` folder:

Edit the following lines in the files markes with `YOUR_HOST`, `YOUR_USERNAME` and `YOUR_PASSWORD`:
```python
def connectToELGA(self):
    mydb = mysql.connector.connect(
        host="YOUR_HOST",
        user="YOUR_USERNAME",
        password="YOUR_PASSWORD"
    )
```
#### PostgreSQL

1. Install PostgreSQL
Follow the official documentation for installing PostgreSQL:
https://www.postgresql.org/download/

2. Create a database for the OMOP CDM
Follow the official documentation for creating a database for the OMOP CDM:
https://github.com/OHDSI/CommonDataModel/releases/

3. Load vocabulary into database
Current vocabulary versions can be found here:
https://athena.ohdsi.org/

4. Configure PostgreSQL connection in Python script:
Please enter the PostgreSQL connection details in the Python scripts located in the `OMOP` folder:

Edit the following lines in the files markes with `YOUR_HOST`, `YOUR_USERNAME` and `YOUR_PASSWORD`:
```python
def connectToOMOP(self):
    con = psycopg2.connect(
        host="YOUR_HOST",
        user="YOUR_USERNAME",
        password="YOUR_PASSWORD")
        
```

## ELGA2OMOP ETL process

### Step 1: Put your CDA files into the `CDA-Files` folder or use the example ones

### Step 2: Add your path to the tree = ET.parse('YOUR_PATH') in the `main.py` file

### Step 3: Run the `main.py` file