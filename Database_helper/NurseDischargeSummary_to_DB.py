import mysql.connector

# Connect to MYSQL DB for Medication Summary Document
def connectToDB():
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        password="password",
        database="CDA_Entlassungsbrief_P"
    )
    return mydb

# Check if document is already in DB
def documentCheck(document_id):
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "SELECT 1 FROM `ClinicalDocument_CDAEntlassungsbrief_P` WHERE id = %s"
    val = (document_id, )
    mycursor.execute(sql, val)
    document_id = mycursor.fetchone()
    mydb.commit()
    mycursor.close()
    mydb.close()
    try:
        if document_id[0] == 1:
            return True
    except:
        return False

# Writing all codes from a document to DB
def writeCodesToDB(codes):
    print("Writing codes to DB ......")
    mydb = connectToDB()
    for c in codes:
        try:
            code = codes[c]["code"]
            codeSystem = codes[c]["codeSystem"]
            codeSystemName = codes[c]["codeSystemName"]
            displayName = codes[c]["displayName"]
        except:
            code = c
            codeSystem = None
            codeSystemName = None
            displayName = None
        mycursor = mydb.cursor()

        sql = "INSERT IGNORE INTO code (code, codeSystem, codeSystemName, displayName) VALUES (%s, %s, %s, %s)"
        val = (code, codeSystem, codeSystemName, displayName)
        mycursor.execute(sql, val)
        mydb.commit()
    mycursor.close()
    mydb.close()

# Writing all template IDs to DB
def writeTemplateIDsToDB(templates):
    print("Writing templates to DB ......")
    mydb = connectToDB()
    for t in templates:
        root = t.attrib["root"]
        try:
            assauth = t.attrib["assigningAuthorityName"]
        except:
            assauth = None

        mycursor = mydb.cursor()
        sql = "INSERT IGNORE INTO templateId (root, assigningAuthorityName) VALUES (%s, %s)"
        val = (root, assauth)
        mycursor.execute(sql, val)
        mydb.commit()
    mycursor.close()
    mydb.close()

# Writing all addresses to DB
def writeAdressesToDB(address_list):
    print("Writing addresses to DB ......")
    mydb = connectToDB()
    for a in address_list:
        try:
            houseNumber = a["houseNumber"]
            streetAddressLine = a["streetName"] + " " + houseNumber
            streetAddressLine.replace("\t", "").replace("\n", " ")
        except:
            try:
                streetAddressLine = a["streetAddressLine"].replace("\t", "").replace("\n", " ")
            except:
                continue
        try:
            postalCode = a["postalCode"]
        except:
            continue
        try:
            city = a["city"]
        except:
            city = None
        try:
            state = a["state"]
        except:
            state = None
        try:
            country = a["country"]
        except:
            country = None
        try:
            additionalLocator = a["additionalLocator"]
        except:
            additionalLocator = None

        mycursor = mydb.cursor()
        sql = "INSERT IGNORE INTO addr (streetAddressLine, postalCode, city, state, country, additionalLocator) VALUES (%s, %s, %s, %s, %s, %s)"
        val = (streetAddressLine, postalCode, city, state, country, additionalLocator)
        mycursor.execute(sql, val)
        mydb.commit()
    mycursor.close()
    mydb.close()

# Function to get the id of an address with streetAddressLine and postalCode
def getAdressID(streetAddressLine, postalCode):
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "SELECT id_addr FROM addr WHERE streetAddressLine = %s AND postalCode = %s"
    val = (streetAddressLine, postalCode)
    mycursor.execute(sql, val)
    addr_id = mycursor.fetchone()
    mydb.commit()
    mycursor.close()
    mydb.close()
    return addr_id

# Writing Patient from CDA-File to DB (if it does not already exist)
def writePatientToDB(patient):
    print("Writing patient to DB ......")
    mydb = connectToDB()
    try:
        addr_id = getAdressID(patient.addr.streetAddressLine, patient.addr.postalCode)
        addr_id = addr_id[0]
    except:
        addr_id = None
    # Writing Patient to DB
    mycursor = mydb.cursor()
    sql = "INSERT IGNORE INTO patientRole (id_SVNR, id_local, addr, given, family, administrativeGenderCode, birthTime) VALUES (%s, %s, %s, %s, %s, %s, %s)"
    val = (
        patient.SVNR.attrib["extension"], patient.local.attrib["extension"], addr_id, patient.patient_name.all_given,
        patient.patient_name.all_family, patient.patient_gender["code"], patient.birthtime)
    mycursor.execute(sql, val)
    mydb.commit()
    mycursor.close()
    mydb.close()

def writeEncompassingEncounterToDB(encompassingEncounter):
    print("Writing encompassing encounter to DB ......")
    try:
        person_addr_id = getAdressID(encompassingEncounter.assignedEntity.assEnt_addr.streetAddressLine,
                                     encompassingEncounter.assignedEntity.assEnt_addr.postalCoce)[0]
    except:
        person_addr_id = None
    try:
        assigned_org_addr_id = getAdressID(encompassingEncounter.assignedEntity.repOrg_addr.streetAddressLine,
                                           encompassingEncounter.assignedEntity.repOrg_addr.postalCoce)[0]
    except:
        assigned_org_addr_id = None
    try:
        location_addr_id = getAdressID(encompassingEncounter.location_addr.streetAddressLine,
                                       encompassingEncounter.location_addr.postalCode)[0]
    except:
        location_addr_id = None
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT IGNORE INTO encompassingEncounter (id, code, effectiveTime_low, effectiveTime_high, assignedEntity_id, person_name, person_addr, organisation_id, organisation_name, organisation_addr, location_id, location_name, location_addr) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
    val = (encompassingEncounter.encompassingEncounter_ID["extension"], encompassingEncounter.code["code"],
           encompassingEncounter.start_time[0:14], encompassingEncounter.end_time[0:14],
           encompassingEncounter.assignedEntity.assEnt_id["extension"],
           encompassingEncounter.assignedEntity.assPers_name.combinedName, person_addr_id,
           encompassingEncounter.assignedEntity.repOrg_id["root"], encompassingEncounter.assignedEntity.repOrg_name,
           assigned_org_addr_id, encompassingEncounter.location_ID, encompassingEncounter.location_name,
           location_addr_id)
    mycursor.execute(sql, val)
    mydb.commit()
    encompassingEncounter_ID = mycursor.lastrowid
    mycursor.close()
    mydb.close()

def writeSectionBrieftextToDB(section_Brieftext):
    print("Writing section Brieftext to DB ......")
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT IGNORE INTO section_Brieftext (templateId, code, title, text, entry) VALUES (%s, %s, %s, %s, %s)"
    val = (section_Brieftext.templateId, section_Brieftext.code, section_Brieftext.title, section_Brieftext.text,
           section_Brieftext.entry)
    mycursor.execute(sql, val)
    mydb.commit()
    section_Brieftext_ID = mycursor.lastrowid
    mycursor.close()
    mydb.close()
    return section_Brieftext_ID

def writePflegediagnosenSectionToDB(section_Pflegediagnosen):
    print("Writing section Pflegediagnosen to DB ......")
    entry = writePflegediagnosenActToDB(section_Pflegediagnosen.entry)
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT IGNORE INTO section_Pflegediagnosen (templateId, code, title, text, entry) VALUES (%s, %s, %s, %s, %s)"
    val = (section_Pflegediagnosen.templateId, section_Pflegediagnosen.code, section_Pflegediagnosen.title, section_Pflegediagnosen.text, entry)
    mycursor.execute(sql, val)
    mydb.commit()
    section_ID = mycursor.lastrowid
    mycursor.close()
    mydb.close()
    return section_ID

def writePflegediagnosenActToDB(act):
    observation_id = writePflegediagnosenActObservationToDB(act.entryRelationship)
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT IGNORE INTO Pflegediagnosen_act (templateId, id, code, statusCode, effectiveTime_low, effectiveTime_high, entryRelationship) VALUES (%s, %s, %s, %s, %s, %s, %s)"
    val = (act.templateIds[-1], act.id, act.code["code"], act.statusCode, act.effectiveTime_low, act.effectiveTime_high, observation_id)
    mycursor.execute(sql, val)
    mydb.commit()
    act_ID = mycursor.lastrowid
    mycursor.close()
    mydb.close()
    return act_ID

def writePflegediagnosenActObservationToDB(observation):
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT IGNORE INTO observation (templateId, id, code, text, statusCode, effectiveTime_low, effectiveTime_high, value) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
    val = (observation.templateIds[-1], observation.id, observation.code, observation.text, observation.statusCode, observation.effectiveTime_low, observation.effectiveTime_high, observation.value["code"])
    mycursor.execute(sql, val)
    mydb.commit()
    observation_ID = mycursor.lastrowid
    mycursor.close()
    mydb.close()
    return observation_ID

def writeMobilitaet_SectionToDB(section):
    section_ID = writeGeneralSectionToDB(section, "section_Mobilitaet")
    return section_ID

def writeMobilitaet_HilfsmittelToDB(section):
    section_ID = writeGeneralSectionToDB(section, "section_MobilitaetHilfsmittel")
    return section_ID

def writeKoerperpflege_SectionToDB(section):
    section_ID = writeGeneralSectionToDB(section, "section_Koerperpflege")
    return section_ID

def writeErnaehrung_SectionToDB(section):
    section_ID = writeGeneralSectionToDB(section, "section_Ernaehrung")
    return section_ID

def writeAusscheidung_SectionToDB(section):
    section_ID = writeGeneralSectionToDB(section, "section_Ausscheidung")
    return section_ID

def writeHautzustand_SectionToDB(section):
    section_ID = writeGeneralSectionToDB(section, "section_Hautzustand")
    return section_ID

def writeAtmung_SectionToDB(section):
    section_ID = writeGeneralSectionToDB(section, "section_Atmung")
    return section_ID

def writeSchlaf_SectionToDB(section):
    section_ID = writeGeneralSectionToDB(section, "section_Schlaf")
    return section_ID

def writeSchmerz_SectionToDB(section):
    section_ID = writeGeneralSectionToDB(section, "section_Schmerz")
    return section_ID

def writeOrientierung_SectionToDB(section):
    section_ID = writeGeneralSectionToDB(section, "section_Orientierung")
    return section_ID

def writeOrientierung_Risiken_SectionToDB(section):
    section_ID = writeGeneralSectionToDB(section, "section_OrientierungRisiken")
    return section_ID

def writeSozialeUmstaendeSectionToDB(section):
    section_ID = writeGeneralSectionToDB(section, "section_SozialeUmstaende")
    return section_ID

def writeKommunikationSectionToDB(section):
    section_ID = writeGeneralSectionToDB(section, "section_Kommunikation")
    return section_ID

def writeKommunikation_HilfsmittelSectionToDB(section):
    section_ID = writeGeneralSectionToDB(section, "section_KommunikationHilfsmittel")
    return section_ID

def writeRollenwahrnehmungSectionToDB(section):
    section_ID = writeGeneralSectionToDB(section, "section_Rollenwahrnehmung")
    return section_ID

def writeVitalparameterSectionToDB(vitalparameter_section):
    print("Writing section Entlassungsmanagement to DB ......")
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT IGNORE INTO section_Vitalparameter (templateId, text, code, title) VALUES (%s, %s, %s, %s)"
    val = (vitalparameter_section.templateIDs[-1], vitalparameter_section.text, vitalparameter_section.code,
           vitalparameter_section.title)
    mycursor.execute(sql, val)
    mydb.commit()
    section_ID = mycursor.lastrowid
    mycursor.close()
    mydb.close()
    for e in vitalparameter_section.entries:
        eo_id = writeVitalparameterEntryOrganizerToDB(e, section_ID)
        for o in e.observations:
            writeVitalparameterObservationToDB(o, eo_id)
    return section_ID

def writeVitalparameterEntryOrganizerToDB(entry_organizer, section_ID):
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT IGNORE INTO vitalparameter_entryOrganizer (templateId, id, code, statusCode, effectiveTime_low, effectiveTime_high, sectio_id) VALUES (%s, %s, %s, %s, %s, %s, %s)"
    val = (entry_organizer.templateIDs[-1], entry_organizer.id["root"], entry_organizer.code, entry_organizer.statusCode, entry_organizer.effectiveTime_low, entry_organizer.effectiveTime_high, section_ID)
    mycursor.execute(sql, val)
    mydb.commit()
    eo_id = mycursor.lastrowid
    mycursor.close()
    mydb.close()
    return eo_id

def writeVitalparameterObservationToDB(observation, eo_id):
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT IGNORE INTO vitalparameter_observation (templateId, code, effectiveTime, interpretationCode, value, unit, range_low, range_high, entryOrganizer_ID) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)"
    val = (observation.templateId, observation.code, observation.effectiveTime, observation.interpretationCode, observation.value, observation.unit, observation.range_low, observation.range_high, eo_id)
    mycursor.execute(sql, val)
    mydb.commit()
    mycursor.close()
    mydb.close()

def writePflegerelevanteInformationenSectionToDB(section):
    section_ID = writeGeneralSectionToDB(section, "section_PflegerelevanteInformationen")
    return section_ID

def writePflegerelevanteInformationen_RisikenSectionToDB(section):
    section_ID = writeGeneralSectionToDB(section, "section_PflegerelevanteInformationenRisiken")
    return section_ID

def writeMedikamentenverabreichungSectionToDB(section):
    section_ID = writeGeneralSectionToDB(section, "section_Medikamentenverabreichung")
    return section_ID

def writeAnmerkungenSectionToDB(section):
    section_ID = writeGeneralSectionToDB(section, "section_Anmerkungen")
    return section_ID

def writeEntlassungsmanagementSectionToDB(section_entlassungsmanagement):
    print("Writing section Entlassungsmanagement to DB ......")
    entry = writeEntlassungsmanagementActToDB(section_entlassungsmanagement.entry)
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT IGNORE INTO section_Entlassungsmanagement (templateId, code, title, text, entry) VALUES (%s, %s, %s, %s, %s)"
    val = (section_entlassungsmanagement.templateIDs[-1], section_entlassungsmanagement.code, section_entlassungsmanagement.title,
           section_entlassungsmanagement.text, entry)
    mycursor.execute(sql, val)
    mydb.commit()
    section_ID = mycursor.lastrowid
    mycursor.close()
    mydb.close()
    return section_ID

def writeEntlassungsmanagementActToDB(act):
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT IGNORE INTO Entlassungsmanagement_act (templateId, id, code, text, statusCode, effectiveTime_low, effectiveTime_high) VALUES (%s, %s, %s, %s, %s, %s, %s)"
    val = (act.templateIDs[-1], act.id, act.code, act.text, act.statusCode, act.effectiveTime_low, act.effectiveTime_high)
    mycursor.execute(sql, val)
    mydb.commit()
    act_ID = mycursor.lastrowid
    mycursor.close()
    mydb.close()
    return act_ID

def writePatientenverfügungSectionToDB(section):
    section_ID = writeGeneralSectionToDB(section, "section_Patientenverfuegung")
    return section_ID

def writeAbschliessendeBemerkungenSectionToDB(section):
    section_ID = writeGeneralSectionToDB(section, "section_AbschliessendeBemerkungen")
    return section_ID

def writeBeilagenSectionToDB(section):
    section_ID = writeGeneralSectionToDB(section, "section_Beilagen")
    return section_ID

def writeGeneralSectionToDB(section_general, section_name):
    print("Writing " + section_name + " to DB ......")
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT IGNORE INTO " + section_name + "(templateId, code, title, text, entry) VALUES (%s, %s, %s, %s, %s)"
    try:
        entry = section_general.entry
    except:
        entry = None
    val = (section_general.templateIDs[-1], section_general.code, section_general.title, section_general.text, entry)
    mycursor.execute(sql, val)
    mydb.commit()
    section_general_ID = mycursor.lastrowid
    mycursor.close()
    mydb.close()
    return section_general_ID

def writeNurseDischargeSummaryToDB(document, recordTarget, encompassingEncounter, brieftext_id, pflegediagnosen_id, section_Mobilitaet, section_MobilitaetHilfsmittel, section_Koerperpflege, section_Ernaehrung, section_Ausscheidung, section_Hautzustand, section_Atmung, section_Schlaf, section_Schmerz, section_Orientierung, section_OrientierungRisiken, section_SozialeUmstaende, section_Kommunikation, section_KommunikationHilfsmittel, section_Rollenwahrnehmung, section_Vitalparameter, section_PflegerelevanteInformationen, section_PflegerelevanteInformationenRisiken, section_Medikamentenverabreichung, section_Anmerkungen, section_Entlassungsmanagement, section_Patientenverfuegung, section_AbschliessendeBemerkungen, section_Beilagen):
    print("Writing document to DB ......")
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT IGNORE INTO ClinicalDocument_CDAEntlassungsbrief_P (id, realmCode, effectiveTime, typeId, code, title, confidentialityCode, languageCode, recordTarget, encompassingEncounter, section_Brieftext, section_Pflegediagnosen, section_Mobilitaet, section_MobilitaetHilfsmittel, section_Koerperpflege, section_Ernaehrung, section_Ausscheidung, section_Hautzustand, section_Atmung, section_Schlaf, section_Schmerz, section_Orientierung, section_OrientierungRisiken, section_SozialeUmstaende, section_Kommunikation, section_KommunikationHilfsmittel, section_Rollenwahrnehmung, section_Vitalparameter, section_PflegerelevanteInformationen, section_PflegerelevanteInformationenRisiken, section_Medikamentenverabreichung, section_Anmerkungen, section_Entlassungsmanagement, section_Patientenverfuegung, section_AbschliessendeBemerkungen, section_Beilagen) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
    val = (document.document_id, document.realmCode, document.effectiveTime[0:14], document.typeId, document.code,
           document.title, document.confidentialityCode, document.languageCode, recordTarget, encompassingEncounter, brieftext_id, pflegediagnosen_id, section_Mobilitaet, section_MobilitaetHilfsmittel, section_Koerperpflege, section_Ernaehrung, section_Ausscheidung, section_Hautzustand, section_Atmung, section_Schlaf, section_Schmerz, section_Orientierung, section_OrientierungRisiken, section_SozialeUmstaende, section_Kommunikation, section_KommunikationHilfsmittel, section_Rollenwahrnehmung, section_Vitalparameter, section_PflegerelevanteInformationen, section_PflegerelevanteInformationenRisiken, section_Medikamentenverabreichung, section_Anmerkungen, section_Entlassungsmanagement, section_Patientenverfuegung, section_AbschliessendeBemerkungen, section_Beilagen)
    mycursor.execute(sql, val)
    mydb.commit()
    mycursor.close()
    mydb.close()

def writeDocumentTemplatesToDB(document):
    print("Writing document-templates to DB ......")
    mydb = connectToDB()
    for template in document.templates:
        mycursor = mydb.cursor()
        sql = "INSERT IGNORE INTO ClinicalDocument_has_template (documentId, templateId) VALUES (%s, %s)"
        val = (document.document_id, template)
        mycursor.execute(sql, val)
        mydb.commit()
        mycursor.close()
    mydb.close()

def writeServiceEventsToDB(serviceEvents_list, documentID):
    print("Writing service events to DB ......")
    mydb = connectToDB()
    for sE in serviceEvents_list:
        if sE.performer != "":
            try:
                reporg_addrid = getAdressID(sE.performer.repOrg_addr.streetAddressLine, sE.performer.repOrg_addr.postalCode)
            except:
                reporg_addrid = None
            mycursor = mydb.cursor()
            sql = "INSERT IGNORE INTO serviceEvent (code, effectiveTime_low, effectiveTime_high, templateId, person_name, person_addr, organisation_id, organisation_name, organisation_addr, CDA_ID) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
            val = (sE.code["code"], sE.effectiveTime_low[0:14], sE.effectiveTime_high[0:14], sE.performer_templateId["root"], sE.performer.assPers_name.combinedName, None, sE.performer.repOrg_id["root"], sE.performer.repOrg_name, reporg_addrid[0], documentID )
            mycursor.execute(sql, val)
            mydb.commit()
            mycursor.close()
        else:
            mycursor = mydb.cursor()
            sql = "INSERT IGNORE INTO serviceEvent (code, effectiveTime_low, effectiveTime_high, CDA_ID) VALUES (%s, %s, %s, %s)"
            val = (sE.code["code"], sE.effectiveTime_low[0:14], sE.effectiveTime_high[0:14], documentID)
            mycursor.execute(sql, val)
            mydb.commit()
            mycursor.close()
    mydb.close()

def writeParticipantToDB(participants, documentID):
    print("Writing participants to DB ......")
    mydb = connectToDB()

    for parti in participants:
        try:
            organisation_addr_id = getAdressID(parti.scopingOrg_addr.streetAddressLine, parti.scopingOrg_addr.postalCode)[0]
        except:
            organisation_addr_id = None

        mycursor = mydb.cursor()
        sql = "INSERT IGNORE INTO participant (typeCode, templateId, functionCode, person_id, person_name, organisation_id, organisation_name, organisation_addr) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
        val = (parti.typeCode, parti.templateID, parti.function_code, parti.assEnt_id, parti.assPers_name_combined, parti.scopingOrg_id, parti.scopingOrg_name, organisation_addr_id)
        mycursor.execute(sql, val)
        mydb.commit()
        participant_DB_id = mycursor.lastrowid
        mycursor.close()

        mycursor = mydb.cursor()
        sql = "INSERT IGNORE INTO ClinicalDocument_has_participant (ClinicalDocument_ID, Participant_ID) VALUES (%s, %s)"
        val = (documentID, participant_DB_id)
        mycursor.execute(sql, val)
        mydb.commit()
        mycursor.close()
    mydb.close()

def writeAuthorToDB(authors, document_id):
    print("Writing authors to DB .......")
    mydb = connectToDB()
    for author in authors:
        try:
            addr_id = getAdressID(author.rep_organisation_addr.streetAddressLine, author.rep_organisation_addr.postalCode)
        except:
            addr_id = None
        mycursor = mydb.cursor()
        sql = "INSERT IGNORE INTO author (time, functionCode, ass_author_id, ass_author_code, ass_author_name, auth_device_code, auth_device_manufacturerModelName, auth_device_softwareName, rep_organisation_id, rep_organisation_name, rep_organisation_addr) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        val = (author.time_of_authoring, author.functionCode["code"], author.ass_author_id["root"], author.auth_device_code, author.ass_author_name.combinedName, author.auth_device_code, author.auth_device_manufacturerModelName, author.auth_device_softwareName, author.rep_organisation_id["root"], author.rep_organisation_name, addr_id[0])
        mycursor.execute(sql, val)
        mydb.commit()
        author_ID = mycursor.lastrowid
        mycursor.close()
        writeAuthorDocumentToDB(author_ID, document_id)
    mydb.close()

def writeAuthorDocumentToDB(author_id, document_id):
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT INTO ClinicalDocument_has_author (ClinicalDocument_ID, author) VALUES (%s, %s)"
    val = (document_id, author_id)
    mycursor.execute(sql, val)
    mydb.commit()
    mycursor.close()
    mydb.close()