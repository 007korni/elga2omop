from Element_Extractors import Patient_Extractor
import psycopg2

class patient_to_PSQL:

    patient = Patient_Extractor.Patient()

    def __init__(self):
        self.patient = ""

    def write_patient_to_PSQL(self, patient):

        SVNR = ""
        BPKGH = ""
        for x in patient.ids:
            if x['root'] == '1.2.40.0.10.1.4.3.1':
                SVNR = x['extension']
            else:
                BPKGH = x['extension']

        birthTime = patient.birthtime
        prefix = " "
        if (len(patient.patient_name.prefix) > 1):
            prefix = prefix.join(patient.patient_name.prefix)
        else:
            prefix = patient.patient_name.prefix[0]

        first = " "
        if (len(patient.patient_name.given) > 1):
            tempfirst = patient.patient_name.given
            first = first.join(tempfirst)
        else:
            first = patient.patient_name.given[0]

        family = patient.patient_name.family[0]

        suffix = " "
        if (len(patient.patient_name.suffix) > 1):
            suffix = suffix.join(patient.patient_name.suffix)
        else:
            suffix = patient.patient_name.suffix[0]

        gender_code = patient.patient_gender['code']

        birthAddress = patient.birthplace.singleAddressLine()

        streetAdressLine_home = patient.addr.streetAddressLine
        city_home = patient.addr.city
        state_home = patient.addr.state
        country_home = patient.addr.country
        postalCode_home = patient.addr.postalCode

        con = psycopg2.connect(
            host="192.168.0.24",
            database="ELGA_import",
            user="postgres",
            password="password")

        cur = con.cursor()
        cur.execute("SELECT concept_id FROM concept WHERE concept_code = '26515-7' ")

        rows = cur.fetchall()
        print(rows)

