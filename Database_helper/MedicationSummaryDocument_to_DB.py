import mysql.connector

# Connect to MYSQL DB for Medication Summary Document
def connectToDB():
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        password="password",
        database="CDA_Medikationsliste"
    )
    return mydb

# Check if document is already in DB
def documentCheck(document_id):
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "SELECT 1 FROM `ClinicalDocument_CDA-Medikationsliste` WHERE id = %s"
    val = (document_id, )
    mycursor.execute(sql, val)
    document_id = mycursor.fetchone()
    mydb.commit()
    mycursor.close()
    mydb.close()
    try:
        if document_id[0] == 1:
            return True
    except:
        return False

# Writing all codes from a document to DB
def writeCodesToDB(codes):
    print("Writing codes to DB ......")
    mydb = connectToDB()
    for c in codes:
        try:
            code = codes[c]["code"]
            codeSystem = codes[c]["codeSystem"]
            codeSystemName = codes[c]["codeSystemName"]
            displayName = codes[c]["displayName"]
        except:
            code = c
            codeSystem = None
            codeSystemName = None
            displayName = None
        mycursor = mydb.cursor()

        sql = "INSERT IGNORE INTO code (code, codeSystem, codeSystemName, displayName) VALUES (%s, %s, %s, %s)"
        val = (code, codeSystem, codeSystemName, displayName)
        mycursor.execute(sql, val)
        mydb.commit()
    mycursor.close()
    mydb.close()

# Writing all template IDs to DB
def writeTemplateIDsToDB(templates):
    print("Writing templates to DB ......")
    mydb = connectToDB()
    for t in templates:
        root = t.attrib["root"]
        try:
            assauth = t.attrib["assigningAuthorityName"]
        except:
            assauth = None

        mycursor = mydb.cursor()
        sql = "INSERT IGNORE INTO templateId (root, assigningAuthorityName) VALUES (%s, %s)"
        val = (root, assauth)
        mycursor.execute(sql, val)
        mydb.commit()
    mycursor.close()
    mydb.close()

# Writing all addresses to DB
def writeAdressesToDB(address_list):
    print("Writing addresses to DB ......")
    mydb = connectToDB()
    for a in address_list:
        try:
            houseNumber = a["houseNumber"]
            streetAddressLine = a["streetName"] + " " + houseNumber
            streetAddressLine.replace("\t", "").replace("\n", " ")
        except:
            try:
                streetAddressLine = a["streetAddressLine"].replace("\t", "").replace("\n", " ")
            except:
                continue
        try:
            postalCode = a["postalCode"]
        except:
            continue
        try:
            city = a["city"]
        except:
            city = None
        try:
            state = a["state"]
        except:
            state = None
        try:
            country = a["country"]
        except:
            country = None
        try:
            additionalLocator = a["additionalLocator"]
        except:
            additionalLocator = None

        mycursor = mydb.cursor()
        sql = "INSERT IGNORE INTO addr (streetAddressLine, postalCode, city, state, country, additionalLocator) VALUES (%s, %s, %s, %s, %s, %s)"
        val = (streetAddressLine, postalCode, city, state, country, additionalLocator)
        mycursor.execute(sql, val)
        mydb.commit()
    mycursor.close()
    mydb.close()

# Function to get the id of an address with streetAddressLine and postalCode
def getAdressID(streetAddressLine, postalCode):
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "SELECT id_addr FROM addr WHERE streetAddressLine = %s AND postalCode = %s"
    val = (streetAddressLine, postalCode)
    mycursor.execute(sql, val)
    addr_id = mycursor.fetchone()
    mydb.commit()
    mycursor.close()
    mydb.close()
    return addr_id

# Writing Patient from CDA-File to DB (if it does not already exist)
def writePatientToDB(patient):
    print("Writing patient to DB ......")
    mydb = connectToDB()
    try:
        addr_id = getAdressID(patient.addr.streetAddressLine, patient.addr.postalCode)
        addr_id = addr_id[0]
    except:
        addr_id = None
    # Writing Patient to DB
    mycursor = mydb.cursor()
    sql = "INSERT IGNORE INTO patientRole (id_SVNR, id_local, addr, given, family, administrativeGenderCode, birthTime) VALUES (%s, %s, %s, %s, %s, %s, %s)"
    val = (
        patient.SVNR.attrib["extension"], patient.local.attrib["extension"], addr_id, patient.patient_name.all_given,
        patient.patient_name.all_family, patient.patient_gender["code"], patient.birthtime)
    mycursor.execute(sql, val)
    mydb.commit()
    mycursor.close()
    mydb.close()

# Writing History of medication use to DB
def writeHistoryOfMedicationUseToDB(section):
    print("Writing History of medication to DB ......")
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT INTO section_HistoryOfMedicationUse (templateID, id, code, title, text) VALUES (%s, %s, %s, %s, %s)"
    val = (section.templateIds[-1]["root"], section.id["extension"], section.code, section.title, section.text)
    mycursor.execute(sql, val)
    mydb.commit()
    section_ID = mycursor.lastrowid
    for suAd in section.substanceAdministrations:
        writeSubstanceAdministrationToDB(suAd, section_ID, True)
    for supply in section.supplies:
        writesupplyToDB(supply, section_ID)
    mycursor.close()
    mydb.close()
    return section_ID

def writeSubstanceAdministrationToDB(suAd, section_ID, standard):
    dosing_in_sA = False
    mydb = connectToDB()
    mycursor = mydb.cursor()
    if standard:
        sql = "INSERT INTO substanceAdministration (templateId, id, id2, statusCode, effectiveTime_low, effectiveTime_high, effectiveTime_period_value, effectiveTime_period_unit, effectiveTime_width_value, effectiveTime_width_unit, effectiveTime_event_code, effectiveTime_offset_value, effectiveTime_offset_unit, dosing, repeatNumber, routeCode, doseQuantity, eR_supply_independentInd, eR_supply_quantity, eR_act_templateID, eR_act_code, section_id, sA_dosing)  VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        try:
            if suAd.entryRealtionship_dosing[1] != None:
                dosing_in_sA = True
        except:
            dosing = None

        try:
            template = suAd.templateIDs[-1]
        except:
            template = None

        try:
            id = suAd.id[0]["extension"]
            id2 = suAd.id[1]["extension"]
        except:
            id = None
            id2 = None

        val = (template, id, id2, suAd.statusCode, suAd.effectiveTime_low, suAd.effectiveTime_high,
               suAd.effectiveTime_period_value, suAd.effectiveTime_period_unit, suAd.effectiveTime_width_value,
               suAd.effectiveTime_width_unit, suAd.effectiveTime_event_code, suAd.effectiveTime_offset_value,
               suAd.effectiveTime_offset_unit, suAd.dosing, suAd.repeatNumber, suAd.routeCode, suAd.doseQuantity,
               suAd.entryRealtionship_supply_independentInd, suAd.entryRealtionship_supply_quantity,
               suAd.entryRealtionship_act_templateID, suAd.entryRealtionship_act_code, section_ID, dosing)
    else:
        sql = "INSERT INTO substanceAdministration (templateId, id, id2, statusCode, effectiveTime_low, effectiveTime_high, effectiveTime_period_value, effectiveTime_period_unit, effectiveTime_width_value, effectiveTime_width_unit, effectiveTime_event_code, effectiveTime_offset_value, effectiveTime_offset_unit, dosing, repeatNumber, routeCode, doseQuantity, eR_supply_independentInd, eR_supply_quantity, eR_act_templateID, eR_act_code, section_id, sA_dosing, supply_id)  VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        try:
            if suAd.entryRealtionship_dosing[1] != None:
                dosing_in_sA = True
        except:
            dosing = None

        try:
            template = suAd.templateIDs[-1]
        except:
            template = None

        try:
            id = suAd.id[0]["extension"]
            id2 = suAd.id[1]["extension"]
        except:
            id = None
            id2 = None

        val = (template, id, id2, suAd.statusCode, suAd.effectiveTime_low, suAd.effectiveTime_high,
               suAd.effectiveTime_period_value, suAd.effectiveTime_period_unit, suAd.effectiveTime_width_value,
               suAd.effectiveTime_width_unit, suAd.effectiveTime_event_code, suAd.effectiveTime_offset_value,
               suAd.effectiveTime_offset_unit, suAd.dosing, suAd.repeatNumber, suAd.routeCode, suAd.doseQuantity,
               suAd.entryRealtionship_supply_independentInd, suAd.entryRealtionship_supply_quantity,
               suAd.entryRealtionship_act_templateID, suAd.entryRealtionship_act_code, None, dosing, section_ID)
    mycursor.execute(sql, val)
    mydb.commit()
    suAd_ID = mycursor.lastrowid
    mycursor.close()
    mydb.close()
    writeconsumableToDB(suAd.consumable, suAd_ID)
    if dosing_in_sA:
        for dose in suAd.dosing:
            writeDosing_substanceAdministrationToDB(suAd.dosing[dose], suAd_ID)

def writeconsumableToDB(consumable, suAd_ID):
    print("--Consumable")
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT INTO consumable (templateId, manufacturedMaterial_templateID, manufacturedMaterial_code, manufacturedMaterial_name, formCode, capacityQuantity, capacityUnit, pharmCode, pharmName, sA_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
    try:
        c_template = consumable.templateIDs[-1]
    except:
        c_template = None
    try:
        mm_template = consumable.manufacturedMaterial_templateIDs[-1]
    except:
        mm_template = None

    val = (
        c_template, mm_template,
        consumable.manufacturedMaterial_code,
        consumable.manufacturedMaterial_name, consumable.formCode, consumable.capacityQuantity, consumable.capacityUnit,
        consumable.pharmCode, consumable.pharmName, suAd_ID)
    mycursor.execute(sql, val)
    mydb.commit()
    mycursor.close()
    mydb.close()

def writeDosing_substanceAdministrationToDB(suAd, suAd_Id):
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT INTO substanceAdministration (templateId, id, statusCode, effectiveTime_low, effectiveTime_high, effectiveTime_period_value, effectiveTime_period_unit, effectiveTime_width_value, effectiveTime_width_unit, effectiveTime_event_code, effectiveTime_offset_value, effectiveTime_offset_unit, dosing, repeatNumber, routeCode, doseQuantity, eR_supply_independentInd, eR_supply_quantity, eR_act_templateID, eR_act_code, section_id, sA_dosing)  VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
    val = (suAd.templateIDs[-1], suAd.id["extension"], suAd.statusCode, suAd.effectiveTime_low, suAd.effectiveTime_high,
           suAd.effectiveTime_period_value, suAd.effectiveTime_period_unit, suAd.effectiveTime_width_value,
           suAd.effectiveTime_width_unit, suAd.effectiveTime_event_code, suAd.effectiveTime_offset_value,
           suAd.effectiveTime_offset_unit, suAd.dosing, suAd.repeatNumber, suAd.routeCode, suAd.doseQuantity,
           suAd.entryRealtionship_supply_independentInd, suAd.entryRealtionship_supply_quantity,
           suAd.entryRealtionship_act_templateID, suAd.entryRealtionship_act_code, None, suAd_Id)
    mycursor.execute(sql, val)
    mydb.commit()
    mycursor.close()
    mydb.close()

def writesupplyToDB(supply, section_ID):
    product_id = writeProductToDB(supply.product)
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT INTO supply (templateId, quantity, product_id, section_id) VALUES (%s, %s, %s, %s)"
    val = (supply.templateIDs[-1], supply.quantity, product_id, section_ID)
    mycursor.execute(sql, val)
    mydb.commit()
    supply_ID = mycursor.lastrowid
    mycursor.close()
    mydb.close()
    for suAd in supply.entryRelationships:
        writeSubstanceAdministrationToDB(suAd, supply_ID, False)

def writeProductToDB(product):
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT INTO product (manufacturedProduct_templateId, manufacturedMaterial_templateId, manufacturedMaterial_code, manufacturedMaterial_name, manufacturedMaterial_pharm_formCode, manufacturedMaterial_pharm_capacityQuantity, manufacturedMaterial_pharm_ingredient_code, manufacturedMaterial_pharm_ingredient_name) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
    val = (product.manufacturedProduct_templateIDs[-1], product.manufacturedMaterial_templateIDs[-1], product.manufacturedMaterial_code, product.manufacturedMaterial_name, product.manufacturedMaterial_pharm_formCode, product.manufacturedMaterial_pharm_capacityQuantity, product.manufacturedMaterial_pharm_ingredient_code, product.manufacturedMaterial_pharm_ingredient_name)
    mycursor.execute(sql, val)
    mydb.commit()
    product_ID = mycursor.lastrowid
    mycursor.close()
    mydb.close()
    return product_ID

def writeMedicationSummaryDocumentToDB(document, recordTarget, section_historyofmedicationuse):
    print("Writing document to DB ......")
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT IGNORE INTO `ClinicalDocument_CDA-Medikationsliste` (id, realmCode, effectiveTime, typeId, code, title, confidentialityCode, languageCode, recordTarget, section_HistoryOfMedicationUse) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
    val = (document.document_id, document.realmCode, document.effectiveTime[0:14], document.typeId, document.code, document.title, document.confidentialityCode, document.languageCode, recordTarget, section_historyofmedicationuse)
    mycursor.execute(sql, val)
    mydb.commit()
    mycursor.close()
    mydb.close()

def writeDocumentTemplatesToDB(document):
    print("Writing document-templates to DB ......")
    mydb = connectToDB()
    for template in document.templates:
        mycursor = mydb.cursor()
        sql = "INSERT IGNORE INTO ClinicalDocument_has_template (documentId, templateId) VALUES (%s, %s)"
        val = (document.document_id, template)
        mycursor.execute(sql, val)
        mydb.commit()
        mycursor.close()
    mydb.close()

def writeAuthorToDB(authors, document_id):
    print("Writing authors to DB .......")
    mydb = connectToDB()
    for author in authors:
        try:
            addr_id = getAdressID(author.rep_organisation_addr.streetAddressLine, author.rep_organisation_addr.postalCode)
        except:
            addr_id = None
        mycursor = mydb.cursor()
        sql = "INSERT IGNORE INTO author (time, functionCode, ass_author_id, ass_author_code, ass_author_name, auth_device_code, auth_device_manufacturerModelName, auth_device_softwareName, rep_organisation_id, rep_organisation_name, rep_organisation_addr) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        val = (author.time_of_authoring, author.functionCode, author.ass_author_id["root"], author.auth_device_code, author.ass_author_name, author.auth_device_code, author.auth_device_manufacturerModelName, author.auth_device_softwareName, author.rep_organisation_id["root"], author.rep_organisation_name, addr_id)
        mycursor.execute(sql, val)
        mydb.commit()
        author_ID = mycursor.lastrowid
        mycursor.close()
        writeAuthorDocumentToDB(author_ID, document_id)
    mydb.close()

def writeAuthorDocumentToDB(author_id, document_id):
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT INTO ClinicalDocument_has_author (ClinicalDocument, author) VALUES (%s, %s)"
    val = (document_id, author_id)
    mycursor.execute(sql, val)
    mydb.commit()
    mycursor.close()
    mydb.close()