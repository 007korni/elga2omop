import mysql.connector


def connectToDB():
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        password="password",
        database="CDA_Entlassungsbrief_Arzt"
    )
    return mydb

# Check if document is already in DB
def documentCheck(document_id):
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "SELECT 1 FROM ClinicalDocument_CDAEntlassungsbrief_A WHERE id = %s"
    val = (document_id, )
    mycursor.execute(sql, val)
    document_id = mycursor.fetchone()
    mydb.commit()
    mycursor.close()
    mydb.close()
    try:
        if document_id[0] == 1:
            return True
    except:
        return False

def writeCodesToDB(codes):
    print("Writing codes to DB ......")
    mydb = connectToDB()
    for c in codes:
        try:
            code = codes[c]["code"]
            codeSystem = codes[c]["codeSystem"]
            codeSystemName = codes[c]["codeSystemName"]
            displayName = codes[c]["displayName"]
        except:
            code = c
            codeSystem = None
            codeSystemName = None
            displayName = None
        mycursor = mydb.cursor()

        sql = "INSERT IGNORE INTO code (code, codeSystem, codeSystemName, displayName) VALUES (%s, %s, %s, %s)"
        val = (code, codeSystem, codeSystemName, displayName)
        mycursor.execute(sql, val)
        mydb.commit()
    mycursor.close()
    mydb.close()


def writeTemplateIDsToDB(templates):
    print("Writing templates to DB ......")
    mydb = connectToDB()
    for t in templates:
        root = t.attrib["root"]
        try:
            assauth = t.attrib["assigningAuthorityName"]
        except:
            assauth = None

        mycursor = mydb.cursor()
        sql = "INSERT IGNORE INTO templateId (root, assigningAuthorityName) VALUES (%s, %s)"
        val = (root, assauth)
        mycursor.execute(sql, val)
        mydb.commit()
    mycursor.close()
    mydb.close()


def writeAdressesToDB(address_list):
    print("Writing addresses to DB ......")
    mydb = connectToDB()
    for a in address_list:
        try:
            houseNumber = a["houseNumber"]
            streetAddressLine = a["streetName"] + " " + houseNumber
            streetAddressLine.replace("\t", "").replace("\n", " ")
        except:
            try:
                streetAddressLine = a["streetAddressLine"].replace("\t", "").replace("\n", " ")
            except:
                continue
        try:
            postalCode = a["postalCode"]
        except:
            continue
        try:
            city = a["city"]
        except:
            city = None
        try:
            state = a["state"]
        except:
            state = None
        try:
            country = a["country"]
        except:
            country = None
        try:
            additionalLocator = a["additionalLocator"]
        except:
            additionalLocator = None

        mycursor = mydb.cursor()
        sql = "INSERT IGNORE INTO addr (streetAddressLine, postalCode, city, state, country, additionalLocator) VALUES (%s, %s, %s, %s, %s, %s)"
        val = (streetAddressLine, postalCode, city, state, country, additionalLocator)
        mycursor.execute(sql, val)
        mydb.commit()
    mycursor.close()
    mydb.close()


def getAdressID(streetAddressLine, postalCode):
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "SELECT id_addr FROM addr WHERE streetAddressLine = %s AND postalCode = %s"
    val = (streetAddressLine, postalCode)
    mycursor.execute(sql, val)
    addr_id = mycursor.fetchone()
    mydb.commit()
    mycursor.close()
    mydb.close()
    return addr_id


def writePatientToDB(patient):
    print("Writing patient to DB ......")
    mydb = connectToDB()
    addr_id = getAdressID(patient.addr.streetAddressLine, patient.addr.postalCode)

    # Writing Patient to DB
    mycursor = mydb.cursor()
    sql = "INSERT IGNORE INTO patientRole (id_SVNR, id_local, addr, given, family, administrativeGenderCode, birthTime) VALUES (%s, %s, %s, %s, %s, %s, %s)"
    val = (
        patient.SVNR.attrib["extension"], patient.local.attrib["extension"], addr_id[0], patient.patient_name.all_given,
        patient.patient_name.all_family, patient.patient_gender["code"], patient.birthtime)
    mycursor.execute(sql, val)
    mydb.commit()
    mycursor.close()
    mydb.close()

def writelegalAuthenticatorToDB(legalAuthenticator):
    print("Writing legalAuthenticator to DB ......")
    mydb = connectToDB()
    if legalAuthenticator.assignedEntity.assEnt_addr is not None:
        assEnt_addr_id = getAdressID(legalAuthenticator.assignedEntity.assEnt_addr.streetAddressLine, legalAuthenticator.assignedEntity.assEnt_addr.postalCode)[0]
    else:
        assEnt_addr_id = None
    if legalAuthenticator.assignedEntity.repOrg_addr is not None:
        repOrg_addr_id = getAdressID(legalAuthenticator.assignedEntity.repOrg_addr.streetAddressLine, legalAuthenticator.assignedEntity.repOrg_addr.postalCode)[0]
    else:
        repOrg_addr_id = None

    # legalAuthenticator
    mycursor = mydb.cursor()
    sql = "INSERT IGNORE INTO legalAuthenticator (time, signatureCode, assEnt_id, assEnt_addr, assPers_name, repOrg_id, repOrg_name, repOrg_addr) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
    val = (legalAuthenticator.time[0:14], legalAuthenticator.signatureCode["code"], legalAuthenticator.assignedEntity.assEnt_id["extension"], assEnt_addr_id, legalAuthenticator.assignedEntity.assPers_name.combinedName, legalAuthenticator.assignedEntity.repOrg_id["root"], legalAuthenticator.assignedEntity.repOrg_name, repOrg_addr_id)
    mycursor.execute(sql, val)
    mydb.commit()
    legalAuthenticator_ID = mycursor.lastrowid
    mycursor.close()
    mydb.close()
    return legalAuthenticator_ID


def writeEncompassingEncounterToDB(encompassingEncounter):
    print("Writing encompassing encounter to DB ......")
    try:
        person_addr_id = getAdressID(encompassingEncounter.assignedEntity.assEnt_addr.streetAddressLine,
                                     encompassingEncounter.assignedEntity.assEnt_addr.postalCoce)[0]
    except:
        person_addr_id = None
    try:
        assigned_org_addr_id = getAdressID(encompassingEncounter.assignedEntity.repOrg_addr.streetAddressLine,
                                           encompassingEncounter.assignedEntity.repOrg_addr.postalCoce)[0]
    except:
        assigned_org_addr_id = None
    try:
        location_addr_id = getAdressID(encompassingEncounter.location_addr.streetAddressLine,
                                       encompassingEncounter.location_addr.postalCode)[0]
    except:
        location_addr_id = None
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT IGNORE INTO encompassingEncounter (id, code, effectiveTime_low, effectiveTime_high, assignedEntity_id, person_name, person_addr, organisation_id, organisation_name, organisation_addr, location_id, location_name, location_addr) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
    val = (encompassingEncounter.encompassingEncounter_ID["extension"], encompassingEncounter.code["code"],
           encompassingEncounter.start_time[0:14], encompassingEncounter.end_time[0:14],
           encompassingEncounter.assignedEntity.assEnt_id["extension"],
           encompassingEncounter.assignedEntity.assPers_name.combinedName, person_addr_id,
           encompassingEncounter.assignedEntity.repOrg_id["root"], encompassingEncounter.assignedEntity.repOrg_name,
           assigned_org_addr_id, encompassingEncounter.location_ID, encompassingEncounter.location_name,
           location_addr_id)
    mycursor.execute(sql, val)
    mydb.commit()
    encompassingEncounter_ID = mycursor.lastrowid
    mycursor.close()
    mydb.close()


def writeSectionBrieftextToDB(section_Brieftext):
    print("Writing section Brieftext to DB ......")
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT IGNORE INTO section_Brieftext (templateId, code, title, text, entry) VALUES (%s, %s, %s, %s, %s)"
    val = (section_Brieftext.templateId, section_Brieftext.code, section_Brieftext.title, section_Brieftext.text,
           section_Brieftext.entry)
    mycursor.execute(sql, val)
    mydb.commit()
    section_Brieftext_ID = mycursor.lastrowid
    mycursor.close()
    mydb.close()
    return section_Brieftext_ID


def writeGeneralSectionToDB(section_general, section_name):
    print("Writing " + section_name + " to DB ......")
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT IGNORE INTO " + section_name + "(templateId, code, title, text, entry) VALUES (%s, %s, %s, %s, %s)"
    try:
        entry = section_general.entry
    except:
        entry = None
    val = (section_general.templateIDs[-1], section_general.code, section_general.title, section_general.text, entry)
    mycursor.execute(sql, val)
    mydb.commit()
    section_general_ID = mycursor.lastrowid
    mycursor.close()
    mydb.close()
    return section_general_ID


def writeHospitalDischargeSectionToDB(section):
    print("Writing hospital discharge section to DB ......")
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT IGNORE INTO section_HospitalDischarge (templateId, code, title, text) VALUES (%s, %s, %s, %s)"
    val = (section.templateIDs[-1], section.code, section.title, section.text)
    mycursor.execute(sql, val)
    mydb.commit()
    section_ID = mycursor.lastrowid
    mycursor.close()
    mydb.close()
    # Entries in DB schreiben
    for entry in section.entries:
        writeHospitalDischargeSection_entryToDB(entry, section_ID)

    return section_ID


def writeHospitalDischargeSection_entryToDB(entry, section_ID):
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT IGNORE INTO HD_entry (act_templateId, act_id, act_code, status_code, effectiveTime_low, effectiveTime_high, HD_ID) VALUES (%s, %s, %s, %s, %s, %s, %s)"
    ef_high = None
    ef_low = None
    try:
        ef_low = entry.effectiveTime_low[0:14]
    except:
        ef_low = None
    try:
        ef_high = entry.effectiveTime_high[0:14]
    except:
        ef_high = None
    val = (entry.act_templateIDs[-1], entry.act_id, entry.act_code, entry.act_statusCode, ef_low,
           ef_high, section_ID)
    mycursor.execute(sql, val)
    mydb.commit()
    entry_ID = mycursor.lastrowid
    mycursor.close()
    mydb.close()
    # observations in DB schreiben
    for observation in entry.observations:
        writeHospitalDischargeObservationToDB(observation, entry_ID)


def writeHospitalDischargeObservationToDB(observation, entry_ID):
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT IGNORE INTO HD_entry_observation (templateId, code, effectiveTime_low, effectiveTime_high, value_code, entry_ID) VALUES (%s, %s, %s, %s, %s, %s)"
    ef_high = None
    ef_low = None
    try:
        ef_low = observation.effectiveTime_low[0:14]
    except:
        ef_low = None
    try:
        ef_high = observation.effectiveTime_high[0:14]
    except:
        ef_high = None
    val = (observation.templateIDs[-1], observation.code, ef_low,
           ef_high, observation.value_code["code"], entry_ID)
    mycursor.execute(sql, val)
    mydb.commit()
    entry_ID = mycursor.lastrowid
    mycursor.close()
    mydb.close()


def writeErhobeneBefunde_VitalparameterToDB(section):
    print("Writing Erhobene Befunde - Vitalparameter section to DB ......")
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT IGNORE INTO section_ErhobeneBefunde_Vitalparameter (templateId, code, title, text) VALUES (%s, %s, %s, %s)"
    val = (section.templateIDs[-1], section.code, section.title, section.text)
    mycursor.execute(sql, val)
    mydb.commit()
    section_ID = mycursor.lastrowid
    mycursor.close()
    mydb.close()
    # Entries in DB schreiben
    for entryOrganizer in section.entries:
        writeErhobeneBefunde_Vitalaparameter_entryOgranizerToDB(entryOrganizer, section_ID)
    return section_ID

def writeErhobeneBefunde_Vitalaparameter_entryOgranizerToDB(entryO, section_ID):
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT INTO EB_EntryOrganizer (templateid, id, code, statuscode, effectivetime_low, effectivetime_high, sectio_id) VALUES (%s, %s, %s, %s, %s, %s, %s)"
    val = (entryO.templateIDs[-1], entryO.id["root"], entryO.code, entryO.statusCode, entryO.effectiveTime_low, entryO.effectiveTime_high, section_ID)
    mycursor.execute(sql, val)
    mydb.commit()
    entryO_ID = mycursor.lastrowid
    mycursor.close()
    mydb.close()
    for observation in entryO.observations:
        writeErhobeneBefunde_Vitalparameter_observationToDB(observation, entryO_ID)


def writeErhobeneBefunde_Vitalparameter_observationToDB(observation, entryOrganizer_ID):
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT INTO EB_EO_observation (templateId, code, effectiveTime, interpretationCode, value, unit, range_low, range_high, entryOrganizer_ID) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)"
    if observation.effectiveTime == "":
        observation.effectiveTime = None
    if observation.interpretationCode == "":
        observation.interpretationCode = None
    val = (observation.templateId, observation.code, observation.effectiveTime, observation.interpretationCode,
           observation.value, observation.unit, observation.range_low, observation.range_high, entryOrganizer_ID)
    mycursor.execute(sql, val)
    mydb.commit()
    mycursor.close()
    mydb.close()


def writeEmpfohleneMedikationToDB(section):
    section_ID = writeGeneralSectionToDB(section, "section_EmpfohleneMedikation")
    for suAd in section.substanceAdministration:
        writeEmpfohleneMedikationsubstanceAdministrationToDB(suAd, section_ID)
    return section_ID


def writeEmpfohleneMedikationsubstanceAdministrationToDB(substancead, section_ID):
    print("--Substance Administration")
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT INTO EM_substanceAdministration (templateId, id, statusCode, effectiveTime_low, effectiveTime_high, repeatNumber, routeCode, doseQuantity, eR_supply_independentInd, eR_supply_quantity, eR_act_templateID, eR_act_code, section_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
    val = (substancead.templateIDs[-1], substancead.id["extension"], substancead.statusCode, substancead.effectiveTime_low,
           substancead.effectiveTime_high, substancead.repeatNumber, substancead.routeCode, substancead.doseQuantity,
           substancead.entryRealtionship_supply_independentInd, substancead.entryRealtionship_supply_quantity,
           substancead.entryRealtionship_act_templateID, substancead.entryRealtionship_act_code, section_ID)
    mycursor.execute(sql, val)
    mydb.commit()
    substanceAdministration_ID = mycursor.lastrowid
    mycursor.close()
    mydb.close()
    writeEmpfohleneMedikationConsumableToDB(substancead.consumable, substanceAdministration_ID)


def writeEmpfohleneMedikationConsumableToDB(consumable, substanceAdministration_ID):
    print("--Consumable")
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT INTO EM_consumable (templateId, manufacturedMaterial_templateID, manufacturedMaterial_code, manufacturedMaterial_name, formCode, capacityQuantity, capacityUnit, pharmCode, pharmName, EM_sA_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
    try:
        c_template = consumable.templateIDs[-1]
    except:
        c_template = None
    try:
        mm_template = consumable.manufacturedMaterial_templateIDs[-1]
    except:
        mm_template = None

    val = (
        c_template, mm_template,
        consumable.manufacturedMaterial_code,
        consumable.manufacturedMaterial_name, consumable.formCode, consumable.capacityQuantity,
        consumable.capacityUnit, consumable.pharmCode, consumable.pharmName, substanceAdministration_ID)
    mycursor.execute(sql, val)
    mydb.commit()
    mycursor.close()
    mydb.close()


def writeSectionMedicationOnAdmissionToDB(section):
    print("Writing Section Medication On Admission section to DB ......")
    section_ID = writeGeneralSectionToDB(section, "section_MedicationOnAdmission")
    for suAd in section.substanceadministrations:
        writeMOAsubstanceAdministrationToDB(suAd, section_ID, True)
    for supply in section.supplies:
        writeMOAsupplyToDB(supply, section_ID)
    return section_ID


def writeMOAsubstanceAdministrationToDB(suAd, section_ID, standard):
    dosing_in_sA = False
    mydb = connectToDB()
    mycursor = mydb.cursor()
    if standard:
        sql = "INSERT INTO MOA_substanceAdministration (templateId, id, statusCode, effectiveTime_low, effectiveTime_high, effectiveTime_period_value, effectiveTime_period_unit, effectiveTime_width_value, effectiveTime_width_unit, effectiveTime_event_code, effectiveTime_offset_value, effectiveTime_offset_unit, dosing, repeatNumber, routeCode, doseQuantity, eR_supply_independentInd, eR_supply_quantity, eR_act_templateID, eR_act_code, section_id, sA_dosing)  VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        try:
            if suAd.entryRealtionship_dosing[1] != None:
                dosing_in_sA = True
        except:
            dosing = None

        try:
            template = suAd.templateIDs[-1]
        except:
            template = None

        try:
            id = suAd.id["extension"]
        except:
            id = None

        val = (template, id, suAd.statusCode, suAd.effectiveTime_low, suAd.effectiveTime_high,
               suAd.effectiveTime_period_value, suAd.effectiveTime_period_unit, suAd.effectiveTime_width_value,
               suAd.effectiveTime_width_unit, suAd.effectiveTime_event_code, suAd.effectiveTime_offset_value,
               suAd.effectiveTime_offset_unit, suAd.dosing, suAd.repeatNumber, suAd.routeCode, suAd.doseQuantity,
               suAd.entryRealtionship_supply_independentInd, suAd.entryRealtionship_supply_quantity,
               suAd.entryRealtionship_act_templateID, suAd.entryRealtionship_act_code, section_ID, dosing)
    else:
        sql = "INSERT INTO MOA_substanceAdministration (templateId, id, statusCode, effectiveTime_low, effectiveTime_high, effectiveTime_period_value, effectiveTime_period_unit, effectiveTime_width_value, effectiveTime_width_unit, effectiveTime_event_code, effectiveTime_offset_value, effectiveTime_offset_unit, dosing, repeatNumber, routeCode, doseQuantity, eR_supply_independentInd, eR_supply_quantity, eR_act_templateID, eR_act_code, section_id, sA_dosing, supply_id)  VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        try:
            if suAd.entryRealtionship_dosing[1] != None:
                dosing_in_sA = True
        except:
            dosing = None

        try:
            template = suAd.templateIDs[-1]
        except:
            template = None

        try:
            id = suAd.id["extension"]
        except:
            id = None

        val = (template, id, suAd.statusCode, suAd.effectiveTime_low, suAd.effectiveTime_high,
               suAd.effectiveTime_period_value, suAd.effectiveTime_period_unit, suAd.effectiveTime_width_value,
               suAd.effectiveTime_width_unit, suAd.effectiveTime_event_code, suAd.effectiveTime_offset_value,
               suAd.effectiveTime_offset_unit, suAd.dosing, suAd.repeatNumber, suAd.routeCode, suAd.doseQuantity,
               suAd.entryRealtionship_supply_independentInd, suAd.entryRealtionship_supply_quantity,
               suAd.entryRealtionship_act_templateID, suAd.entryRealtionship_act_code, None, dosing, section_ID)
    mycursor.execute(sql, val)
    mydb.commit()
    suAd_ID = mycursor.lastrowid
    mycursor.close()
    mydb.close()
    writeMOAconsumableToDB(suAd.consumable, suAd_ID)
    if dosing_in_sA:
        for dose in suAd.dosing:
            writeDosing_substanceAdministrationToDB(suAd.dosing[dose], suAd_ID)

def writeMOAconsumableToDB(consumable, suAd_ID):
    print("--Consumable")
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT INTO MOA_sA_consumable (templateId, manufacturedMaterial_templateID, manufacturedMaterial_code, manufacturedMaterial_name, formCode, capacityQuantity, capacityUnit, pharmCode, pharmName, MOA_sA_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
    try:
        c_template = consumable.templateIDs[-1]
    except:
        c_template = None
    try:
        mm_template = consumable.manufacturedMaterial_templateIDs[-1]
    except:
        mm_template = None

    val = (
        c_template, mm_template,
        consumable.manufacturedMaterial_code,
        consumable.manufacturedMaterial_name, consumable.formCode, consumable.capacityQuantity, consumable.capacityUnit,
        consumable.pharmCode, consumable.pharmName, suAd_ID)
    mycursor.execute(sql, val)
    mydb.commit()
    mycursor.close()
    mydb.close()

def writeDosing_substanceAdministrationToDB(suAd, suAd_Id):
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT INTO MOA_substanceAdministration (templateId, id, statusCode, effectiveTime_low, effectiveTime_high, effectiveTime_period_value, effectiveTime_period_unit, effectiveTime_width_value, effectiveTime_width_unit, effectiveTime_event_code, effectiveTime_offset_value, effectiveTime_offset_unit, dosing, repeatNumber, routeCode, doseQuantity, eR_supply_independentInd, eR_supply_quantity, eR_act_templateID, eR_act_code, section_id, sA_dosing)  VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
    val = (suAd.templateIDs[-1], suAd.id["extension"], suAd.statusCode, suAd.effectiveTime_low, suAd.effectiveTime_high,
           suAd.effectiveTime_period_value, suAd.effectiveTime_period_unit, suAd.effectiveTime_width_value,
           suAd.effectiveTime_width_unit, suAd.effectiveTime_event_code, suAd.effectiveTime_offset_value,
           suAd.effectiveTime_offset_unit, suAd.dosing, suAd.repeatNumber, suAd.routeCode, suAd.doseQuantity,
           suAd.entryRealtionship_supply_independentInd, suAd.entryRealtionship_supply_quantity,
           suAd.entryRealtionship_act_templateID, suAd.entryRealtionship_act_code, None, suAd_Id)
    mycursor.execute(sql, val)
    mydb.commit()
    mycursor.close()
    mydb.close()

def writeMOAsupplyToDB(supply, section_ID):
    product_id = writeMOsuProductToDB(supply.product)
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT INTO MOA_supply (templateId, quantity, product_id, section_id) VALUES (%s, %s, %s, %s)"
    val = (supply.templateIDs[-1], supply.quantity, product_id, section_ID)
    mycursor.execute(sql, val)
    mydb.commit()
    supply_ID = mycursor.lastrowid
    mycursor.close()
    mydb.close()
    for suAd in supply.entryRelationships:
        writeMOAsubstanceAdministrationToDB(suAd, supply_ID, False)

def writeMOsuProductToDB(product):
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT INTO MOA_su_product (manufacturedProduct_templateId, manufacturedMaterial_templateId, manufacturedMaterial_code, manufacturedMaterial_name, manufacturedMaterial_pharm_formCode, manufacturedMaterial_pharm_capacityQuantity, manufacturedMaterial_pharm_ingredient_code, manufacturedMaterial_pharm_ingredient_name) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
    val = (product.manufacturedProduct_templateIDs[-1], product.manufacturedMaterial_templateIDs[-1], product.manufacturedMaterial_code, product.manufacturedMaterial_name, product.manufacturedMaterial_pharm_formCode, product.manufacturedMaterial_pharm_capacityQuantity, product.manufacturedMaterial_pharm_ingredient_code, product.manufacturedMaterial_pharm_ingredient_name)
    mycursor.execute(sql, val)
    mydb.commit()
    product_ID = mycursor.lastrowid
    mycursor.close()
    mydb.close()
    return product_ID

def writeMedicalDischargeLetterToDB(document, recordTarget, encompassingEncounter, sectionAbschliessendeBemerkung_Id, sectionAdvancedDirectives_Id, sectionAllergie_Id, sectionAnamnese_Id, sectionAufnahmegrund_Id, sectionBrieftext_Id, sectionEmpfohleneMedikation_Id, section_ErhobeneBefunde_Id, section_ErhobeneBefunde_AustehendeBefunde_Id, section_ErhobeneBefunde_AuszugAusBefunde_Id, section_ErhobeneBefunde_BeigelegteBefunde_Id, section_ErhobeneBefunde_Operationsbericht_Id, section_ErhobeneBefunde_Vitalparameter_Id, sectionFruehereErkrankungen_Id, sectionFruehereErkrankungen_BisherigeMassnahmen_Id, sectionHospitalDischarge, sectionMedicationAdministered, sectionMedicationOnAdmission, sectionOutcomesMeasurement, sectionProcedureNarrative, sectionRehabilitationsziele, sectionTreatmentPlan, sectionTreatmentPlan_Entlassungszustand, sectionTreatmentPlan_PlanOfCareNote, sectionTreatmentPlan_Termin, sectionZusammenfassungDesAufenthalts):
    print("Writing document to DB ......")
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT IGNORE INTO ClinicalDocument_CDAEntlassungsbrief_A (id, realmCode, effectiveTime, typeId, code, title, confidentialityCode, languageCode, recordTarget, encompassingEncounter, sectionAbschliessendeBemerkung_Id, sectionAdvancedDirectives_Id, sectionAllergie_Id, sectionAnamnese_Id, sectionAufnahmegrund_Id, sectionBrieftext_Id, sectionEmpfohleneMedikation_Id, section_ErhobeneBefunde_Id, section_ErhobeneBefunde_AustehendeBefunde_Id, section_ErhobeneBefunde_AuszugAusBefunde_Id, section_ErhobeneBefunde_BeigelegteBefunde_Id, section_ErhobeneBefunde_Operationsbericht_Id, section_ErhobeneBefunde_Vitalparameter_Id, sectionFruehereErkrankungen_Id, sectionFruehereErkrankungen_BisherigeMassnahmen_Id, sectionHospitalDischarge, sectionMedicationAdministered, sectionMedicationOnAdmission, sectionOutcomesMeasurement, sectionProcedureNarrative, sectionRehabilitationsziele, sectionTreatmentPlan, sectionTreatmentPlan_Entlassungszustand, sectionTreatmentPlan_PlanOfCareNote, sectionTreatmentPlan_Termin, sectionZusammenfassungDesAufenthalts) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
    val = (document.document_id, document.realmCode, document.effectiveTime[0:14], document.typeId, document.code,
           document.title, document.confidentialityCode, document.languageCode, recordTarget, encompassingEncounter,
           sectionAbschliessendeBemerkung_Id, sectionAdvancedDirectives_Id, sectionAllergie_Id,
           sectionAnamnese_Id, sectionAufnahmegrund_Id, sectionBrieftext_Id, sectionEmpfohleneMedikation_Id,
           section_ErhobeneBefunde_Id, section_ErhobeneBefunde_AustehendeBefunde_Id, section_ErhobeneBefunde_AuszugAusBefunde_Id,
           section_ErhobeneBefunde_BeigelegteBefunde_Id, section_ErhobeneBefunde_Operationsbericht_Id, section_ErhobeneBefunde_Vitalparameter_Id,
           sectionFruehereErkrankungen_Id, sectionFruehereErkrankungen_BisherigeMassnahmen_Id, sectionHospitalDischarge,
           sectionMedicationAdministered, sectionMedicationOnAdmission, sectionOutcomesMeasurement, sectionProcedureNarrative,
           sectionRehabilitationsziele, sectionTreatmentPlan, sectionTreatmentPlan_Entlassungszustand, sectionTreatmentPlan_PlanOfCareNote,
           sectionTreatmentPlan_Termin, sectionZusammenfassungDesAufenthalts)
    mycursor.execute(sql, val)
    mydb.commit()
    mycursor.close()
    mydb.close()

    #sectionBrieftext_ID, section_EmpfohleneMedikation_ID, section_ErhobeneBefunde_ID,
    #section_ErhobeneBefunde_AustehendeBefunde_ID, section_ErhobeneBefunde_AuszugAusBefunde_ID, section_ErhobeneBefunde_BeigelegteBefunde_ID,
    #section_ErhobeneBefunde_Operationsbericht_ID, section_ErhobeneBefunde_Vitalparameter_ID, section_HospitalDischarge_ID, section_MedicationOnAdmission_ID
def writeMedicalDischargeLetterToDB_v2(document, recordTarget, legalauthenticator, encompassingEncounter, sectionBrieftext_Id, sectionEmpfohleneMedikation_Id, section_ErhobeneBefunde_Id, section_ErhobeneBefunde_AustehendeBefunde_Id, section_ErhobeneBefunde_AuszugAusBefunde_Id, section_ErhobeneBefunde_BeigelegteBefunde_Id, section_ErhobeneBefunde_Operationsbericht_Id, section_ErhobeneBefunde_Vitalparameter_Id, sectionHospitalDischarge, sectionMedicationOnAdmission):
    print("Writing document to DB ......")
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT IGNORE INTO ClinicalDocument_CDAEntlassungsbrief_A (id, realmCode, effectiveTime, typeId, code, title, confidentialityCode, languageCode, recordTarget, legalAuthenticator, encompassingEncounter, sectionBrieftext_Id, sectionEmpfohleneMedikation_Id, section_ErhobeneBefunde_Id, section_ErhobeneBefunde_AustehendeBefunde_Id, section_ErhobeneBefunde_AuszugAusBefunde_Id, section_ErhobeneBefunde_BeigelegteBefunde_Id, section_ErhobeneBefunde_Operationsbericht_Id, section_ErhobeneBefunde_Vitalparameter_Id, sectionHospitalDischarge, sectionMedicationOnAdmission) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
    val = (document.document_id, document.realmCode, document.effectiveTime[0:14], document.typeId, document.code,
           document.title, document.confidentialityCode, document.languageCode, recordTarget, legalauthenticator, encompassingEncounter,
           sectionBrieftext_Id, sectionEmpfohleneMedikation_Id,
           section_ErhobeneBefunde_Id, section_ErhobeneBefunde_AustehendeBefunde_Id, section_ErhobeneBefunde_AuszugAusBefunde_Id,
           section_ErhobeneBefunde_BeigelegteBefunde_Id, section_ErhobeneBefunde_Operationsbericht_Id, section_ErhobeneBefunde_Vitalparameter_Id,
           sectionHospitalDischarge, sectionMedicationOnAdmission)
    mycursor.execute(sql, val)
    mydb.commit()
    mycursor.close()
    mydb.close()

def writeDocumentTemplatesToDB(document):
    print("Writing document-templates to DB ......")
    mydb = connectToDB()
    for template in document.templates:
        mycursor = mydb.cursor()
        sql = "INSERT IGNORE INTO ClinicalDocument_has_template (documentId, templateId) VALUES (%s, %s)"
        val = (document.document_id, template)
        mycursor.execute(sql, val)
        mydb.commit()
        mycursor.close()
    mydb.close()

def writeServiceEventsToDB(serviceEvents_list, documentID):
    print("Writing service events to DB ......")
    mydb = connectToDB()
    for sE in serviceEvents_list:
        if sE.performer != "":
            try:
                reporg_addrid = getAdressID(sE.performer.repOrg_addr.streetAddressLine, sE.performer.repOrg_addr.postalCode)
            except:
                reporg_addrid = None
            mycursor = mydb.cursor()
            sql = "INSERT IGNORE INTO serviceEvent (code, effectiveTime_low, effectiveTime_high, templateId, person_name, person_addr, organisation_id, organisation_name, organisation_addr, CDA_ID) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
            val = (sE.code["code"], sE.effectiveTime_low[0:14], sE.effectiveTime_high[0:14], sE.performer_templateId["root"], sE.performer.assPers_name.combinedName, None, sE.performer.repOrg_id["root"], sE.performer.repOrg_name, reporg_addrid[0], documentID )
            mycursor.execute(sql, val)
            mydb.commit()
            mycursor.close()
        else:
            mycursor = mydb.cursor()
            sql = "INSERT IGNORE INTO serviceEvent (code, effectiveTime_low, effectiveTime_high, CDA_ID) VALUES (%s, %s, %s, %s)"
            val = (sE.code["code"], sE.effectiveTime_low[0:14], sE.effectiveTime_high[0:14], documentID)
            mycursor.execute(sql, val)
            mydb.commit()
            mycursor.close()
    mydb.close()

def writeParticipantToDB(participants, documentID):
    print("Writing participants to DB ......")
    mydb = connectToDB()

    for parti in participants:
        try:
            organisation_addr_id = getAdressID(parti.scopingOrg_addr.streetAddressLine, parti.scopingOrg_addr.postalCode)[0]
        except:
            organisation_addr_id = None

        mycursor = mydb.cursor()
        sql = "INSERT IGNORE INTO participant (typeCode, templateId, functionCode, person_id, person_name, organisation_id, organisation_name, organisation_addr) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
        val = (parti.typeCode, parti.templateID, parti.function_code, parti.assEnt_id, parti.assPers_name_combined, parti.scopingOrg_id, parti.scopingOrg_name, organisation_addr_id)
        mycursor.execute(sql, val)
        mydb.commit()
        participant_DB_id = mycursor.lastrowid
        mycursor.close()

        mycursor = mydb.cursor()
        sql = "INSERT IGNORE INTO ClinicalDocument_has_participant (ClinicalDocument_ID, Participant_ID) VALUES (%s, %s)"
        val = (documentID, participant_DB_id)
        mycursor.execute(sql, val)
        mydb.commit()
        mycursor.close()
    mydb.close()

def writeAuthorToDB(authors, document_id):
    print("Writing authors to DB .......")
    mydb = connectToDB()
    for author in authors:
        try:
            addr_id = getAdressID(author.rep_organisation_addr.streetAddressLine, author.rep_organisation_addr.postalCode)
        except:
            addr_id = None
        mycursor = mydb.cursor()
        sql = "INSERT IGNORE INTO author (time, functionCode, ass_author_id, ass_author_code, ass_author_name, auth_device_code, auth_device_manufacturerModelName, auth_device_softwareName, rep_organisation_id, rep_organisation_name, rep_organisation_addr) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        val = (author.time_of_authoring[0:14], author.functionCode["code"], author.ass_author_id["root"], author.ass_author_code["code"], author.ass_author_name.combinedName, author.auth_device_code, author.auth_device_manufacturerModelName, author.auth_device_softwareName, author.rep_organisation_id["root"], author.rep_organisation_name, addr_id[0])
        mycursor.execute(sql, val)
        mydb.commit()
        author_ID = mycursor.lastrowid
        mycursor.close()
        writeAuthorDocumentToDB(author_ID, document_id)
    mydb.close()

def writeAuthorDocumentToDB(author_id, document_id):
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT INTO ClinicalDocument_has_author (ClinicalDocument, author) VALUES (%s, %s)"
    val = (document_id, author_id)
    mycursor.execute(sql, val)
    mydb.commit()
    mycursor.close()
    mydb.close()