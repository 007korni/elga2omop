import mysql.connector


def connectToDB():
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        password="password",
        database="CDA_Laborbefunde"
    )
    return mydb

# Check if document is already in DB
def documentCheck(document_id):
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "SELECT 1 FROM `ClinicalDocument_CDALaborbefund` WHERE id = %s"
    val = (document_id, )
    mycursor.execute(sql, val)
    document_id = mycursor.fetchone()
    mydb.commit()
    mycursor.close()
    mydb.close()
    try:
        if document_id[0] == 1:
            return True
    except:
        return False

def writeCodesToDB(codes):
    print("Writing codes to DB ......")
    mydb = connectToDB()
    for c in codes:
        try:
            code = codes[c]["code"]
            codeSystem = codes[c]["codeSystem"]
            codeSystemName = codes[c]["codeSystemName"]
            displayName = codes[c]["displayName"]
        except:
            code = c
            codeSystem = None
            codeSystemName = None
            displayName = None
        mycursor = mydb.cursor()

        sql = "INSERT IGNORE INTO code (code, codeSystem, codeSystemName, displayName) VALUES (%s, %s, %s, %s)"
        val = (code, codeSystem, codeSystemName, displayName)
        mycursor.execute(sql, val)
        mydb.commit()
    mycursor.close()
    mydb.close()

def writeTemplateIDsToDB(templates):
    print("Writing templates to DB ......")
    mydb = connectToDB()
    for t in templates:
        root = t.attrib["root"]
        try:
            assauth = t.attrib["assigningAuthorityName"]
        except:
            assauth = None

        mycursor = mydb.cursor()
        sql = "INSERT IGNORE INTO templateId (root, assigningAuthorityName) VALUES (%s, %s)"
        val = (root, assauth)
        mycursor.execute(sql, val)
        mydb.commit()
    mycursor.close()
    mydb.close()

def writeAdressesToDB(address_list):
    print("Writing addresses to DB ......")
    mydb = connectToDB()
    for a in address_list:
        try:
            houseNumber = a["houseNumber"]
            streetAddressLine = a["streetName"] + " " + houseNumber
            streetAddressLine.replace("\t", "").replace("\n", " ")
        except:
            try:
                streetAddressLine = a["streetAddressLine"].replace("\t", "").replace("\n", " ")
            except:
                continue
        try:
            postalCode = a["postalCode"]
        except:
            continue
        try:
            city = a["city"]
        except:
            city = None
        try:
            state = a["state"]
        except:
            state = None
        try:
            country = a["country"]
        except:
            country = None
        try:
            additionalLocator = a["additionalLocator"]
        except:
            additionalLocator = None

        mycursor = mydb.cursor()
        sql = "INSERT IGNORE INTO addr (streetAddressLine, postalCode, city, state, country, additionalLocator) VALUES (%s, %s, %s, %s, %s, %s)"
        val = (streetAddressLine, postalCode, city, state, country, additionalLocator)
        mycursor.execute(sql, val)
        mydb.commit()
    mycursor.close()
    mydb.close()

def getAdressID(streetAddressLine, postalCode):
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "SELECT id_addr FROM addr WHERE streetAddressLine = %s AND postalCode = %s"
    val = (streetAddressLine, postalCode)
    mycursor.execute(sql, val)
    addr_id = mycursor.fetchone()
    mydb.commit()
    mycursor.close()
    mydb.close()
    return addr_id

def writePatientToDB(patient):
    print("Writing patient to DB ......")
    mydb = connectToDB()
    addr_id = getAdressID(patient.addr.streetAddressLine, patient.addr.postalCode)

    # Writing Patient to DB
    mycursor = mydb.cursor()
    sql = "INSERT IGNORE INTO patientRole (id_SVNR, id_local, addr, given, family, administrativeGenderCode, birthTime) VALUES (%s, %s, %s, %s, %s, %s, %s)"
    val = (patient.SVNR.attrib["extension"], patient.local.attrib["extension"], addr_id[0], patient.patient_name.all_given, patient.patient_name.all_family, patient.patient_gender["code"], patient.birthtime)
    mycursor.execute(sql, val)
    mydb.commit()
    mycursor.close()
    mydb.close()

def writeEncompassingEncounterToDB(encompassingEncounter):
    print("Writing encompassing encounter to DB ......")
    try:
        person_addr_id = getAdressID(encompassingEncounter.assignedEntity.assEnt_addr.streetAddressLine, encompassingEncounter.assignedEntity.assEnt_addr.postalCoce)[0]
    except:
        person_addr_id = None
    try:
        assigned_org_addr_id = getAdressID(encompassingEncounter.assignedEntity.repOrg_addr.streetAddressLine, encompassingEncounter.assignedEntity.repOrg_addr.postalCoce)[0]
    except:
        assigned_org_addr_id = None
    try:
        location_addr_id = getAdressID(encompassingEncounter.location_addr.streetAddressLine, encompassingEncounter.location_addr.postalCode)[0]
    except:
        location_addr_id = None
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT IGNORE INTO encompassingEncounter (id, code, effectiveTime_low, effectiveTime_high, assignedEntity_id, person_name, person_addr, organisation_id, organisation_name, organisation_addr, location_id, location_name, location_addr) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
    val = (encompassingEncounter.encompassingEncounter_ID["extension"], encompassingEncounter.code["code"], encompassingEncounter.start_time[0:14], encompassingEncounter.end_time[0:14], encompassingEncounter.assignedEntity.assEnt_id["extension"], encompassingEncounter.assignedEntity.assPers_name.combinedName, person_addr_id ,encompassingEncounter.assignedEntity.repOrg_id["root"], encompassingEncounter.assignedEntity.repOrg_name, assigned_org_addr_id, encompassingEncounter.location_ID, encompassingEncounter.location_name, location_addr_id)
    mycursor.execute(sql, val)
    mydb.commit()
    mycursor.close()
    mydb.close()

def writeSectionBrieftextToDB(section_Brieftext):
    print("Writing section Brieftext to DB ......")
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT IGNORE INTO section_Brieftext (templateId, code, title, text, entry) VALUES (%s, %s, %s, %s, %s)"
    val = (section_Brieftext.templateId, section_Brieftext.code, section_Brieftext.title, section_Brieftext.text, section_Brieftext.entry)
    mycursor.execute(sql, val)
    mydb.commit()
    section_Brieftext_ID = mycursor.lastrowid
    mycursor.close()
    mydb.close()
    return section_Brieftext_ID

def writeSectionUeberweisungsgrundToDB(section_Ueberweisungsgrund):
    print("Writing section Ueberweisungsgrund to DB ......")
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT IGNORE INTO section_Ueberweisungsgrund (templateId, code, title, text) VALUES (%s, %s, %s, %s)"
    val = (section_Ueberweisungsgrund.templateId, section_Ueberweisungsgrund.code, section_Ueberweisungsgrund.title, section_Ueberweisungsgrund.text,)
    mycursor.execute(sql, val)
    mydb.commit()
    section_Ueberweisungsgrund_ID = mycursor.lastrowid
    mycursor.close()
    mydb.close()
    return section_Ueberweisungsgrund_ID

def writeSectionSpecimenToDB(specimen_section):
    print("Writing specimen section to DB ......")
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT IGNORE INTO section_Specimen(code, statusCode) VALUES (%s, %s)"
    val = (specimen_section.code["code"], specimen_section.statusCode["code"])
    mycursor.execute(sql, val)
    mydb.commit()
    section_specimen_ID = mycursor.lastrowid
    mycursor.close()
    mydb.close()
    print("Writing procedure to DB ......")
    for procedure in specimen_section.procedures:
        procedure_id = writeProcedureToDB(procedure, section_specimen_ID)
        writeParticipantSpecimenToDB(procedure.participant_specimen, procedure_id)
    return section_specimen_ID

def writeProcedureToDB(procedure, section_specimen_ID):

    # Insert perfomer into DB
    performer_ID = writePerformerToDB(procedure.performer)

    #Insert procedure into DB
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT IGNORE INTO lab_procedure(code, effectiveTime, targetSiteCode, performer, section_specimen) VALUES (%s, %s, %s, %s, %s)"
    val = (procedure.code["code"], procedure.effectiveTime["value"][0:14], procedure.targetSiteCode["code"], performer_ID, section_specimen_ID)
    mycursor.execute(sql, val)
    mydb.commit()
    procedure_id = mycursor.lastrowid
    mycursor.close()
    mydb.close()
    return procedure_id

def writePerformerToDB(performer):
    try:
        assEnt_addressID = getAdressID(performer.assEnt_addr.streetAddressLine, performer.assEnt_addr.postalCode)[0]
    except:
        assEnt_addressID = None
    try:
        repOrg_addressID = getAdressID(performer.repOrg_addr.streetAddressLine, performer.repOrg_addr.postalCode)[0]
    except:
        repOrg_addressID = None

    # Insert perfomer into DB
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT IGNORE INTO performer(assignedEntity_id, assignedEntity_addr, assignedPerson_name, representedOrganisation_id, representedOrganisation_name, representedOrganisation_addr) VALUES (%s, %s, %s, %s, %s, %s)"
    val = (performer.assEnt_id["extension"], assEnt_addressID, performer.assPers_name.combinedName, performer.repOrg_id["root"], performer.repOrg_name, repOrg_addressID)
    mycursor.execute(sql, val)
    mydb.commit()
    performer_ID = mycursor.lastrowid
    mycursor.close()
    mydb.close()
    return performer_ID

def writeParticipantSpecimenToDB(part_spec, procedure_id):
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT IGNORE INTO participant_specimen(id, code, procedure_id) VALUES (%s, %s, %s)"
    val = (part_spec.id, part_spec.code, procedure_id)
    mycursor.execute(sql, val)
    mydb.commit()
    mydb.close()

def writeDocumentInformationToDB(document, recordTarget_id, encompassingEncounter_id, brieftext_id, ueberweisung_id, specimen_section_id):
    print("Writing document to DB ......")
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT IGNORE INTO ClinicalDocument_CDALaborbefund (" \
          "id, " \
          "realmCode, " \
          "effectiveTime, " \
          "typeId, " \
          "code, " \
          "title, " \
          "confidentialityCode, " \
          "languageCode, " \
          "recordTarget, " \
          "encompassingEncounter," \
          "sectionBrieftext_Id," \
          "sectionUeberweisungsgrund_Id," \
          "sectionSpecimen_Id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
    val = (document.document_id, document.realmCode, document.effectiveTime[0:14], document.typeId, document.code, document.title, document.confidentialityCode, document.languageCode, recordTarget_id, encompassingEncounter_id, brieftext_id, ueberweisung_id, specimen_section_id)
    mycursor.execute(sql, val)
    mydb.commit()
    mycursor.close()
    mydb.close()

def writeDocumentTemplatesToDB(document):
    print("Writing document-templates to DB ......")
    mydb = connectToDB()
    for template in document.templates:
        mycursor = mydb.cursor()
        sql = "INSERT IGNORE INTO ClinicalDocument_has_template (documentId, templateId) VALUES (%s, %s)"
        val = (document.document_id, template)
        mycursor.execute(sql, val)
        mydb.commit()
        mycursor.close()
    mydb.close()

def writeServiceEventsToDB(serviceEvents_list, documentID):
    print("Writing service events to DB ......")
    mydb = connectToDB()
    for sE in serviceEvents_list:
        if sE.performer != "":
            try:
                reporg_addrid = getAdressID(sE.performer.repOrg_addr.streetAddressLine, sE.performer.repOrg_addr.postalCode)
            except:
                reporg_addrid = None
            mycursor = mydb.cursor()
            sql = "INSERT IGNORE INTO serviceEvent (code, effectiveTime_low, effectiveTime_high, templateId, person_name, person_addr, organisation_id, organisation_name, organisation_addr, LaborbefundID) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
            val = (sE.code["code"], sE.effectiveTime_low[0:14], sE.effectiveTime_high[0:14], sE.performer_templateId["root"], sE.performer.assPers_name.combinedName, None, sE.performer.repOrg_id["root"], sE.performer.repOrg_name, reporg_addrid[0], documentID )
            mycursor.execute(sql, val)
            mydb.commit()
            mycursor.close()
        else:
            mycursor = mydb.cursor()
            sql = "INSERT IGNORE INTO serviceEvent (code, effectiveTime_low, effectiveTime_high, LaborbefundID) VALUES (%s, %s, %s, %s)"
            val = (sE.code["code"], sE.effectiveTime_low[0:14], sE.effectiveTime_high[0:14], documentID)
            mycursor.execute(sql, val)
            mydb.commit()
            mycursor.close()
    mydb.close()

def writeSpecialitySectionToDB(speciality_section_list, documentID):
    print("Writing speciality sections to DB ......")
    mydb = connectToDB()
    for spec_sec in speciality_section_list:
        print("Writing section " + spec_sec.title)
        mycursor = mydb.cursor()
        sql = "INSERT IGNORE INTO section_speciality (templateId, code, title, Laborbefund_Id) VALUES (%s, %s, %s, %s)"
        val = (spec_sec.templateId, spec_sec.code, spec_sec.title, documentID)
        mycursor.execute(sql, val)
        mydb.commit()
        speciality_section_id = mycursor.lastrowid
        mycursor.close()

        for entry_rel in spec_sec.entryRelationships:
            print("Writing entry relationship " + entry_rel.code)
            mycursor = mydb.cursor()
            sql = "INSERT IGNORE INTO entryRelationship (templateId, code, speciality_section_id) VALUES (%s, %s, %s)"
            val = (entry_rel.templateId, entry_rel.code, speciality_section_id)
            mycursor.execute(sql, val)
            mydb.commit()
            entry_rel_id = mycursor.lastrowid
            mycursor.close()

            for observation in entry_rel.observations:
                mycursor = mydb.cursor()
                sql = "INSERT IGNORE INTO observation (templateId, code, effectiveTime, interpretationCode, value, unit, range_low, range_high, entryRelationship_ID) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)"
                val = (observation.templateId, observation.code, observation.effectiveTime, observation.interpretationCode, observation.value, observation.unit, observation.range_low, observation.range_high, entry_rel_id)
                mycursor.execute(sql, val)
                mydb.commit()
                mycursor.close()
    mydb.close()

def writeParticipantToDB(participants, documentID):
    print("Writing participants to DB ......")
    mydb = connectToDB()

    for parti in participants:
        try:
            organisation_addr_id = getAdressID(parti.scopingOrg_addr.streetAddressLine, parti.scopingOrg_addr.postalCode)[0]
        except:
            organisation_addr_id = None

        mycursor = mydb.cursor()
        sql = "INSERT IGNORE INTO participant (typeCode, templateId, functionCode, person_id, person_name, organisation_id, organisation_name, organisation_addr) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
        val = (parti.typeCode, parti.templateID, parti.function_code, parti.assEnt_id, parti.assPers_name_combined, parti.scopingOrg_id, parti.scopingOrg_name, organisation_addr_id)
        mycursor.execute(sql, val)
        mydb.commit()
        participant_DB_id = mycursor.lastrowid
        mycursor.close()

        mycursor = mydb.cursor()
        sql = "INSERT IGNORE INTO ClinicalDocument_has_participant (ClinicalDocument_ID, Participant_ID) VALUES (%s, %s)"
        val = (documentID, participant_DB_id)
        mycursor.execute(sql, val)
        mydb.commit()
        mycursor.close()
    mydb.close()
