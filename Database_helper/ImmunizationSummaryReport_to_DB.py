import mysql.connector

def connectToDB():
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        password="password",
        database="CDA_document_e-Impfpass"
    )
    return mydb

# Check if document is already in DB
def documentCheck(document_id):
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "SELECT 1 FROM `ClinicalDocument_CDAeImpfpass` WHERE id = %s"
    val = (document_id, )
    mycursor.execute(sql, val)
    document_id = mycursor.fetchone()
    mydb.commit()
    mycursor.close()
    mydb.close()
    try:
        if document_id[0] == 1:
            return True
    except:
        return False

def writeCodesToDB(codes):
    print("Writing codes to DB ......")
    mydb = connectToDB()
    for c in codes:
        try:
            code = codes[c]["code"]
            codeSystem = codes[c]["codeSystem"]
            codeSystemName = codes[c]["codeSystemName"]
            displayName = codes[c]["displayName"]
        except:
            code = c
            codeSystem = None
            codeSystemName = None
            displayName = None
        mycursor = mydb.cursor()

        sql = "INSERT IGNORE INTO code (code, codeSystem, codeSystemName, displayName) VALUES (%s, %s, %s, %s)"
        val = (code, codeSystem, codeSystemName, displayName)
        mycursor.execute(sql, val)
        mydb.commit()
    mycursor.close()
    mydb.close()

def writeTemplateIDsToDB(templates):
    print("Writing templates to DB ......")
    mydb = connectToDB()
    for t in templates:
        root = t.attrib["root"]
        try:
            assauth = t.attrib["assigningAuthorityName"]
        except:
            assauth = None

        mycursor = mydb.cursor()
        sql = "INSERT IGNORE INTO templateId (root, assigningAuthorityName) VALUES (%s, %s)"
        val = (root, assauth)
        mycursor.execute(sql, val)
        mydb.commit()
    mycursor.close()
    mydb.close()

def writeAdressesToDB(address_list):
    print("Writing addresses to DB ......")
    mydb = connectToDB()
    for a in address_list:
        try:
            houseNumber = a["houseNumber"]
            streetAddressLine = a["streetName"] + " " + houseNumber
            streetAddressLine.replace("\t", "").replace("\n", " ")
        except:
            try:
                streetAddressLine = a["streetAddressLine"].replace("\t", "").replace("\n", " ")
            except:
                continue
        try:
            postalCode = a["postalCode"]
        except:
            continue
        try:
            city = a["city"]
        except:
            city = None
        try:
            state = a["state"]
        except:
            state = None
        try:
            country = a["country"]
        except:
            country = None
        try:
            additionalLocator = a["additionalLocator"]
        except:
            additionalLocator = None

        mycursor = mydb.cursor()
        sql = "INSERT IGNORE INTO addr (streetAddressLine, postalCode, city, state, country, additionalLocator) VALUES (%s, %s, %s, %s, %s, %s)"
        val = (streetAddressLine, postalCode, city, state, country, additionalLocator)
        mycursor.execute(sql, val)
        mydb.commit()
    mycursor.close()
    mydb.close()

def getAdressID(streetAddressLine, postalCode):
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "SELECT id_addr FROM addr WHERE streetAddressLine = %s AND postalCode = %s"
    val = (streetAddressLine, postalCode)
    mycursor.execute(sql, val)
    addr_id = mycursor.fetchone()
    mydb.commit()
    mycursor.close()
    mydb.close()
    return addr_id

def writePatientToDB(patient):
    print("Writing patient to DB ......")
    mydb = connectToDB()
    addr_id = getAdressID(patient.addr.streetAddressLine, patient.addr.postalCode)

    # Writing Patient to DB
    mycursor = mydb.cursor()
    sql = "INSERT IGNORE INTO patientRole (id_SVNR, id_local, addr, given, family, administrativeGenderCode, birthTime) VALUES (%s, %s, %s, %s, %s, %s, %s)"
    val = (patient.SVNR.attrib["extension"], patient.local.attrib["extension"], addr_id[0], patient.patient_name.all_given, patient.patient_name.all_family, patient.patient_gender["code"], patient.birthtime)
    mycursor.execute(sql, val)
    mydb.commit()
    mycursor.close()
    mydb.close()

def writeSectionImmunizationToDB(immunization_section):
    print("Writing section immunization to DB ......")
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT IGNORE INTO section_immunization(code, title, text) VALUES (%s, %s, %s)"
    val = (immunization_section.code, immunization_section.title, immunization_section.text)
    mycursor.execute(sql, val)
    mydb.commit()
    immunization_section_id = mycursor.lastrowid
    mycursor.close()
    mydb.close()
    for suAD in immunization_section.substanceAdministrations:
        writesubstanceAdministrationToDB(suAD, immunization_section_id)
    return immunization_section_id

def writesubstanceAdministrationToDB(suAD_element, immunization_section_id):
    author_id = authorToDB(suAD_element.author)
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT INTO substanceAdministration(suAd_id, code, statusCode, effectiveTime, doseQuantity, consumable_code, consumable_name, lotNumberText, consumable_ATC, consumable_ATC_name, author_id, immunizationsection_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
    val = (suAD_element.id["root"], suAD_element.code, suAD_element.statusCode["code"], suAD_element.effectiveTime[0:14], suAD_element.doseQuantity, suAD_element.consumable_code, suAD_element.consumable_name, suAD_element.lotNumberText, suAD_element.consumable_ATC, suAD_element.consumable_ATC_name, author_id, immunization_section_id)
    mycursor.execute(sql, val)
    mydb.commit()
    mycursor.close()
    mydb.close()

def authorToDB(author):
    author_reporg_addr_id = getAdressID(author.rep_organisation_addr.streetAddressLine, author.rep_organisation_addr.postalCode)[0]
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT IGNORE INTO author(effectiveTime, assignedAuthor_id, assignedAuthor_name, representedOrganisation_id, representedOrganisation_addr, representedOrganisation_name) VALUES (%s, %s, %s, %s, %s, %s)"
    val = (author.time_of_authoring[0:14], author.ass_author_id["root"], author.ass_author_name.combinedName, author.rep_organisation_id["root"], author_reporg_addr_id, author.rep_organisation_name)
    mycursor.execute(sql, val)
    mydb.commit()
    author_id = mycursor.lastrowid
    mycursor.close()
    mydb.close()
    return author_id

def writeSectionImpfrelevanteErkrankungenToDB(IE_section):
    print("Writing section impfrelevante Erkrankungen to DB ......")
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT INTO section_impfrelevanteErkrankungen(code, title, text) VALUES (%s, %s, %s)"
    val = (IE_section.code, IE_section.title, IE_section.text)
    mycursor.execute(sql, val)
    mydb.commit()
    IE_section_id = mycursor.lastrowid
    mycursor.close()
    mydb.close()
    for IE_act in IE_section.entries:
        writeIEactToDB(IE_act, IE_section_id)
    return IE_section_id

def writeIEactToDB(IE_act, IE_section_id):
    author_id = authorToDB(IE_act.author)
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT IGNORE INTO IE_act(code, statusCode, effectiveTime_low, effectiveTime_high, author, section_impfrelevanteErkrankung_id) VALUES (%s, %s, %s, %s, %s, %s)"
    val = (IE_act.code, IE_act.statusCode, IE_act.effectiveTime_low, IE_act.effectiveTime_high, author_id, IE_section_id)
    mycursor.execute(sql, val)
    mydb.commit()
    IE_section_id = mycursor.lastrowid
    mycursor.close()
    mydb.close()
    for IE_observation in IE_act.observations:
        writeIEobservationToDB(IE_observation, IE_section_id)

def writeIEobservationToDB(observation, IE_act_id):
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT IGNORE INTO IE_observation(code, statusCode, effectiveTime_low, effectiveTime_high, value_code, IE_act_id) VALUES (%s, %s, %s, %s, %s, %s)"
    val = (observation.code, observation.statusCode, observation.effectiveTime_low, observation.effectiveTime_high, observation.value_code["code"], IE_act_id)
    mycursor.execute(sql, val)
    mydb.commit()

def writeSectionAntikoerperToDB(AK_section):
    print("Writing section Antikörper to DB ......")
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT IGNORE INTO section_antikoerper(code, title, text) VALUES (%s, %s, %s)"
    val = (AK_section.code, AK_section.title, AK_section.text)
    mycursor.execute(sql, val)
    mydb.commit()
    AK_section_id = mycursor.lastrowid
    mycursor.close()
    mydb.close()
    for AK_act in AK_section.entries:
        writeAKactToDB(AK_act, AK_section_id)
    return AK_section_id

def writeAKactToDB(ak_act, AK_section_id):
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT INTO AK_act(code, statusCode, section_AK_id) VALUES (%s, %s, %s)"
    val = (ak_act.code, ak_act.statusCode, AK_section_id)
    mycursor.execute(sql, val)
    mydb.commit()
    AK_act_id = mycursor.lastrowid
    mycursor.close()
    mydb.close()
    writeAKentryrelationshipToDB(ak_act.entryRelationship, AK_act_id)

def writeAKentryrelationshipToDB(ak_entryRel, AKact_id):
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT INTO AK_entryRelationship(code, statusCode, AK_act_id) VALUES (%s, %s, %s)"
    val = (ak_entryRel.code, ak_entryRel.statusCode, AKact_id)
    mycursor.execute(sql, val)
    mydb.commit()
    AK_entryrel_id = mycursor.lastrowid
    mycursor.close()
    mydb.close()
    for observation in ak_entryRel.observations:
        writeAKobservationToDB(observation, AK_entryrel_id)

def writeAKobservationToDB(observation, ak_entryRel_id):
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT INTO AK_observation(code, statusCode, effectiveTime, value, value_unit, interpretationCode, range_low, range_high, range_interpretationCode, AK_entryrelationship_id) VALUES (%s, %s, %s , %s, %s, %s, %s, %s , %s, %s)"
    val = (observation.code, observation.statusCode, observation.effectiveTime[0:14], observation.value, observation.value_unit, observation.interpretationCode, observation.range_low, observation.range_high, observation.range_interpretation_code, ak_entryRel_id)
    mycursor.execute(sql, val)
    mydb.commit()
    mycursor.close()
    mydb.close()

# Final
def writeDocumentInformationToDB(document, recordTarget_id, immunization_id, personengruppe_id, impfrelevanteerkrankung_id, antikoerper_id):
    print("Writing document to DB ......")
    mydb = connectToDB()
    mycursor = mydb.cursor()
    sql = "INSERT IGNORE INTO ClinicalDocument_CDAeImpfpass (" \
          "id, " \
          "realmCode, " \
          "effectiveTime, " \
          "typeId, " \
          "code, " \
          "title, " \
          "confidentialityCode, " \
          "languageCode, " \
          "recordTarget, " \
          "section_immunization_ID," \
          "section_personengruppe_ID," \
          "section_impfrelevanteErkrankungen_ID," \
          "section_antikoerper_ID) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
    val = (document.document_id, document.realmCode, document.effectiveTime[0:14], document.typeId, document.code, document.title, document.confidentialityCode, document.languageCode, recordTarget_id, immunization_id, personengruppe_id, impfrelevanteerkrankung_id, antikoerper_id)
    mycursor.execute(sql, val)
    mydb.commit()
    mycursor.close()
    mydb.close()

def writeDocumentTemplatesToDB(document):
    print("Writing document-templates to DB ......")
    mydb = connectToDB()
    for template in document.templates:
        mycursor = mydb.cursor()
        sql = "INSERT IGNORE INTO ClinicalDocument_has_template (documentId, templateId) VALUES (%s, %s)"
        val = (document.document_id, template)
        mycursor.execute(sql, val)
        mydb.commit()
        mycursor.close()
    mydb.close()

def writeServiceEventsToDB(serviceEvents_list, documentID):
    print("Writing service events to DB ......")
    mydb = connectToDB()
    for sE in serviceEvents_list:
        if sE.performer != "":
            try:
                reporg_addrid = getAdressID(sE.performer.repOrg_addr.streetAddressLine, sE.performer.repOrg_addr.postalCode)
            except:
                reporg_addrid = None
            mycursor = mydb.cursor()
            sql = "INSERT IGNORE INTO serviceEvent (code, effectiveTime_low, effectiveTime_high, templateId, person_name, person_addr, organisation_id, organisation_name, organisation_addr, eImpfpassID) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
            val = (sE.code["code"], sE.effectiveTime_low[0:14], sE.effectiveTime_high[0:14], sE.performer_templateId["root"], sE.performer.assPers_name.combinedName, None, sE.performer.repOrg_id["root"], sE.performer.repOrg_name, reporg_addrid[0], documentID )
            mycursor.execute(sql, val)
            mydb.commit()
            mycursor.close()
        else:
            mycursor = mydb.cursor()
            sql = "INSERT IGNORE INTO serviceEvent (code, effectiveTime_low, effectiveTime_high, eImpfpassID) VALUES (%s, %s, %s, %s)"
            val = (sE.code["code"], sE.effectiveTime_low[0:14], sE.effectiveTime_high[0:14], documentID)
            mycursor.execute(sql, val)
            mydb.commit()
            mycursor.close()
    mydb.close()