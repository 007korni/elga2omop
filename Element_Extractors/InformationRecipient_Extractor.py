from Element_Extractors import Name_Extractor, Address_Extractor


class InformationRecipient:
    typeCode = ''
    # Intended Recipient
    recipient_id = ''
    # Information Recipient
    recipient_name = ''  # Entweder vollwertiger Name oder nur Kurzform wie 'Hausarzt'
    # receivedOrganization
    receivedOrganization_id = ''
    receivedOrganization_name = ''
    receivedOrganization_telecom = []
    receivedOrganization_addr = ''

    def __init__(self):
        self.typeCode = ''
        self.recipient_id = ''
        self.receivedOrganization_id = ''
        self.receivedOrganization_name = ''
        self.receivedOrganization_telecom = []
        self.receivedOrganization_addr = ''

    def extractInformationRecipientFromXML(self, informationRecipient_element):
        self.typeCode = informationRecipient_element.attrib['typeCode']
        intendedRecipient = informationRecipient_element.find('.//{urn:hl7-org:v3}intendedRecipient')
        for child in intendedRecipient:
            if child.tag == '{urn:hl7-org:v3}id':
                self.recipient_id = child.attrib
            if child.tag == '{urn:hl7-org:v3}informationRecipient':
                temp_name = child.find('.//{urn:hl7-org:v3}name')
                self.recipient_name = temp_name.text
                if self.recipient_name:
                    self.recipient_name = Name_Extractor.Name()
                    self.recipient_name.extractName(temp_name)


        receivedOrganization = intendedRecipient.find('.//{urn:hl7-org:v3}receivedOrganization')
        for child in receivedOrganization:
            if child.tag == '{urn:hl7-org:v3}id':
                self.receivedOrganization_id = child.attrib
            if child.tag == '{urn:hl7-org:v3}name':
                self.receivedOrganization_name = child.text
            if child.tag == '{urn:hl7-org:v3}telecom':
                self.receivedOrganization_telecom.append(child.attrib['value'])
            if child.tag == '{urn:hl7-org:v3}addr':
                self.receivedOrganization_addr = Address_Extractor.Address()
                self.receivedOrganization_addr.extractAdressFromXML(child)

    def printInformationRecipient(self):
        print('Information Recipient')
        print('TypeCode: ')
        print(self.typeCode)
        print('----------------------------------------')
        print('ID: ')
        print(self.recipient_id)
        print('Name: ')
        try:
            self.recipient_name.printName()
        except:
            print(self.recipient_name)
        print('----------------------------------------')
        print('Organisations ID: ')
        print(self.receivedOrganization_id)
        print('Name: ')
        print(self.receivedOrganization_name)
        print('Kontakt: ')
        print(self.receivedOrganization_telecom)
        print('Adresse: ')
        self.receivedOrganization_addr.printAddress()
