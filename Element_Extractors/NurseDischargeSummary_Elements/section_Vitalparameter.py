import xml.etree.ElementTree as ET
import re
from Element_Extractors.LabReport_Elements import Lab_observation_Extractor


class Section_Vitalparamater():

    def __init__(self):
        self.templateIDs = []
        self.text = ""
        self.code = ""
        self.title = ""
        self.entries = []

    def extractVitalparameterFromXML(self, section_element):
        for e in section_element:
            if e.tag == "{urn:hl7-org:v3}templateId":
                self.templateIDs.append(e.attrib["root"])
            elif e.tag == "{urn:hl7-org:v3}code":
                self.code = e.attrib["code"]
            elif e.tag == "{urn:hl7-org:v3}title":
                self.title = e.text
            elif e.tag == "{urn:hl7-org:v3}text":
                try:
                    self.text = ET.tostring(e, encoding='unicode')
                    self.text = re.sub("\<(.*?)\>", "", self.text).replace("\n           ", "").replace("\n",
                                                                                                        "").replace(
                        "                       ", "")
                    # self.text = e.text.replace("\n", "").replace("                       ", "")
                except:
                    self.text = e.text
            elif e.tag == "{urn:hl7-org:v3}entry":
                entry = self.EntryOrganizer()
                entry.extractEntryOrganizer(e)
                self.entries.append(entry)


    class EntryOrganizer():

        def __init__(self):
            self.templateIDs = []
            self.id = ""
            self.code = ""
            self.statusCode = ""
            self.effectiveTime_low = None
            self.effectiveTime_high = ""
            self.observations = []


        def extractEntryOrganizer(self, entry_element):
            organizer_element = entry_element.find("{urn:hl7-org:v3}organizer")
            for e in organizer_element:
                if e.tag == "{urn:hl7-org:v3}templateId":
                    self.templateIDs.append(e.attrib["root"])
                elif e.tag == "{urn:hl7-org:v3}id":
                    self.id = e.attrib
                elif e.tag == "{urn:hl7-org:v3}code":
                    self.code = e.attrib["code"]
                elif e.tag == "{urn:hl7-org:v3}statusCode":
                    self.statusCode = e.attrib["code"]
                elif e.tag == "{urn:hl7-org:v3}effectiveTime":
                    try:
                        self.effectiveTime_low = e.attrib["value"][0:14]
                        self.effectiveTime_high = None
                    except:
                        self.extractEffectiveTime(e)
            observations = entry_element.findall(".//{urn:hl7-org:v3}observation")
            effectiveTime = self.effectiveTime_low
            for o in observations:
                observation = Lab_observation_Extractor.Observation()
                observation.extractObservationNoTime(o, effectiveTime)
                self.observations.append(observation)

        def extractEffectiveTime(self, time_element):
            try:
                self.effectiveTime_low = time_element.find("{urn:hl7-org:v3}low").attrib["value"]
                if len(self.effectiveTime_low) >= 14:
                    self.effectiveTime_low = self.effectiveTime_low[0:14]
                elif len(self.effectiveTime_low) == 8:
                    self.effectiveTime_low = self.effectiveTime_low[0:8]
                elif len(self.effectiveTime_low) == 6:
                    self.effectiveTime_low = self.effectiveTime_low[0:6] + "01"
                    try:
                        self.effectiveTime_high = time_element.find("{urn:hl7-org:v3}high").attrib["value"]
                    except:
                        self.effectiveTime_high = self.effectiveTime_low[0:6] + "30"
                else:
                    self.effectiveTime_low = self.effectiveTime_low[0:4]
            except:
                self.effectiveTime_low = None
            try:
                self.effectiveTime_high = time_element.find("{urn:hl7-org:v3}high").attrib["value"]
                if len(self.effectiveTime_high) >= 14:
                    self.effectiveTime_high = self.effectiveTime_high[0:14]
                elif len(self.effectiveTime_high) == 8:
                    self.effectiveTime_high = self.effectiveTime_high[0:8]
                elif len(self.effectiveTime_high) == 6:
                    self.effectiveTime_high = self.effectiveTime_high[0:6] + "01"
                else:
                    self.effectiveTime_high = self.effectiveTime_high[0:4]
            except:
                self.effectiveTime_high = None

