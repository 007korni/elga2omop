import xml.etree.ElementTree as ET
import re

class allg_section():

    def __init__(self):
        self.templateIDs = []
        self.code = ""
        self.title = ""
        self.text = ""

    def extractSectionFromXML(self, section_element):
        for e in section_element:
            if e.tag == "{urn:hl7-org:v3}templateId":
                self.templateIDs.append(e.attrib["root"])
            elif e.tag == "{urn:hl7-org:v3}code":
                self.code = e.attrib["code"]
            elif e.tag == "{urn:hl7-org:v3}title":
                self.title = e.text
            elif e.tag == "{urn:hl7-org:v3}text":
                try:
                    self.text = ET.tostring(e, encoding='unicode')
                    self.text = re.sub("\<(.*?)\>", "", self.text).replace("\n           ", "").replace("\n", "").replace("                       ", "")
                    #self.text = e.text.replace("\n", "").replace("                       ", "")
                except:
                    self.text = e.text