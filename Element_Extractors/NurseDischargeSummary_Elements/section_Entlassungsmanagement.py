import xml.etree.ElementTree as ET
import re

class Entlassungsmanagement:

    def __init__(self):
        self.templateIDs = []
        self.code = None
        self.title = None
        self.text = None
        self.entry = None

    def extractEntlassungsmanagementFromXML(self, section_element):
        for e in section_element:
            if e.tag == "{urn:hl7-org:v3}templateId":
                self.templateIDs.append(e.attrib["root"])
            elif e.tag == "{urn:hl7-org:v3}code":
                self.code = e.attrib["code"]
            elif e.tag == "{urn:hl7-org:v3}title":
                self.title = e.text
            elif e.tag == "{urn:hl7-org:v3}text":
                try:
                    self.text = ET.tostring(e, encoding='unicode')
                    self.text = re.sub("\<(.*?)\>", "", self.text).replace("\n           ", "").replace("\n",
                                                                                                        "").replace(
                        "                       ", "")
                except:
                    self.text = e.text
            elif e.tag == "{urn:hl7-org:v3}entry":
                self.entry = self.act()
                self.entry.extractActFromXML(e.find("{urn:hl7-org:v3}act"))

    class act:

        def __init__(self):
            self.templateIDs = []
            self.id = None
            self.code = None
            self.text = None
            self.statusCode = None
            self.effectiveTime_low = None
            self.effectiveTime_high = None

        def extractActFromXML(self, act_element):
            for a in act_element:
                if a.tag == "{urn:hl7-org:v3}templateId":
                    self.templateIDs.append(a.attrib["root"])
                elif a.tag == "{urn:hl7-org:v3}id":
                    self.id = a.attrib["root"]
                elif a.tag == "{urn:hl7-org:v3}code":
                    self.code = a.attrib["code"]
                elif a.tag == "{urn:hl7-org:v3}statusCode":
                    self.statusCode = a.attrib["code"]
                elif a.tag == "{urn:hl7-org:v3}text":
                    try:
                        self.text = ET.tostring(a, encoding='unicode')
                        self.text = re.sub("\<(.*?)\>", "", self.text).replace("\n           ", "").replace("\n",
                                                                                                            "").replace(
                            "                       ", "")
                    except:
                        self.text = a.text
                elif a.tag == "{urn:hl7-org:v3}effectiveTime":
                    self.extractEffectiveTime(a)

        def extractEffectiveTime(self, time_element):
                try:
                    self.effectiveTime_low = time_element.find("{urn:hl7-org:v3}low").attrib["value"]
                    if len(self.effectiveTime_low) > 14:
                        self.effectiveTime_low = self.effectiveTime_low[0:14]
                    elif len(self.effectiveTime_low) == 8:
                        self.effectiveTime_low = self.effectiveTime_low[0:8]
                    elif len(self.effectiveTime_low) == 6:
                        self.effectiveTime_low = self.effectiveTime_low[0:6] + "01"
                        try:
                            self.effectiveTime_high = time_element.find("{urn:hl7-org:v3}high").attrib["value"]
                        except:
                            self.effectiveTime_high = self.effectiveTime_low[0:6] + "30"
                    else:
                        self.effectiveTime_low = self.effectiveTime_low[0:4]
                except:
                    self.effectiveTime_low = None
                try:
                    self.effectiveTime_high = time_element.find("{urn:hl7-org:v3}high").attrib["value"]
                    if len(self.effectiveTime_high) > 14:
                        self.effectiveTime_high = self.effectiveTime_high[0:14]
                    elif len(self.effectiveTime_high) == 8:
                        self.effectiveTime_high = self.effectiveTime_high[0:8]
                    elif len(self.effectiveTime_high) == 6:
                        self.effectiveTime_high = self.effectiveTime_high[0:6] + "01"
                    else:
                        self.effectiveTime_high = self.effectiveTime_high[0:4]
                except:
                    self.effectiveTime_high = None