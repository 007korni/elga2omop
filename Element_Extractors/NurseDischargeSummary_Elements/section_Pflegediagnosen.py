import xml.etree.ElementTree as ET
import re

class Pflegediagnosen:

    def __init__(self):
        self.templateId = None
        self.code = None
        self.title = None
        self.text = None
        self.entry = None

    def extractPflegediagnosenFromXML(self, section_element):
        for a in section_element:
            if a.tag == "{urn:hl7-org:v3}templateId":
                self.templateId = a.attrib["root"]
            elif a.tag == "{urn:hl7-org:v3}code":
                self.code = a.attrib["code"]
            elif a.tag == "{urn:hl7-org:v3}title":
                self.title = a.text
            elif a.tag == "{urn:hl7-org:v3}text":
                try:
                    self.text = ET.tostring(a, encoding='unicode')
                    self.text = re.sub("\<(.*?)\>", "", self.text).replace("\n           ", "").replace("\n",
                                                                                                        "").replace(
                        "                       ", "")
                except:
                    self.text = a.text
            elif a.tag == "{urn:hl7-org:v3}entry":
                self.entry = self.Act()
                self.entry.extractActFromXML(a.find("{urn:hl7-org:v3}act"))

    class Act:

        def __init__(self):
            self.templateIds = []
            self.id = None
            self.code = dict()
            self.statusCode = None
            self.effectiveTime_low = None
            self.effectiveTime_high = None
            self.entryRelationship = None

        def extractActFromXML(self, act_element):
            for a in act_element:
                if a.tag == "{urn:hl7-org:v3}templateId":
                    self.templateIds.append(a.attrib["root"])
                elif a.tag == "{urn:hl7-org:v3}id":
                    self.id = a.attrib["root"]
                elif a.tag == "{urn:hl7-org:v3}code":
                        try:
                            self.code = a.attrib["code"]
                        except:
                            if a.attrib["nullFlavor"] == "UNK":
                                self.code["code"] = "UNK"
                            elif a.attrib["nullFlavor"] == "NA":
                                self.code["code"] = "NA"

                elif a.tag == "{urn:hl7-org:v3}statusCode":
                    self.statusCode = a.attrib["code"]
                elif a.tag == "{urn:hl7-org:v3}text":
                    try:
                        self.text = ET.tostring(a, encoding='unicode')
                        self.text = re.sub("\<(.*?)\>", "", self.text).replace("\n           ", "").replace("\n",
                                                                                                            "").replace(
                            "                       ", "")
                    except:
                        self.text = a.text
                elif a.tag == "{urn:hl7-org:v3}effectiveTime":
                    self.extractEffectiveTime(a)
                elif a.tag == "{urn:hl7-org:v3}entryRelationship":
                    self.entryRelationship = self.Observation()
                    self.entryRelationship.extractObservation(a.find("{urn:hl7-org:v3}observation"))


        def extractEffectiveTime(self, time_element):
                try:
                    self.effectiveTime_low = time_element.find("{urn:hl7-org:v3}low").attrib["value"]
                    if len(self.effectiveTime_low) > 14:
                        self.effectiveTime_low = self.effectiveTime_low[0:14]
                    elif len(self.effectiveTime_low) == 8:
                        self.effectiveTime_low = self.effectiveTime_low[0:8]
                    elif len(self.effectiveTime_low) == 6:
                        self.effectiveTime_low = self.effectiveTime_low[0:6] + "01"
                        try:
                            self.effectiveTime_high = time_element.find("{urn:hl7-org:v3}high").attrib["value"]
                        except:
                            self.effectiveTime_high = self.effectiveTime_low[0:6] + "30"
                    else:
                        self.effectiveTime_low = self.effectiveTime_low[0:4]
                except:
                    self.effectiveTime_low = None
                try:
                    self.effectiveTime_high = time_element.find("{urn:hl7-org:v3}high").attrib["value"]
                    if len(self.effectiveTime_high) > 14:
                        self.effectiveTime_high = self.effectiveTime_high[0:14]
                    elif len(self.effectiveTime_high) == 8:
                        self.effectiveTime_high = self.effectiveTime_high[0:8]
                    elif len(self.effectiveTime_high) == 6:
                        self.effectiveTime_high = self.effectiveTime_high[0:6] + "01"
                    else:
                        self.effectiveTime_high = self.effectiveTime_high[0:4]
                except:
                    self.effectiveTime_high = None

        class Observation:

            def __init__(self):
                self.templateIds = []
                self.id = None
                self.code = None
                self.text = None
                self.statusCode = None
                self.effectiveTime_low = None
                self.effectiveTime_high = None
                self.value = None

            def extractObservation(self, observation_element):
                for o in observation_element:
                    if o.tag == "{urn:hl7-org:v3}templateId":
                        self.templateIds.append(o.attrib["root"])
                    elif o.tag == "{urn:hl7-org:v3}id":
                        self.id = o.attrib["root"]
                    elif o.tag == "{urn:hl7-org:v3}code":
                        self.code = o.attrib["code"]
                    elif o.tag == "{urn:hl7-org:v3}statusCode":
                        self.statusCode = o.attrib["code"]
                    elif o.tag == "{urn:hl7-org:v3}text":
                        try:
                            self.text = ET.tostring(o, encoding='unicode')
                            self.text = re.sub("\<(.*?)\>", "", self.text).replace("\n           ", "").replace("\n",
                                                                                                                "").replace(
                                "                       ", "")
                        except:
                            self.text = o.text
                    elif o.tag == "{urn:hl7-org:v3}effectiveTime":
                        self.extractEffectiveTime(o)
                    elif o.tag == "{urn:hl7-org:v3}value":
                        self.value = o.attrib

            def extractEffectiveTime(self, time_element):
                try:
                    self.effectiveTime_low = time_element.find("{urn:hl7-org:v3}low").attrib["value"]
                    if len(self.effectiveTime_low) > 14:
                        self.effectiveTime_low = self.effectiveTime_low[0:14]
                    elif len(self.effectiveTime_low) == 8:
                        self.effectiveTime_low = self.effectiveTime_low[0:8]
                    elif len(self.effectiveTime_low) == 6:
                        self.effectiveTime_low = self.effectiveTime_low[0:6] + "01"
                        try:
                            self.effectiveTime_high = time_element.find("{urn:hl7-org:v3}high").attrib["value"]
                        except:
                            self.effectiveTime_high = self.effectiveTime_low[0:6] + "30"
                    else:
                        self.effectiveTime_low = self.effectiveTime_low[0:4]
                except:
                    self.effectiveTime_low = None
                try:
                    self.effectiveTime_high = time_element.find("{urn:hl7-org:v3}high").attrib["value"]
                    if len(self.effectiveTime_high) > 14:
                        self.effectiveTime_high = self.effectiveTime_high[0:14]
                    elif len(self.effectiveTime_high) == 8:
                        self.effectiveTime_high = self.effectiveTime_high[0:8]
                    elif len(self.effectiveTime_high) == 6:
                        self.effectiveTime_high = self.effectiveTime_high[0:6] + "01"
                    else:
                        self.effectiveTime_high = self.effectiveTime_high[0:4]
                except:
                    self.effectiveTime_high = None