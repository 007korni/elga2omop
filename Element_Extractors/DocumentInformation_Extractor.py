class Document:

    document_id = ''
    effectiveTime = ''
    realmCode = ''
    code = []
    title = ''
    confidentialityCode = ''
    languageCode = ''
    templates = []

    def __init__(self):
        self.document_id = []
        self.effectiveTime = ''
        self.realmCode = ''
        self.typeId = ''
        self.code = ''
        self.title = ''
        self.confidentialityCode = []
        self.languageCode = ''
        self.templates = []

    def extractDocumentInformationFromXML(self, root_element):

        for child in root_element:
            if child.tag == '{urn:hl7-org:v3}realmCode':
                self.realmCode = child.attrib['code']

            if child.tag == '{urn:hl7-org:v3}typeId':
                self.typeId = child.attrib['extension']

            if child.tag == '{urn:hl7-org:v3}id':
                self.document_id = child.attrib['extension']
                #self.document_id.append(child.attrib['extension'])
                #self.document_id.append(child.attrib['assigningAuthorityName'])

            if child.tag == '{urn:hl7-org:v3}templateId':
                self.templates.append(child.attrib['root'])
                #try:
                #    self.templates.append(child.attrib['assigningAuthorityName'])
                #except:
                #    self.templates.append('NULL')

            if child.tag == '{urn:hl7-org:v3}code':
                self.code = child.attrib["code"]
                #self.code.append(child.attrib['code'])
                #self.code.append(child.attrib['displayName'])
                #self.code.append(child.attrib['codeSystem'])
                #self.code.append(child.attrib['codeSystemName'])

            if child.tag == '{urn:hl7-org:v3}confidentialityCode':
                self.confidentialityCode = child.attrib["code"]
                #self.confidentialityCode.append(child.attrib['code'])
                #self.confidentialityCode.append(child.attrib['displayName'])
                #self.confidentialityCode.append(child.attrib['codeSystem'])
                #self.confidentialityCode.append(child.attrib['codeSystemName'])

            if child.tag == '{urn:hl7-org:v3}languageCode':
                self.languageCode = child.attrib['code']

            if child.tag == '{urn:hl7-org:v3}title':
                self.title = child.text

            if child.tag == '{urn:hl7-org:v3}effectiveTime':
                self.effectiveTime = child.attrib['value']
