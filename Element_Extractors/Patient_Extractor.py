from Element_Extractors import Address_Extractor, Name_Extractor


class Patient:

    def __init__(self):
        self.local = ''
        self.SVNR = ''
        self.BPKGH = ''
        self.addr = ''
        self.telecom = []
        self.patient_name = ''
        self.patient_gender = ''
        self.birthtime = ''
        self.martialStatusCode = ''
        self.religiousAffiliationCode = ''
        self.language = ''
        self.birthplace = ''
        self.guardians = ''

    def extractPatientFromXML(self, recordTarget_element):
        patientRole = recordTarget_element.find('.//{urn:hl7-org:v3}patientRole')
        patient = patientRole.find('.//{urn:hl7-org:v3}patient')
        ids = patientRole.findall('{urn:hl7-org:v3}id')
        if len(ids) == 1:
            self.SVNR = patientRole.find('{urn:hl7-org:v3}id[@assigningAuthorityName="Österreichische Sozialversicherung"]')
            if self.SVNR == None:
                self.local = ids[0]
        elif len(ids) == 2:
            self.local = ids[0]
            self.SVNR = patientRole.find('{urn:hl7-org:v3}id[@assigningAuthorityName="Österreichische Sozialversicherung"]')
        else:
            self.local = ids[0]
            self.SVNR = patientRole.find('{urn:hl7-org:v3}id[@assigningAuthorityName="Österreichische Sozialversicherung"]')
            self.BPKGH = ids[2]

        for child in patientRole:
            if child.tag == '{urn:hl7-org:v3}addr':
                self.addr = Address_Extractor.Address()
                self.addr.extractAdressFromXML(child)
            if child.tag == '{urn:hl7-org:v3}telecom':
                self.telecom.append(child.attrib['value'])

        for child in patient:
            if child.tag == '{urn:hl7-org:v3}name':
                self.patient_name = Name_Extractor.Name()
                self.patient_name.extractName(child)
            if child.tag == '{urn:hl7-org:v3}administrativeGenderCode':
                self.patient_gender = child.attrib
            if child.tag == '{urn:hl7-org:v3}birthTime':
                self.birthtime = child.attrib['value']
            if child.tag == '{urn:hl7-org:v3}maritalStatusCode':
                self.martialStatusCode = child.attrib
            if child.tag == '{urn:hl7-org:v3}religiousAffiliationCode':
                self.religiousAffiliationCode = child.attrib
            if child.tag == '{urn:hl7-org:v3}birthplace':
                addr = child.find('.//{urn:hl7-org:v3}addr')
                self.birthplace = Address_Extractor.Address()
                self.birthplace.extractAdressFromXML(addr)


    def printPatientInformation(self):
        print('Namen: ')
        self.patient_name.printName()
        print('----------------------------------------')
        print('Adresse: ')
        self.addr.printAddress()
        print('----------------------------------------')
        print('IDs: ')
        print(self.ids)
        print('----------------------------------------')
        print('Kontaktmöglichkeiten: ')
        print(self.telecom)
        print('----------------------------------------')
        print('Geschlecht: ')
        print(self.patient_gender)
        print('----------------------------------------')
        print('Geburtsdatum: ')
        print(self.birthtime)
        print('----------------------------------------')
        print('Geburtsort: ')
        self.birthplace.printAddress()
        print('----------------------------------------')
        print('Familienstand: ')
        print(self.martialStatusCode)
        print('----------------------------------------')
        print('Religion: ')
        print(self.religiousAffiliationCode)
        print('----------------------------------------')


