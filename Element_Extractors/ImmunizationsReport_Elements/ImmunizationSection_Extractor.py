import xml.etree.ElementTree as ET
import re
from Element_Extractors import Author_Extractor

class ImmunizationSection:

    def __init__(self):
        # Mehrere templateIDs
        self.templateIds = []
        # Section Id mit root und extension
        self.id = ""
        # Section Codierung - HISTORY OF IMMUNIZATIONS
        self.code = ""
        # Titel der Sektion
        self.title = ""
        # Text - menschenlesbarer Teil
        self.text = ""
        # Entry - substanceAdministration
        self.substanceAdministrations = []

    def extractImmunizationSectionFromXML(self, immunizationsection_element):
        substanceAdministrations_elements = immunizationsection_element.findall("{urn:hl7-org:v3}entry/{urn:hl7-org:v3}substanceAdministration")
        for e in immunizationsection_element:
            if e.tag == "{urn:hl7-org:v3}templateId":
                self.templateIds.append(e.attrib)
            elif e.tag == "{urn:hl7-org:v3}id":
                self.id = e.attrib
            elif e.tag == "{urn:hl7-org:v3}code":
                self.code = e.attrib["code"]
            elif e.tag == "{urn:hl7-org:v3}title":
                self.title = e.text
            elif e.tag == "{urn:hl7-org:v3}text":
                self.text = ET.tostring(e, encoding='unicode')
                self.text = re.sub("\<(.*?)\>", "", self.text).replace("\n           ", "").replace("\n", "").replace(
                    "                       ", "")
        for subAD in substanceAdministrations_elements:
            substanceadministration = substanceAdministration()
            substanceadministration.extractsubstanceAdministration(subAD)
            self.substanceAdministrations.append(substanceadministration)


class substanceAdministration:

    def __init__(self):
        # Entry - SubstanceAdministration
        # Mehrere templateIDs
        self.templateIds = []
        # Id mit root und extenstion
        self.id = ""
        # Codierung - IMMUNIZ
        self.code = ""
        # Status der Impfung
        self.statusCode = ""
        # Datum der Impfung
        self.effectiveTime = ""
        # Dosis
        self.doseQuantity = ""
        # Impfstoff
        # PZN des Impfstoffs
        self.consumable_code = ""
        # Name des Impfstoffs
        self.consumable_name = ""
        # Chargennummer
        self.lotNumberText = ""
        # ATC code
        self.consumable_ATC = ""
        # ATC code name
        self.consumable_ATC_name = ""
        #Author
        self.author = None

    def extractsubstanceAdministration(self, substanceadministration_element):
        for e in substanceadministration_element:
            if e.tag == "{urn:hl7-org:v3}templateId":
                self.templateIds.append(e.attrib)
            elif e.tag == "{urn:hl7-org:v3}id":
                self.id = e.attrib
            elif e.tag == "{urn:hl7-org:v3}code":
                self.code = e.attrib["code"]
            elif e.tag == "{urn:hl7-org:v3}statusCode":
                self.statusCode = e.attrib
            elif e.tag == "{urn:hl7-org:v3}effectiveTime":
                self.effectiveTime = e.attrib["value"]
            elif e.tag == "{urn:hl7-org:v3}doseQuantity":
                try:
                    if e.attrib["nullFlavor"] == "UNK":
                        self.doseQuantity = None
                    elif e.attrib["nullFlavor"] == "NA":
                        self.doseQuantity = None
                except:
                    self.doseQuantity = e.attrib
            elif e.tag == "{urn:hl7-org:v3}consumable":
                self.extractConsumable(e)
            elif e.tag == "{urn:hl7-org:v3}author":
                self.extractAuthor(e)

    def extractConsumable(self, consumable_element):
        for e in consumable_element.find("{urn:hl7-org:v3}manufacturedProduct/{urn:hl7-org:v3}manufacturedMaterial"):
            if e.tag == "{urn:hl7-org:v3}code":
                self.consumable_code = e.attrib["code"]
            elif e.tag == "{urn:hl7-org:v3}name":
                self.consumable_name = e.text
            elif e.tag == "{urn:hl7-org:v3}lotNumberText":
                self.lotNumberText = e.text
            elif e.tag == "{urn:ihe:pharm:medication}ingredient":
                #TODO Code in DB schreiben
                self.consumable_ATC = e.find("{urn:ihe:pharm:medication}ingredient/{urn:ihe:pharm:medication}code").attrib["code"]
                self.consumable_ATC_name = e.find("{urn:ihe:pharm:medication}ingredient/{urn:ihe:pharm:medication}name").text.replace("\n", "").replace("                       ", "")

    def extractAuthor(self, author_element):
        self.author = Author_Extractor.Author()
        self.author.extractAuthorsFromXML(author_element)
