import xml.etree.ElementTree as ET
import re
from Element_Extractors import Author_Extractor


# Expositionsrisikogruppen
class PersonengruppenSection:

    def __init__(self):
        # Mehrere templateIDs
        self.templateIds = []
        # Section Id mit root und extension
        self.id = ""
        # Section Codierung - HISTORY OF IMMUNIZATIONS
        self.code = ""
        # Titel der Sektion
        self.title = ""
        # Text - menschenlesbarer Teil
        self.text = ""
        self.entries = []

    def extractPersonengruppenSectionFromXML(self, personengruppen_element):
        entry_elements = personengruppen_element.findall("{urn:hl7-org:v3}entry/{urn:hl7-org:v3}act")
        for e in personengruppen_element:
            if e.tag == "{urn:hl7-org:v3}templateId":
                self.templateIds.append(e.attrib)
            elif e.tag == "{urn:hl7-org:v3}id":
                self.id = e.attrib
            elif e.tag == "{urn:hl7-org:v3}code":
                self.code = e.attrib["code"]
            elif e.tag == "{urn:hl7-org:v3}title":
                self.title = e.text
            elif e.tag == "{urn:hl7-org:v3}text":
                self.text = ET.tostring(e, encoding='unicode')
                self.text = re.sub("\<(.*?)\>", "", self.text).replace("\n           ", "").replace("\n", "").replace(
                    "                       ", "")
        for entry_element in entry_elements:
            ent_elem = Act()
            ent_elem.extractAct(entry_element)
            self.entries.append(ent_elem)



class Act:

    def __init__(self):
        self.templateIds = []
        self.id = None
        self.code = dict()
        self.statusCode = None
        self.effectiveTime_low = None
        self.effectiveTime_high = None
        self.author = None
        self.observations = []

    def extractAct(self, act_element):
        observations_elements = act_element.findall(".//{urn:hl7-org:v3}entryRelationship/{urn:hl7-org:v3}observation")
        for e in act_element:
            if e.tag == "{urn:hl7-org:v3}templateId":
                self.templateIds.append(e.attrib)
            elif e.tag == "{urn:hl7-org:v3}id":
                self.id = e.attrib
            elif e.tag == "{urn:hl7-org:v3}code":
                try:
                    if e.attrib["nullFlavor"] == "UNK":
                        self.code["code"] = "UNK"
                    elif e.attrib["nullFlavor"] == "NA":
                        self.code["code"] = "NA"
                except:
                    self.code = e.attrib["code"]
            elif e.tag == "{urn:hl7-org:v3}statusCode":
                self.statusCode = e.attrib["code"]
            elif e.tag == "{urn:hl7-org:v3}effectiveTime":
                try:
                    self.effectiveTime_low = e.find("{urn:hl7-org:v3}low").attrib
                except:
                    self.effectiveTime_low = None
                try:
                    self.effectiveTime_high = e.find("{urn:hl7-org:v3}high").attrib
                except:
                    self.effectiveTime_high = None
            elif e.tag == "{urn:hl7-org:v3}author":
                self.author = Author_Extractor.Author()
                self.author.extractAuthorsFromXML(e)

        for o in observations_elements:
            observation = Observation()
            observation.extractObservation(o)
            self.observations.append(observation)


class Observation:

    def __init__(self):
        self.templateIds = []
        self.id = None
        self.code = None
        self.statusCode = None
        self.effectiveTime_low = None
        self.effectiveTime_high = None
        self.value_code = None

    def extractObservation(self, observation_element):
        for e in observation_element:
            if e.tag == "{urn:hl7-org:v3}templateId":
                self.templateIds.append(e.attrib)
            elif e.tag == "{urn:hl7-org:v3}id":
                self.id = e.attrib
            elif e.tag == "{urn:hl7-org:v3}code":
                self.code = e.attrib["code"]
            elif e.tag == "{urn:hl7-org:v3}statusCode":
                self.statusCode = e.attrib["code"]
            elif e.tag == "{urn:hl7-org:v3}effectiveTime":
                try:
                    self.effectiveTime_low = e.find("{urn:hl7-org:v3}low").attrib
                except:
                    self.effectiveTime_low = None
                try:
                    self.effectiveTime_high = e.find("{urn:hl7-org:v3}high").attrib
                except:
                    self.effectiveTime_high = None
            elif e.tag == "{urn:hl7-org:v3}value":
                self.value_code = e.attrib