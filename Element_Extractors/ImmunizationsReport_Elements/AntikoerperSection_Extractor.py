import xml.etree.ElementTree as ET
import re

from Element_Extractors import Author_Extractor


class AntikoerperSection:

    def __init__(self):
        # Mehrere templateIDs
        self.templateIds = []
        # Section Id mit root und extension
        self.id = ""
        # Section Codierung
        self.code = ""
        # Titel der Sektion
        self.title = ""
        # Text - menschenlesbarer Teil
        self.text = ""
        # Entries
        self.entries = []

    def extractAntikoerperSectionFromXML(self, antikoerpersection_element):
        entry_elements = antikoerpersection_element.findall("{urn:hl7-org:v3}entry/{urn:hl7-org:v3}act")
        for e in antikoerpersection_element:
            if e.tag == "{urn:hl7-org:v3}templateId":
                self.templateIds.append(e.attrib)
            elif e.tag == "{urn:hl7-org:v3}id":
                self.id = e.attrib
            elif e.tag == "{urn:hl7-org:v3}code":
                self.code = e.attrib["code"]
            elif e.tag == "{urn:hl7-org:v3}title":
                self.title = e.text
            elif e.tag == "{urn:hl7-org:v3}text":
                self.text = ET.tostring(e, encoding='unicode')
                self.text = re.sub("\<(.*?)\>", "", self.text).replace("\n           ", "").replace("\n", "").replace(
                    "                       ", "")
        for entry_el in entry_elements:
            entry_element = Act()
            entry_element.extractAct(entry_el)
            self.entries.append(entry_element)


class Act:

    def __init__(self):
        self.code = dict()
        self.statusCode = None
        self.author = None
        self.entryRelationship = ""

    def extractAct(self, act_element):
        for e in act_element:
            if e.tag == "{urn:hl7-org:v3}code":
                try:
                    if e.attrib["nullFlavor"] == "UNK":
                        self.code["code"] = "UNK"
                    elif e.attrib["nullFlavor"] == "NA":
                        self.code["code"] = "NA"
                except:
                    self.code = e.attrib["code"]
            elif e.tag == "{urn:hl7-org:v3}statusCode":
                self.statusCode = e.attrib["code"]
            elif e.tag == "{urn:hl7-org:v3}author":
                self.author = Author_Extractor.Author()
                self.author.extractAuthorsFromXML(e)
            elif e.tag == "{urn:hl7-org:v3}entryRelationship":
                self.entryRelationship = EntryRelationship()
                self.entryRelationship.extractentryRelationshio(e)


class EntryRelationship:

    def __init__(self):
        self.code = dict()
        self.statusCode = None
        self.observations = []

    def extractentryRelationshio(self, entryrel_element):
        observation_element = entryrel_element.findall("{urn:hl7-org:v3}organizer/{urn:hl7-org:v3}component/{urn:hl7-org:v3}observation")
        for e in entryrel_element.find("{urn:hl7-org:v3}organizer"):
            if e.tag == "{urn:hl7-org:v3}code":
                try:
                    if e.attrib["nullFlavor"] == "UNK":
                        self.code["code"] = "UNK"
                    elif e.attrib["nullFlavor"] == "NA":
                        self.code["code"] = "NA"
                except:
                    self.code = e.attrib["code"]
            elif e.tag == "{urn:hl7-org:v3}statusCode":
                self.statusCode = e.attrib["code"]

        for o in observation_element:
            observation = Observation()
            observation.extractObservation(o)
            self.observations.append(observation)


class Observation:

    def __init__(self):
        self.id = None
        self.code = None
        self.statusCode = None
        self.effectiveTime = None
        self.value = ""
        self.value_unit = ""
        self.interpretationCode = None
        self.range_low = ""
        self.range_high = ""
        self.range_interpretation_code = None

    def extractObservation(self, observation_element):
        for oe in observation_element:
            if oe.tag == "{urn:hl7-org:v3}id":
                self.id = oe.attrib
            elif oe.tag == "{urn:hl7-org:v3}code":
                try:
                    if oe.attrib["nullFlavor"] == "UNK":
                        self.code = "UNK"
                    elif oe.attrib["nullFlavor"] == "NA":
                        self.code = "NA"
                except:
                    self.code = oe.attrib["code"]
            elif oe.tag == "{urn:hl7-org:v3}statusCode":
                self.statusCode = oe.attrib["code"]
            elif oe.tag == "{urn:hl7-org:v3}effectiveTime":
                self.effectiveTime = oe.attrib["value"]
            elif oe.tag == "{urn:hl7-org:v3}value":
                self.value = oe.attrib["value"]
                self.value_unit = oe.attrib["unit"]
            elif oe.tag == "{urn:hl7-org:v3}interpretationCode":
                self.interpretationCode = oe.attrib["code"]
            elif oe.tag == "{urn:hl7-org:v3}referenceRange":
                self.extractreferenceRange(oe)

    def extractreferenceRange(self, range_element):
        try:
            self.range_low = range_element.find("{urn:hl7-org:v3}observationRange/{urn:hl7-org:v3}value/{urn:hl7-org:v3}low").attrib["value"]
        except:
            self.range_low = None
        try:
            self.range_high = range_element.find("{urn:hl7-org:v3}observationRange/{urn:hl7-org:v3}value/{urn:hl7-org:v3}high").attrib["value"]
        except:
            self.range_high = None
        self.range_interpretation_code = range_element.find("{urn:hl7-org:v3}observationRange/{urn:hl7-org:v3}interpretationCode").attrib["code"]