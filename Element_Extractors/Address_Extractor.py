class Address:

    def __init__(self):
        self.streetAddressLine = ''
        self.houseNumber = ''
        self.postalCode = ''
        self.city = ''
        self.state = ''
        self.country = ''
        self.additionalLocator = ''

    def extractAdressFromXML(self, addr_element):

        #TODO Extraktor für Granularität 1

        for child in addr_element:
            if child.tag == '{urn:hl7-org:v3}streetAddressLine':
                self.streetAddressLine = child.text
            if child.tag == '{urn:hl7-org:v3}streetName': #Für Granularität 3
                self.streetAddressLine = child.text
            if child.tag == '{urn:hl7-org:v3}houseNumber': #Für Granularität 3
                self.streetAddressLine = self.streetAddressLine + ' ' + child.text
            if child.tag == '{urn:hl7-org:v3}postalCode':
                self.postalCode = child.text
            if child.tag == '{urn:hl7-org:v3}city':
                self.city = child.text
            if child.tag == '{urn:hl7-org:v3}state':
                self.state = child.text
            if child.tag == '{urn:hl7-org:v3}country':
                self.country = child.text
            if child.tag == '{urn:hl7-org:v3}additionalLocator':
                self.additionalLocator = child.text

    def printAddress(self):
        print('Straße: ' + self.streetAddressLine)
        print('Postleitzahl: ' + self.postalCode)
        print('Stadt: ' + self.city)
        print('Bundesland: ' + self.state)
        print('Land: ' + self.country)
        if self.additionalLocator != '':
            print('Zusatzinformationen: ' + self.additionalLocator)


    def singleAddressLine(self):

        address = self.streetAddressLine + " " + self.postalCode + " " + self.city + " " + self.state + " " + self.country
        return address