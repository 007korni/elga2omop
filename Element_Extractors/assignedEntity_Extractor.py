from Element_Extractors import Address_Extractor, Name_Extractor

class assignedEntity:

    def __init__(self):

        self.assEnt_id = dict()
        self.assEnt_id["extension"] = None
        self.assEnt_addr = None
        self.assEnt_telecom = []
        # Assigned Person
        self.assPers_name = None
        # Represented Organisation
        self.repOrg_id = dict()
        self.repOrg_id["root"] = None
        self.repOrg_name = None
        self.repOrg_telecom = []
        self.repOrg_addr = None

    def extractAssignedEntityFromXML(self,assignedEntity_element):
        for child in assignedEntity_element:
            if child.tag == '{urn:hl7-org:v3}id':
                try:
                    if child.attrib["nullFlavor"] == "UNK":
                        self.assEnt_id["extension"] = "UNK"
                    elif child.attrib["nullFlavor"] == "NA":
                        self.assEnt_id["extension"] = "NA"
                except:
                    self.assEnt_id = child.attrib
            if child.tag == '{urn:hl7-org:v3}addr':
                self.assEnt_addr = Address_Extractor.Address()
                self.assEnt_addr.extractAdressFromXML(child)
            if child.tag == '{urn:hl7-org:v3}telecom':
                self.assEnt_telecom.append(child.attrib)
            if child.tag == '{urn:hl7-org:v3}assignedPerson':
                self.assignedPersonExtract(child)
            if child.tag == '{urn:hl7-org:v3}representedOrganization':
                self.representedOrganizationExtractor(child)

    def assignedPersonExtract(self,assignedPerson_element):
        for child in assignedPerson_element:
            if child.tag == '{urn:hl7-org:v3}name':
                self.assPers_name = Name_Extractor.Name()
                self.assPers_name.extractName(child)

    def representedOrganizationExtractor(self,representedOrganization_element):
        for child in representedOrganization_element:
            if child.tag == '{urn:hl7-org:v3}id':
                try:
                    if child.attrib["nullFlavor"] == "UNK":
                        self.repOrg_id["root"] = "UNK"
                    elif child.attrib["nullFlavor"] == "NA":
                        self.repOrg_id["root"] = "NA"
                except:
                    self.repOrg_id = child.attrib
            if child.tag == '{urn:hl7-org:v3}name':
                self.repOrg_name = child.text
            if child.tag == '{urn:hl7-org:v3}telecom':
                self.repOrg_telecom.append(child.attrib)
            if child.tag == '{urn:hl7-org:v3}addr':
                self.repOrg_addr = Address_Extractor.Address()
                self.repOrg_addr.extractAdressFromXML(child)