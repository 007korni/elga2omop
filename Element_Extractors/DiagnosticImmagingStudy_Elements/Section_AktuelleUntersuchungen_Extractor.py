import xml.etree.ElementTree as ET
import re
from Element_Extractors.LabReport_Elements import Lab_observation_Extractor


class AktuelleUntersuchungenSection:

    def __init__(self):
        # Mehrere templateIDs
        self.templateIds = []
        # Section Codierung
        self.code = ""
        # Titel der Sektion
        self.title = ""
        # Text - menschenlesbarer Teil
        self.text = ""
        # Entries
        self.entries = []

    def extractAktuelleUntersuchungenSectionFromXML(self, AUsection_element):
        entry_elements = AUsection_element.findall("{urn:hl7-org:v3}entry/{urn:hl7-org:v3}observation")
        for e in AUsection_element:
            if e.tag == "{urn:hl7-org:v3}templateId":
                self.templateIds.append(e.attrib["root"])
            elif e.tag == "{urn:hl7-org:v3}id":
                self.id = e.attrib
            elif e.tag == "{urn:hl7-org:v3}code":
                self.code = e.attrib["code"]
            elif e.tag == "{urn:hl7-org:v3}title":
                self.title = e.text
            elif e.tag == "{urn:hl7-org:v3}text":
                self.text = ET.tostring(e, encoding='unicode')
                self.text = re.sub("\<(.*?)\>", "", self.text).replace("\n           ", "").replace("\n", "").replace(
                    "                       ", "")
        for entry_el in entry_elements:
            entry_element = Lab_observation_Extractor.Observation()
            entry_element.extractObservation(entry_el)
            self.entries.append(entry_element)