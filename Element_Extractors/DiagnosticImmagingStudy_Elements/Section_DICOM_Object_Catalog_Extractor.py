import xml.etree.ElementTree as ET
import re


class DICOMobjectCatalogSection:

    def __init__(self):
        # Mehrere templateIDs
        self.templateIds = []
        # Section Codierung
        self.code = None
        self.entry = []

        # Entry -> Act(Study) -> entryRelationship -> Act (Series) -> entryRelationship -> observation (SopInstance UID)

    def extractDICOMObjectCatalogFromXML(self, section_element):
        entry_elements = section_element.findall("{urn:hl7-org:v3}entry")
        for e in section_element:
            if e.tag == "{urn:hl7-org:v3}templateId":
                self.templateIds.append(e.attrib["root"])
            elif e.tag == "{urn:hl7-org:v3}id":
                self.id = e.attrib
            elif e.tag == "{urn:hl7-org:v3}code":
                self.code = e.attrib["code"]

        for entry in entry_elements:
            act = self.Act_study()
            act.extractAct(entry.find("{urn:hl7-org:v3}act"))
            self.entry.append(act)

    #study
    class Act_study:

        def __init__(self):
            self.templateIds = []
            self.id = None
            self.code = None
            self.text = None
            self.effectiveTime = None
            self.entryRelationships = []  # Acts bzw. Serien

        def extractAct(self, act_element):
            entryRel_elements = []
            for a in act_element:
                if a.tag == "{urn:hl7-org:v3}templateId":
                    self.templateIds.append(a.attrib["root"])
                elif a.tag == "{urn:hl7-org:v3}id":
                    self.id = a.attrib
                elif a.tag == "{urn:hl7-org:v3}code":
                    self.code = a.attrib["code"]
                elif a.tag == "{urn:hl7-org:v3}entryRelationship":
                    entryRel_elements.append(a.find("{urn:hl7-org:v3}act"))
                elif a.tag == "{urn:hl7-org:v3}effectiveTime":
                    try:
                        self.effectiveTime = a.attrib["value"][0:14]
                    except:
                        self.effectiveTime = None
                elif a.tag == "{urn:hl7-org:v3}text":
                    self.text = ET.tostring(a, encoding='unicode')
                    self.text = re.sub("\<(.*?)\>", "", self.text).replace("\n           ", "").replace("\n",
                                                                                                        "").replace(
                        "                       ", "")

            for el in entryRel_elements:
                act = DICOMobjectCatalogSection.Act_series()
                act.extractAct_series(el)
                self.entryRelationships.append(act)

    class Act_series:

        def __init__(self):
            self.templateIds = []
            self.id = None
            self.code = None
            self.qualifierName = None
            self.qualifierValue = None
            self.observations = []

        def extractAct_series(self, act_element):
            observations = act_element.findall(".//{urn:hl7-org:v3}observation")
            for o in observations:
                obs = DICOMobjectCatalogSection.Observation()
                obs.extractObservation(o)
                self.observations.append(obs)

            for a in act_element:
                if a.tag == "{urn:hl7-org:v3}templateId":
                    self.templateIds.append(a.attrib["root"])
                elif a.tag == "{urn:hl7-org:v3}id":
                    self.id = a.attrib
                elif a.tag == "{urn:hl7-org:v3}code":
                    self.code = a.attrib["code"]
                    self.qualifierName = a.find("{urn:hl7-org:v3}qualifier/{urn:hl7-org:v3}name").attrib["code"]
                    self.qualifierValue = a.find("{urn:hl7-org:v3}qualifier/{urn:hl7-org:v3}value").attrib["code"]

    # SopInstance UID
    class Observation:

        def __init__(self):
            self.templateIds = []
            self.id = None
            self.code = None
            self.text = None
            self.effectiveTime = None

        def extractObservation(self, observation_element):
            for o in observation_element:
                if o.tag == "{urn:hl7-org:v3}templateId":
                    self.templateIds.append(o.attrib["root"])
                elif o.tag == "{urn:hl7-org:v3}id":
                    self.id = o.attrib
                elif o.tag == "{urn:hl7-org:v3}code":
                    self.code = o.attrib["code"]
                elif o.tag == "{urn:hl7-org:v3}text":
                    self.text = o.find("{urn:hl7-org:v3}reference").attrib["value"]
                elif o.tag == "{urn:hl7-org:v3}effectiveTime":
                    try:
                        self.effectiveTime = o.attrib["value"][0:14]
                    except:
                        self.effectiveTime = None
