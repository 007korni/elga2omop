from Element_Extractors.LabReport_Elements import Lab_observation_Extractor


class entryRelationship:

    def __init__(self):
        self.templateId = ""
        self.code = ""
        self.observations = []

    def extractEntryRelationshipFromXML(self, entryrelationship_element):

        try:
            self.code = entryrelationship_element.find(".//{urn:hl7-org:v3}code").attrib["code"]
        except:
            self.code = None
        self.templateId = entryrelationship_element.find(".//{urn:hl7-org:v3}templateId").attrib["root"]


        self.extraxtObservationsFromEntryRelationshio(entryrelationship_element)

    def extraxtObservationsFromEntryRelationshio(self, entryrelationship_element):
        observations = entryrelationship_element.findall(".//{urn:hl7-org:v3}observation")
        for obs in observations:
            observation = Lab_observation_Extractor.Observation()
            observation.extractObservation(obs)
            self.observations.append(observation)

