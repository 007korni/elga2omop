from Element_Extractors import Procedure_Extractor

class SpecimenSection:

    def __init__(self):
        self.code = ""
        self.statusCode = ""
        self.procedures = []

    def extractSpecimenSection(self, specimensection_element):
        self.procedures = self.extractProcedure(specimensection_element)
        self.code = specimensection_element.find("{urn:hl7-org:v3}code").attrib
        self.statusCode = specimensection_element.find(".//{urn:hl7-org:v3}statusCode").attrib


    def extractProcedure(self, specimensection_element):
        procedures = []
        procedures_elements = specimensection_element.findall(".//{urn:hl7-org:v3}procedure")
        for proc in procedures_elements:
            procedure = Procedure_Extractor.Procedure()
            procedure.extractProcedureFromElement(proc)
            procedures.append(procedure)
        return procedures
