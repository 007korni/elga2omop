class Observation:

    def __init__(self):
        self.templateId = ""
        self.code = ""
        self.statusCode = ""
        self.effectiveTime = None
        self.interpretationCode = ""
        self.value = ""
        self.unit = ""
        self.range_low = ""
        self.range_high = ""

    def extractObservation(self, observation_element):
        for child in observation_element:
            if child.tag == "{urn:hl7-org:v3}code":
                try:
                    self.code = child.attrib["code"]
                except:
                    if child.attrib["nullFlavor"] == "OTH":
                        self.code = child.find("{urn:hl7-org:v3}translation").attrib["code"]
            elif child.tag == "{urn:hl7-org:v3}templateId":
                self.templateId = child.attrib["root"]
            elif child.tag == "{urn:hl7-org:v3}statusCode":
                self.statusCode = child.attrib["code"]
            elif child.tag == "{urn:hl7-org:v3}effectiveTime":
                try:
                    self.effectiveTime = child.attrib["value"][0:14]
                except:
                    self.effectiveTime = None
            elif child.tag == "{urn:hl7-org:v3}value":
                try:
                    self.value = child.attrib["value"]
                    self.unit = child.attrib["unit"]
                except:
                    try:
                        self.value = child.attrib["code"]
                    except:
                        self.value = ""
                        self.unit = ""
            elif child.tag == "{urn:hl7-org:v3}interpretationCode":
                self.interpretationCode = child.attrib["code"]
            elif child.tag == "{urn:hl7-org:v3}referenceRange":
                try:
                    self.range_low = child.find(".//{urn:hl7-org:v3}low").attrib["value"]
                except:
                    self.range_low = ""
                try:
                    self.range_high = child.find(".//{urn:hl7-org:v3}high").attrib["value"]
                except:
                    self.range_high = ""

    def extractObservationNoTime(self, observation_element, effectivetime):
        for child in observation_element:
            if child.tag == "{urn:hl7-org:v3}code":
                try:
                    self.code = child.attrib["code"]
                except:
                    if child.attrib["nullFlavor"] == "OTH":
                        self.code = child.find("{urn:hl7-org:v3}translation").attrib["code"]
            elif child.tag == "{urn:hl7-org:v3}templateId":
                self.templateId = child.attrib["root"]
            elif child.tag == "{urn:hl7-org:v3}statusCode":
                self.statusCode = child.attrib["code"]
            elif child.tag == "{urn:hl7-org:v3}effectiveTime":
                try:
                    self.effectiveTime = child.attrib["value"][0:14]
                    if self.effectiveTime == "":
                        self.effectiveTime = effectivetime
                except:
                    self.effectiveTime = effectivetime
            elif child.tag == "{urn:hl7-org:v3}value":
                try:
                    self.value = child.attrib["value"]
                    self.unit = child.attrib["unit"]
                except:
                    self.value = ""
                    self.unit = ""
            elif child.tag == "{urn:hl7-org:v3}interpretationCode":
                self.interpretationCode = child.attrib["code"]
            elif child.tag == "{urn:hl7-org:v3}referenceRange":
                try:
                    self.range_low = child.find(".//{urn:hl7-org:v3}low").attrib["value"]
                except:
                    self.range_low = ""
                try:
                    self.range_high = child.find(".//{urn:hl7-org:v3}high").attrib["value"]
                except:
                    self.range_high = ""
        if self.effectiveTime == None:
            self.effectiveTime = effectivetime