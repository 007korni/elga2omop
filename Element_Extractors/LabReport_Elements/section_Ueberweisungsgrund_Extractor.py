class section_Ueberweisungsgrund:

    def __init__(self):
        self.templateId = ""
        self.code = ""
        self.title = ""
        self.text = ""

    def extractUeberweisungsgrundFromXML(self, root):
        ueberweisungsgrund_section_element = root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}templateId[@root='1.2.40.0.34.11.4.2.4']/..")
        for element in ueberweisungsgrund_section_element:
            if element.tag == "{urn:hl7-org:v3}templateId":
                self.templateId = element.attrib["root"]
            elif element.tag == "{urn:hl7-org:v3}code":
                self.code = element.attrib["code"]
            elif element.tag == "{urn:hl7-org:v3}title":
                self.title = element.text
            elif element.tag == "{urn:hl7-org:v3}text":
                self.text = element.text.replace("\t", "").replace("\n", " ")