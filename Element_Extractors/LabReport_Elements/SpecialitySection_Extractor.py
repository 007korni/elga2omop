from Element_Extractors.LabReport_Elements import entryRelationshipExtractor


class SpecialitySection:

    def __init__(self):
        self.code = ""
        self.title = ""
        self.templateId = ""
        self.entryRelationships = []

    def extractSpecialitySectionFromXML(self, specialitysection_element):
        for child in specialitysection_element:
            if child.tag == "{urn:hl7-org:v3}code":
                self.code = child.attrib["code"]
            elif child.tag == "{urn:hl7-org:v3}title":
                self.title = child.text
            elif child.tag == "{urn:hl7-org:v3}templateId":
                self.templateId = child.attrib["root"]

        self.entryRelationships = self.getentryRelationships(specialitysection_element)

    def getentryRelationships(self, section_element):
        entryrelationship_elements = section_element.findall(".//{urn:hl7-org:v3}entry/{urn:hl7-org:v3}act/{urn:hl7-org:v3}entryRelationship")
        entryrelationships = []
        for entryRel in entryrelationship_elements:
            if entryRel.find(".//{urn:hl7-org:v3}observation"):
                entryrelationship = entryRelationshipExtractor.entryRelationship()
                entryrelationship.extractEntryRelationshipFromXML(entryRel)
                entryrelationships.append(entryrelationship)
            else:
                continue

        return entryrelationships
