from Element_Extractors import Address_Extractor, Name_Extractor


class DataEnterer:
    time_of_input = ''
    ids = dict()
    addr = ''
    telecom = []
    name = ''

    # TODO represented organisation

    def extractDataEntererFromXML(self, dataEntererElement):

        for child in dataEntererElement:
            if child.tag == '{urn:hl7-org:v3}time':
                self.time_of_input = child.attrib['value']
        # TODO assignedEntity Extractor schreiben
        assignedEntity = dataEntererElement.find('.//{urn:hl7-org:v3}assignedEntity')
        for child in assignedEntity:
            if child.tag == '{urn:hl7-org:v3}id':
                self.ids = child.attrib
            if child.tag == '{urn:hl7-org:v3}addr':
                temp_addr = Address_Extractor.Address()
                temp_addr.extractAdressFromXML(child)
                self.addr = temp_addr
            if child.tag == '{urn:hl7-org:v3}telecom':
                self.telecom.append(child.attrib['value'])
            if child.tag == '{urn:hl7-org:v3}assignedPerson':
                name_element = child.find('.//{urn:hl7-org:v3}name')
                name = Name_Extractor.Name()
                name.extractName(name_element)
                self.name = name

    def printDataEntererInformation(self):
        print('Eingabedatum :' + self.time_of_input)
        print('----------------------------------------')
        print('IDs: ')
        print(self.ids)
        print('----------------------------------------')
        print('Adresse: ')
        try:
            self.addr.printAddress()
        except:
            print('Keine Adresse vorhanden!')
        print('----------------------------------------')
        print('Kontakt: ')
        print(self.telecom)
        print('----------------------------------------')
        print('Name: ')
        self.name.printName()
        print('----------------------------------------')
