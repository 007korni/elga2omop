from Element_Extractors import Address_Extractor
class Custodian:

    def __init__(self):
        self.id = ''
        self.name = ''
        self.telecom = []
        self.addr = ''

    def extractCustodianFromXML(self, custodian_element):
        representedCustodianOrganization = custodian_element.find('.//{urn:hl7-org:v3}assignedCustodian/{urn:hl7-org:v3}representedCustodianOrganization')
        for child in representedCustodianOrganization:
            if child.tag == '{urn:hl7-org:v3}id':
                self.id = child.attrib
            if child.tag == '{urn:hl7-org:v3}name':
                self.name = child.text
            if child.tag == '{urn:hl7-org:v3}telecom':
                self.telecom.append(child.attrib['value'])
            if child.tag == '{urn:hl7-org:v3}addr':
                self.addr = Address_Extractor.Address()
                self.addr.extractAdressFromXML(child)

    def printCustodianInformation(self):
        print('Custodian:')
        print('ID: ')
        print(self.id)
        print('----------------------------------------')
        print('Name: ')
        print(self.name)
        print('----------------------------------------')
        print('Kontakt: ')
        print(self.telecom)
        print('----------------------------------------')
        print('Adresse: ')
        self.addr.printAddress()
        print('----------------------------------------')