from Element_Extractors import Name_Extractor, Address_Extractor


class Participant:

    def __init__(self):
        self.typeCode = None
        self.associatedEntity_classCode = None
        self.templateID = None
        self.assEnt_telecom = []
        self.assPers_name = None
        self.assPers_name_combined = None
        self.assEnt_id = None
        self.function_code = None
        self.scopingOrg_id = None
        self.scopingOrg_name = None
        self.scopingOrg_telecom = []
        self.scopingOrg_addr = None

    def extractParticipantFromXML(self, participant_element):
        self.typeCode = participant_element.attrib['typeCode']
        for child in participant_element:
            if child.tag == '{urn:hl7-org:v3}templateId':
                self.templateID = child.attrib["root"]
            if child.tag == '{urn:hl7-org:v3}functionCode':
                self.function_code = child.attrib ["code"]
            if child.tag == '{urn:hl7-org:v3}associatedEntity':
                self.associatedEntity_classCode = child.attrib['classCode']
                self.extractAssociatedEntity(child)

    def extractAssociatedEntity(self, associatedEntity_element):
        for child in associatedEntity_element:
            if child.tag == '{urn:hl7-org:v3}telecom':
                self.assEnt_telecom.append(child.attrib)
            if child.tag == '{urn:hl7-org:v3}id':
                try:
                    self.assEnt_id = child.attrib["root"]
                except:
                    self.assEnt_id = None
            if child.tag == '{urn:hl7-org:v3}associatedPerson':
                self.extractAssociatedPerson(child)
            if child.tag == '{urn:hl7-org:v3}scopingOrganization':
                self.extractScopingOrganization(child)

    def extractAssociatedPerson(self, associatedPerson_element):
        for child in associatedPerson_element:
            if child.tag == '{urn:hl7-org:v3}name':
                self.assPers_name = Name_Extractor.Name()
                self.assPers_name.extractName(child)
                self.assPers_name_combined = self.getassignedPersonName()

    def extractScopingOrganization(self, scopingOrganization_element):
        for child in scopingOrganization_element:
            if child.tag == '{urn:hl7-org:v3}id':
                try:
                    self.scopingOrg_id = child.attrib["root"]
                except:
                    self.scopingOrg_id = None
            if child.tag == '{urn:hl7-org:v3}name':
                self.scopingOrg_name = child.text
            if child.tag == '{urn:hl7-org:v3}telecom':
                self.scopingOrg_telecom.append(child.attrib)
            if child.tag == '{urn:hl7-org:v3}addr':
                self.scopingOrg_addr = Address_Extractor.Address()
                self.scopingOrg_addr.extractAdressFromXML(child)

    def getassignedPersonName(self):
        if self.assPers_name != None:
            return self.assPers_name.combinedName

        return self.assPers_name

