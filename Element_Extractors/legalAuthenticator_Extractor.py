from Element_Extractors import assignedEntity_Extractor
class legalAuthenticator:

    def __init__(self):
        self.time = None
        self.signatureCode = None
        # Assigned Entity
        self.assignedEntity = None

    def extractLegalAuthenticatorFromXML(self, legalAuthenticator_element):
        for child in legalAuthenticator_element:
            if child.tag == '{urn:hl7-org:v3}time':
                self.time = child.attrib['value']
            if child.tag == '{urn:hl7-org:v3}signatureCode':
                self.signatureCode = child.attrib
            if child.tag == '{urn:hl7-org:v3}assignedEntity':
                self.assignedEntity = assignedEntity_Extractor.assignedEntity()
                self.assignedEntity.extractAssignedEntityFromXML(child)



