from Element_Extractors import Address_Extractor, Name_Extractor

class Author:

    def __init__(self):
        self.functionCode = None
        self.time_of_authoring = None
        #assigned author:
        self.ass_author_id = None
        self.ass_author_code = None
        self.ass_author_telecom = []
        self.ass_author_name = None
        #authoringdevice:
        self.auth_device_code = None
        self.auth_device_manufacturerModelName = None
        self.auth_device_softwareName = None
        #represented organisation
        self.rep_organisation_id = None
        self.rep_organisation_name = None
        self.rep_organisation_telecom = []
        self.rep_organisation_addr = None

    def extractAuthorsFromXML(self, author_element):


        assigned_author = author_element.find('.//{urn:hl7-org:v3}assignedAuthor')

        for child in assigned_author:
            if child.tag == '{urn:hl7-org:v3}id':
                    self.ass_author_id = child.attrib
            if child.tag == '{urn:hl7-org:v3}code':
                    self.ass_author_code = child.attrib
            if child.tag == '{urn:hl7-org:v3}telecom':
                    self.ass_author_telecom.append(child.attrib)


        rep_organisation = assigned_author.find('.//{urn:hl7-org:v3}representedOrganization')

        for child in rep_organisation:
            if child.tag == '{urn:hl7-org:v3}id':
                self.rep_organisation_id = child.attrib
            if child.tag == '{urn:hl7-org:v3}name':
                self.rep_organisation_name = child.text
            if child.tag == '{urn:hl7-org:v3}telecom':
                self.rep_organisation_telecom.append(child.attrib['value'])
            if child.tag == '{urn:hl7-org:v3}addr':
                temp_addr = Address_Extractor.Address()
                temp_addr.extractAdressFromXML(child)
                self.rep_organisation_addr = temp_addr

        assigned_author_person = assigned_author.find('.//{urn:hl7-org:v3}assignedPerson')
        if(assigned_author_person != None):
            assigned_author_person_name = assigned_author_person.find('.//{urn:hl7-org:v3}name')
            self.ass_author_name = Name_Extractor.Name()
            self.ass_author_name.extractName(assigned_author_person_name)

        else:
            auth_device = author_element.find('.//{urn:hl7-org:v3}assignedAuthoringDevice')
            for child in auth_device:
                if child.tag == '{urn:hl7-org:v3}code':
                    self.auth_device_code = child.attrib
                if child.tag == '{urn:hl7-org:v3}manufacturerModelName':
                    self.auth_device_manufacturerModelName = child.text
                if child.tag == '{urn:hl7-org:v3}softwareName':
                    self.auth_device_softwareName = child.text


        for child in author_element:
            if child.tag == '{urn:hl7-org:v3}functionCode':
                self.functionCode = child.attrib
            if child.tag == '{urn:hl7-org:v3}time':
                self.time_of_authoring = child.attrib['value']

    def printAuthorInformation(self):
        print('Funktionscode: ')
        print(self.functionCode)
        print('----------------------------------------')
        print('Zeitpunkt der Eintragung: ' + self.time_of_authoring)
        print('----------------------------------------')
        #Bei einer Person
        if self.ass_author_name != '':
            print('Author ID: ')
            print(self.ass_author_id)
            print('Author Code: ')
            print(self.ass_author_code)
            print('Author Kontakt: ')
            print(self.ass_author_telecom)
            print('Author Name: ')
            self.ass_author_name.printName()
            print('----------------------------------------')
        #Bei einem Device
        else:
            print('DeviceCode: ')
            print(self.auth_device_code)
            print('Manufactur Model Name: ')
            print(self.auth_device_manufacturerModelName)
            print('Software Name: ')
            print(self.auth_device_softwareName)
            print('----------------------------------------')
        #Organisation
        print('Organisations ID: ')
        print(self.rep_organisation_id)
        print('Name der Organisation: ')
        print(self.rep_organisation_name)
        print('Kontaktdaten: ')
        print(self.rep_organisation_telecom)
        print('Adresse: ')
        self.rep_organisation_addr.printAddress()
        print('----------------------------------------')