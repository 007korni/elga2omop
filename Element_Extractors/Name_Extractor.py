class Name:


    prefix = []
    given = []
    family = []
    suffix = []

    def __init__(self):
        self.prefix = []
        self.given = []
        self.all_given = ""
        self.family = []
        self.all_family = ""
        self.combinedName = ""
        self.suffix = []

    def extractName(self, name_element):
        #TODO Granularität Stufe 1

        for child in name_element:
            if child.tag == '{urn:hl7-org:v3}given':
                self.given.append(child.text)
            if child.tag == '{urn:hl7-org:v3}prefix':
                self.prefix.append(child.text)
            if child.tag == '{urn:hl7-org:v3}family':
                try:
                    if child.attrib['qualifier'] == "BR":
                        continue
                except:
                    self.family.append(child.text)
            if child.tag == '{urn:hl7-org:v3}suffix':
                self.suffix.append(child.text)

        self.all_given = self.allgiven()
        self.all_family = self.allfamily()
        self.combinedName = self.all_given + " " + self.all_family

    def printName(self):

        for prefix in self.prefix:
            print('Prefix: ' + prefix)

        for given in self.given:
            print('Vorname: ' + given)

        for family in self.family:
            print('Nachname: ' + family)

        for suffix in self.suffix:
            print('Suffix: ' + suffix)

    def allprefix(self):
        allprefix = " ".join(self.prefix)
        return allprefix

    def allsuffix(self):
        allsuffix = " ".join(self.suffix)
        return allsuffix

    def allfamily(self):
        allfamily = " ".join(self.family)
        return allfamily

    def allgiven(self):
        allgiven = " ".join(self.given)
        return allgiven