from Element_Extractors import assignedEntity_Extractor


class Procedure():

    def __init__(self):
        self.code = ""
        self.effectiveTime = ""
        self.targetSiteCode = ""
        self.performer = ""
        self.participant_specimen = ""

    def extractProcedureFromElement(self, procedure_element):

        for child in procedure_element:
            if child.tag == "{urn:hl7-org:v3}code":
                self.code = child.attrib
            elif child.tag == "{urn:hl7-org:v3}effectiveTime":
                self.effectiveTime = child.attrib
            elif child.tag == "{urn:hl7-org:v3}targetSiteCode":
                self.targetSiteCode = child.attrib
            elif child.tag == "{urn:hl7-org:v3}performer":
                self.performer = assignedEntity_Extractor.assignedEntity()
                self.performer.extractAssignedEntityFromXML(child.find("{urn:hl7-org:v3}assignedEntity"))
            elif child.tag == "{urn:hl7-org:v3}participant":
                self.participant_specimen = participant_specimen()
                self.participant_specimen.extractparicipant_speciment(child)


class participant_specimen():

    def __init__(self):
        self.id = ""
        self.code = ""

    def extractparicipant_speciment(self, participant_element):
        self.id = participant_element.find("{urn:hl7-org:v3}participantRole/{urn:hl7-org:v3}id").attrib["extension"]
        self.code = participant_element.find("{urn:hl7-org:v3}participantRole/{urn:hl7-org:v3}playingEntity/{urn:hl7-org:v3}code").attrib["code"]

