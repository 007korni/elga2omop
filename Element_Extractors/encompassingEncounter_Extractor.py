from Element_Extractors import assignedEntity_Extractor, Address_Extractor

class EncompassingEncounter:

    def __init__(self):
        self.encompassingEncounter_ID = ""
        self.code = []
        self.start_time = ""
        self.end_time = ""
        self.assignedEntity = ""
        self.location_ID = ""
        self.location_name = ""
        self.location_addr = ""

    def extractEncompassingEncounter(self, encompassingEncounterElement):

        for child in encompassingEncounterElement:
            if child.tag == "{urn:hl7-org:v3}id":
                self.encompassingEncounter_ID = child.attrib
                #self.encompassingEncounter_ID.append(child.attrib['extension'])
                #self.encompassingEncounter_ID.append(child.attrib['assigningAuthorityName'])

            if child.tag == "{urn:hl7-org:v3}code":
                self.code = child.attrib
                #self.code.append(child.attrib['code'])
                #self.code.append(child.attrib['displayName'])
                #self.code.append(child.attrib['codeSystem'])
                #self.code.append(child.attrib['codeSystemName'])

            if child.tag == "{urn:hl7-org:v3}effectiveTime":
                for t in child:
                    if t.tag == '{urn:hl7-org:v3}low':
                        self.start_time = t.attrib['value']
                    if t.tag == '{urn:hl7-org:v3}high':
                        self.end_time = t.attrib['value']

        try:
            assignedEntity_element = encompassingEncounterElement.find("{urn:hl7-org:v3}responsibleParty/{urn:hl7-org:v3}assignedEntity")
            self.assignedEntity = assignedEntity_Extractor.assignedEntity()
            self.assignedEntity.extractAssignedEntityFromXML(assignedEntity_element)
        except:
            self.assignedEntity = assignedEntity_Extractor.assignedEntity()


        location_element = encompassingEncounterElement.find("{urn:hl7-org:v3}location/{urn:hl7-org:v3}healthCareFacility/{urn:hl7-org:v3}serviceProviderOrganization")
        for loc_element in location_element:
            if loc_element.tag == "{urn:hl7-org:v3}id":
                self.location_ID = loc_element.attrib["root"]
            if loc_element.tag == "{urn:hl7-org:v3}name":
                self.location_name = loc_element.text
            if loc_element.tag == "{urn:hl7-org:v3}addr":
                self.location_addr = Address_Extractor.Address()
                self.location_addr.extractAdressFromXML(loc_element)



    def printInformation(self):
        print("Aufenthalt:")
        print("ID: " + self.encompassingEncounter_ID[1])
        print("Code: " + self.code[1])
        print("Start: " + self.start_time)
        print("Ende: " + self.end_time)


