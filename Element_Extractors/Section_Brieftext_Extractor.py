class section_Brieftext:

    def __init__(self):
        self.templateId = ""
        self.code = ""
        self.title = ""
        self.text = ""
        self.entry = None

    def extractBrieftextFromXML(self, root):
        brieftext_section_element = root.find(".//{urn:hl7-org:v3}section/{urn:hl7-org:v3}code[@code='BRIEFT']/..")
        for element in brieftext_section_element:
            if element.tag == "{urn:hl7-org:v3}templateId":
                self.templateId = element.attrib["root"]
            elif element.tag == "{urn:hl7-org:v3}code":
                self.code = element.attrib["code"]
            elif element.tag == "{urn:hl7-org:v3}title":
                self.title = element.text
            elif element.tag == "{urn:hl7-org:v3}text":
                content = element.find("{urn:hl7-org:v3}content").text.replace("\t", "").replace("\n", " ")
                self.text = content
        # elif element.tag == "{urn:hl7-org:v3}entry":
