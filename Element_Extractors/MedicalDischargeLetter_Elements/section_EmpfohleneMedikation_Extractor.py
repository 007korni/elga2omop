import xml.etree.ElementTree as ET
import re

class section_EmpfohleneMedikation():

    def __init__(self):
        self.templateIDs = []
        self.code = ""
        self.title = ""
        self.text = ""
        self.substanceAdministration = [] #Die Frage ist ob das überhaupt für OMOP relevant ist, da ja nicht bestätigt wird ob das Medikament jemals verabreicht wird!

    def extractEmpfohleneMedikationFromXML(self, section_element):
        for e in section_element:
            if e.tag == "{urn:hl7-org:v3}templateId":
                self.templateIDs.append(e.attrib["root"])
            elif e.tag == "{urn:hl7-org:v3}code":
                self.code = e.attrib["code"]
            elif e.tag == "{urn:hl7-org:v3}title":
                self.title = e.text
            elif e.tag == "{urn:hl7-org:v3}text":
                try:
                    self.text = ET.tostring(e, encoding='unicode')
                    self.text = re.sub("\<(.*?)\>", "", self.text).replace("\n           ", "").replace("\n",
                                                                                                        "").replace(
                        "                       ", "")
                    # self.text = e.text.replace("\n", "").replace("                       ", "")
                except:
                    self.text = e.text
        substanceAdministration_elements = section_element.findall(".//{urn:hl7-org:v3}entry/{urn:hl7-org:v3}substanceAdministration")
        for sA in substanceAdministration_elements:
            substanceAdministration = self.SubstanceAdministration()
            substanceAdministration.extractSubstanceAdministrationFromSection(sA)
            self.substanceAdministration.append(substanceAdministration)

    class SubstanceAdministration():

        def __init__(self):
            self.templateIDs = []
            self.id = "" # Wird zu dictonary
            self.statusCode = ""
            self.effectiveTime_low = "" #Datum der Einnahme
            self.effectiveTime_high = ""
            self.repeatNumber = ""
            self.routeCode = None
            self.doseQuantity = ""
            self.consumable = ""
            self.entryRealtionship_supply_independentInd = ""
            self.entryRealtionship_supply_quantity = ""
            self.entryRealtionship_act_templateID = ""
            self.entryRealtionship_act_code = ""

        def extractSubstanceAdministrationFromSection(self, substanceAdministrationElement):
            for e in substanceAdministrationElement:
                if e.tag == "{urn:hl7-org:v3}templateId":
                    self.templateIDs.append(e.attrib["root"])
                elif e.tag == "{urn:hl7-org:v3}id":
                    self.id = e.attrib
                elif e.tag == "{urn:hl7-org:v3}statusCode":
                    self.statusCode = e.attrib["code"]
                elif e.tag == "{urn:hl7-org:v3}effectiveTime":
                    self.extractEffectiveTime(e)
                elif e.tag == "{urn:hl7-org:v3}repeatNumber":
                    self.repeatNumber = e.attrib["value"]
                elif e.tag == "{urn:hl7-org:v3}routeCode":
                    self.routeCode = e.attrib["code"]
                elif e.tag == "{urn:hl7-org:v3}doseQuantity":
                    self.doseQuantity = e.attrib["value"]
                elif e.tag == "{urn:hl7-org:v3}consumable":
                    self.consumable = self.Consumable()
                    self.consumable.extractConsumable(e)
                elif e.tag == "{urn:hl7-org:v3}entryRelationship":
                    for er in e:
                        if er.tag == "{urn:hl7-org:v3}supply":
                            self.extractEntryRelationshipSupply(er)
                        elif er.tag == "{urn:hl7-org:v3}act":
                            self.extractEntryRelationshipAct(er)


        def extractEntryRelationshipSupply(self, supply_element):
            for s in supply_element:
                if s.tag == "{urn:hl7-org:v3}independentInd":
                    self.entryRealtionship_supply_independentInd = s.attrib["value"]
                elif s.tag == "{urn:hl7-org:v3}quantity":
                    self.entryRealtionship_supply_quantity = s.attrib["value"]

        def extractEntryRelationshipAct(self, act_element):
            for a in act_element:
                if a.tag == "{urn:hl7-org:v3}templateId":
                    self.entryRealtionship_act_templateID = a.attrib["root"]
                elif a.tag == "{urn:hl7-org:v3}code":
                    self.entryRealtionship_act_code = a.attrib["code"]


        def extractEffectiveTime(self, time_element):
                try:
                    self.effectiveTime_low = time_element.find("{urn:hl7-org:v3}low").attrib["value"]
                    if len(self.effectiveTime_low) > 14:
                        self.effectiveTime_low = self.effectiveTime_low[0:14]
                    elif len(self.effectiveTime_low) == 8:
                        self.effectiveTime_low = self.effectiveTime_low[0:8]
                    elif len(self.effectiveTime_low) == 6:
                        self.effectiveTime_low = self.effectiveTime_low[0:6] + "01"
                        try:
                            self.effectiveTime_high = time_element.find("{urn:hl7-org:v3}high").attrib["value"]
                        except:
                            self.effectiveTime_high = self.effectiveTime_low[0:6] + "30"
                    else:
                        self.effectiveTime_low = self.effectiveTime_low[0:4]
                except:
                    self.effectiveTime_low = None
                try:
                    self.effectiveTime_high = time_element.find("{urn:hl7-org:v3}high").attrib["value"]
                    if len(self.effectiveTime_high) > 14:
                        self.effectiveTime_high = self.effectiveTime_high[0:14]
                    elif len(self.effectiveTime_high) == 8:
                        self.effectiveTime_high = self.effectiveTime_high[0:8]
                    elif len(self.effectiveTime_high) == 6:
                        self.effectiveTime_high = self.effectiveTime_high[0:6] + "01"
                    else:
                        self.effectiveTime_high = self.effectiveTime_high[0:4]
                except:
                    self.effectiveTime_high = None

        class Consumable():

            def __init__(self):
                self.templateIDs = []
                self.manufacturedMaterial_templateIDs = []
                self.manufacturedMaterial_code = ""
                self.manufacturedMaterial_name = ""
                self.formCode = None
                self.capacityQuantity = None
                self.capacityUnit = None
                self.pharmCode = None
                self.pharmName = None

            def extractConsumable(self, consumable_element):
                for e in (consumable_element.find("{urn:hl7-org:v3}manufacturedProduct")):
                    if e.tag == "{urn:hl7-org:v3}templateId":
                        self.templateIDs.append(e.attrib["root"])
                    elif e.tag == "{urn:hl7-org:v3}manufacturedMaterial":
                        for m in e:
                            if m.tag == "{urn:hl7-org:v3}templateId":
                                self.manufacturedMaterial_templateIDs.append(m.attrib["root"])
                            elif m.tag == "{urn:hl7-org:v3}code":
                                self.manufacturedMaterial_code = m.attrib["code"]
                            elif m.tag == "{urn:hl7-org:v3}name":
                                self.manufacturedMaterial_name = m.text
                            elif m.tag == "{urn:ihe:pharm:medication}formCode":
                                self.formCode = m.attrib["code"]
                            elif m.tag == "{urn:ihe:pharm:medication}asContent":
                                 quantitiy_element = m.find(".//{urn:ihe:pharm:medication}capacityQuantity")
                                 self.capacityQuantity = quantitiy_element.attrib["value"]
                                 self.capacityUnit = quantitiy_element.attrib["unit"]
                            elif m.tag == "{urn:ihe:pharm:medication}ingredient":
                                code = m.find(".//{urn:ihe:pharm:medication}code")
                                self.pharmCode = code.attrib["code"]
                                name = m.find(".//{urn:ihe:pharm:medication}name")
                                self.pharmName = name.text
