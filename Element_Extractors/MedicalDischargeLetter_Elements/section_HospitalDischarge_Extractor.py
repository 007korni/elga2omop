class section_HospitalDischarge():

    def __init__(self):
        self.templateIDs = []
        self.code = ""
        self.title = ""
        self.text = ""
        self.entries = []

    def extractDiagnoseBeiEntlassungSectionFromXML(self, section_element):
        entry_list = []
        for e in section_element:
            if e.tag == "{urn:hl7-org:v3}templateId":
                self.templateIDs.append(e.attrib["root"])
            elif e.tag == "{urn:hl7-org:v3}code":
                self.code = e.attrib["code"]
            elif e.tag == "{urn:hl7-org:v3}title":
                self.title = e.text
            elif e.tag == "{urn:hl7-org:v3}text":
                self.text = e.text.replace("\n", "").replace("                       ", "")
            elif e.tag == "{urn:hl7-org:v3}entry":
                entry_list.append(e.find("{urn:hl7-org:v3}act"))
        for entry in entry_list:
            self.entries.append(self.extractEntry(entry))

    def extractEntry(self,entry_element):
        entry = Entry()
        entry.extractEntry(entry_element)
        return entry

# Hospital Discharge Section Entry
class Entry():

    def __init__(self):
        self.act_templateIDs = []
        self.act_id = ""
        self.act_code = ""
        self.act_statusCode = ""
        self.effectiveTime_low = ""
        self.effectiveTime_high = ""
        self.observations = []

    def extractEntry(self, entryElement):
        observations_list = []
        for e in entryElement:
            if e.tag == "{urn:hl7-org:v3}templateId":
                self.act_templateIDs.append(e.attrib["root"])
            elif e.tag == "{urn:hl7-org:v3}code":
                try:
                    self.act_code = e.attrib["code"]
                except:
                    self.act_code = None
            elif e.tag == "{urn:hl7-org:v3}id":
                self.act_id = e.attrib["root"]
            elif e.tag == "{urn:hl7-org:v3}statusCode":
                try:
                    self.act_code = e.attrib["code"]
                finally:
                    self.act_code = None
            elif e.tag == "{urn:hl7-org:v3}effectiveTime":
                try:
                    self.effectiveTime_low = e.find("{urn:hl7-org:v3}low").attrib["value"]
                except:
                    self.effectiveTime_low = None
                try:
                    self.effectiveTime_high = e.find("{urn:hl7-org:v3}high").attrib["value"]
                except:
                    self.effectiveTime_high = None
            elif e.tag == "{urn:hl7-org:v3}entryRelationship":
                observations_list = e.findall("{urn:hl7-org:v3}observation")
                for obs in observations_list:
                    self.observations.append(self.extractObservation(obs))

    def extractObservation(self, observation_element):
        observation = HD_Observation()
        observation.extractObservation(observation_element)
        return observation

# Hospital Discharge Section Observation
class HD_Observation():

    def __init__(self):
        self.templateIDs = []
        self.id = ""
        # Code der Observation
        self.code = ""
        self.effectiveTime_low = ""
        self.effectiveTime_high = ""
        # Diagnose Code
        self.value_code = ""

    def extractObservation(self, obervation_element):
        for e in obervation_element:
            if e.tag == "{urn:hl7-org:v3}templateId":
                self.templateIDs.append(e.attrib["root"])
            elif e.tag == "{urn:hl7-org:v3}id":
                self.id = e.attrib["root"]
            elif e.tag == "{urn:hl7-org:v3}code":
                try:
                    self.code = e.attrib["code"]
                except:
                    self.code = None
            elif e.tag == "{urn:hl7-org:v3}effectiveTime":
                try:
                    self.effectiveTime_low = e.find("{urn:hl7-org:v3}low").attrib["value"]
                except:
                    self.effectiveTime_low = None
                try:
                    self.effectiveTime_high = e.find("{urn:hl7-org:v3}high").attrib["value"]
                except:
                    self.effectiveTime_high = None
            elif e.tag == "{urn:hl7-org:v3}value":
                self.value_code = e.attrib