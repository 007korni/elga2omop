from Element_Extractors import serviceEvent, assignedEntity_Extractor


class serviceEvent_Extractor:
    service_event = serviceEvent.serviceEvent()
    list_of_serviceEvent = []

    def __init__(self):

        self.service_event = serviceEvent.serviceEvent()
        self.list_of_serviceEvent = []

    def extractServiceEventsFromXML(self, root_element):

        documentation_ofs = root_element.findall('.//{urn:hl7-org:v3}documentationOf')

        for x in documentation_ofs:
            serviceEvent_element = x.find('.//{urn:hl7-org:v3}serviceEvent')
            self.service_event = serviceEvent.serviceEvent()
            for child in serviceEvent_element:
                if child.tag == '{urn:hl7-org:v3}code':
                    self.service_event.code = child.attrib

                if child.tag == '{urn:hl7-org:v3}effectiveTime':
                    for t in child:
                        if t.tag == '{urn:hl7-org:v3}low':
                            self.service_event.effectiveTime_low = t.attrib['value']
                        if t.tag == '{urn:hl7-org:v3}high':
                            self.service_event.effectiveTime_high = t.attrib['value']

                if child.tag == '{urn:hl7-org:v3}performer':
                    self.extractPerfomer(child)

            self.list_of_serviceEvent.append(self.service_event)

        return self.list_of_serviceEvent

    def extractPerfomer(self, performer_xml):
        for child in performer_xml:
            if child.tag == '{urn:hl7-org:v3}templateId':
                self.service_event.performer_templateId = child.attrib

            elif child.tag == '{urn:hl7-org:v3}time':
                for t in child:
                    if t.tag == '{urn:hl7-org:v3}low':
                        self.service_event.performer_Time_low = t.attrib['value']
                    if t.tag == '{urn:hl7-org:v3}high':
                        self.service_event.performer_Time_high = t.attrib['value']

            elif child.tag == '{urn:hl7-org:v3}assignedEntity':
                self.service_event.performer = assignedEntity_Extractor.assignedEntity()
                self.service_event.performer.extractAssignedEntityFromXML(child)
